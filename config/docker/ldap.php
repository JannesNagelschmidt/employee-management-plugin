<?php

return [
	'ldap_url' => env('LDAP_URL', 'localhost'),
	'ldap_serv' => env('LDAP_SERV', '')
];
