<?php namespace JannesNagelschmidt\Mitarbeiter;

use Illuminate\Support\Facades\Mail;
use JannesNagelschmidt\Mitarbeiter\Models\Arbeitszeit;
use JannesNagelschmidt\Mitarbeiter\Models\Corona;
use JannesNagelschmidt\Mitarbeiter\Models\Logs;
use JannesNagelschmidt\Mitarbeiter\Models\Mitarbeiter;
use JannesNagelschmidt\Mitarbeiter\Models\Token;
use System\Classes\PluginBase;
use Model;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Http\Request;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Validator;


class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Jannesnagelschmidt\Mitarbeiter\Components\Timetracker' => 'timetracker',
            'Jannesnagelschmidt\Mitarbeiter\Components\Timetrackfrontend' => 'timetrackfrontend',
            'Jannesnagelschmidt\Mitarbeiter\Components\Singleemployee' => 'singleemployee',
            'Jannesnagelschmidt\Mitarbeiter\Components\Coronagraph' => 'coronagraph',
            'Jannesnagelschmidt\Mitarbeiter\Components\Register' => 'register',
            'Jannesnagelschmidt\Mitarbeiter\Components\Export' => 'export',
            'Jannesnagelschmidt\Mitarbeiter\Components\Holiday' => 'urlaub',
            'Jannesnagelschmidt\Mitarbeiter\Components\Emt' => 'emt',
            'Jannesnagelschmidt\Mitarbeiter\Components\Emt_Magic' => 'emt_magic',
            'Jannesnagelschmidt\Mitarbeiter\Components\Emt_Berechnung' => 'emt_calc',
            'Jannesnagelschmidt\Mitarbeiter\Components\Emt_Time' => 'emt_time',
            'Jannesnagelschmidt\Mitarbeiter\Components\Emt_Profile' => 'emt_profile',
            'Jannesnagelschmidt\Mitarbeiter\Components\Emt_Statics' => 'emt_statics',
            'Jannesnagelschmidt\Mitarbeiter\Components\Emt_Internalcorrection' => 'emt_internalcorrection',
            'Jannesnagelschmidt\Mitarbeiter\Components\Emt_Punctually' => 'emt_punctually',
            'Jannesnagelschmidt\Mitarbeiter\Components\Emt_Capacity' => 'emt_capacity',
            'Jannesnagelschmidt\Mitarbeiter\Components\Emt_Request' => 'emt_request',
            'Jannesnagelschmidt\Mitarbeiter\Components\Emt_Vacation' => 'emt_vacation',
            'Jannesnagelschmidt\Mitarbeiter\Components\Emt_Vacationplan' => 'emt_vacationplan',
            'Jannesnagelschmidt\Mitarbeiter\Components\Presence' => 'presence',
            'Jannesnagelschmidt\Mitarbeiter\Components\Correction' => 'korrektur',
            'Jannesnagelschmidt\Mitarbeiter\Components\Calculate' => 'calculate'

        ];
    }

    public function register()
    {
        $this->registerConsoleCommand('emp:feierabend', 'JannesNagelschmidt\Mitarbeiter\Console\Feierabend');
        $this->registerConsoleCommand("emp:corona", 'JannesNagelschmidt\Mitarbeiter\Console\Corona');
        $this->registerConsoleCommand("emp:available", 'JannesNagelschmidt\Mitarbeiter\Console\Available');
        $this->registerConsoleCommand("emp:dep", 'JannesNagelschmidt\Mitarbeiter\Console\DepartmentCommand');
    }

    public function registerSchedule($schedule)
    {
        /*$schedule->call(function () {
            $this->checkEveryoneLeftTheBuilding();
        })->cron('00 21 * * *');*/
        $schedule->command('emp:available')->cron('* * * * *');
        $schedule->command('emp:corona')->cron('1 */4 * * *');
        $schedule->command('emp:feierabend')->cron('00 2 * * *');
    }

    public static function saveCoronaNumber() {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://public.opendatasoft.com/api/records/1.0/search/?dataset=covid-19-germany-landkreise&q=schleswig-flensburg&facet=last_update&facet=name&facet=rs&facet=bez&facet=bl");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $out = curl_exec($ch);
        curl_close($ch);
        $blup = json_decode($out, true);
        $number = $blup['records'][0]['fields']['cases7_per_100k'];

        $entry = new Corona;
        $entry->cases = $number;
        $entry->save();
    }

    public function testFunc() {
        echo "blup";
        return "bla";
    }

    public static function createLog($text) {
        $entry = new Logs();
        $entry->text = $text;
        $entry->save();
    }

    public static function checkEveryoneLeftTheBuilding() {
        $userWhoForgot = array();
        $tokens = Token::all();
        $mitarbeiter = Mitarbeiter::all();
        $mail = "zeiterfassung@hans-natur.de";
        //$mail = "mknop@hans-natur.de";

        foreach($mitarbeiter as $x => $employee) {
            $breakTime = date('Y-m-d 22:59:59', strtotime('-1 days'));
            $time = date('Y-m-d 23:00:00', strtotime('-1 days'));
            $lu = date('Y-m-d', strtotime($time));
            $test = Arbeitszeit::where([
                ['user_id', $employee->id],
                ['status', '!=', 'vacation'],
                ['status', '!=', 'sick'],
                ['status', 'NOT LIKE', 'Korrektur%'],
                ['lookup', $lu]
            ])->latest('id')->first();
            if($test) {
                if($test->status != 'leaving') {
                    if($test->status != 'forgot') {

                        //$converted = (new Models\Mitarbeiter)->convertTimeToUSERzone($time, 'Europe/Amsterdam');
                        if($test->status == 'break_start') {
                            Arbeitszeit::addTimetrack($employee->id, $breakTime, 'break_end');
                        }
                        DB::table('jannesnagelschmidt_mitarbeiter_token')->where('user_id', $employee->id)
                            ->update(['status' => 'leaving']);
                        array_push($userWhoForgot, $employee->first_name . ' '.$employee->last_name);
                        Arbeitszeit::addTimetrack($employee->id, $time, 'leaving');
                        Arbeitszeit::addTimetrack($employee->id, $time, 'forgot');
                    }
                }
            }
        }

        $params = array(
            "names"  =>  $userWhoForgot,
        );
        if(count($userWhoForgot) > 0) {
            Mail::sendTo($mail, 'jannesnagelschmidt.mitarbeiter::mail.forgotToLeave', $params, function($message) {
                $message->from('onlineshop@hans-natur.de', 'Employee Management Plugin');
                $message->subject('Vergessen sich aus zu stempeln');
                $message->replyTo('onlineshop@hans-natur.de');
            });
        }
        else {
            array_push($userWhoForgot, 'Niemand');
            $params = array(
                "names"  =>  $userWhoForgot,
            );
            Mail::sendTo("mknop@hans-natur.de", 'jannesnagelschmidt.mitarbeiter::mail.forgotToLeave', $params, function($message) {
                $message->from('onlineshop@hans-natur.de', 'Employee Management Plugin');
                $message->subject('Vergessen sich aus zu stempeln');
                $message->replyTo('onlineshop@hans-natur.de');
            });
        }
    }

    public function registerSettings()
    {
    }


}
