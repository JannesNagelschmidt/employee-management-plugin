<?php namespace Jannesnagelschmidt\Mitarbeiter\Components;
ini_set('max_execution_time', 180); //3 minutes

use Carbon\Carbon;
use Cms\Classes\ComponentBase;

use JannesNagelschmidt\Mitarbeiter\Controllers\Employee;
use JannesNagelschmidt\Mitarbeiter\Models\Calculated;
use Jannesnagelschmidt\Mitarbeiter\Models\Mitarbeiter;
use Jannesnagelschmidt\Mitarbeiter\Models\Token;
use Jannesnagelschmidt\Mitarbeiter\Models\Arbeitszeit;
use JannesNagelschmidt\Mitarbeiter\Models\Urlaub;
use Model;

class Calculate extends ComponentBase
{
    public function componentDetails() {
        return [
            'name' => 'Calculator',
            'description' => 'Calculates all the times'
        ];
    }

    public function onRun() {

    }

    public function getTimestampsByDate($date, $user_id) {
        $searchDate = date('Y-m-d', strtotime($date));
        return Arbeitszeit::where([
            ['user_id', $user_id],
            ['lookup', $searchDate]
        ])->orderBy('timestamp', 'asc')->get();
    }

    public function checkIfEntryExists($user_id, $datetime) {
         $entry = Calculated::where([
            ['lu_date', date('Y-m-d', strtotime($datetime))],
            ['user_id', $user_id],
            ['manually_created', 0]
        ])->first();
         if($entry) {
             $time = $entry->worked;
             $explode = explode(":", $time, 2);
             $number = intval($explode[0]);
             //return var_dump($number);
             //$date = Carbon::createFromFormat('H', $time);
             return true;
         }
        return false;
    }

    public function checkIfSickAtThatDay($user_id, $datetime) {
        $entry = Arbeitszeit::where([
            ['user_id', $user_id],
            ['lookup', date('Y-m-d', strtotime($datetime))],
            ['status', 'sick']
        ])->first();

        if($entry) {
            return true;
        }
        return false;
    }

    public function onMakeTimestampsCool() {
        $times = Arbeitszeit::whereNull('lookup')->get();
        foreach($times as $x => $item) {
            $date = $item->timestamp;
            $lookup = date('Y-m-d', strtotime($date));
            $item->lookup = $lookup;
            $item->save();
        }
        return "done";
    }

    public function onCalculate() {
        $mitarbeiter = Mitarbeiter::all();
        //$mitarbeiter = Mitarbeiter::where('id', 96)->get();
        $timesArray = array();
        foreach($mitarbeiter as $x => $worker) {
            $times = Arbeitszeit::where('user_id', $worker->id)->get();
            foreach($times as $y => $time) {
                if($time->status == 'coming') {
                    $today = date('Y-m-d');
                    $timestamp = date('Y-m-d', strtotime($time['timestamp']));
                    if($today == $timestamp) {
                        $checker = true;
                    }
                    else {
                        $checker = $this->checkIfEntryExists($worker->id, $time['timestamp']);
                    }
                }
                elseif ($time->status == 'vacation' || $time->status == 'sick') {
                    $checker = true;
                    if(!$this->checkIfEntryExists($worker->id, $time['timestamp'])) {
                        if($worker->hours_per_week) {
                            $weekTime = $worker->hours_per_week;
                        }
                        else {
                            $weekTime = 39;
                        }
                        $calculate = new Calculated();
                        $calculate->user_id = $worker->id;
                        $worked = (new Timetrackfrontend)->convertTime(($weekTime/5));
                        $calculate->worked = $worked;
                        $calculate->timestamp = date('Y-m-d H:i:s', strtotime($time->timestamp));
                        $calculate->break = '00:00';
                        $calculate->lu_date = date('Y-m-d', strtotime($time->timestamp));
                        $calculate->save();
                        $checker = true;
                    }
                }
                else {
                    $checker = false;
                }
                if(!$checker) {
                    $sum = strtotime('00:00:00');
                    $breakBiggerThanThirty = false;
                    $workedMoreThanSix = true;
                    $theBreaks = array();
                    $break_startSum = 0;
                    $break_endSum = 0;
                    $break = 0;
                    $end = 0;
                    $start = 0;
                    $forgot = false;
                    if($time->status == 'coming') {
                        $date = $time['timestamp'];
                        $theTimes = $this->getTimestampsByDate($date, $worker->id);
                        foreach ($theTimes as $x => $item) {
                            $bla = strtotime($item['timestamp']);
                            //$bla = date('Y-m-d H:i:s', strtotime($item['time']));
                            if ($item['status'] == 'coming') {
                                $start = $bla;
                            }
                            if ($item['status'] == 'leaving') {
                                $end = $bla;
                            }
                            if ($item['status'] == 'break_start') {
                                $break_start = $bla;
                                $break_startSum = $break_start + $break_startSum;
                            }
                            if ($item['status'] === 'break_end' && $x > 1) {
                                $break_end = $bla;
                                $break_end = $break_end - $sum;
                                $tempStart = strtotime($theTimes[$x - 1]['timestamp']);
                                $tempStart = $tempStart - $sum;
                                $tempResult = $break_end - $tempStart;
                                array_push($theBreaks, $tempResult);
                                $break_endSum = $break_end + $break_endSum;
                            }
                            if ($item['state'] == 'forgot') {
                                $forgot = true;
                            }
                        }

                        $IST = $end - $start;
                        $hoursWorked = intval(date('H', $IST));
                        $minutesWorked = intval(date('i', $IST));
                        if(($minutesWorked >= 30 && $hoursWorked >= 6) || $hoursWorked > 6) {
                            $workedMoreThanSix = false;
                        }
                        foreach($theBreaks as $item) {
                            $tempTime = (int)date('i', $item);
                            $tempTimeHour = (int)date('H', $item);
                            if(!$workedMoreThanSix) {
                                if($tempTime >= 30 || $tempTimeHour > 0) {
                                    $breakBiggerThanThirty = true;
                                }
                            }
                        }
                        if(!$breakBiggerThanThirty && !$workedMoreThanSix) {
                            $thirtyMinutes = strtotime('00:30:00');
                            $thirtyMinutes = $thirtyMinutes - $sum;
                            $tempBreak = 0;
                            $breakPos = 0;
                            if(count($theBreaks) > 0) {
                                foreach($theBreaks as $x => $item) {
                                    if($item > $tempBreak) {
                                        $breakPos = $x;
                                    }
                                    $tempBreak = $item;
                                }
                                $theBreaks[$breakPos] = $thirtyMinutes;
                                foreach($theBreaks as $item) {
                                    $break = $break + $item;
                                }
                            }
                            else {
                                $break = $thirtyMinutes;
                            }
                        }
                        else {
                            $break = $break_endSum - $break_startSum;
                        }
                        $IST = $IST - $break;
                        $minutesNotRounded = date('i', $IST);
                        $roundedMinutes = (new Timetrackfrontend)->roundDownToAny($minutesNotRounded, 1);
                        $worked = date('H', $IST).':'.(new Timetrackfrontend)->lz($roundedMinutes);
                        $tempArray = array();
                        if($forgot) {
                            $tempArray['worked']  = '00:00';
                            $tempArray['break'] = '00:00';
                        }
                        else {
                            $tempArray['worked']  = $worked;
                            $tempArray['break'] = date('H:i',$break);
                        }
                        $tempArray['user_id'] = $worker->id;
                        $tempArray['date'] = date('Y-m-d', strtotime($time->timestamp));
                        $calculate = new Calculated();
                        $calculate->user_id = $worker->id;
                        $calculate->worked = $worked;
                        $calculate->timestamp = date('Y-m-d H:i:s', strtotime($time->timestamp));
                        $calculate->break = date('H:i',$break);
                        $calculate->lu_date = date('Y-m-d', strtotime($time->timestamp));
                        $calculate->save();
                        array_push($timesArray, $tempArray);
                    }
                }

            }
        }
        $this->handleSickOnSameDayAsWork();


        return $timesArray;

        //return $allDatesForDay;

    }

    public function handleSickOnSameDayAsWork() {
        $mitarbeiter = Mitarbeiter::all();

        foreach($mitarbeiter as $x => $item) {
            $user_id = $item->id;
            $calculated = Calculated::where([
                ['user_id', $user_id],
                ['manually_created', 0]
            ])->get();
            foreach($calculated as $y => $calc) {
                $date = $calc->lu_date;
                $times = Arbeitszeit::where([
                    ['user_id', $user_id],
                    ['lookup', $date],
                    ['status', 'sick']
                ])->first();
                if($times) {
                    $calc->delete();
                    $workPerWeek = (new Timetrackfrontend)->getWorkWeekIntervals($user_id, $date);
                    $this->createCalculatedTime($user_id, $workPerWeek, $date);
                }
            }
        }
    }

    public function createCalculatedTime($user_id, $workperweek, $date) {
        $calculate = new Calculated();
        $calculate->user_id = $user_id;
        $worked = (new Timetrackfrontend)->convertTime(($workperweek/5));
        $calculate->worked = $worked;
        $calculate->timestamp = date('Y-m-d H:i:s', strtotime($date));
        $calculate->break = '00:00';
        $calculate->lu_date = $date;
        $calculate->save();
    }

}