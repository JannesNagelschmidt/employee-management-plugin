<?php namespace Jannesnagelschmidt\Mitarbeiter\Components;

use Cms\Classes\ComponentBase;

use Jannesnagelschmidt\Mitarbeiter\Models\Mitarbeiter;
use Jannesnagelschmidt\Mitarbeiter\Models\Corona;
use Jannesnagelschmidt\Mitarbeiter\Models\Token;
use Jannesnagelschmidt\Mitarbeiter\Models\Arbeitszeit;

class Coronagraph extends ComponentBase
{
    public function componentDetails() {
        return [
            'name' => 'Coronagraph',
            'description' => 'Shows the graph for corona'
        ];
    }

    public function onRun() {
        $this->corona = $this->loadCorona();
    }

    protected function loadCorona() {
        return Corona::all();
    }

    public $corona;
}