<?php namespace Jannesnagelschmidt\Mitarbeiter\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Http\Request;
use JannesNagelschmidt\Mitarbeiter\Models\Arbeitszeit;
use Jannesnagelschmidt\Mitarbeiter\Models\Mitarbeiter;
use Symfony\Component\Ldap\Ldap;
use Throwable;


class Correction extends ComponentBase {
    public function componentDetails(){
        return [
            'name' => 'Frontend Korrektur',
            'description' => 'Lets you correct or insert timestamp'
        ];
    }
    public function onRun(){
        $this->arbeit = $this->loadArbeitszeit();
    }

    public function checkIfUserIsAuthorized($pw, $user, $fromLoad = false) {
        $back = array();
        if(!$fromLoad) {
            session_unset();
            session_start();
        }
        $name = $user;
        $password = $pw;
        $url = config('ldap.ldap_serv');
        $admindn = 'CN=LDAP User,OU=Servicebenutzer,dc=hansnatur,dc=loc';

        $adminpassword = 'Pai6Peng!';
        $dnReturn = '';

        $ldap = Ldap::create('ext_ldap', [
            'host' => $url,
            'debug' => true
        ]);
        try {
            $ldap->bind($admindn, $adminpassword);
            $query = $ldap->query('DC=hansnatur,DC=loc', '(&(objectClass=*)(sAMAccountName='.$name.'))');
            $results = $query->execute();
            $collection = $results->toArray();

            foreach ($collection as $entry) {
                $bla = $entry->getAttributes();
                $dnReturn = $bla['distinguishedName'][0];
            }
            $ldap->bind($dnReturn, $password);
            $query = $ldap->query('DC=hansnatur,DC=loc', '(&(objectClass=*)(sAMAccountName='.$name.'))');
            $results = $query->execute();
            $collection = $results->toArray();

            foreach ($collection as $entry) {
                $blup = $entry->getAttributes();
            }

            $firstName = $blup['givenName'][0];
            $lastName = $blup['sn'][0];

            $thePerson = Mitarbeiter::where([['first_name','=',$firstName],['last_name','=',$lastName]])->first();
            if(isset($thePerson)) {
                $arbeitszeiten = Arbeitszeit::where('user_id', $thePerson->id)->orderBy('timestamp')->get();

                array_push($back, $thePerson);
                array_push($back, $arbeitszeiten);
                array_push($back, $this->getCorrectionLayout());
                //session_start();
                if(!$fromLoad) {
                    $_SESSION["user"] = $name;
                    $_SESSION["pw"] = $password;
                    $_SESSION["login_time_stamp"] = time();
                }
                if($thePerson->correction_rights == 1) {
                    return $back;
                }
                else {
                    return "UNAUTHORISIERT";
                }
            }
            else {
                return "NEIN";
            }
        }
        catch(Throwable $t) {
            return "NEIN";
        }
    }

    public function checkAuthorizedUser(Request $request) {
        $input = $request->all();

        $name = $input['name'];
        $password = $input['password'];
        return $this->checkIfUserIsAuthorized($password, $name);
    }

    public function checkUserLoggedIn() {
        session_start();
        if(isset($_SESSION["user"])) {
            if(time()-$_SESSION['login_time_stamp'] > 36000) {
                session_unset();
                session_destroy();
                return "NEIN";
            }
            else {
                $pw = $_SESSION['pw'];
                $user = $_SESSION['user'];
                return $this->checkIfUserIsAuthorized($pw, $user, true);
            }
        }
        else {
            return "NEIN";
        }
    }

    protected function loadArbeitszeit(){
        return Arbeitszeit::all();
    }
    public $arbeit;

    public function deleteTime(Request $request) {
        $input = $request->all();
        $id = $input['timeId'];
        return Arbeitszeit::where('id', $id)->delete();
    }

    public function updateTime(Request $request) {
        $input = $request->all();
        $time = $input['time'];
        $date = $input['date'];
        $status = $input['status'];
        $id = $input['id'];

        $entry = Arbeitszeit::where([
            ['user_id', $id],
            ['status', $status],
            ['timestamp', 'like', '%'.$date.'%']
        ])->first();

        if($entry) {
            $entry->timestamp = $time;
            $entry->save();
            return 'JA';
        }
        else {
            Arbeitszeit::addTimetrack($id, $time, $status);
            return 'JA';
        }
    }


    public function getCorrectionLayout() {
        $out = '';
        $out .= '<div class="correction--wrapper">';
        $out .= '<div class="correction--inner-wrapper">';
        $out .= '<label for="lastName">Vorname</label>';
        $out .= '<input id="corrFirstName" type="text" name="firstName" class="correction--input">';
        $out .= '<label for="lastName">Nachname</label>';
        $out .= '<input id="corrLastName" type="text" name="lastName" class="correction--input">';
        $out .= '</div>';
        $out .= '<div class="correction--inner-wrapper">';
        $out .= '<label for="dateToFind">Datum</label>';
        $out .= '<input id="corrDate" class="correction--input" type="date" name="dateToFind">';
        $out .= '</div>';
        $out .= '<button class="btn--frontend" id="getDataOfDay">Senden</button>';
        $out .= '</div>';
        return $out;
    }

    public function getUserByNameAndDate(Request $request) {
        $input = $request->all();
        $returner = array();
        $name = $input['searchName'];
        $firstName = $input['firstName'];
        $empl = Mitarbeiter::where([
            ['last_name', $name],
            ['first_name', $firstName]
        ])->first();
        $date = $input['date'];
        if($empl) {
            $times = Arbeitszeit::where([
                ['user_id', $empl->id],
                ['timestamp', 'like', '%'.$date.'%']
            ])->get();
        }
        else {
            return "NEIN";
        }
        if(count($times) > 0) {
            array_push($returner, $times);
            array_push($returner, $empl);
            return $returner;
        }
        else {
            return "ZEIT NÖ";
        }

    }

}