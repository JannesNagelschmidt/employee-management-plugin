<?php namespace Jannesnagelschmidt\Mitarbeiter\Components;

use Cms\Classes\ComponentBase;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Input;
use JannesNagelschmidt\Mitarbeiter\Models\Arbeitszeit;
use JannesNagelschmidt\Mitarbeiter\Models\Calculated;
use JannesNagelschmidt\Mitarbeiter\Models\ChangeRequests;
use JannesNagelschmidt\Mitarbeiter\Models\Department;
use JannesNagelschmidt\Mitarbeiter\Models\Mitarbeiter;
use JannesNagelschmidt\Mitarbeiter\Models\Sick;
use JannesNagelschmidt\Mitarbeiter\Models\Urlaub;
use October\Rain\Support\Facades\Flash;


class Emt_Berechnung extends ComponentBase
{
    public $loggedIn;
    public $employee;
    public $employees;
    public $corrections;
    public $department;
    public $type;
    public $weekdays = [
        'Monday' => 'Montag',
        'Tuesday' => 'Dienstag',
        'Wednesday' => 'Mittwoch',
        'Thursday' => 'Donnerstag',
        'Friday' => 'Freitag',
        'Saturday' => 'Samstag',
        'Sunday' => 'Sonntag'
    ];

    public function componentDetails() {
        return [
            'name' => 'Berechnung',
            'description' => 'Gibt die gearbeitete Zeit für einen bestimmten Zeitraum zurück'
        ];
    }

    public function onRun() {
        $emt = Emt_Time::checkUser();
        $corrections = array();
        $today = date('Y-m-d');
        if($emt && isset($_SESSION['pw'])) {
            $pw = $_SESSION['pw'];
            $user = $_SESSION['user'];
            $bla = Singleemployee::getUserAndTimestampsByLdapCredentials($pw, $user, true);
            $this->employee = $bla;
            if($bla[0]['id'] == 187 || $bla[0]['id'] == 176 || $bla[0]['id'] == 140 || $bla[0]['id'] == 219 || $bla[0]['id'] == 220) {
                $this->loggedIn = true;
                $dep_id = $bla[0]['department_relation_id_id'];
                $department = Department::where('id', $dep_id)->first();
                $this->department = $department;
                $employees = Mitarbeiter::where('status', 'LIKE', 'aktiv')->get();
                $employees = json_decode(json_encode($employees),true);
                usort($employees, function ($a, $b) {
                    return strcmp($a['last_name'], $b['last_name']);
                });
                $this->employees = $employees;
                foreach($employees as $item) {
                    $entries = Calculated::where([['user_id', $item['id']],['manually_created', true]])->get();
                    $temp['employee'] = $item;
                    $temp['timestamps'] = $entries;
                    $work = (new Timetrackfrontend)->getWorkWeekIntervals($item['id'], $today);
                    if($work) {
                        $perDay = (new Timetrackfrontend)->convertTime($work/5);
                    }
                    $temp['hours_per_day'] = $perDay;
                    array_push($corrections, $temp);
                }

                $this->corrections = $corrections;
            }
            else if((new Emt)->isLeader($bla[0]['id'])) {
                $this->loggedIn = true;
                $dep_id = $bla[0]['department_relation_id_id'];
                $department = Department::where('id', $dep_id)->first();
                $this->department = $department;
                $employees = Mitarbeiter::where([
                    ['status', 'LIKE', 'aktiv']
                ])->get();
                $employees = json_decode(json_encode($employees),true);
                usort($employees, function ($a, $b) {
                    return strcmp($a['last_name'], $b['last_name']);
                });
                $this->employees = $employees;
                foreach($employees as $item) {
                    $entries = Calculated::where([['user_id', $item['id']],['manually_created', true]])->get();
                    $temp['employee'] = $item;
                    $temp['timestamps'] = $entries;
                    $work = (new Timetrackfrontend)->getWorkWeekIntervals($item['id'], $today);
                    $perDay = (new Timetrackfrontend)->convertTime($work/5);
                    $temp['hours_per_day'] = $perDay;
                    array_push($corrections, $temp);
                }

                $this->corrections = $corrections;
            }
        }
    }


    public function onCalcTime() {
        $data = post();
        if(array_key_exists('user_id', $data) && array_key_exists('start', $data) && array_key_exists('end', $data)) {
            if(isset($data['user_id']) && isset($data['start']) && isset($data['end'])) {
                $id = $data['user_id'];
                $start = $data['start'];
                $end = $data['end'];

                return $this->getWorkForFullCalendar($id, $start, $end);
            }
            else {
                header('HTTP/1.1 500 Internal Server Booboo');
                header('Content-Type: application/json; charset=UTF-8');
                die(json_encode(array('message' => 'Bitte Felder ausfüllen!', 'code' => 555)));
            }
        }
        return $data;
    }

    public function getWorkForFullCalendar($id, $start, $end)
    {
        $user_id = $id;
        $user = Mitarbeiter::where('id', $user_id)->first();
        $return = array();
        $multidimensionyoo = array();
        $startDate = strtotime($start);
        $endDate = strtotime($end);
        if(date('w', $endDate) != 0) {
            $nextSunday = date('Y-m-d', strtotime('next sunday', $endDate));
            $endDate = strtotime($nextSunday);
            $end = date('Y-m-d', $endDate);
        }
        $startYear = date('Y', $startDate);
        $holidays = (new Timetrackfrontend)->getHolidays($startYear);
        //$holidays = json_decode($holidays, true);
        $weihnachten['date'] = $startYear.'-12-24';
        $weihnachten['global'] = true;
        $weihnachten['localName'] = 'Heiligabend';
        array_push($holidays, $weihnachten);
        $silvester['date'] = $startYear.'-12-31';
        $silvester['global'] = true;
        $silvester['localName'] = 'Silvester';
        array_push($holidays, $silvester);
        $holidayArray = array();
        array_push($holidayArray, $holidays);
        $last_lu = 0;
        //$calculated = Calculated::where('user_id', $user_id)->get();
        $timetracks = Arbeitszeit::where([['user_id', $user_id], ['status', 'coming']])
            ->orWhere([['user_id', $user_id], ['status', 'vacation']])
            ->orWhere([['user_id', $user_id], ['status', 'sick']])
            ->orWhere([['user_id', $user_id], ['status', 'LIKE', 'Korrektur%']])->orderBy('timestamp', 'asc')->get();
        $toolate = (new Emt_Punctually)->getToolate($user, $timetracks);
        foreach ($timetracks as $x => $item) {
            if ($startYear != date('Y', strtotime($item->lookup))) {
                $startYear = date('Y', strtotime($item->lookup));
                $holidays = (new Timetrackfrontend)->getHolidays($startYear);
                $weihnachten['date'] = $startYear . '-12-24';
                $weihnachten['global'] = true;
                $weihnachten['localName'] = 'Heiligabend';
                array_push($holidays, $weihnachten);
                $silvester['date'] = $startYear . '-12-31';
                $silvester['global'] = true;
                $silvester['localName'] = 'Silvester';
                array_push($holidays, $silvester);
                array_push($holidayArray, $holidays);
            }
            $calculated = Calculated::where([['user_id', $user_id], ['lu_date', $item->lookup]])->get();
            $timestamps = Arbeitszeit::where([['user_id', $user_id], ['lookup', $item->lookup]])->get();
            $hasSick = false;
            foreach ($timestamps as $u => $sickchecker) {
                if ($sickchecker->status == 'sick') {
                    $hasSick = true;
                    $timestamps[0] = $sickchecker;
                }
            }
            if ($x > 0) {
                if ($last_lu !== $timestamps[0]->lookup) {
                    $temp['calculated'] = $calculated;
                    $temp['timestamps'] = $timestamps;
                    array_push($multidimensionyoo, $temp);
                }
            } else {
                $temp['calculated'] = $calculated;
                $temp['timestamps'] = $timestamps;
                array_push($multidimensionyoo, $temp);
                /*$temp['calculated'] = $calculated;
                $temp['timestamps'] = $timestamps;
                array_push($multidimensionyoo, $temp);*/
            }

            $last_lu = $timestamps[0]->lookup;

        }
        $return['entries'] = $multidimensionyoo;
        $return['toolate'] = $toolate;
        $return['holidays'] = $holidayArray;
        $return['sundays'] = (new Emt)->onGetSundayShit($user_id, $holidays);
        //$return['sundays'] = $this->onGetSundayShit($user_id, $holidays, $start, $end);

        return $return;
    }

    public function onGetSundayShit($user_id, $holidays, $start, $end) {
        $user = Mitarbeiter::where('id', $user_id)->first();
        $return = array();
        $today = date('Y-m-d');
        $smallest = Arbeitszeit::where('user_id', $user_id)->orderBy('timestamp', 'asc')->first();
        $startYear = date('Y', strtotime($smallest->lookup));
        $start =  new DateTime($start);
        $end = new DateTime($end);
        $end = $end->modify('+1 day');
        $interval = \DateInterval::createFromDateString('1 day');
        $period = new \DatePeriod($start, $interval, $end);
        foreach($period as $x => $dt) {
            $workHours = (new Timetrackfrontend)->getWorkWeekIntervals($user_id, $dt->format('Y-m-d'));
            $workHours = (new Timetrackfrontend)->convertTime($workHours);
            if($startYear != $dt->format('Y')) {
                $startYear = $dt->format('Y');
                $holidays = (new Timetrackfrontend)->getHolidays($startYear);
            }
            if($dt->format('w') == 0) {
                $ddate = $dt->format('Y-m-d');
                $test = (new Timetrackfrontend)->getWorkTimestampsByDate($ddate);
                $timestampsWeek = array();
                foreach($test as $x => $item) {
                    $value = (new Timetrackfrontend)->getTimestampForUser($item, $user_id, $holidays);
                    array_push($timestampsWeek, $value);
                }
                $workedVal = (new Timetrackfrontend)->getWorkedHoursWeek($timestampsWeek, $user_id, $holidays);
                $weekPlusMinus = (new Timetrackfrontend)->getWeekPlusMinus($workedVal, $workHours);
                $temp['date'] = $dt->format('Y-m-d');
                $temp['workedVal'] = $workedVal;
                $temp['soll'] = $workHours;
                $temp['plusminus'] = $weekPlusMinus;
                array_push($return, $temp);
            }
        }

        //var_dump($smallest);
        return $return;
    }

}