<?php namespace Jannesnagelschmidt\Mitarbeiter\Components;

use Carbon\Carbon;
use Cms\Classes\ComponentBase;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Support\Facades\Redirect;
use Input;
use JannesNagelschmidt\Mitarbeiter\Models\Arbeitszeit;
use JannesNagelschmidt\Mitarbeiter\Models\Calculated;
use JannesNagelschmidt\Mitarbeiter\Models\ChangeRequests;
use JannesNagelschmidt\Mitarbeiter\Models\Department;
use JannesNagelschmidt\Mitarbeiter\Models\Mitarbeiter;
use JannesNagelschmidt\Mitarbeiter\Models\Sick;
use JannesNagelschmidt\Mitarbeiter\Models\Urlaub;
use October\Rain\Support\Facades\Flash;


class Emt_Capacity extends ComponentBase
{
    public $loggedIn;
    public $employee;
    public $employees;
    public $capacities;
    public $department;
    public $test;
    public $timestamp_status = [
        'coming' => 'Kommt',
        'leaving' => 'Geht',
        'break_start' => 'Pause Start',
        'break_end' => 'Pause Ende',
        'vacation' => 'Urlaub',
        'sick' => 'Krank'
    ];
    public $weekdays = [
        'Monday' => 'Montag',
        'Tuesday' => 'Dienstag',
        'Wednesday' => 'Mittwoch',
        'Thursday' => 'Donnerstag',
        'Friday' => 'Freitag',
        'Saturday' => 'Samstag',
        'Sunday' => 'Sonntag'
    ];

    public function componentDetails() {
        return [
            'name' => 'Emt_Kapazitäten',
            'description' => 'Zeigt die Kapazitäten für den heutigen Tag an'
        ];
    }

    public function onRun() {
        $emt = Emt_Time::checkUser();
        $thisYear = date('Y');
        $employees = array();
        $capacities = array();
        $totalWorkers = 0;
        $totalMini = 0;
        $capacities['available'] = 0;
        $capacities['available_mini'] = 0;
        $counter = 0;
        $counter_mini = 0;
        $counterVac = 0;
        $counterSick = 0;
        $counterVacMini = 0;
        $counterSickMini = 0;
        if($emt && isset($_SESSION['pw'])) {
            $pw = $_SESSION['pw'];
            $user = $_SESSION['user'];
            $bla = Singleemployee::getUserAndTimestampsByLdapCredentials($pw, $user, true);
            if((new Emt)->isLeader($bla[0]['id'])) {
                $dep = $bla[0]['id'];
                $this->test = $bla[0];
                //$department = Department::where('id', $dep)->first();
                $department = Department::where('leader_id_id', $dep)->get();
                $this->department = $department;
                $mitarbeiter_real = array();
                foreach($department as $x => $deppies) {
                    $mitarbeiter = Mitarbeiter::where([
                        ['department_relation_id_id', $deppies->id],
                        ['status', 'aktiv']
                    ])->get();
                    array_push($mitarbeiter_real, $mitarbeiter);
                    $mini_mitarbeiter = Mitarbeiter::where([
                        ['department_relation_id_id', $deppies->id],
                        ['status', 'aktiv'],
                        ['is_minijob', '=', true]
                    ])->get();
                    $unmini_mitarbeiter = Mitarbeiter::where([
                        ['department_relation_id_id', $deppies->id],
                        ['status', 'aktiv'],
                        ['is_minijob', '!=', true]
                    ])->get();
                    $totalWorkers += count($unmini_mitarbeiter);
                    $totalMini += count($mini_mitarbeiter);
                }
            }
            if($mitarbeiter_real) {
                foreach($mitarbeiter_real as $y => $blup) {
                    $mitarbeiter = $blup;
                    foreach($mitarbeiter as $x => $item) {
                        $today = date('Y-m-d');
                        $offset = 1*60*60;
                        $now = date('Y-m-d H:i:s');
                        $carbonNow = Carbon::parse($now);
                        $time = Arbeitszeit::where([
                            ['user_id', $item->id],
                            ['lookup', $today],
                            ['status', '!=', 'korrektur']
                        ])->get();
                        if(count($time)) {
                            $status = $time[(count($time)-1)]->status;

                            $start = '00:00';
                            $carbon_start = '';
                            $carbon_break = '';
                            $breaks = 0;
                            foreach($time as $y => $blup) {
                                if($blup->status == 'coming') {
                                    $start = $blup->timestamp;
                                    $carbon_start = Carbon::parse($start);
                                }
                                elseif($blup->status == 'break_end') {
                                    $a = Carbon::parse($time[$y-1]['timestamp']);
                                    $b = Carbon::parse($blup->timestamp);
                                    $break = $b->diffInSeconds($a);
                                    $breaks += $break;
                                }
                                elseif($blup->status == 'leaving') {
                                    $carbonNow = Carbon::parse($blup->timestamp);
                                    $offset = 0;
                                }
                            }
                            if($status == 'leaving' || $status == 'forgot') {
                                $temp['working'] = false;
                            }
                            else if ($status == 'sick') {
                                $temp['working'] = 'sick';
                                $counterSick++;
                            }
                            else if ($status == 'vacation') {
                                $temp['working'] = 'vacation';
                                $counterVac++;
                            }
                            else if ($status == 'sick' && $item->is_minijob) {
                                $temp['working'] = 'sick';
                                $counterSickMini++;
                            }
                            else if ($status == 'vacation' && $item->is_minijob) {
                                $temp['working'] = 'vacation';
                                $counterVacMini++;
                            }
                            else {
                                $temp['working'] = 'working';
                            }
                            $temp['employee'] = $item;
                            $temp['last_time'] = $time;
                            $workHours = (new Timetrackfrontend)->getWorkWeekIntervals($item->id, $today);
                            $workHours = $workHours/5;
                            $workHoursTime = (new Timetrackfrontend)->convertTime($workHours);
                            $workLeft = $carbonNow->diffInSeconds($carbon_start);
                            $workLeftNoBreak = $carbonNow->diffInSeconds($carbon_start);
                            $workLeft = $workLeft - $breaks;
                            $workLeft += $offset;
                            $temp['workHours'] = $workHoursTime;
                            $temp['breaks'] = gmdate('H:i', $breaks);
                            $temp['left'] = gmdate('H:i', $workLeft);
                            $temp['left_no_break'] = gmdate('H:i', $workLeftNoBreak);
                            array_push($employees, $temp);
                            if($status != 'leaving' && $status != 'forgot' && $status != 'vacation' && $status != 'sick' && !$item->is_minijob) {
                                $counter++;
                                if(array_key_exists('available', $capacities)) {
                                    $capacities['available'] += $workHours;
                                }
                                else {
                                    $capacities['available'] = $workHours;
                                }
                            }
                            elseif($item->is_minijob) {
                                if(array_key_exists('available', $capacities)) {
                                    $capacities['available_mini'] += $workHours;
                                }
                                else {
                                    $capacities['available_mini'] = $workHours;
                                }
                                $counter_mini++;
                            }

                        }
                        else {
                            $temp['employee'] = $item;
                            $temp['last_time'] = "Abwesend";
                            $temp['working'] = false;
                            array_push($employees, $temp);
                        }
                    }
                }

            }

            $capacities['available'] = (new Timetrackfrontend)->convertTime($capacities['available']);
            $capacities['available_mini'] = (new Timetrackfrontend)->convertTime($capacities['available_mini']);
            $capacities['number'] = $counter.'/'.$totalWorkers;
            $capacities['mini_number'] = $counter_mini.'/'.$totalMini;
            $capacities['vacation'] = $counterVac.'/'.$totalWorkers;
            $capacities['sick'] = $counterSick.'/'.$totalWorkers;
            $capacities['vacationmini'] = $counterVacMini.'/'.$totalMini;
            $capacities['sickmini'] = $counterSickMini.'/'.$totalMini;
            $this->capacities = $capacities;
            usort($employees, function ($a, $b) {
                return strcmp($a['employee']->last_name, $b['employee']->last_name);
            });
            $this->employees = $employees;
            $this->employee = $bla;
            $this->loggedIn = $emt;
        }
    }

}