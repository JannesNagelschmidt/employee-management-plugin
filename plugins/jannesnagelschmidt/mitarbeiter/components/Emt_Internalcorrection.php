<?php namespace Jannesnagelschmidt\Mitarbeiter\Components;

use Cms\Classes\ComponentBase;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Support\Facades\Redirect;
use Input;
use JannesNagelschmidt\Mitarbeiter\Models\Arbeitszeit;
use JannesNagelschmidt\Mitarbeiter\Models\Calculated;
use JannesNagelschmidt\Mitarbeiter\Models\ChangeRequests;
use JannesNagelschmidt\Mitarbeiter\Models\Department;
use JannesNagelschmidt\Mitarbeiter\Models\Logs;
use JannesNagelschmidt\Mitarbeiter\Models\Mitarbeiter;
use JannesNagelschmidt\Mitarbeiter\Models\Sick;
use JannesNagelschmidt\Mitarbeiter\Models\Urlaub;
use October\Rain\Support\Facades\Flash;


class Emt_Internalcorrection extends ComponentBase
{
    public $loggedIn;
    public $employee;
    public $employees;
    public $corrections;
    public $department;
    public $type;
    public $weekdays = [
        'Monday' => 'Montag',
        'Tuesday' => 'Dienstag',
        'Wednesday' => 'Mittwoch',
        'Thursday' => 'Donnerstag',
        'Friday' => 'Freitag',
        'Saturday' => 'Samstag',
        'Sunday' => 'Sonntag'
    ];

    public function componentDetails() {
        return [
            'name' => 'Emt_InterneKorrektur',
            'description' => 'Interne Korrektur'
        ];
    }

    public function onAddCorrection() {
        $user_id = post('user_id');
        $hours = post('hours');
        $owner = post('thisuser');
        $minutes = post('minutes');
        $date = post('date');
        $strDate = strtotime($date);
        $dateYMD = date('Y-m-d', $strDate);
        if(!$minutes) {
            $minutes = '00';
        }
        if(!$hours && $minutes) {
            $hours = '00';
        }
        $negative = false;
        if($hours < 0) {
            $negative = true;
            $hours = $hours * (-1);
        }
        if($minutes < 0) {
            $negative = true;
            $minutes = $minutes * (-1);
        }
        elseif($minutes < 10) {
            $minutes = (new Timetrackfrontend)->lz($minutes);
        }
        $type = post('type');
        $note = post('note');
        if($hours && $minutes && $type) {
            $lookup = $dateYMD;
            $worked = $hours.':'.$minutes;
            $break = '00:00';
            $timestamp = date('Y-m-d H:i:s', strtotime($dateYMD.' 00:00:00'));
            $entry = new Calculated();
            $entry->user_id = $user_id;
            $entry->worked = $worked;
            $entry->lu_date = $lookup;
            $entry->timestamp = $timestamp;
            $entry->break = $break;
            $entry->manually_created = 1;
            $entry->needs_correction = 0;
            $entry->type = $type;
            $entry->note = $note;
            $entry->negative = $negative;
            $entry->save();
            Arbeitszeit::addTimetrack($user_id, $timestamp, 'Korrektur von '.$owner);
            $blup = new Logs();
            $blup->text = 'Korrektur von '.$owner.' '.$timestamp;
            $blup->save();
            return Redirect::back();
        }
        else {
            header('HTTP/1.1 500 Internal Server Booboo');
            header('Content-Type: application/json; charset=UTF-8');
            die(json_encode(array('message' => 'Erforderliche Felder angeben!', 'code' => 555)));
        }
    }

    public function onDeleteCorrection() {
        $id = post('id');
        $entry = Calculated::where('id', $id)->first();
        $timestamp = Arbeitszeit::where([
            ['lookup', $entry->lu_date],
            ['status', 'LIKE', '%Korrektur%'],
            ['user_id', $entry->user_id]
            ])->first();
        if($timestamp) {
            $timestamp->delete();
        }
        if(post('thisuser')) {
            $log = new Logs();
            $log->text = post('thisuser') .' '.$id;
            $log->save();
        }
        $entry->delete();
        return Redirect::back();

        //return post('id');
    }

    public function onRun() {
        $type = [
            'correction' => 'Korrektur',
            'special_vacation' => 'Sonderurlaub',
            'school' => 'Schule',
            'quarantine' => 'Quarantäne'
        ];
        $this->type = $type;
        $emt = Emt_Time::checkUser();
        $corrections = array();
        $today = date('Y-m-d');
        if($emt && isset($_SESSION['pw'])) {
            $pw = $_SESSION['pw'];
            $user = $_SESSION['user'];
            $bla = Singleemployee::getUserAndTimestampsByLdapCredentials($pw, $user, true);
            $this->employee = $bla;
            if($bla[0]['id'] == 187 || $bla[0]['id'] == 140 || $bla[0]['id'] == 219 || $bla[0]['id'] == 220) {
                $this->loggedIn = true;
                $dep_id = $bla[0]['department_relation_id_id'];
                $department = Department::where('id', $dep_id)->first();
                $this->department = $department;
                $employees = Mitarbeiter::where('status', 'LIKE', 'aktiv')->get();
                $employees = json_decode(json_encode($employees),true);
                usort($employees, function ($a, $b) {
                    return strcmp($a['last_name'], $b['last_name']);
                });
                $this->employees = $employees;
                foreach($employees as $item) {
                    $entries = Calculated::where([['user_id', $item['id']],['manually_created', true]])->get();
                    $temp['employee'] = $item;
                    $temp['timestamps'] = $entries;
                    $work = (new Timetrackfrontend)->getWorkWeekIntervals($item['id'], $today);
                    if($work) {
                        $perDay = (new Timetrackfrontend)->convertTime($work/5);
                    }
                    else {
                        $perDay = 0;
                    }
                    $temp['hours_per_day'] = $perDay;
                    array_push($corrections, $temp);
                }

                $this->corrections = $corrections;
            }
            else if((new Emt)->isLeader($bla[0]['id'])) {
                $this->loggedIn = true;
                $dep_id = $bla[0]['department_relation_id_id'];
                $department = Department::where('id', $dep_id)->first();
                $this->department = $department;
                $employees = Mitarbeiter::where([
                    ['department_relation_id_id', $dep_id],
                    ['status', 'LIKE', 'aktiv']
                ])->get();
                $employees = json_decode(json_encode($employees),true);
                usort($employees, function ($a, $b) {
                    return strcmp($a['last_name'], $b['last_name']);
                });
                $this->employees = $employees;
                foreach($employees as $item) {
                    $entries = Calculated::where([['user_id', $item['id']],['manually_created', true]])->get();
                    $temp['employee'] = $item;
                    $temp['timestamps'] = $entries;
                    $work = (new Timetrackfrontend)->getWorkWeekIntervals($item['id'], $today);
                    $perDay = (new Timetrackfrontend)->convertTime($work/5);
                    $temp['hours_per_day'] = $perDay;
                    array_push($corrections, $temp);
                }

                $this->corrections = $corrections;
            }
        }
    }


}