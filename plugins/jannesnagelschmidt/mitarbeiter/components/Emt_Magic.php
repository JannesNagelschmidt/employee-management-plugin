<?php namespace Jannesnagelschmidt\Mitarbeiter\Components;

use Cassandra\Time;
use Cms\Classes\ComponentBase;
use DateTime;
use DateTimeZone;
use http\Url;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Input;
use JannesNagelschmidt\Mitarbeiter\Models\Urlaub;
use Jenssegers\Date\Date;
use Mail;
use JannesNagelschmidt\Mitarbeiter\Models\Arbeitszeit;
use JannesNagelschmidt\Mitarbeiter\Models\Calculated;
use JannesNagelschmidt\Mitarbeiter\Models\ChangeRequests;
use JannesNagelschmidt\Mitarbeiter\Models\Department;
use JannesNagelschmidt\Mitarbeiter\Models\Mitarbeiter;
use JannesNagelschmidt\Mitarbeiter\Models\Notifications;


class Emt_Magic extends ComponentBase
{

    public $user;
    public $lead;
    public $state;
    public $type;
    public $action;
    public $numberOfUnanswered;
    public $isLeader;
    public $notification;
    public $notifictionUnanswered;
    public $leaderNotifications;
    public $leaderNotiNum;
    public $department_id;
    public $totalAmount;


    public function componentDetails() {
        return [
            'name' => 'Emt_Magic',
            'description' => 'Zauberei'
        ];
    }

    public function onSetNotifactionSeen() {
        $id = post('id');
        $entry = Notifications::where('id', $id)->first();
        $entry->seen = 1;
        $entry->save();
        //return "EYOOOO ". post('id');
    }

    public function onRun() {
        $types = [
            'add'=>'Hinzufügen',
            'delete'=>'Löschen',
            'correct'=>'Korrigieren'
        ];
        $states = [
            'pending' => 'Warten auf Bearbeitung',
            'declined' => 'Abgelehnt',
            'accepted' => 'Bestätigt',
            'URLAUB' => 'URLAUB'
        ];
        $timestamp_status = [
            'coming' => 'Kommt',
            'leaving' => 'Geht',
            'break_start' => 'Pause Start',
            'break_end' => 'Pause Ende'
        ];
        $leader = false;
        $number = 0;
        $this->type = $types;
        $this->state = $states;
        $this->action = $timestamp_status;

        $pw = $_SESSION['pw'];
        $user = $_SESSION['user'];
        $blup = Singleemployee::getUserAndTimestampsByLdapCredentials($pw, $user, true);
        //$this->user = $bla;
        $bla = array();
        $thePerson = Mitarbeiter::where('id', 194)->first();
        $arbeitszeiten = Arbeitszeit::where([
            ['user_id', $thePerson->id],
            ['status', '!=', 'vacation'],
            ['status', '!=', 'sick'],
            ['status', '!=', 'Korrektur%']
        ])->orderBy('timestamp')->get();

        array_push($bla, $thePerson);
        array_push($bla, $arbeitszeiten);


            $user_id = $bla[0]['id'];
            $notifications = Notifications::where('employ_id_id_id', $user_id)->get();
            $unseen = Notifications::where([
                ['employ_id_id_id', $user_id],
                ['seen', 0]
            ])->get();
            $this->notifictionUnanswered = count($unseen);
            $notNew = json_decode(json_encode($notifications),true);
            rsort($notNew);
            $this->notification = $notNew;
            $theLeader = $this->leaderFunc($user_id);
            $this->lead = $theLeader;
            $gesamt = $this->getPlusMinus($user_id);
            $this->totalAmount = $gesamt;
            if($this->isLeader($user_id)) {
                $this->isLeader = true;
                $this->leaderNotifications = $this->getLeaderNotifications($user_id);
                $department = Department::where('leader_id_id', $user_id)->first();
                $this->department_id = $department->id;
                foreach($theLeader as $item) {
                    if($item['entry']['status'] == 'pending' || $item['entry']['status'] == 'Beantragt') {
                        $number++;
                    }
                }
                $this->numberOfUnanswered = $number;
            }




        //return $loggedin;
    }

    public function onGetUserForMagic() {
        $data = post();

        return $data['user_id'];
    }

    public function getLeaderNotifications($id) {
        $notiLead = Notifications::where('employ_id_id_id', $id)->get();
        $blup = array();
        $counter = 0;
        foreach($notiLead as $item) {
            /*if($id != $item->employ_id_id_id) {*/
                if(!$item->seen) {
                    $counter++;
                }
                $temp['notification'] = $item;
                $temp['employee'] = Mitarbeiter::where('id', $item->employ_id_id_id)->first();
                array_push($blup, $temp);
            /*}*/
        }
        $this->leaderNotiNum = $counter;
        rsort($blup);
        return $blup;
    }

    public function onHideEntry() {
        $data = post();
        $id = $data['id'];
        if(array_key_exists('isVacation', $data)) {
            $entry = Urlaub::where('id', $id)->first();
            $entry->visible = 0;
            $entry->save();
        }
        else {
            $entry = ChangeRequests::where('id', $id)->first();
            $entry->visible = 0;
            $entry->save();
        }
        return Redirect::back();
    }

    public function onDeleteNotification() {
        $id = post('noti_id');
        $entry = Notifications::where('id', $id)->first();
        $entry->delete();
        return Redirect::back();
    }



    public function changeTimestamp($id = false, $delete = false, $new_time = false, $status = false, $user_id = false) {
        $states = [
            'pending' => 'Warten auf Bearbeitung',
            'declined' => 'Abgelehnt',
            'accepted' => 'Bestätigt',
            'URLAUB' => 'URLAUB'
        ];
        if($user_id) {
            $user = Mitarbeiter::where('id', $user_id)->first();
        }
        else {
            $user = false;
        }
        if($id) {
            $entry = Arbeitszeit::where('id', $id)->first();
            $lookup = $entry->lookup;
            $calculated = Calculated::where([
                ['lu_date', $lookup],
                ['user_id', $user_id]
            ])->first();
            if($calculated) {
                $calculated->delete();
            }
            if($delete) {
                $entry->delete();

                //Emt_Profile::sendMailNotification($user_id, $entry->timestamp,"/antrag", "Antrag angenommen");
                //Emt_Time::createNotification($user_id, 'Antrag "Löschen" für '.$lookup.' gelöscht', "update", "/antrag");
            }
            else {
                $entry->timestamp = $new_time;
                $entry->status = $status;
                $entry->save();
                //Emt_Time::createNotification($user_id, 'Antrag "Korrektur" für '.$lookup.' korrigiert', "update", "/antrag");
                //Emt_Profile::sendMailNotification($user_id, $new_time,"/antrag", "Antrag zur Korrektur angenommen");
            }
        }
        else {
            $lookup = date('Y-m-d', strtotime($new_time));
            $calculated = Calculated::where([
                ['lu_date', $lookup],
                ['user_id', $user_id]
            ])->first();
            if($calculated) {
                $calculated->delete();
            }
            $entry = new Arbeitszeit();
            $entry->timestamp = $new_time;
            $entry->user_id = $user_id;
            $entry->lookup = $lookup;
            $entry->status = $status;
            $entry->save();
            //Emt_Time::createNotification($user_id, 'Antrag "Hinzufügen" für'.$lookup.' hinzugefügt', "update", "/antrag");
            //Emt_Profile::sendMailNotification($user_id, $new_time,"/antrag", "Antrag angenommen");
        }
    }

    public function onToggleAllEntries() {
        $user_id = post('leader_id');
        $vacations = Urlaub::where([
            ['leader_id_id', $user_id],
            ['status', 'Genehmigt']
            ])->orWhere([
                ['leader_id_id', $user_id],
                ['status', 'Abgelehnt']
            ])->get();
        $entries = ChangeRequests::where([
            ['status', 'accepted'],
            ['leader_id',  $user_id]
        ])->orWhere([
            ['status', 'declined'],
            ['leader_id', $user_id]
        ])->get();
        if(count($entries) > 0) {
            if($entries[0]->visible) {
                foreach($entries as $entry) {
                    $entry->visible = 0;
                    $entry->save();
                }
            }
            else {
                foreach($entries as $entry) {
                    $entry->visible = 1;
                    $entry->save();
                }
            }
        }
        if(count($vacations) > 0) {
            if($vacations[0]->visible) {
                foreach($vacations as $entry) {
                    $entry->visible = 0;
                    $entry->save();
                }
            }
            else {
                foreach($vacations as $entry) {
                    $entry->visible = 1;
                    $entry->save();
                }
            }
        }
        return Redirect::back();
    }

    public function getPlusMinus($user_id) {
        $thisYear = date('Y');
        $employee = Mitarbeiter::where('id', $user_id)->first();
        $entry = Calculated::where('user_id', $user_id)->first();
        if($entry) {
            //echo $entry->lu_date;
            $start =  new DateTime($entry->lu_date);
            $thisYear = $start->format('Y');
            $holidays = (new Timetrackfrontend)->getHolidays($thisYear);
            $end = new DateTime();
            $end->sub(new \DateInterval('P1D'));
            $interval = \DateInterval::createFromDateString('1 day');
            $entries = Calculated::where('user_id', $user_id)->get();
            $value = 0;
            $period = new \DatePeriod($start, $interval, $end);
            $blup = array();


            foreach($period as $dt) {
                if($dt->format('Y') != $thisYear) {
                    $thisYear = $dt->format('Y');
                    $holidays = (new Timetrackfrontend)->getHolidays($thisYear);
                }
                $isFeiertag = (new Timetrackfrontend)->checkPublicHolidays($dt->format('Y-m-d'), $holidays);
                if(!$isFeiertag && !($employee->is_minijob)) {
                    $workHours = (new Timetrackfrontend)->getWorkWeekIntervals($user_id, $dt->format('Y-m-d'));
                    $SOLL = (new Timetrackfrontend)->convertTime(($workHours/5));
                    $sum = strtotime('00:00:00');
                    $strSOLL = strtotime($SOLL) - $sum;
                    $item = Calculated::where([
                        ['user_id', $user_id]
                        ,['lu_date', $dt->format('Y-m-d')]
                    ])->first();
                    if(!$item && $dt->format('w') != 0 && $dt->format('w') != 6) {
                        //echo $dt->format('Y-m-d').'</br>';
                        //echo $dt->format('Y-m-d');
                        $a['result'] = date('H:i:s', $strSOLL);
                        $a['manually'] = false;
                        $a['negative'] = true;
                        $a['date'] = $dt->format('Y-m-d');
                        $a['soll'] = $SOLL;
                        $a['ist'] = "00:00";
                        array_push($blup, $a);
                    }
                    elseif($item) {
                        if($item->manually_created) {
                            $a['result'] = date('H:i:s', $strSOLL);
                            $a['manually'] = false;
                            $a['negative'] = true;
                            $a['date'] = $dt->format('Y-m-d');
                            $a['soll'] = $SOLL;
                            $a['ist'] = "00:00";
                            array_push($blup, $a);
                        }
                    }
                }
            }

            foreach($entries as $x => $item) {
                $result = 0;
                $sum = strtotime('00:00:00');
                $value = 0;
                $biggerThanADay = false;
                $negative = false;
                $dateYMD = $item->lu_date;
                $workHours = (new Timetrackfrontend)->getWorkWeekIntervals($user_id, $dateYMD);
                $SOLL = (new Timetrackfrontend)->convertTime(($workHours/5));
                $IST = $item->worked;
                $strIST = strtotime($IST) - $sum;
                $strSOLL = strtotime($SOLL) - $sum;
                if($strIST < 0 && $item->manually_created) {
                    $negative = $item->negative;
                    $biggerThanADay = true;
                    //echo $strIST;
                    if(strpos($IST, ':') !== false) {
                        $arr = explode(":", $IST, 2);
                        $istHours = (int)$arr[0];
                        $istMinutes = $arr[1];
                        $istMinutes = (new Timetrackfrontend)->lz($istMinutes);
                    }
                    else {
                        $istHours = $IST;
                        $istMinutes = "00";
                    }
                    $days = floor($istHours / 24);
                    $rest = ($istHours/24) - $days;
                    $rest = $rest * 24;
                    $rest = $rest.':'.$istMinutes;
                    //echo $rest;
                    $a['result'] = $rest;
                    $a['negative'] = $negative;
                    $a['date'] = $dateYMD;
                    $a['soll'] = "00:00";
                    $a['ist'] = $IST;
                    array_push($blup, $a);
                    for($i = 0; $i < $days; $i++) {
                        $result = "24:00:00";
                        $a['result'] = $result;
                        $a['negative'] = $negative;
                        $a['date'] = $dateYMD;
                        $a['soll'] = "00:00";
                        $a['ist'] = $IST;
                        array_push($blup, $a);
                    }
                    //$strIST = $istHours * 60 * 60;
                    //echo $strIST;
                }

                if($x == 0 && !$item->manually_created) {
                    if($strSOLL > $strIST) {
                        $result = $strSOLL-$strIST;
                        //$value = $result * (-1);
                        $negative = true;
                    }
                    else {
                        $result = $strIST-$strSOLL;
                    }
                }
                if(($strIST != $strSOLL) && !$item->manually_created) {
                    if($strSOLL > $strIST) {
                        $negative = true;
                        $result = $strSOLL-$strIST;
                        //$value = $value - $result;
                    }
                    else {
                        $result = $strIST-$strSOLL;
                    }
                }
                else if($item->manually_created) {
                    if($item->negative) {
                        $result = $strIST;
                        $SOLL = "00:00";
                        $negative = true;
                    }
                    else {
                        $result = $strIST;
                        $negative = false;
                        $SOLL = "00:00";
                    }
                }
                if(!$biggerThanADay) {
                    $a['result'] = date('H:i', $result);
                    $a['negative'] = $negative;
                    $a['date'] = $dateYMD;
                    $a['soll'] = $SOLL;
                    $a['manually'] = $item->manually_created;
                    $a['ist'] = $IST;
                    array_push($blup, $a);
                }
                //$a['result'] = $result;
            }
            //var_dump($blup);
            //$sum1 = 0;
            $posi = 0;
            $nega = 0;
            foreach($blup as $y => $item) {

                if($item['negative']) {
                    $nega += strtotime($item['result']) - $sum;
                }
                else {
                    $posi += strtotime($item['result']) - $sum;
                }
            }
            $pH = $posi / 3600;
            $pM = ($pH - floor($pH)) * 60;
            $pS = ($pH - floor($pH)) * 3600;
            $pE = floor($pH) . ':'. (new Timetrackfrontend)->lz(round($pM));

            $nH = $nega / 3600;
            $nM = ($nH - floor($nH)) * 60;
            $nS = ($nH - floor($nH)) * 3600;
            $nE = floor($nH) . ':'. (new Timetrackfrontend)->lz(round($nM));
            /*echo $pE . "\r\n";
            echo $nE . "\r\n";*/

            if($pH > $nH) {
                $isNegative = false;
                $valueH = $pH-$nH;
            }
            else {
                $isNegative = true;
                $valueH = $nH-$pH;
            }
            $realValueM = $valueH - floor($valueH);
            $realEndValueM = round($realValueM*60);

            $value = $posi - $nega;
            $realend = floor($valueH) . ':' .  (new Timetrackfrontend)->lz(round($realEndValueM));
            $back = array();
            if($isNegative) {
                if(round($realEndValueM) == 0 && floor($valueH) == 0) {
                    $back['time'] = '+/-'.$realend;
                    $back['negative'] = false;
                }
                else {
                    $back['time'] = '-'.$realend;
                    $back['negative'] = true;
                }
            }
            else {
                $back['time'] = '+'.$realend;
                $back['negative'] = false;
            }
            return $back;
        }
        else {
            $back['time'] = '00:00';
            return $back;
        }


    }

    public function vacationAccepted($entry) {
        $start =  new DateTime($entry->start_date);
        $end = new DateTime($entry->end_date);
        $user_id = $entry->mitarbeiter_id;
        $end = $end->modify('+1 day');
        $interval = \DateInterval::createFromDateString('1 day');
        $period = new \DatePeriod($start, $interval, $end);
        foreach($period as $dt) {
            if($dt->format('w') != 0 && $dt->format('w') != 6) {
                Arbeitszeit::addTimetrack($user_id, $dt->format('Y-m-d H:i:s'), 'vacation');
            }
        }
    }

    public function onChangeStatus() {
        $types = [
            'add'=>'Hinzufügen',
            'delete'=>'Löschen',
            'correct'=>'Korrigieren'
        ];
        $states = [
            'pending' => 'Warten auf Bearbeitung',
            'declined' => 'Abgelehnt',
            'accepted' => 'Bestätigt',
            'URLAUB' => 'URLAUB',
            'abgelehnt' => 'abgelehnt'
        ];
        $data = post();
        $status = $data['status'];
        $id = $data['id'];
        $user_id = $data['user_id'];
        switch($status) {
            case 'accepted':
                if(array_key_exists('isVacation', $data)) {
                    if($data['isVacation']) {
                        $entry = Urlaub::where('id', $id)->first();
                        $entry->status = 'Genehmigt';
                        $entry->visible = 0;
                        $entry->save();
                        Emt_Time::createNotification($user_id, 'Urlaubsntrag '.$states[$status], "neu", "/antrag", $id);
                        Emt_Profile::sendMailNotification($user_id, ' ',"/antrag#".$id, "Urlaubsantrag genehmigt");
                        //$this->vacationAccepted($entry);
                    }
                    else {
                        $entry = ChangeRequests::where('id', $id)->first();
                        $new_time = $entry->new_time;
                        if($entry->timestamp_status) {
                            $timestamp_status = $entry->timestamp_status;
                        }
                        else {
                            $timestamp_status = false;
                        }
                        if($entry->og_time) {
                            $og_time = $entry->og_time;
                        }
                        else {
                            $og_time = false;
                        }
                        if($entry->timestamp_id) {
                            $timestamp_id = $entry->timestamp_id;
                        }
                        else {
                            $timestamp_id = false;
                        }
                        $type = $entry->type;
                        if($type == 'delete') {
                            $delete = true;
                        }
                        else {
                            $delete = false;
                        }

                        $this->changeTimestamp($timestamp_id, $delete, $new_time, $timestamp_status, $user_id);

                        $entry->status = $status;
                        $entry->visible = 0;
                        $entry->save();
                        Emt_Time::createNotification($user_id, 'Antrag "'.$types[$entry->type].'" '.$states[$status], "neu", "/antrag");
                        Emt_Profile::sendMailNotification($user_id, ' ',"/antrag", "Antrag angenommen");
                        return Redirect::back();
                    }
                }
                else {
                    $entry = ChangeRequests::where('id', $id)->first();
                    $new_time = $entry->new_time;
                    if($entry->timestamp_status) {
                        $timestamp_status = $entry->timestamp_status;
                    }
                    else {
                        $timestamp_status = false;
                    }
                    if($entry->og_time) {
                        $og_time = $entry->og_time;
                    }
                    else {
                        $og_time = false;
                    }
                    if($entry->timestamp_id) {
                        $timestamp_id = $entry->timestamp_id;
                    }
                    else {
                        $timestamp_id = false;
                    }
                    $type = $entry->type;
                    if($type == 'delete') {
                        $delete = true;
                    }
                    else {
                        $delete = false;
                    }

                    $this->changeTimestamp($timestamp_id, $delete, $new_time, $timestamp_status, $user_id);

                    $entry->status = $status;
                    $entry->visible = 0;
                    $entry->save();
                    Emt_Time::createNotification($user_id, 'Antrag "'.$types[$entry->type].'" '.$states[$status], "neu", "/antrag");
                    Emt_Profile::sendMailNotification($user_id, ' ',"/antrag", "Antrag angenommen");
                    return Redirect::back();
                }
                break;
            case 'declined':
                if(array_key_exists('isVacation', $data)) {
                    if($data['isVacation']) {
                        $entry = Urlaub::where('id', $id)->first();
                        $name = $data['leader_name'];
                        $oldMemo = $entry->name;
                        if($data['memo']) {
                            $entry->name=$oldMemo."\n".'['.$name.']:'.$data['memo'];
                        }
                        $entry->status = 'Abgelehnt';
                        $entry->visible = 0;
                        $entry->save();
                        Emt_Time::createNotification($user_id, 'Urlaubsntrag '.$states[$status], "neu", "/antrag", $id);
                        Emt_Profile::sendMailNotification($user_id, ' ',"/antrag#".$id, "Urlaubsantrag abgelehnt");
                        return Redirect::back();
                    }

                }
                else {
                    $entry = ChangeRequests::where('id', $id)->first();
                    $entry->status = $status;
                    $entry->visible = 0;
                    $oldMemo = $entry->memo;
                    if($data['memo']) {
                        if($data['leader_name']) {
                            $memoAdd = $data['memo'];
                            $leader = $data['leader_name'];
                            $entry->memo = $oldMemo."\n".'['.$leader.']:'.$memoAdd;
                        }
                        else {
                            $memoAdd = $data['memo'];
                            $entry->memo = $oldMemo."\n".'[Antwort]:'.$memoAdd;
                        }
                    }
                    $entry->save();
                    Emt_Time::createNotification($user_id, 'Antrag "'.$types[$entry->type].'" '.$states[$status], "neu", "/antrag", $id);
                    Emt_Profile::sendMailNotification($user_id, ' ',"/antrag#".$id, "Antrag abgelehnt");
                    return Redirect::back();
                }
                break;
            default:
                return 'NEIN';
        }
        return Redirect::back();
    }

    public function onAddMemo() {
        $data = post();
        if(array_key_exists('isVacation',$data)) {
            if($data['isVacation']) {
                $id = post('id');
                $user_id = $data['user_id'];
                $memo = post('memo');
                $self_id = $data['self_id'];
                $name = post('name');
                $leader = Emt_Profile::getLeaderByUserId($user_id);
                $oldMemo = '';
                $entry = Urlaub::where('id', $id)->first();
                $oldMemo = $entry->name;
                if($memo) {
                    $entry->name=$oldMemo."\n".'['.$name.']:'.$memo;
                    $entry->save();
                }
                $time = $entry->start_date;
                Emt_Time::createNotification($user_id, $name. ' hat etwas geschrieben...', "update", "/antrag", $id);
                if($user_id == $self_id) {
                    Emt_Profile::sendMailNotification($leader->id, $time, "/antrag#".$id, "hat eine Notiz geschrieben", $name);
                }
                else {
                    Emt_Profile::sendMailNotification($user_id, $time, "/antrag#".$id, "hat eine Notiz geschrieben", $leader->first_name.' '.$leader->last_name);
                }
            }
        }
        else {
            $memo = $data['memo'];
            $id = $data['id'];
            $user_id = $data['user_id'];
            $leader = Emt_Profile::getLeaderByUserId($user_id);
            $name = $data['name'];
            $self_id = $data['self_id'];
            $entry = ChangeRequests::where('id', $id)->first();
            $entry->memo = $entry->memo."\n".'['.$name.']:'.$memo;
            $entry->save();

            if($entry->new_time) {
                $time = $entry->new_time;
            }
            else {
                $time = $entry->og_time;
            }
            Emt_Time::createNotification($user_id, $name. ' hat etwas geschrieben...', "update", "/antrag", $id);
            if($user_id == $self_id) {
                Emt_Profile::sendMailNotification($leader->id, $time, "/antrag#".$id, "hat eine Notiz geschrieben", $name);
            }
            else {
                Emt_Profile::sendMailNotification($user_id, $time, "/antrag#".$id, "hat eine Notiz geschrieben", $leader->first_name.' '.$leader->last_name);
            }
        }
        return Redirect::back();
    }

    public function isLeader($id) {
        $bla = Department::where('leader_id_id', $id)->first();
        if($bla !== NULL && $bla) {
            return true;
        }
        else return false;
    }

    public function leaderFunc($id) {
        $leaderCheck = Department::where('leader_id_id', $id)->get();
        $returnArr = array();
        if($leaderCheck) {
            //$this->isLeader = true;
            $entries = ChangeRequests::where([
                ['leader_id', $id],
                ['visible', 1]
            ])->get();
            $vacationEntries = Urlaub::where([
                ['leader_id_id', $id],
                ['visible', 1]
            ])->get();
            $newVac = json_decode(json_encode($vacationEntries), true);
            $new = json_decode(json_encode($entries),true);
            rsort($new);
            rsort($newVac);
            foreach($new as $x => $entry) {
                $person = Mitarbeiter::where('id', $entry['user_id'])->first();
                $tempArr = [
                    'person'=>$person,
                    'entry'=>$entry
                ];
                array_push($returnArr, $tempArr);
            }
            foreach($newVac as $y => $item) {
                $person = Mitarbeiter::where('id', $item['mitarbeiter_id'])->first();
                $tempArr2 = [
                    'person'=>$person,
                    'entry'=>$item
                ];
                array_push($returnArr, $tempArr2);
            }
            //rsort($returnArr);
            return $returnArr;
        }
        else {
            $this->isLeader = false;
            return false;
        }
    }

    public function onTrackTime() {
        $user_id = post('user_id');
        $status = post('status');
        $offset = 1*60*60;
        $timestamp = gmdate('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s'))+$offset);
        //$timestamp = date('Y-m-d H:i:s');
        Arbeitszeit::addTimetrack($user_id, $timestamp, $status, 'homeoffice');
        return Redirect::back();
    }

    public function onLogoutWindows() {
        (new Singleemployee)->endSessionEmployee();
        return Redirect::back();
    }

    public function onDeleteAllNotis() {
        $user_id = post('user_id');

        $entries = Notifications::where('employ_id_id_id', $user_id)->get();

        foreach ($entries as $entry) {
            $entry->delete();
        }
        return Redirect::back();
    }

    public function onAllNotisRead() {
        $user_id = post('user_id');
        if(post('is_leader')) {
            $entries = Notifications::where('leader_id', $user_id)->get();
        }
        else {
            $entries = Notifications::where('employ_id_id_id', $user_id)->get();
        }
        foreach ($entries as $entry) {
            $entry->seen = 1;
            $entry->save();
        }
        return Redirect::back();
    }

    public function onLoginWindows() {
        $data = Input::all();

        $test = Singleemployee::getUserAndTimestampsByLdapCredentials($data['password'], $data['user']);
        if($test == 'NEIN!') {
            header('HTTP/1.1 500 Internal Server Booboo');
            header('Content-Type: application/json; charset=UTF-8');
            die(json_encode(array('message' => 'Passwort oder Nutzername falsch!', 'code' => 555)));
        }
        $this->user = $test;
        //return $test;
        return Redirect::back();
    }

    public function getWorkForFullCalendar(Request $request) {
        $data =  $request->all();
        $user_id = $data['user_id'];
        $user = Mitarbeiter::where('id', $user_id)->first();
        $return = array();
        $multidimensionyoo = array();
        $smallest = Arbeitszeit::where('user_id', $user_id)->orderBy('timestamp', 'asc')->first();
        $startYear = date('Y', strtotime($smallest->lookup));
        $holidays = (new Timetrackfrontend)->getHolidays($startYear);
        //$holidays = json_decode($holidays, true);
        $weihnachten['date'] = $startYear.'-12-24';
        $weihnachten['global'] = true;
        $weihnachten['localName'] = 'Heiligabend';
        array_push($holidays, $weihnachten);
        $silvester['date'] = $startYear.'-12-31';
        $silvester['global'] = true;
        $silvester['localName'] = 'Silvester';
        array_push($holidays, $silvester);
        $holidayArray = array();
        array_push($holidayArray, $holidays);

        //$calculated = Calculated::where('user_id', $user_id)->get();
        $timetracks = Arbeitszeit::where([['user_id', $user_id], ['status', 'coming']])
            ->orWhere([['user_id', $user_id], ['status', 'vacation']])
            ->orWhere([['user_id', $user_id], ['status', 'sick']])
            ->orWhere([['user_id', $user_id], ['status', 'korrektur']])->get();
        $toolate = (new Emt_Punctually)->getToolate($user, $timetracks);
        foreach($timetracks as $x => $item) {
            if($startYear != date('Y', strtotime($item->lookup))) {
                $startYear = date('Y', strtotime($item->lookup));
                $holidays = (new Timetrackfrontend)->getHolidays($startYear);
                $weihnachten['date'] = $startYear.'-12-24';
                $weihnachten['global'] = true;
                $weihnachten['localName'] = 'Heiligabend';
                array_push($holidays, $weihnachten);
                $silvester['date'] = $startYear.'-12-31';
                $silvester['global'] = true;
                $silvester['localName'] = 'Silvester';
                array_push($holidays, $silvester);
                array_push($holidayArray, $holidays);
            }
            $calculated = Calculated::where([['user_id', $user_id], ['lu_date', $item->lookup]])->first();
            $timestamps = Arbeitszeit::where([['user_id', $user_id], ['lookup', $item->lookup]])->get();
            if($x > 0 && $last_lu !== $timestamps[0]->lookup) {
                $temp['calculated'] = $calculated;
                $temp['timestamps'] = $timestamps;
                array_push($multidimensionyoo, $temp);
            }
            else {
                $temp['calculated'] = $calculated;
                $temp['timestamps'] = $timestamps;
                array_push($multidimensionyoo, $temp);
            }
            $last_lu = $timestamps[0]->lookup;
        }
        $return['entries'] = $multidimensionyoo;
        $return['toolate'] = $toolate;
        $return['holidays'] = $holidayArray;
        $return['sundays'] = $this->onGetSundayShit($user_id, $holidays);

        return $return;
    }

    public function onGetSundayShit($user_id, $holidays) {
        $user = Mitarbeiter::where('id', $user_id)->first();
        $return = array();
        $today = date('Y-m-d');
        $smallest = Arbeitszeit::where('user_id', $user_id)->orderBy('timestamp', 'asc')->first();
        $startYear = date('Y', strtotime($smallest->lookup));
        $start =  new DateTime($smallest->lookup);
        $end = new DateTime($today);
        $end = $end->modify('+1 day');
        $interval = \DateInterval::createFromDateString('1 day');
        $period = new \DatePeriod($start, $interval, $end);
        foreach($period as $x => $dt) {
            $workHours = (new Timetrackfrontend)->getWorkWeekIntervals($user_id, $dt->format('Y-m-d'));
            if($startYear != $dt->format('Y')) {
                $startYear = $dt->format('Y');
                $holidays = (new Timetrackfrontend)->getHolidays($startYear);
            }
            if($dt->format('w') == 0) {
                $ddate = $dt->format('Y-m-d');
                $test = (new Timetrackfrontend)->getWorkTimestampsByDate($ddate);
                $timestampsWeek = array();
                foreach($test as $x => $item) {
                    $value = (new Timetrackfrontend)->getTimestampForUser($item, $user_id, $holidays);
                    array_push($timestampsWeek, $value);
                }
                $workedVal = (new Timetrackfrontend)->getWorkedHoursWeek($timestampsWeek, $user_id, $holidays);
                $weekPlusMinus = (new Timetrackfrontend)->getWeekPlusMinus($workedVal, $workHours);
                $temp['date'] = $dt->format('Y-m-d');
                $temp['workedVal'] = $workedVal;
                $temp['soll'] = $workHours;
                $temp['plusminus'] = $weekPlusMinus;
                array_push($return, $temp);
            }
        }

        //var_dump($smallest);
        return $return;
    }

    function onGetCalenderData() {
        return (new Emt_Vacationplan)->onGetCalenderData();
    }
}