<?php namespace Jannesnagelschmidt\Mitarbeiter\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Input;
use JannesNagelschmidt\Mitarbeiter\Models\Department;
use JannesNagelschmidt\Mitarbeiter\Models\Inventory;
use Mail;
use JannesNagelschmidt\Mitarbeiter\Models\Arbeitszeit;
use JannesNagelschmidt\Mitarbeiter\Models\ChangeRequests;
use JannesNagelschmidt\Mitarbeiter\Models\Mitarbeiter;
use October\Rain\Support\Facades\Flash;


class Emt_Profile extends ComponentBase
{

    public function componentDetails() {
        return [
            'name' => 'Emt-Profil',
            'description' => 'Profil für Mitarbeiter Frontend'
        ];
    }

    public $user;
    public $employ;
    public $department;
    public $inventory;

    public function onRun() {
        $loggedin = Emt_Time::checkUser();
        if($loggedin && isset($_SESSION['pw'])) {
            $pw = $_SESSION['pw'];
            $user = $_SESSION['user'];
            $bla = Singleemployee::getUserAndTimestampsByLdapCredentials($pw, $user, true);
            $this->employ = $bla;
            $this->department = Department::where('id', $bla[0]['department_relation_id_id'])->first();
            $this->user = $loggedin;
            $inventory = $this->getInventory($bla[0]['id']);
            $this->inventory = $inventory;
        }
        else {

        }
    }

    public function getInventory($user_id) {
        $inventory2user = DB::table('jannesnagelschmidt_mitarbeiter_inventory2employee')->where('user_id', $user_id)->get();
        $returnArray = array();
        foreach($inventory2user as $item) {
            $entry = Inventory::where('id', $item->inventory_id)->first();
            array_push($returnArray, $entry);
        }
        return $returnArray;
    }

    public static function getLeaderByUserId($user_id) {
        $user = Mitarbeiter::where('id', $user_id)->first();
        $department = Department::where('id', $user->department_relation_id_id)->first();
        if($department) {
            return Mitarbeiter::where('id', $department->leader_id_id)->first();
        }
        else {
            return false;
        }
    }

    public static function sendMailNotification($user_id, $time, $link, $text, $name = null) {
        $user = Mitarbeiter::where('id', $user_id)->first();
        if($user->email !== NULL) {
            if($user->email_notification && $user->email) {
                if($name) {

                }
                else {
                    $name = $user->first_name.' '.$user->last_name;
                }
                $params = array(
                    "text" => $name.' '.$text,
                    "time" => $time,
                    "link" => $link
                );
                Mail::sendTo($user->email, 'jannesnagelschmidt.mitarbeiter::mail.notification', $params);
            }
        }
    }

    public function onChangeBirthdayVisible() {
        $value = post('birthday');
        $user_id = post('id');
        if($value !== 'false') {
            $realVal = 1;
        }
        else {
            $realVal = 0;
        }
        $user = Mitarbeiter::where('id', $user_id)->first();
        $user->birthday_is_public = $realVal;
        $user->save();
    }

    public function onChangeMailNotification() {
        $value = post('mail');
        $user_id = post('id');
        if($value !== 'false') {
            $realVal = 1;
        }
        else {
            $realVal = 0;
        }
        $user = Mitarbeiter::where('id', $user_id)->first();
        $user->email_notification = $realVal;
        $user->save();
        //return Redirect::back();
    }


}