<?php namespace Jannesnagelschmidt\Mitarbeiter\Components;

use Cms\Classes\ComponentBase;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Support\Facades\Redirect;
use Input;
use JannesNagelschmidt\Mitarbeiter\Models\Arbeitszeit;
use JannesNagelschmidt\Mitarbeiter\Models\Calculated;
use JannesNagelschmidt\Mitarbeiter\Models\ChangeRequests;
use JannesNagelschmidt\Mitarbeiter\Models\Department;
use JannesNagelschmidt\Mitarbeiter\Models\Mitarbeiter;
use JannesNagelschmidt\Mitarbeiter\Models\Sick;
use JannesNagelschmidt\Mitarbeiter\Models\Urlaub;
use October\Rain\Support\Facades\Flash;


class Emt_Punctually extends ComponentBase
{
    public $loggedIn;
    public $employee;
    public $employees;
    public $department;
    public $weekdays = [
        'Monday' => 'Montag',
        'Tuesday' => 'Dienstag',
        'Wednesday' => 'Mittwoch',
        'Thursday' => 'Donnerstag',
        'Friday' => 'Freitag',
        'Saturday' => 'Samstag',
        'Sunday' => 'Sonntag'
    ];

    public function componentDetails() {
        return [
            'name' => 'Emt_Pünktlichkeit',
            'description' => 'Zeigt die Mitarbeiter der Abteilung an und wann sie zu spät gekommen sind'
        ];
    }

    public function onRun() {
        $emt = Emt_Time::checkUser();
        $thisYear = date('Y');
        $employees = array();
        if($emt && isset($_SESSION['pw'])) {
            $pw = $_SESSION['pw'];
            $user = $_SESSION['user'];
            $bla = Singleemployee::getUserAndTimestampsByLdapCredentials($pw, $user, true);
            if((new Emt)->isLeader($bla[0]['id'])) {
                $department = Department::where('leader_id_id', $bla[0]['id'])->get();
                $this->department = $department[0];
                foreach($department as $x => $dep_item) {
                    $mitarbeiter = Mitarbeiter::where([
                        ['department_relation_id_id', $dep_item->id],
                        ['status', 'aktiv']
                    ])->get();
                    foreach($mitarbeiter as $item) {
                        $times = Arbeitszeit::where('user_id', $item->id)->get();
                        $tooLate = $this->getToolate($item, $times);
                        $tempArr['user']['first_name'] = $item->first_name;
                        $tempArr['user']['last_name'] = $item->last_name;
                        $tempArr['time'] = $tooLate;
                        if($tooLate) {
                            $sorted = $this->putInYears($tooLate);
                            rsort($sorted);
                            $tempArr['sorted'] = $sorted;
                            $tempArr['count'] = count($tooLate);
                            array_push($employees, $tempArr);
                        }
                        //return var_dump($tempArr['sorted'][1]);

                    }
                }
            }
            usort($employees, function ($a, $b) {
                return strcmp($a['user']['last_name'], $b['user']['last_name']);
            });

            $this->employees = $employees;
            $this->employee = $bla;
            $this->loggedIn = $emt;
        }
    }

    public function onChangeTolerance() {
        $tolerance = post('tolerance');
        $id = post('department_relation_id');
        $dep = Department::where('id', $id)->first();
        $dep->punctuallity = $tolerance;
        $dep->save();
        return Redirect::back();
    }

    public function putInYears($time) {
        $start = 2022;
        $end = intval(date('Y'));
        $return = array();
        for($i = $start; $i <= $end; $i++) {
            $tempArr = array();
            array_push($tempArr, $i);
            foreach($time as $x => $item) {
                $year = date('Y', strtotime($item->timestamp));
                if($i == $year) {
                    array_push($tempArr, $item->timestamp);
                }
            }
            if($tempArr) {
                 //$return[$i] = $tempArr;
                array_push($return, $tempArr);
            }
        }
        return $return;
    }



    public function getToolate($user, $times) {
        $start_time = $user->start_time;
        $department = Department::where('id', $user->department_relation_id_id)->first();
        $punctuallity = $department->punctuallity;
        /*if($start_time) {*/
            $ret = array();
            foreach($times as $x => $item) {
                $start_time = Emt::getStartTimeByDate($user->id, $item->lookup);
                $str_start = strtotime($start_time);
                $lol = strtotime('00:00:00');
                $str_start = $str_start - $lol;
                $blup = strtotime('+'.$punctuallity.' minutes', $str_start);
                $bla = strtotime('-'.$punctuallity.' minutes', $str_start);
                if($item->status == 'coming') {
                    $value = strtotime(date('H:i', strtotime($item->timestamp)));
                    $value = $value - $lol;
                    if($value > $blup) {
                        array_push($ret, $item);
                    }
                    else if($value < $bla) {
                        array_push($ret, $item);
                    }
                }
            }
            rsort($ret);
            return $ret;
        /*}
        else {
            return false;
        }*/
    }


}