<?php namespace Jannesnagelschmidt\Mitarbeiter\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Redirect;
use Input;
use JannesNagelschmidt\Mitarbeiter\Models\Urlaub;
use Mail;
use JannesNagelschmidt\Mitarbeiter\Models\Arbeitszeit;
use JannesNagelschmidt\Mitarbeiter\Models\ChangeRequests;
use JannesNagelschmidt\Mitarbeiter\Models\Mitarbeiter;
use October\Rain\Support\Facades\Flash;


class Emt_Request extends ComponentBase
{

    public $user;
    public $employ;
    public $requests;
    public $vacationRequests;
    public $state;
    public $type;
    public $action;

    public function componentDetails() {
        return [
            'name' => 'Emt-Anträge',
            'description' => 'Anträge für Mitarbeiter Frontend'
        ];
    }

    public function onRun() {
        $types = [
            'add'=>'Hinzufügen',
            'delete'=>'Löschen',
            'correct'=>'Korrigieren'
        ];
        $states = [
            'pending' => 'Warten auf Bearbeitung',
            'declined' => 'Abgelehnt',
            'accepted' => 'Bestätigt'
        ];
        $timestamp_status = [
            'coming' => 'Kommt',
            'leaving' => 'Geht',
            'break_start' => 'Pause Start',
            'break_end' => 'Pause Ende'
        ];
        $this->type = $types;
        $this->state = $states;
        $this->action = $timestamp_status;
        $loggedin = self::checkUser();
        if($loggedin && isset($_SESSION['pw'])) {
            $pw = $_SESSION['pw'];
            $user = $_SESSION['user'];
            $bla = Singleemployee::getUserAndTimestampsByLdapCredentials($pw, $user, true);
            $this->employ = $bla;
            $entries = ChangeRequests::where('user_id', $bla[0]->id)->get();
            $vacationEntries = Urlaub::where('mitarbeiter_id', $bla[0]->id)->get();
            $newVac = json_decode(json_encode($vacationEntries), true);
            $new = json_decode(json_encode($entries),true);
            rsort($new);
            rsort($newVac);
            $this->vacationRequests = $newVac;
            $this->user = $loggedin;
            $this->requests = $new;
        }
        else {

        }
        //return $loggedin;
    }

    public static function checkUser() {
        if(isset($_SESSION["user"]) && isset($_SESSION['pw'])) {
            if(time()-$_SESSION['login_time_stamp'] > 36000) {
                session_unset();
                session_destroy();
                return "NEIN";
            }
            else {
                $pw = $_SESSION['pw'];
                $user = $_SESSION['user'];
                return true;
            }
        }
        else {
            return "NEIN";
        }
    }

    public function onAddMemoVacation() {
        $id = post('id');
        $memo = post('memo');
        $name = post('name');
        $oldMemo = '';
        $entry = Urlaub::where('id', $id)->first();
        $oldMemo = $entry->name;
        if($memo) {
            $entry->name=$oldMemo."\n".'['.$name.']:'.$memo;
            $entry->save();
        }

    }

    public function onDeleteVacationRequest() {
        $id = post('id');
        $entry = Urlaub::where('id', $id)->first();
        $entry->delete();
        return Redirect::back();
    }

    public function onDeleteRequest() {
        $id = post('id');
        $entry = ChangeRequests::where('id', $id)->first();
        $user_id = $entry->user_id;
        $employee = Mitarbeiter::where('id', $user_id)->first();
        $leader = Emt_Profile::getLeaderByUserId($user_id);
        $entry->delete();
        if($entry->new_time) {
            $timestamp = $entry->new_time;
        }
        else {
            $timestamp = $entry->og_time;
        }
        Emt_Profile::sendMailNotification($leader->id, $timestamp, '/antrag_employees', ", ".$employee->first_name." ".$employee->last_name."hat einen Antrag gelöscht");
        Emt_Time::createNotification($user_id, 'Antrag gelöscht ', 'neu', '/antrag');
        return Redirect::back();
    }

    public function onLogoutWindows() {
        (new Singleemployee)->endSessionEmployee();
        return Redirect::back();
    }

    public function onLoginWindows() {
        $data = Input::all();

        $test = Singleemployee::getUserAndTimestampsByLdapCredentials($data['password'], $data['user']);
        if($test == 'NEIN!') {
            header('HTTP/1.1 500 Internal Server Booboo');
            header('Content-Type: application/json; charset=UTF-8');
            die(json_encode(array('message' => 'Passwort oder Nutzername falsch!', 'code' => 555)));
        }
        $this->user = $test;
        //return $test;
        return Redirect::back();
    }


}