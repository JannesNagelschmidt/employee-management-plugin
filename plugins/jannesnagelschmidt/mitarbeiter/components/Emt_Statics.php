<?php namespace Jannesnagelschmidt\Mitarbeiter\Components;

use Cms\Classes\ComponentBase;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Support\Facades\Redirect;
use Input;
use JannesNagelschmidt\Mitarbeiter\Models\Arbeitszeit;
use JannesNagelschmidt\Mitarbeiter\Models\Calculated;
use JannesNagelschmidt\Mitarbeiter\Models\ChangeRequests;
use JannesNagelschmidt\Mitarbeiter\Models\Department;
use JannesNagelschmidt\Mitarbeiter\Models\Mitarbeiter;
use JannesNagelschmidt\Mitarbeiter\Models\Sick;
use JannesNagelschmidt\Mitarbeiter\Models\Urlaub;
use October\Rain\Support\Facades\Flash;


class Emt_Statics extends ComponentBase
{
    public $loggedIn;
    public $employee;
    public $urlaub;
    public $urlaubsTage;
    public $sick;
    public $sickDays;
    public $vacOver;
    public $tooLate;
    public $lastYears;
    public $weekdays = [
        'Monday' => 'Montag',
        'Tuesday' => 'Dienstag',
        'Wednesday' => 'Mittwoch',
        'Thursday' => 'Donnerstag',
        'Friday' => 'Freitag',
        'Saturday' => 'Samstag',
        'Sunday' => 'Sonntag'
    ];

    public function componentDetails() {
        return [
            'name' => 'Emt_Statistiken',
            'description' => 'Statistiken für Mitarbeiter Frontend'
        ];
    }

    public function onRun() {
        $emt = Emt_Time::checkUser();
        $thisYear = date('Y');
        $startYear = date('Y', strtotime('01-01-2022'));
        $urlaubse = array();
        $krankheitse = array();
        $intervalYears = (intval($thisYear) - intval($startYear)) +1;
        if($emt && isset($_SESSION['pw'])) {
            $pw = $_SESSION['pw'];
            $user = $_SESSION['user'];
            $bla = Singleemployee::getUserAndTimestampsByLdapCredentials($pw, $user, true);
            $einundzwanzig = $bla[0]->rest_urlaub_21;
            $ubertrag = 0;
            if(is_null($einundzwanzig)) {
                $einundzwanzig = 0;
            }
            for($i = 1; $i <= $intervalYears; $i++) {
                $urlaub = Urlaub::where([
                    ['mitarbeiter_id', $bla[0]->id],
                    ['start_date', 'LIKE', $thisYear.'%'],
                    ['status','Genehmigt']
                ])->get();
                $tempArr = array();
                $tempArr['urlaub'] = $urlaub;
                $tage = $this->kackUrlaub($urlaub);
                $einundzwanzigZauberei = 0;
                // rest_urlaub_21 = 3
                // tage = 16
                // rest = 9
                // 16 - 3
                // 25
                if($tage <= $einundzwanzig) {
                    $einundzwanzig = $einundzwanzig - $tage. ' von insgesamt '.$bla[0]->rest_urlaub_21;
                    $tage = 0;
                }
                else {
                    $tage = $tage - $einundzwanzig;
                    $einundzwanzig = 0;
                    $einundzwanzig = $einundzwanzig. ' von insgesamt '.$bla[0]->rest_urlaub_21;
                }

                //$einundzwanzig = 0;

                /*else {
                    $tempArr['tage'] = $tage;
                }*/
                $tempArr['tage'] = $tage;
                $tempArr['jahr'] = $thisYear;
                $tempArr['einundzwanzig'] = $einundzwanzig;
                $rest = ($bla[0]->vac_days) - $tage;
                $tempArr['über'] = $rest;

                if($i > 1) {
                    $ubertrag += $rest;
                }
                array_push($urlaubse, $tempArr);
                $thisYear = $thisYear-1;
            }
            $this->lastYears = $ubertrag;
            $thisYear = date('Y');
            for($i = 1; $i <= $intervalYears; $i++) {
                $sick = Sick::where([
                    ['mitarbeiter_id', $bla[0]->id],
                    ['start_day', 'LIKE', $thisYear.'%'],
                ])->get();
                $tempArr = array();
                $tempArr['tage'] = $this->kackUrlaub($sick);
                $tempArr['jahr'] = $thisYear;
                array_push($krankheitse, $tempArr);
                $thisYear = $thisYear-1;
            }
            $tooLate = $this->getToolate($bla);
            $this->tooLate = $tooLate;
            $sick = Sick::where('mitarbeiter_id', $bla[0]->id)->get();
            $this->sick = $sick;
            $this->sickDays = $krankheitse;
            $this->employee = $bla;
            $this->loggedIn = $emt;
            $this->urlaub = $urlaubse;
        }
    }


    public function kackUrlaub($entries) {
        $return = 0;
        foreach($entries as $x => $item) {
            if(isset($item->start_date)) {
                $start = new DateTime($item->start_date);
                $end = new DateTime($item->end_date);
                $strStart = strtotime($item->start_date);
                $strEnd = strtotime($item->end_date);
            }
            elseif(($item->start_day)) {
                $start = new DateTime($item->start_day);
                $end = new DateTime($item->end_day);
                $strStart = strtotime($item->start_day);
                $strEnd = strtotime($item->end_day);
            }
            else {
                return false;
            }
            $end->modify('+1 day');
            $interval = $end->diff($start);
            $days = $interval->days;
            $period = new DatePeriod($start, new DateInterval('P1D'), $end);

            foreach($period as $dt) {
                $startYear = date('Y', strtotime($dt->format('Y-m-d')));
                $holidays = (new Timetrackfrontend)->getHolidays($startYear);
                $weihnachten['date'] = $startYear.'-12-24';
                $weihnachten['global'] = true;
                $weihnachten['localName'] = 'Heiligabend';
                array_push($holidays, $weihnachten);
                $silvester['date'] = $startYear.'-12-31';
                $silvester['global'] = true;
                $silvester['localName'] = 'Silvester';
                array_push($holidays, $silvester);
                $curr = $dt->format('D');
                if ($curr == 'Sat' || $curr == 'Sun' || ((new \Jannesnagelschmidt\Mitarbeiter\Components\Timetrackfrontend)->checkPublicHolidays($dt->format('Y-m-d'), $holidays))) {
                    $days--;
                }
            }
            $return += $days;
        }
        return $return;
    }

    public function getToolate($user) {
        $start_time = $user[0]->start_time;
        $times = $user[1];
        $str_start = strtotime($start_time);
        $blup = strtotime('+10 minutes', $str_start);
        $ret = array();
        foreach($times as $x => $item) {
            if($item->status == 'coming') {
                $value = strtotime(date('H:i', strtotime($item->timestamp)));
                if($value >= $blup) {
                    array_push($ret, $item);
                }
            }
        }
        rsort($ret);
        return $ret;
    }


}