<?php namespace Jannesnagelschmidt\Mitarbeiter\Components;

use Cms\Classes\ComponentBase;
use Mail;
use Illuminate\Support\Facades\Redirect;
use Input;
use JannesNagelschmidt\Mitarbeiter\Models\Arbeitszeit;
use JannesNagelschmidt\Mitarbeiter\Models\Calculated;
use JannesNagelschmidt\Mitarbeiter\Models\ChangeRequests;
use JannesNagelschmidt\Mitarbeiter\Models\Department;
use JannesNagelschmidt\Mitarbeiter\Models\Mitarbeiter;
use JannesNagelschmidt\Mitarbeiter\Models\Notifications;
use JannesNagelschmidt\Mitarbeiter\Models\Urlaub;
use October\Rain\Support\Facades\Flash;


class Emt_Time extends ComponentBase
{

    public $user;
    public $employ;
    public $colleagues;

    public function componentDetails() {
        return [
            'name' => 'Emt_Time',
            'description' => 'Zeiten für Mitarbeiter Frontend'
        ];
    }

    public function onRun() {
        $loggedin = self::checkUser();
        if($loggedin && isset($_SESSION['pw'])) {
            $pw = $_SESSION['pw'];
            $user = $_SESSION['user'];
            $bla = Singleemployee::getUserAndTimestampsByLdapCredentials($pw, $user, true);
            $departmentPeople = Mitarbeiter::where([
                ['id', '!=', $bla[0]['id']],
                ['status', '!=', 'Passiv']
                ])->get();
            $this->colleagues = $departmentPeople;
            $this->employ = $bla;
            $this->user = $loggedin;
        }
        else {

        }
        //return $loggedin;
    }

    public static function createNotification($user_id, $text, $type, $link, $req_id = null) {
        $user = Mitarbeiter::where('id', $user_id)->first();
        $offset = 1*60*60;
        if($user) {
            $dep = $user->department_relation_id_id;
            $department = Department::where('id', $dep)->first();
            $lead_id = $department->lead_id_id;
            if($lead_id != $user_id) {
                $entry = new Notifications();
                $entry->employ_id_id_id = $lead_id;
                $entry->leader_id = $lead_id;
                $entry->text = $text;
                $entry->type = $type;
                $entry->link = $link;
                $entry->request_id = $req_id;
                $entry->created_at = date('Y-m-d H:i:s',time() + $offset);
                $entry->save();
            }
            $entry = new Notifications();
            $entry->employ_id_id_id = $user_id;
            $entry->leader_id = $lead_id;
            $entry->text = $text;
            $entry->type = $type;
            $entry->link = $link;
            $entry->request_id = $req_id;
            $entry->created_at = date('Y-m-d H:i:s',time() + $offset);
            $entry->save();
        }

    }


    public function onVacationRequest() {
        $user_id = post('user_id');
        $substitute = post('substitute');
        $subUser = Mitarbeiter::where('id', $substitute)->first();
        $start = strtotime(post('date_from').' 00:00:00');
        $end = strtotime(post('date_to').' 00:00:00');
        $note = post('name');
        $user = Mitarbeiter::where('id', $user_id)->first();
        $department = Department::where('id', $user->department_relation_id_id)->first();
        $leader = Mitarbeiter::where('id', $department->leader_id_id)->first();

        $entry = new Urlaub();
        $entry->start_date = date('Y-m-d H:i:s', $start);
        $entry->end_date = date('Y-m-d H:i:s', $end);
        $entry->mitarbeiter_id = $user_id;
        $entry->status = "Beantragt";
        $entry->approved = $leader->first_name.' '.$leader->last_name;
        $entry->leader_id = $department->leader_id_id;
        $entry->name = $note;
        $entry->substitute = $substitute;
        $entry->save();
        $leader = Emt_Profile::getLeaderByUserId($user_id);
        self::createNotification($user_id, 'Urlaubsantrag erstellt', 'neu', '/antrag');
        self::createNotification($substitute, $user->first_name.' '.$user->last_name.' hat dich als Vertretung angegeben', 'neu', '/antrag');
        Emt_Profile::sendMailNotification($department->leader_id_id, $start, "/antrag_employees", ', Urlaubsantrag von ' . $user->first_name .' ' . $user->last_name);
        $params = array(
            "text" => $user->first_name. ' ' .$user->last_name." hat dich als Urlaubsvertretung angegeben.",
            "start" => $start,
            "end" => $end
        );
        Mail::sendTo($subUser->email, 'jannesnagelschmidt.mitarbeiter::mail.vacation_sub', $params);

        return "yo";
    }

    public function onChangeRequest() {
        if(post()) {
            $offset = 60*60;
            $state = post('state');
            $user_id = post('user_id');
            $user = Mitarbeiter::where('id', $user_id)->first();
            $name = $user->first_name .' '.$user->last_name;
            $dep = post('department');
            $department = Department::where('id', '=', $dep)->first();
            $leader_id = $department->leader_id_id;
            $memo = post('memo');
            $entry = new ChangeRequests();
            $entry->status = 'pending';
            $entry->type = $state;
            if($memo) {
                $entry->memo = '['.$user->first_name.']:'.$memo;
            }
            $entry->user_id = $user_id;
            $entry->leader_id = $leader_id;
            if(post('time_id')) {
                $entry->timestamp_id = post('time_id');
            }
            switch ($state) {
                case 'correct':
                    $newTime = post('time');
                    $time_id = post('time_id');
                    $og_time = Arbeitszeit::where('id', $time_id)->first();
                    $entry->og_time = $og_time->timestamp;
                    $newTime = gmdate('Y-m-d H:i:s', strtotime($newTime)+$offset);
                    $timestampStatus = post('timestamp_status');
                    $entry->new_time = $newTime;
                    $entry->timestamp_status = $timestampStatus;
                    if(!$newTime || !$timestampStatus) {
                        header('HTTP/1.1 500 Internal Server Booboo');
                        header('Content-Type: application/json; charset=UTF-8');
                        die(json_encode(array('message' => 'Erforderliche Felder fehlen!', 'code' => 555)));
                    }
                    $entry->save();
                    $thing = ChangeRequests::where([
                        ['new_time', $newTime],
                        ['user_id', $user_id]
                    ])->first();
                    $leader = Emt_Profile::getLeaderByUserId($user_id);
                    self::createNotification($user_id, 'Korrektur Antrag erstellt', 'neu', '/antrag', $thing->id);
                    Emt_Profile::sendMailNotification($leader->id, $newTime, "/antrag_employees", ', Korrekturantrag von ' . $user->first_name .' ' . $user->last_name);
                    break;
                case 'add':
                    $newTime = post('time');
                    $newTime = gmdate('Y-m-d H:i:s', strtotime($newTime)+$offset);
                    $timestampStatus = post('timestamp_status');
                    $entry->new_time = $newTime;
                    $entry->timestamp_status = $timestampStatus;
                    if(!$newTime || !$timestampStatus) {
                        header('HTTP/1.1 500 Internal Server Booboo');
                        header('Content-Type: application/json; charset=UTF-8');
                        die(json_encode(array('message' => 'Erforderliche Felder fehlen!', 'code' => 555)));
                    }
                    $entry->save();
                    $thing = ChangeRequests::where([
                        ['new_time', $newTime],
                        ['user_id', $user_id]
                    ])->first();
                    self::createNotification($user_id, 'Neuer Antrag erstellt', 'neu', '/antrag', $thing->id);
                    $leader = Emt_Profile::getLeaderByUserId($user_id);
                    Emt_Profile::sendMailNotification($leader->id, $newTime, "/antrag_employees", ', Hinzufügen Antrag von ' . $user->first_name .' ' . $user->last_name);
                    break;
                case 'delete':
                    $time_id = post('time_id');
                    $og_time = Arbeitszeit::where('id', $time_id)->first();
                    $entry->og_time = $og_time->timestamp;
                    $entry->timestamp_status = $og_time->status;
                    $entry->save();
                    self::createNotification($user_id, 'Lösch Antrag erstellt', 'neu', '/antrag');
                    $leader = Emt_Profile::getLeaderByUserId($user_id);
                    Emt_Profile::sendMailNotification($leader->id, $og_time->timestamp, "/antrag_employees", ', Lösch Antrag von ' . $user->first_name .' ' . $user->last_name);
                    break;
                default:
                    return 'error';
            }
            return "Erfolgreich ausgeführt!";
        }
        else {
            return 'NEIN';
        }
    }


    public function onGetWorkForDate() {
        $time = post('time');
        $user_id = post('user_id');
        $phpTime = strtotime($time);
        $date = date('Y-m-d', $phpTime);
        $times = Arbeitszeit::where([
            ['user_id', $user_id],
            ['lookup', $date]
        ])->get();
        //array_multisort($times, SORT_ASC);
        if(count($times) >= 1) {
            $new = json_decode(json_encode($times),true);
            foreach ($new as $key => $node) {
                $timestamps[$key]    = $node['timestamp'];
            }
            array_multisort($timestamps, SORT_ASC, $new);
            return $new;
        }
        else if(!$this->isWeekend($date)){
            $vac = (new Timetrackfrontend)->vacationHandling($user_id, $date);
            $sick = (new Timetrackfrontend)->sickHandling($user_id, $date);
            if($vac) {
                $vacationArray[0]['timestamp'] = '';
                $vacationArray[0]['status'] = 'URLAUB';
                return $vacationArray;
            }
            elseif ($sick) {
                $vacationArray[0]['timestamp'] = '';
                $vacationArray[0]['status'] = 'KRANK';
                return $vacationArray;
            }
        }
    }


    function isWeekend($date) {
        return (date('N', strtotime($date)) >= 6);
    }

    public function onGetCalculatedData() {
        $time = post('time');
        $user_id = post('user_id');
        $phpTime = strtotime($time);
        $date = date('Y-m-d', $phpTime);
        $entry = Calculated::where([
            ['lu_date', $date],
            ['user_id', $user_id]
        ])->first();
        if($entry) {
            return $entry;
        }
        else {
            $stringForArray = 'Noch nicht ermittelt';
            if(!$this->isWeekend($date)){
                $vac = (new Timetrackfrontend)->vacationHandling($user_id, $date);
                $sick = (new Timetrackfrontend)->sickHandling($user_id, $date);
                if($vac) {
                    $stringForArray = 'URLAUB';
                }
                elseif ($sick) {
                    $stringForArray = 'KRANK';
                }
            }
            $bla['worked'] = $stringForArray;
            $bla['break'] = '';
            return $bla;
        }

    }

    public static function checkUser() {
        if(isset($_SESSION["user"]) && isset($_SESSION['pw'])) {
            if(time()-$_SESSION['login_time_stamp'] > 36000) {
                session_unset();
                session_destroy();
                return "NEIN";
            }
            else {
                $pw = $_SESSION['pw'];
                $user = $_SESSION['user'];
                return true;
            }
        }
        else {
            return "NEIN";
        }
    }

    public function onNextDayTime() {
        return post();
    }

    public function onLogoutWindows() {
        (new Singleemployee)->endSessionEmployee();
        return Redirect::back();
    }

    public function onLoginWindows() {
        $data = Input::all();

        $test = Singleemployee::getUserAndTimestampsByLdapCredentials($data['password'], $data['user']);
        if($test == 'NEIN!') {
            header('HTTP/1.1 500 Internal Server Booboo');
            header('Content-Type: application/json; charset=UTF-8');
            die(json_encode(array('message' => 'Passwort oder Nutzername falsch!', 'code' => 555)));
        }
        $this->user = $test;
        //return $test;
        return Redirect::back();
    }


}