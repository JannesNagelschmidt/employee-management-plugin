<?php namespace Jannesnagelschmidt\Mitarbeiter\Components;

use Cms\Classes\ComponentBase;
use Mail;
use Illuminate\Support\Facades\Redirect;
use Input;
use JannesNagelschmidt\Mitarbeiter\Models\Arbeitszeit;
use JannesNagelschmidt\Mitarbeiter\Models\Calculated;
use JannesNagelschmidt\Mitarbeiter\Models\ChangeRequests;
use JannesNagelschmidt\Mitarbeiter\Models\Department;
use JannesNagelschmidt\Mitarbeiter\Models\Mitarbeiter;
use JannesNagelschmidt\Mitarbeiter\Models\Notifications;
use JannesNagelschmidt\Mitarbeiter\Models\Urlaub;
use October\Rain\Support\Facades\Flash;


class Emt_Vacation extends ComponentBase
{

    public $user;
    public $employ;
    public $colleagues;
    public $department;

    public function componentDetails() {
        return [
            'name' => 'Emt_Vacation',
            'description' => 'Urlaubsplan für Mitarbeiter Frontend'
        ];
    }

    public function onRun() {
        $loggedin = self::checkUser();
        if($loggedin && isset($_SESSION['pw'])) {
            $pw = $_SESSION['pw'];
            $user = $_SESSION['user'];
            $bla = Singleemployee::getUserAndTimestampsByLdapCredentials($pw, $user, true);
            $this->employ = $bla;
            $this->user = $loggedin;
            $dep = $bla[0]['department_relation_id_id'];
            $department = Department::where('id', $dep)->first();
            $departmentPeople = Mitarbeiter::where([
                ['id', '!=', $bla[0]['id']],
                ['status', '!=', 'Passiv']
            ])->get();
            $people_sorted = json_decode(json_encode($departmentPeople), true);
            usort($people_sorted, function ($a, $b) {
                return strcmp($a['last_name'], $b['last_name']);
            });
            $this->colleagues = $people_sorted;
            $this->department = $department;
        }
        //return $loggedin;
    }


    public function onVacationRequest() {
        $user_id = post('user_id');
        $substitute = post('substitute');
        if($substitute) {
            $subUser = Mitarbeiter::where('id', $substitute)->first();
        }
        $start = strtotime(post('date_from').' 00:00:00');
        $end = strtotime(post('date_to').' 00:00:00');
        $note = post('name');
        $user = Mitarbeiter::where('id', $user_id)->first();
        $department = Department::where('id', $user->department_relation_id_id)->first();
        $leader = Mitarbeiter::where('id', $department->leader_id_id)->first();

        $entry = new Urlaub();
        $entry->start_date = date('Y-m-d H:i:s', $start);
        $entry->end_date = date('Y-m-d H:i:s', $end);
        $entry->mitarbeiter_id = $user_id;
        $entry->status = "Beantragt";
        $entry->approved = $leader->first_name.' '.$leader->last_name;
        $entry->leader_id = $department->leader_id_id;
        $entry->name = $note;
        $entry->substitute = $substitute;
        $entry->save();
        $leader = Emt_Profile::getLeaderByUserId($user_id);
        Emt_Time::createNotification($user_id, 'Urlaubsantrag erstellt', 'neu', '/antrag');
        Emt_Time::createNotification($substitute, $user->first_name.' '.$user->last_name.' hat dich als Vertretung angegeben', 'neu', '/antrag');
        Emt_Profile::sendMailNotification($department->leader_id_id, $start, "/antrag_employees", ', Urlaubsantrag von ' . $user->first_name .' ' . $user->last_name);
        $params = array(
            "text" => $user->first_name. ' ' .$user->last_name." hat dich als Urlaubsvertretung angegeben.",
            "start" => $start,
            "end" => $end
        );
        if($substitute) {
            $blibla = $subUser->email;
            if(isset($blibla)) {
                Mail::sendTo($subUser->email, 'jannesnagelschmidt.mitarbeiter::mail.vacation_sub', $params);
            }
        }

        return "yo";
    }


    public function onGetCalenderData() {
        return (new Emt_Vacationplan)->onGetCalenderData();
    }

    public static function checkUser() {
        if(isset($_SESSION["user"]) && isset($_SESSION['pw'])) {
            if(time()-$_SESSION['login_time_stamp'] > 36000) {
                session_unset();
                session_destroy();
                return "NEIN";
            }
            else {
                $pw = $_SESSION['pw'];
                $user = $_SESSION['user'];
                return true;
            }
        }
        else {
            return "NEIN";
        }
    }

    public function onNextDayTime() {
        return post();
    }

    public function onLogoutWindows() {
        (new Singleemployee)->endSessionEmployee();
        return Redirect::back();
    }

    public function onLoginWindows() {
        $data = Input::all();

        $test = Singleemployee::getUserAndTimestampsByLdapCredentials($data['password'], $data['user']);
        if($test == 'NEIN!') {
            header('HTTP/1.1 500 Internal Server Booboo');
            header('Content-Type: application/json; charset=UTF-8');
            die(json_encode(array('message' => 'Passwort oder Nutzername falsch!', 'code' => 555)));
        }
        $this->user = $test;
        //return $test;
        return Redirect::back();
    }


}