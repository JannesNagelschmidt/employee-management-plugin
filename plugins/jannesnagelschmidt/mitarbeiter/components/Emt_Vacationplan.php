<?php namespace Jannesnagelschmidt\Mitarbeiter\Components;

use Cms\Classes\ComponentBase;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Support\Facades\Redirect;
use Input;
use JannesNagelschmidt\Mitarbeiter\Models\Arbeitszeit;
use JannesNagelschmidt\Mitarbeiter\Models\Bereich;
use JannesNagelschmidt\Mitarbeiter\Models\Calculated;
use JannesNagelschmidt\Mitarbeiter\Models\ChangeRequests;
use JannesNagelschmidt\Mitarbeiter\Models\Department;
use JannesNagelschmidt\Mitarbeiter\Models\Mitarbeiter;
use JannesNagelschmidt\Mitarbeiter\Models\Sick;
use JannesNagelschmidt\Mitarbeiter\Models\Urlaub;
use October\Rain\Support\Facades\Flash;


class Emt_Vacationplan extends ComponentBase
{
    public $loggedIn;
    public $employee;
    public $employees;
    public $department;
    public $weekdays = [
        'Monday' => 'Montag',
        'Tuesday' => 'Dienstag',
        'Wednesday' => 'Mittwoch',
        'Thursday' => 'Donnerstag',
        'Friday' => 'Freitag',
        'Saturday' => 'Samstag',
        'Sunday' => 'Sonntag'
    ];

    public function componentDetails() {
        return [
            'name' => 'Urlaubsplanung',
            'description' => 'Zeigt die Urlaubstage der Abteilung an'
        ];
    }

    public function onRun() {
        $emt = Emt_Time::checkUser();
        $thisYear = date('Y');
        $employees = array();
        if($emt && isset($_SESSION['pw'])) {
            $pw = $_SESSION['pw'];
            $user = $_SESSION['user'];
            $bla = Singleemployee::getUserAndTimestampsByLdapCredentials($pw, $user, true);
            if((new Emt)->leaderFunc($bla[0]['id'])) {
                $dep = $bla[0]['department_relation_id_id'];
                $department = Department::where('id', $dep)->first();
                $dep_name = $department->name;
                $this->department = $department;
            }
            $this->employee = $bla;
            $this->loggedIn = $emt;
        }
    }

    public function onGetCalenderData() {
        $data =  post();
        $user_id = $data['user_id'];
        $startYear = date('Y');
        $holidays = (new Timetrackfrontend)->getHolidays($startYear);
        //$holidays = json_decode($holidays, true);
        $weihnachten['date'] = $startYear.'-12-24';
        $weihnachten['global'] = true;
        $weihnachten['localName'] = 'Heiligabend';
        array_push($holidays, $weihnachten);
        $silvester['date'] = $startYear.'-12-31';
        $silvester['global'] = true;
        $silvester['localName'] = 'Silvester';
        array_push($holidays, $silvester);
        $holidayArray = array();
        array_push($holidayArray, $holidays);
        $return = array();
        $employ = Mitarbeiter::where('id', $user_id)->first();
        $dep_id = $employ->department_relation_id_id;

        if((new Emt)->isLeader($user_id)) {
            $departments = Department::where('leader_id_id', $user_id)->get();
            foreach($departments as $b => $deps) {
                $employees = Mitarbeiter::where('department_relation_id_id', $deps->id)->get();
                foreach($employees as $y => $entry) {
                    $id = $entry->id;
                    if($entry->sub_department_relation_id) {
                        $subdep = Bereich::where('id', $entry->sub_department_relation_id)->first();
                    }
                    $vacas = Urlaub::where([['mitarbeiter_id', $id], ['status', 'Genehmigt']])->get();
                    $sick = Sick::where('mitarbeiter_id', $id)->get();
                    if($id == $user_id) {
                        $temp['self'] = true;
                    }
                    else {
                        $temp['self'] = false;
                    }
                    $temp['vacation'] = $vacas;
                    $temp['sick'] = $sick;
                    $temp['employee'] = $entry;
                    if($entry->sub_department_relation_id) {
                        $temp['subdep'] = $subdep;
                    }
                    else {
                        $temp['subdep'] = false;
                    }
                    array_push($return, $temp);
                }
            }
        }
        else {
            $deps = Department::where('id', $employ->department_relation_id_id)->first();
            $employees = Mitarbeiter::where('department_relation_id_id', $deps->id)->get();
            foreach($employees as $y => $entry) {
                $id = $entry->id;
                if($entry->sub_department_relation_id) {
                    $subdep = Bereich::where('id', $entry->sub_department_relation_id)->first();
                }
                $vacas = Urlaub::where([['mitarbeiter_id', $id], ['status', 'Genehmigt']])->get();
                $sick = Sick::where('mitarbeiter_id', $id)->get();
                if($id == $user_id) {
                    $temp['self'] = true;
                }
                else {
                    $temp['self'] = false;
                }
                $temp['vacation'] = $vacas;
                $temp['sick'] = $sick;
                $temp['employee'] = $entry;
                if($entry->sub_department_relation_id) {
                    $temp['subdep'] = $subdep;
                }
                else {
                    $temp['subdep'] = false;
                }
                array_push($return, $temp);
            }
        }

        $realReturn = array();
        $realReturn['entries'] = $return;
        $realReturn['holidays'] = $holidayArray;

        //$calculated = Calculated::where('user_id', $user_id)->get();


        return $realReturn;
    }

    public function putInYears($time) {
        $start = 2020;
        $end = intval(date('Y'));
        $return = array();
        for($i = $start; $i <= $end; $i++) {
            $tempArr = array();
            array_push($tempArr, $i);
            foreach($time as $x => $item) {
                $year = date('Y', strtotime($item->timestamp));
                if($i == $year) {
                    array_push($tempArr, $item->timestamp);
                }
            }
            if($tempArr) {
                 //$return[$i] = $tempArr;
                array_push($return, $tempArr);
            }
        }
        return $return;
    }



    public function getToolate($user, $times) {
        $start_time = $user->start_time;
        $department = Department::where('id', $user->department_relation_id_id)->first();
        $punctuallity = $department->punctuallity;
        if($start_time) {
            $str_start = strtotime($start_time);
            $blup = strtotime('+'.$punctuallity.' minutes', $str_start);
            $bla = strtotime('-'.$punctuallity.' minutes', $str_start);
            $ret = array();
            foreach($times as $x => $item) {
                if($item->status == 'coming') {
                    $value = strtotime(date('H:i', strtotime($item->timestamp)));
                    if($value > $blup) {
                        array_push($ret, $item);
                    }
                    else if($value < $bla) {
                        array_push($ret, $item);
                    }
                }
            }
            rsort($ret);
            return $ret;
        }
        else {
            return false;
        }
    }


}