<?php namespace Jannesnagelschmidt\Mitarbeiter\Components;

use Cms\Classes\ComponentBase;
use Input;
use Validator;
use Redirect;
use Jannesnagelschmidt\Mitarbeiter\Models\Sick;

class Export extends ComponentBase{

    public function componentDetails(){
        return [
          'name' => 'Frontend Exporter',
          'description' => 'Export Lists from frontend'
        ];
    }
    public function onRun(){
        $this->sick = $this->loadSick();

    }
    protected function loadSick(){
        return Sick::all();
    }
    public $sick;

}