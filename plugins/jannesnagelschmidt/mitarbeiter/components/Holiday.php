<?php namespace Jannesnagelschmidt\Mitarbeiter\Components;

use Cms\Classes\ComponentBase;
use Input;
use Validator;
use Redirect;
use Jannesnagelschmidt\Mitarbeiter\Models\Urlaub;

class Holiday extends ComponentBase{

    public function componentDetails(){
        return [
            'name' => 'Frontend Hilday',
            'description' => 'Shows holiday Lists from frontend'
        ];
    }
    public function onRun(){
        $this->holiday = $this->loadHoliday();

    }
    protected function loadHoliday(){
        return Urlaub::all();
    }
    public $holiday;

}