<?php namespace Jannesnagelschmidt\Mitarbeiter\Components;

use Cms\Classes\ComponentBase;
use Input;
use Validator;
use Redirect;
use Jannesnagelschmidt\Mitarbeiter\Models\Arbeitszeit;
use Jannesnagelschmidt\Mitarbeiter\Models\Mitarbeiter;

class Presence extends ComponentBase{

    public function componentDetails(){
        return [
            'name' => 'Frontend Anwesende',
            'description' => 'Shows present employees'
        ];
    }
    public function onRun(){
        $this->presence = $this->loadPresence();
        $this->users = $this->loadUsers();

    }
    protected function loadPresence(){
        return Arbeitszeit::all();
    }
    protected function loadUsers(){
        return Mitarbeiter::all();
    }
    public $presence;
    public $users;

}