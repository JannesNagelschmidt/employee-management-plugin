<?php namespace Jannesnagelschmidt\Mitarbeiter\Components;

use Cms\Classes\ComponentBase;
use Input;
use Validator;
use Redirect;
use Jannesnagelschmidt\Mitarbeiter\Models\Mitarbeiter;

class Register extends ComponentBase
{
    public function componentDetails(){
        return [
            'name' => 'Frontend Form',
            'description' => 'Register employees'
        ];
    }
    public function onSave(){
        $mitarbeiter = new Mitarbeiter();
        $mitarbeiter->first_name = Input::get('first_name');
        $mitarbeiter->last_name = Input::get('last_name');
        $mitarbeiter->birth_name = Input::get('birth_name');
        $mitarbeiter->place_of_birth = Input::get('place_of_birth');
        $mitarbeiter->date_of_birth = Input::get('date_of_birth');
        $mitarbeiter->street = Input::get('street');
        $mitarbeiter->plz = Input::get('plz');
        $mitarbeiter->city = Input::get('city');
        $mitarbeiter->email = Input::get('email');
        $mitarbeiter->mobile_phone = Input::get('mobile_phone');
        $mitarbeiter->private_phone = Input::get('private_phone');
        $mitarbeiter->kids_number = Input::get('kids_number');
        $mitarbeiter->kinderfreibetrag = Input::get('kinderfreibetrag');
        $mitarbeiter->denomination = Input::get('denomination');
        $mitarbeiter->country = Input::get('country');
        $mitarbeiter->tax_class = Input::get('tax_class');
        $mitarbeiter->medical_insurance = Input::get('medical_insurance');
        $mitarbeiter->social_insurance_number = Input::get('social_insurance_number');
        $mitarbeiter->tax_id = Input::get('tax_id');
        $mitarbeiter->bank = Input::get('bank');
        $mitarbeiter->account_owner = Input::get('account_owner');
        $mitarbeiter->iban = Input::get('iban');
        $mitarbeiter->bic = Input::get('bic');
        $mitarbeiter->second_job = Input::get('second_job');
        $mitarbeiter->second_job_company = Input::get('second_job_company');
        $mitarbeiter->second_job_address = Input::get('second_job_address');
        $mitarbeiter->minijob = Input::get('minijob');
        $mitarbeiter->adress_second_job = Input::get('adress_second_job');
        $mitarbeiter->minijob_work = Input::get('minijob_work');
        $mitarbeiter->hours_per_week_minijob = Input::get('hours_per_week_minijob');


        $mitarbeiter->save();
    }
}