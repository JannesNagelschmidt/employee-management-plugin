<?php namespace Jannesnagelschmidt\Mitarbeiter\Components;


use Cms\Classes\ComponentBase;
use Illuminate\Http\Request;
use JannesNagelschmidt\Mitarbeiter\Models\Arbeitszeit;
use Jannesnagelschmidt\Mitarbeiter\Models\Mitarbeiter;
use Symfony\Component\Ldap\Ldap;
use Throwable;

class Singleemployee extends ComponentBase
{
    /**
     * @var string
     */
    public $timestamp;
    protected $round = 5;
    public $sumForWeek = 0;

    public function componentDetails() {
        return [
            'name' => 'Singleemployee',
            'description' => 'Single view for employee'
        ];
    }

    public function onRun() {
        //$this->getRouter()->getParameters();
        //$this->employee = $this->getEmployee();
    }

    public static function getUserAndTimestampsByLdapCredentials($pw, $user, $fromLoad = false) {
        $back = array();
        if(!$fromLoad) {
            session_unset();
            //session_destroy();
            session_start();
        }
        //session_destroy();
        $name = $user;
        $password = $pw;
        $url = config('ldap.ldap_serv');
        $admindn = 'CN=LDAP User,OU=Servicebenutzer,dc=hansnatur,dc=loc';

        $adminpassword = 'Pai6Peng!';
        $dnReturn = '';

        $ldap = Ldap::create('ext_ldap', [
            'host' => $url,
            'debug' => true
        ]);
        try {
            $ldap->bind($admindn, $adminpassword);
            $query = $ldap->query('DC=hansnatur,DC=loc', '(&(objectClass=*)(sAMAccountName='.$name.'))');
            $results = $query->execute();
            $collection = $results->toArray();

            foreach ($collection as $entry) {
                $bla = $entry->getAttributes();
                $dnReturn = $bla['distinguishedName'][0];
            }
            $ldap->bind($dnReturn, $password);
            $query = $ldap->query('DC=hansnatur,DC=loc', '(&(objectClass=*)(sAMAccountName='.$name.'))');
            $results = $query->execute();
            $collection = $results->toArray();
            foreach ($collection as $entry) {
                $blup = $entry->getAttributes();
            }

            $firstName = $blup['givenName'][0];
            $lastName = $blup['sn'][0];

            $thePerson = Mitarbeiter::where([['first_name','=',$firstName],['last_name','=',$lastName]])->first();
            if(isset($thePerson)) {
                $arbeitszeiten = Arbeitszeit::where([
                    ['user_id', $thePerson->id],
                    ['status', '!=', 'vacation'],
                    ['status', '!=', 'sick'],
                    ['status', '!=', 'Korrektur%']
                ])->orderBy('timestamp')->get();

                array_push($back, $thePerson);
                array_push($back, $arbeitszeiten);
                //session_start();
                if(!$fromLoad) {
                    $_SESSION["user"] = $name;
                    $_SESSION["pw"] = $password;
                    $_SESSION["login_time_stamp"] = time();
                }
                return $back;
            }
            else {
                return "NEIN!";
            }
        }
        catch(Throwable $t) {
            return 'NEIN!';
        }
    }

    public function endSessionEmployee() {
        if(session_status() == 1){
            session_start();
            session_unset();
            session_destroy();
        }
        return session_status();
    }


    public function getEmployee(Request $request) {
        $input = $request->all();

        $name = $input['name'];
        $password = $input['password'];
        return $this->getUserAndTimestampsByLdapCredentials($password, $name);
    }

    public function checkUserLoggedIn() {
        session_start();
        if(isset($_SESSION["user"])) {
            if(time()-$_SESSION['login_time_stamp'] > 36000) {
                session_unset();
                session_destroy();
                return "NEIN";
            }
            else {
                $pw = $_SESSION['pw'];
                $user = $_SESSION['user'];
                return $this->getUserAndTimestampsByLdapCredentials($pw, $user, true);
            }
        }
        else {
            return "NEIN";
        }
    }

    public $employee;

}