<?php namespace Jannesnagelschmidt\Mitarbeiter\Components;

use Cms\Classes\ComponentBase;

use Jannesnagelschmidt\Mitarbeiter\Models\Mitarbeiter;
use Jannesnagelschmidt\Mitarbeiter\Models\Token;
use Jannesnagelschmidt\Mitarbeiter\Models\Arbeitszeit;

class Timetracker extends ComponentBase
{
    public function componentDetails() {
        return [
            'name' => 'Timetracker',
            'description' => 'Frontend Timetracker tool'
        ];
    }

    public function onRun() {
        $this->mitarbeiter = $this->loadMitarbeiter();
    }

    protected function loadMitarbeiter() {
        return Mitarbeiter::all();
    }

    public $mitarbeiter;
}