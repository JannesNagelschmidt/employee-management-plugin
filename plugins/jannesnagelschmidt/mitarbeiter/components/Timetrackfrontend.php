<?php namespace Jannesnagelschmidt\Mitarbeiter\Components;

use Cassandra\Time;
use Cms\Classes\ComponentBase;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use JannesNagelschmidt\Mitarbeiter\Controllers\Krankheitstage;
use Jannesnagelschmidt\Mitarbeiter\FormWidgets\TimeDisplay;
use JannesNagelschmidt\Mitarbeiter\Models\Calculated;
use JannesNagelschmidt\Mitarbeiter\Models\Department;
use JannesNagelschmidt\Mitarbeiter\Models\Sick;
use Jannesnagelschmidt\Mitarbeiter\Models\Mitarbeiter;
use Jannesnagelschmidt\Mitarbeiter\Models\Arbeitszeit;
use JannesNagelschmidt\Mitarbeiter\Models\Urlaub;
use Jenssegers\Date\Date;
use stdClass;
use Symfony\Component\HttpFoundation\RequestStack;


class Timetrackfrontend extends ComponentBase
{
    /**
     * @var string
     */
    public $timestamps;
    protected $round = 5;
    public $sumForWeek = 0;

    public function componentDetails() {
        return [
            'name' => 'Timetrackfrontend',
            'description' => 'Frontend timedisplay'
        ];
    }

    public function onRun() {
        $arbeitszeit = Arbeitszeit::all();
        $mitarbeiter = Mitarbeiter::all();
        $this->timestamps = $arbeitszeit;
        /*$this->mitarbeiter = $this->loadMitarbeiter();
        $this->timestamp = $this->loadTimestamp();*/
        /*$this->sick = $this->loadSick();
        $this->vacation = $this->loadVacation();*/
        //$this->checkPublicHolidays('2021-04-09', $this->holidays);
    }

    protected function loadMitarbeiter() {
        return Mitarbeiter::all();
    }

    protected  function loadTimestamp() {
        return Arbeitszeit::all();
    }

    protected  function loadVacation() {
        return Urlaub::all();
    }

    protected  function loadSick() {
        return Sick::all();
    }

    public function onGetTemplate() {
        //return ($this->timestamp);
    }



    function dateDifference($start_date, $end_date)
    {
        // calulating the difference in timestamps
        $diff = strtotime($start_date) - strtotime($end_date);

        // 1 day = 24 hours
        // 24 * 60 * 60 = 86400 seconds
        return ceil(abs($diff / 86400));
    }

    function roundDownToAny($n,$x=5) {
        return floor($n / $x) * $x;
    }

    function is_decimal( $val )
    {
        return is_numeric( $val ) && floor( $val ) != $val;
    }

    public function getTimestampForUser($date, $id, $holidays) {
        $returnArray = array();
        $dateValue = date('Y-m-d', strtotime($date));
        $temp = array();
        $holiday = $holidays;
        $returner = array();
        //return false;
        $entry = Arbeitszeit::where([
            ['lookup', $date],
            ['user_id', '=', $id],
            ['status', 'NOT LIKE', 'Korrektur%']
        ])->get();
        $calcEntry = Calculated::where([
            ['lu_date', $date],
            ['user_id', $id],
            ['manually_created', 1]
        ])->get();
        if(count($entry)) {
            /*$entryVac = $this->vacationHandling($id, $date);
            $entrySic = $this->sickHandling($id, $date);*/
            $entryHoliday = $this->checkPublicHolidays($date, $holiday);
            if($entryHoliday)  {
                $temp = array();
                $temp[0]['timestamp'] = $date;
                $temp[0]['status'] = 'public_holiday';
                return $temp;
            }
            else {
                if (count($calcEntry)) {
                    $entryArray = array();
                    //$blup = (array) $entry;
                    foreach($entry as $bla) {
                        array_push($entryArray, $bla);
                    }
                    array_push($entryArray, $calcEntry[0]);
                    $entry = $entryArray;
                }
                return $entry;
            }
        }
        if (count($calcEntry)) {
            $entryHoliday = $this->checkPublicHolidays($date, $holiday);
            if($entryHoliday)  {
                $temp = array();
                $temp[0]['timestamp'] = $date;
                $temp[0]['status'] = 'public_holiday';
                return $temp;
            }
            else {
                return $calcEntry;
            }
        }
        else {
            $entryHoliday = $this->checkPublicHolidays($date, $holiday);
            if($entryHoliday)  {
                $temp = array();
                $temp[0]['timestamp'] = $date;
                $temp[0]['status'] = 'public_holiday';
                return $temp;
            }
        }
        return false;
    }

    /*// pipikaka
    public function renderBreaks($element) {
        $start = 0;
        $sum = strtotime('00:00:00');
        $end = 0;
        $break_start = 0;
        $break_end = 0;
        $breakBiggerThanThirty = false;
        $workedMoreThanSix = true;
        $theBreaks = array();
        $break_startSum = 0;
        $break_endSum = 0;
        $break = 0;
        $breakPos = 0;
        $IST = 0;
        $returner = '';
        if(count($element)) {
            $returner .= '<div class="currentMonth--text-break currentMonth--text text-break--hasBreak"><i class="break--button-icon fas fa-coffee"></i></div>';
        }
        else {
            $returner .= '<div class="currentMonth--text-break currentMonth--text"></div>';
        }
        $returner .= '<div class="break--wrapper">';
        $returner .= '<i class="fas fa-window-close break--close"></i>';
        if(count($element)) {
            foreach($element as $y => $item) {
                if(gettype($item['time']) == 'object') {
                    if($item['status'] == 'coming') {
                        $start = strtotime($item['time']->format('Y-m-d H:i:s'));
                    }
                    if($item['status'] == 'leaving') {
                        $end = strtotime($item['time']->format('Y-m-d H:i:s'));
                    }
                    if($item['status'] == 'break_start') {
                        $break_start = strtotime($item['time']->format('Y-m-d H:i:s'));
                        $break_startSum = $break_start + $break_startSum;
                    }
                    if($item['status'] === 'break_end') {
                        $break_end = strtotime($item['time']->format('Y-m-d H:i:s'));
                        $break_end = $break_end - $sum;
                        $tempStart = strtotime($element[$y-1]['time']->format('Y-m-d H:i:s'));
                        $tempStart = $tempStart - $sum;
                        $tempResult = $break_end - $tempStart;
                        array_push($theBreaks, $tempResult);
                        $break_endSum = $break_end + $break_endSum;
                    }
                    if($item['status'] == 'forgot') {
                        //$returner .= 'verg.';
                    }
                }
                else {
                    $bla = strtotime($item['time']);
                    //$bla = date('Y-m-d H:i:s', strtotime($item['time']));
                    if($item['status'] == 'coming') {
                        $start = $bla;
                    }
                    if($item['status'] == 'leaving') {
                        $end = $bla;
                    }
                    if($item['status'] == 'break_start') {
                        $break_start = $bla;
                        $break_startSum = $break_start + $break_startSum;
                    }
                    if($item['status'] === 'break_end') {
                        $break_end = $bla;
                        $break_end = $break_end - $sum;
                        $tempStart = strtotime($element[$y-1]['time']);
                        $tempStart = $tempStart - $sum;
                        $tempResult = $break_end - $tempStart;
                        array_push($theBreaks, $tempResult);
                        $break_endSum = $break_end + $break_endSum;
                    }
                    if($item['status'] == 'forgot') {
                        //$returner .= 'verg.';
                    }
                }
            }
            $IST = $end - $start;
            $hoursWorked = intval(date('H', $IST));
            $minutesWorked = intval(date('i', $IST));
            if(($minutesWorked >= 30 && $hoursWorked >= 6) || $hoursWorked > 6) {
                $workedMoreThanSix = false;
            }
            foreach($theBreaks as $item) {
                $tempTime = (int)date('i', $item);
                $tempTimeHour = (int)date('H', $item);
                if(!$workedMoreThanSix) {
                    if($tempTime >= 30 || $tempTimeHour > 0) {
                        $breakBiggerThanThirty = true;
                    }
                }
            }
            if($breakBiggerThanThirty && !$workedMoreThanSix) {
                $thirtyMinutes = strtotime('00:30:00');
                $thirtyMinutes = $thirtyMinutes - $sum;
                $tempBreak = 0;
                $breakPos = 0;
                if(count($theBreaks) > 0) {
                    foreach($theBreaks as $x => $item) {
                        if($item > $tempBreak) {
                            $breakPos = $x;
                        }
                        $tempBreak = $item;
                    }
                    $theBreaks[$breakPos] = $thirtyMinutes;
                    foreach($theBreaks as $item) {
                        $break = $break + $item;
                    }
                }
                else {
                    $break = $thirtyMinutes;
                }
            }

            $breakCounter = 0;

            foreach($element as $x => $breakItem) {
                if(gettype($breakItem['time']) == 'object') {
                    //$returner .= '<p class="break--entry">'.$breakItem.'</p>';
                    if(!$workedMoreThanSix && ($breakPos == $breakCounter && ($breakItem['status'] == 'break_start' || $breakItem['status'] == 'break_end'))) {
                        $returner .= '<p class="break--entry '.count($element).'-'.$breakPos.' ">'.$breakItem['time']->format('H:i:s').' - '.$breakItem['status'].' Auf 30 Minuten gesetzt</p>';
                        if($breakItem['status'] == 'break_end') {
                            $breakCounter++;
                        }
                    }
                    else {
                        if($breakItem['status'] == 'break_end') {
                            $breakCounter++;
                        }
                        $returner .= '<p class="break--entry '.count($element).'-'.$breakPos.' ">'.$breakItem['time']->format('H:i:s').' - '.$breakItem['status'].'</p>';
                    }
                }
                else {
                    $bla = date('H:i:s',strtotime($breakItem['time']));
                    if(!$workedMoreThanSix && ($breakPos == $breakCounter && ($breakItem['status'] == 'break_start' || $breakItem['status'] == 'break_end'))) {
                        $returner .= '<p class="break--entry '.count($element).'-'.$breakPos.'">'.$bla.' - '.$breakItem['status'].' Auf 30 Minuten gesetzt</p>';
                        if($breakItem['status'] == 'break_end') {
                            $breakCounter++;
                        }
                    }
                    else {
                        if($breakItem['status'] == 'break_end') {
                            $breakCounter++;
                        }
                        $returner .= '<p class="break--entry '.count($element).'-'.$breakPos.'">'.$bla.' - '.$breakItem['status'].'</p>';
                    }
                    //$returner .= '<p class="break--entry '.count($element).'-'.$breakPos.'">'.$bla.' - '.$breakItem['status'].'</p>';
                }
                //$returner .= gettype($breakItem);
            }
        }
        $returner .= '</div>';
        return $returner;
    }*/

    public function getHolidays($year) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://date.nager.at/api/v2/publicholidays/'.$year.'/DE');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $holidayList = curl_exec($ch);
        curl_close($ch);

        $jsonHoliday = json_decode($holidayList, true);

        return $jsonHoliday;

    }

    public function checkPublicHolidays($date, $germanHolidays) {
        $weekDay = date('w', strtotime($date));
        if($germanHolidays && ($weekDay != 6) && ($weekDay != 0)) {
            //var_dump($germanHolidays);
            /*highlight_string("<?php\n\$data =\n" . var_export($germanHolidays, true) . ";\n?>");*/
            foreach($germanHolidays as $x => $item) {
                if(array_key_exists('date', $item)) {
                    if (date('Y-m-d', strtotime($date)) == date('Y-m-d', strtotime($item['date'])) && $item['global']) {
                        return $item['localName'];
                    }
                }
            }
        }
        else {
            return false;
        }
    }

   /* public function renderMonthDepartment($month, $monthArray = NULL, $user_id, $year = NULL, $holidays) {

        $ddate = date($year."-".$month."-01");
        //$ddate = date("Y-".$month."-01");
        $date = new \DateTime($ddate);
        $returner = '';

        $empolyee = Mitarbeiter::where('id', $user_id)->first();
        if(strtolower($empolyee->status) == 'aktiv') {



        $overtime = $empolyee->overtime;
        $nextMonth = date("Y-".($month+1)."-01");
        $vacCounter = 0;
        $sickCounter = 0;


        $returner = '<div class="currentMonth--wrapper">';
        $start = $date->format('Y')."-".$date->format("m")."-01";
        $end = $date->format("Y")."-".$date->format("m")."-" . date('t', strtotime($date->format("Y-m-d")));
        $returner .= '<div class="currentMonth--col-wrapper headcol--wrapper">';
        $returner .= '<span class="headcol--text headcol--text-name">'.$empolyee->first_name.'</span>';
        $returner .= '<span class="headcol--text headcol--text-name">'.$empolyee->last_name.'</span>';
        $returner .= '<span class="headcol--text headcol--text-ist">IST</span>';
        $returner .= '<span class="headcol--text headcol--text-break">PAUSE</span>';
        $returner .= '<span class="headcol--text headcol--text-soll">SOLL</span>';
        $returner .= '<span class="headcol--text">ÜBER</span>';
        $returner .= '<span class="headcol--text">Ges. Woche</span>';
        $returner .= '</div>';

        $today = date('N', strtotime($ddate));
        $datesBeforeFirst = array();
        $datesAfterLast = array();
        $endReal = date('N', strtotime($end));
        $endReal = 7-$endReal;
        $iterations = $today-1;
        $datesBeforedateArray = array();

        // checks week before current month
        if($iterations > 1) {
            for ($i = $iterations; $i >= 1; $i--) {
                $firstOfMonth = strtotime($ddate);
                $blup = strtotime('-'.$i.' day', $firstOfMonth);
                array_push($datesBeforedateArray, $blup);
                $entries = $this->getTimestampForUser(date('Y-m-d', $blup), $user_id, $holidays);
                if($entries) {
                    array_push($datesBeforeFirst, $entries);
                }
                else {
                    array_push($datesBeforeFirst, false);
                }
            }
        }
        else if($iterations == 1){
            $firstOfMonth = strtotime($ddate);
            $blup = strtotime('-1 day', $firstOfMonth);
            array_push($datesBeforedateArray, $blup);
            $entries = $this->getTimestampForUser(date('Y-m-d', $blup), $user_id, $holidays);
            if($entries) {
                array_push($datesBeforeFirst, $entries);
            }
        }

        if(count($datesBeforeFirst) > 1 && $iterations > 0) {
            foreach($datesBeforeFirst as $x => $item) {
                if($item) {
                    $returner .= '<div class="currentMonth--col-wrapper test2">';
                    if($this->vacationHandling($user_id, date('Y-m-d', $datesBeforedateArray[$x]))) {
                        $returner .= '<p class="currentMonth--text currentMonth--text-ist">URL.</p>';
                    }
                    else if($this->sickHandling($user_id, date('Y-m-d', $datesBeforedateArray[$x]))) {
                        $returner .= '<p class="currentMonth--text currentMonth--text-ist">KRNK</p>';
                    }
                    else {
                        $workTimeArray = $this->getWorkTimeNew($item, $user_id);
                        $returner .= '<p class="currentMonth--text currentMonth--text-ist">'.$workTimeArray['worked'].'</p>';
                    }
                    $breaks = $this->getBreaksForWorkDay($item);
                    $returner .= $this->renderBreaks($breaks);
                    $returner .= '<p class="currentMonth--text-soll currentMonth--text"></p>';
                    // $returner .= '<p class="currentMonth--text currentMonth--text--filler"></p>';
                    $returner .= '<p class="currentMonth--text currentMonth--text--filler"></p>';
                    $returner .= '</div>';
                }
                else {
                    $returner .= '<div class="currentMonth--col-wrapper">';
                    if($this->vacationHandling($user_id, date('Y-m-d', $datesBeforedateArray[$x]))) {
                        $returner .= '<p class="currentMonth--text currentMonth--text-ist">URL.</p>';
                    }
                    else if($this->sickHandling($user_id, date('Y-m-d', $datesBeforedateArray[$x]))) {
                        $returner .= '<p class="currentMonth--text currentMonth--text-ist">KRNK</p>';
                    }
                    else {
                        $returner .= '<p class="currentMonth--text currentMonth--text-ist">00:00</p>';
                    }
                    $returner .= '<p class="currentMonth--text-break currentMonth--text"></p>';
                    $returner .= '<p class="currentMonth--text-soll currentMonth--text"></p>';
                    $returner .= '<p class="currentMonth--text currentMonth--text--filler"></p>';
                    $returner .= '</div>';
                }

            }
        }
        else {
            if(count($datesBeforeFirst) >= 1) {
                $returner .= '<div class="test currentMonth--col-wrapper">';
                if($datesBeforeFirst) {
                    foreach($datesBeforeFirst as $x => $item) {
                        $breaks = $this->getBreaksForWorkDay($item);
                        $blaArray = array();
                        $workTimeArray = $this->getWorkTimeNew($item, $user_id);
                        $returner .= '<p class="currentMonth--text currentMonth--text-ist">'.$workTimeArray['worked'].'</p>';
                        //$returner .= '<p class="currentMonth--text currentMonth--text-ist">'.$this->getSumForWorkDay($item).'</p>';
                        $returner .= $this->renderBreaks($breaks);
                        //$returner .= '<p class="currentMonth--text currentMonth--text-ist">'.$this->getSumForWorkDay($item[0]).'</p>';
                        //$returner .= '<p class="currentMonth--text-break currentMonth--text"></p>';
                        $returner .= '<p class="currentMonth--text-soll currentMonth--text"></p>';
                    }
                }
                else {
                    if(count($datesBeforedateArray)) {
                        if($this->vacationHandling($user_id, date('Y-m-d', $datesBeforedateArray[0]))) {
                            $returner .= '<p class="currentMonth--text currentMonth--text-ist">URL.</p>';
                            $vacCounter = $vacCounter + 1;
                        }
                        else if($this->sickHandling($user_id, date('Y-m-d', $datesBeforedateArray[0]))) {
                            $returner .= '<p class="currentMonth--text currentMonth--text-ist">KRNK</p>';
                            $sickCounter = $sickCounter + 1;
                        }
                    }
                    else {
                        $returner .= '<p class="currentMonth--text currentMonth--text-ist">00:00</p>';
                        $returner .= '<p class="currentMonth--text currentMonth--text--filler"></p>';
                    }
                    $returner .= '<p class="currentMonth--text currentMonth--text--filler"></p>';
                }
                //$returner .= '<p class="currentMonth--text currentMonth--text--filler"></p>';
                $returner .= '<p class="currentMonth--text currentMonth--text--filler"></p>';
                //$returner .= '<p class="currentMonth--text currentMonth--text-date">'.$date_name.'</p>';

                $returner .= '</div>';
            }
            else {
                if(count($datesBeforedateArray) > 0) {
                    $returner .= '<div class="test currentMonth--col-wrapper" data-fick="'.count($datesBeforedateArray).'">';
                    $returner .= '<p class="currentMonth--text currentMonth--text-ist">00:00</p>';
                    $returner .= '<p class="currentMonth--text currentMonth--text-break"></p>';
                    $returner .= '<p class="currentMonth--text currentMonth--text-soll"></p>';
                    $returner .= '<p class="currentMonth--text currentMonth--text--filler"></p>';
                    $returner .= '</div>';
                }
            }
        }

        // checks current month

        while(strtotime($ddate) <= strtotime($end)) {
            $day_name = date('l', strtotime($ddate));
            $dayOfWeek = date('N', strtotime($ddate));
            $date_name = date('d.m', strtotime($ddate));
            $dateYMD = date('Y-m-d', strtotime($ddate));
            $day_num = date('d', strtotime($ddate));
            $holiday = $this->checkPublicHolidays($dateYMD, $holidays);
            $workHours = $this->getWorkWeekIntervals($user_id, $dateYMD);
            if($workHours) {

            }
            else {
                $workHours = $empolyee->hours_per_week;
            }

            if(array_key_exists((int)$day_num, $monthArray)) {
                $returner .= '<div class="currentMonth--col-wrapper '.$holiday.'">';
                if($this->vacationHandling($user_id, $dateYMD)) {
                    $vacCounter = $vacCounter + 1;
                    $returner .= '<p class="currentMonth--text currentMonth--text-holiday currentMonth--text-ist">URL.</p>';
                    $returner .= '<p class="currentMonth--text currentMonth--text-holiday currentMonth--text-ist"></p>';
                }
                else if($this->sickHandling($user_id, $dateYMD)) {
                    $sickCounter = $sickCounter +1;
                    $returner .= '<p class="currentMonth--text currentMonth--text-holiday currentMonth--text-ist">KRNK</p>';
                    $returner .= '<p class="currentMonth--text currentMonth--text-holiday currentMonth--text-ist"></p>';
                }
                else {
                    // getWorkTimeNew
                    $workTimeArray = $this->getWorkTimeNew($monthArray[(int)$day_num], $user_id);
                    $returner .= '<p class="currentMonth--text currentMonth--text-ist">'.$workTimeArray['worked'].'</p>';
                    //$returner .= '<p class="currentMonth--text currentMonth--text-ist">'.$this->getSumForWorkDay($monthArray[(int)$day_num]).'</p>';
                    $breaks = $this->getBreaksForWorkDay($monthArray[(int)$day_num]);
                    $returner .= $this->renderBreaks($breaks);
                }
                //$returner .= '<p class="currentMonth--text-soll currentMonth--text">'.$this->getWorkWeekIntervals($user_id, $dateYMD).'</p>';
                //$returner .= '<p class="currentMonth--text-break currentMonth--text"></p>';
                $returner .= '<p class="currentMonth--text-soll currentMonth--text"></p>';
                $returner .= '<p class="currentMonth--text currentMonth--text--filler"></p>';
                $returner .= '</div>';
                //array_push($overtimeArray, $overTimeDay);
            }
            else {
                if($day_name == 'Sunday') {
                    $returner .= '<div class="currentMonth--col-wrapper currentMonth--col-sunday">';
                    $returner .= '<p class="currentMonth--text ">00:00</p>';
                    //getWorkWeekIntervals
                    $returner .= '<p class="currentMonth--text-break currentMonth--text">00:00</p>';
                    //$returner .= '<p class="currentMonth--text-soll currentMonth--text">'.$this->getWorkWeekIntervals($user_id, $dateYMD).'</p>';
                    $returner .= '<p class="currentMonth--text-soll currentMonth--text">'.$workHours.'</p>';

                    $test = $this->getWorkTimestampsByDate($ddate);
                    $timestampsWeek = array();
                    foreach($test as $x => $item) {
                        $value = $this->getTimestampForUser($item, $user_id, $holidays);
                        array_push($timestampsWeek, $value);
                    }

                    $workedVal = $this->getWorkedHoursWeek($timestampsWeek, $user_id, $holidays);
                    $weekPlusMinus = $this->getWeekPlusMinus($workedVal, $workHours);
                    if(is_array($weekPlusMinus)) {
                        if($vacCounter >= 5) {
                            $returner .= '<p class="currentMonth--text currentMonth--text-über aha">URL.</p>';
                        }
                        if($sickCounter >= 5) {
                            $returner .= '<p class="currentMonth--text currentMonth--text-über aha">KRNK</p>';
                        }
                        else {
                            if(($vacCounter < 5 && $sickCounter < 5)) {
                                if($weekPlusMinus[0]) {
                                    $returner .= '<p class="currentMonth--text currentMonth--text-negative currentMonth--text-über aha">'.$weekPlusMinus[1].'</p>';
                                }
                                else {
                                    //$returner .= '<p class="currentMonth--text currentMonth--text-positive currentMonth--text-über aha">'.$vacCounter.'</p>';
                                    $returner .= '<p class="currentMonth--text currentMonth--text-positive currentMonth--text-über aha">'.$weekPlusMinus[1].'</p>';
                                }
                            }
                        }
                    }
                    else {
                        $valueForWeekPlusMinus = "00:00";
                        $returner .= '<p class="currentMonth--text currentMonth--text-über aha">'.$weekPlusMinus[1].'</p>';
                    }
                    if(($vacCounter >= 5) || $sickCounter >= 5) {
                        $returner .= '<p class="currentMonth--text currentMonth--text-week  WEEKRESULT currentMonth--text-IST">'.$workHours.'</p>';
                    }
                    else {
                        $returner .= '<p class="currentMonth--text currentMonth--text-week  WEEKRESULT currentMonth--text-IST">'.$workedVal.'</p>';
                        //$timestampsWeek
                    }
                    //$returner .= '<p class="currentMonth--text currentMonth--text-week  currentMonth--text-IST">'.$timestampsWeek[2].'</p>';
                    $returner .= '</div>';
                    $sickCounter = 0;
                    $vacCounter = 0;
                }
                else {
                    $returner .= '<div class="currentMonth--col-wrapper '.$holiday.' '.$dayOfWeek.'">';
                    if($this->vacationHandling($user_id, $dateYMD)) {
                        $vacCounter = $vacCounter + 1;
                        $returner .= '<p class="currentMonth--text currentMonth--text-holiday currentMonth--text-ist">URL.</p>';
                    }
                    else if($this->sickHandling($user_id, $dateYMD)) {
                        $sickCounter = $sickCounter +1;
                        $returner .= '<p class="currentMonth--text currentMonth--text-holiday currentMonth--text-ist">KRNK</p>';
                    }
                    else {
                        $returner .= '<p class="currentMonth--text currentMonth--text-ist">00:00</p>';
                    }
                    //$returner .= '<p class="currentMonth--text-soll currentMonth--text">'.$this->getWorkWeekIntervals($user_id, $dateYMD).'</p>';
                    if($holiday) {
                        $returner .= '<div class="currentMonth--text-break currentMonth--text text-break--hasBreak text--holiday"><i class="break--button-icon fas fa-calendar-day"></i></div>';
                        $returner .= '<div class="break--wrapper">';
                        $returner .= '<i class="fas fa-window-close break--close"></i>';
                        $returner .= '<p class="break--entry">'.$holiday.'</p>';
                        $returner .= '</div>';
                    }
                    else {
                        $returner .= '<p class="currentMonth--text-break currentMonth--text"></p>';
                    }
                    $returner .= '<p class="currentMonth--text-soll currentMonth--text"></p>';
                    $returner .= '<p class="currentMonth--text currentMonth--text--filler"></p>';
                    $returner .= '</div>';

                }
            }
            $ddate = date("Y-m-d", strtotime("+1 day", strtotime($ddate)));
        }

        $returner .= '</div>';
        }


        return $returner;
    }*/

    public function getSumForWorkDay($entry) {
        // hier
        $output = '';

        $start = 0;
        $sum = strtotime('00:00:00');
        $end = 0;
        $break_start = 0;
        $break_end = 0;
        $breakBiggerThanThirty = false;
        $workedMoreThanSix = true;
        $theBreaks = array();
        $break_startSum = 0;
        $break_endSum = 0;
        $break = 0;
        $IST = 0;

        if($entry) {
            //$entry = (array)$entry;
            foreach ($entry as $x => $item) {
                if($item && (gettype($item) !== "string") && (gettype($item) !== "boolean")) {
                    if(array_key_exists('state', $item)) {
                        if($item['state'] == 'coming') {
                            $start = strtotime($item['time']->format('Y-m-d H:i:s'));
                        }
                        if($item['state'] == 'leaving') {
                            $end = strtotime($item['time']->format('Y-m-d H:i:s'));
                        }
                        if($item['state'] == 'break_start') {
                            $break_start = strtotime($item['time']->format('Y-m-d H:i:s'));
                            $break_startSum = $break_start + $break_startSum;
                        }
                        if($item['state'] === 'break_end' && (count($item) > 3) && isset($entry)) {
                            $break_end = strtotime($item['time']->format('Y-m-d H:i:s'));
                            $break_end = $break_end - $sum;
                            $tempStart = strtotime($entry[$x-1]['time']->format('Y-m-d H:i:s'));
                            $tempStart = $tempStart - $sum;
                            $tempResult = $break_end - $tempStart;
                            array_push($theBreaks, $tempResult);
                            $break_endSum = $break_end + $break_endSum;
                        }
                        if($item['state'] == 'forgot') {
                            return 'verg.';
                        }
                    }
                    if (array_key_exists('status', $item)) {
                        if($item['status'] == 'coming') {
                            $start = strtotime($item['timestamp']);
                        }
                        if($item['status'] == 'leaving') {
                            $end = strtotime($item['timestamp']);
                        }
                        if($item['status'] == 'break_start') {
                            $break_start = strtotime($item['timestamp']);
                            $break_startSum = $break_start + $break_startSum;
                        }
                        if($item['status'] == 'break_end' && (count($item) > 3) && isset($entry) && $x > 1) {
                            $break_end = strtotime($item['timestamp']);
                            $tempResult = $break_end - strtotime($entry[$x-1]['timestamp']);
                            array_push($theBreaks, $tempResult);
                            $break_endSum = $break_end + $break_endSum;
                        }
                        if($item['status'] == 'forgot') {
                            return 'verg.';
                        }
                    }
                    if(gettype($item) == 'object') {
                        if($item->status == 'coming') {
                            $start = strtotime($item->timestamp);
                            $start = $start - $sum;
                        }
                        if($item->status == 'leaving') {
                            $end = strtotime($item->timestamp);
                            $end = $end - $sum;
                        }
                        if($item->status == 'break_start') {
                            $break_start = strtotime($item->timestamp);
                            $break_startSum = $break_start + $break_startSum;
                        }
                        if($item->status == 'break_end' && (count($entry) > 3) && isset($entry) && $x > 1) {
                            // HIER IST DER FEHLER!!!
                            $break_end = strtotime($item->timestamp);
                            $tempResult = $break_end - strtotime($entry[$x-1]->timestamp);
                            array_push($theBreaks, $tempResult);
                            $break_endSum = $break_end + $break_endSum;
                        }
                        if($item->status == 'forgot') {
                            return 'verg.';
                        }
                    }
                }
            }
        }
        $IST = $end - $start;
        $hoursWorked = intval(date('H', $IST));
        $minutesWorked = intval(date('i', $IST));
        if(($minutesWorked >= 30 && $hoursWorked >= 6) || $hoursWorked > 6) {
            $workedMoreThanSix = false;
        }
        foreach($theBreaks as $item) {
            $tempTime = (int)date('i', $item);
            $tempTimeHour = (int)date('H', $item);
            if(!$workedMoreThanSix) {
                if($tempTime >= 30 || $tempTimeHour > 0) {
                    $breakBiggerThanThirty = true;
                }
            }
        }
        if(!$breakBiggerThanThirty && !$workedMoreThanSix) {
            $thirtyMinutes = strtotime('00:30:00');
            $thirtyMinutes = $thirtyMinutes - $sum;
            $tempBreak = 0;
            $breakPos = 0;
            if(count($theBreaks) > 0) {
                foreach($theBreaks as $x => $item) {
                    if($item > $tempBreak) {
                        $breakPos = $x;
                    }
                    $tempBreak = $item;
                }
                $theBreaks[$breakPos] = $thirtyMinutes;
                foreach($theBreaks as $item) {
                    $break = $break + $item;
                }
            }
            else {
                $break = $thirtyMinutes;
            }
        }
        else {
            $break_startSum = $break_startSum - $sum;
            $break_endSum = $break_endSum - $sum;
            $break = $break_endSum - $break_startSum;
            $break = $break - $sum;
        }
        $breakMinutes = intval(date('i', $break));
        $breakHours = intval(date('H', $break));
        /* if($hoursWorked > 6 && $breakMinutes <= 30 && $breakHours < 1) {
             $thirtyMinutes = strtotime('00:30:00')-$sum;
             $IST = $IST - $thirtyMinutes;
         }
         else {*/
        $IST = $IST - $break;
        /*}*/
        $minutesNotRounded = date('i', $IST);
        $roundedMinutes = $this->roundDownToAny($minutesNotRounded, 1);
        return date('H', $IST).':'.$this->lz($roundedMinutes);
    }


    public function checkInternalCorrections($weekArray, $user_id) {
        $sum = strtotime("00:00:00");
        $lol = strtotime("00:00:00");
        $test = array();
        $returnarr['sum'] = 0;
        $returnarr['plusminus'] = true;
        foreach ($weekArray as $item) {
            $manuallyCreatedTest = false;
            if($item) {
                foreach($item as $timestmap) {
                    if(isset($timestmap['manually_created'])) {
                        if ($timestmap['manually_created']) {
                            $manuallyCreatedTest = true;
                        }
                    }
                }
                if($manuallyCreatedTest) {
                    $entry = Calculated::where([
                        ['user_id', $user_id],
                        ['lu_date', $item[0]['lookup']],
                        ['manually_created', 1]
                    ])->get();
                    if(count($entry)) {
                        foreach($entry as $o => $calcEntry) {
                            $sum = $sum - $lol;
                            $worked = strtotime($calcEntry->worked)-$lol;
                            if(!$calcEntry->negative) {
                                if($returnarr['plusminus']) {
                                    $sum += $worked;
                                }
                                else {
                                    if(date('H:i', $sum) > date('H:i', $worked)) {
                                        $sum = ($sum - $worked) - $lol;
                                        $returnarr['plusminus'] = false;
                                    }
                                    else {
                                        $sum = $worked - $sum;
                                        $returnarr['plusminus'] = true;
                                    }
                                }
                            }
                            else {
                                if($sum > $worked) {
                                    $sum -= $worked;
                                    $returnarr['plusminus'] = true;
                                }
                                else {
                                    $sum = $worked - $sum;
                                    $returnarr['plusminus'] = false;
                                }
                            }
                            /*$blaarr['sum'] = date('H:i', ($sum));
                            $blaarr['plusminus'] = $returnarr['plusminus'];
                            array_push($test, $blaarr);*/
                        }
                    }
                }
            }
        }
        $returnarr['sum'] = date('H:i', $sum);
        return $returnarr;
        //return date('H:i', $sum);
    }

    public function getWorkedHoursWeek($weekArray, $user_id, $holidays) {

        $result = array();
        foreach($weekArray as $x => $item) {
            if($item) {
                if(isset($item[0]['manually_created'])) {
                    if($item[0]['manually_created']) {
                        if(strtotime($item[0]['worked']) > 0) {
                            $time = $item[0]['worked'];
                            array_push($result, $time);
                        }
                    }
                }
                else {
                    $bla = $item[0]['timestamp'];
                    /*$vacation = $this->vacationHandling($user_id, $bla);
                    $sick = $this->sickHandling($user_id, $bla);*/
                    $dateYMD = date('Y-m-d', strtotime($bla));
                    $holiday = $this->checkPublicHolidays($dateYMD, $holidays);
                    //$vacation = $this->getVacationByDate(date("Y-m-d",$bla), $user_id);
                    $weekDay = intval(date('N', strtotime($bla)));
                    $isSick = false;
                    foreach($item as $u => $sickcheck) {
                        if($sickcheck['status'] == 'sick') {
                            $isSick = true;
                        }
                    }
                    if($item[0]['status'] == 'sick' || $item[0]['status'] == 'vacation' || $isSick) {
                        $value = $this->getWorkWeekIntervals($user_id, date('Y-m-d', strtotime($bla)));
                        $divide = $value/5;
                        $time = $this->convertTime($divide);

                        array_push($result, $time);
                    }
                    else if($holiday && $weekDay != 6) {
                        $value = $this->getWorkWeekIntervals($user_id, date('Y-m-d', strtotime($bla)));
                        $divide = $value / 5;
                        $time = $this->convertTime($divide);

                        array_push($result, $time);
                    }
                    else {
                        $value = $this->getSumForWorkDay($item);
                        array_push($result, $value);
                    }
                }
            }
        }
        //var_dump($result);
        $test = $this->calculateSumRest($result);
        $result = $test;
        $blupi = $this->checkInternalCorrections($weekArray, $user_id);
        $test = $test.':00';
        $sumWithSecs = $blupi['sum'].':00';
        if($sumWithSecs != '00:00:00') {
            if($blupi['plusminus']) {
                $result = $this->sum_the_time($test, $sumWithSecs);
            }
            else {
                $result = $this->sub_the_time($test, $sumWithSecs);
            }
        }
        //$realResultNew = strtotime($test) + strtotime($blupi);
        //return $test.' ' .date('H:i', $blupi);+
        return $result;
    }

    function sub_the_time($time1, $time2) {
        $lol = strtotime("00:00:00");
        $sum = "";
        $str1 = 0;
        $str2 = 0;
        list($hour, $minutes) = explode(':', $time1);
        $str1 += $hour*3600;
        $str1 += $minutes*60;
        list($hour, $minutes) = explode(':', $time2);
        $str2 += $hour*3600;
        $str2 += $minutes*60;
        if($str1 > $str2) {
            $sum = $str1 - $str2;
        }
        else {
            $sum = $str2 - $str1;
        }
        $sumH = floor($sum / 3600);
        $sum -= $sumH*3600;
        $sumM = floor($sum / 60);
        $sumM = $this->lz($sumM);
        $sumH = $this->lz($sumH);
        return "$sumH:$sumM";
    }

    function sum_the_time($time1, $time2) {
        $times = array($time1, $time2);
        $seconds = 0;
        foreach ($times as $time)
        {
            list($hour,$minute,$second) = explode(':', $time);
            $seconds += $hour*3600;
            $seconds += $minute*60;
            $seconds += $second;
        }
        $hours = floor($seconds/3600);
        $seconds -= $hours*3600;
        $minutes  = floor($seconds/60);
        $seconds -= $minutes*60;
        if($seconds < 9)
        {
            $seconds = "0".$seconds;
        }
        if($minutes < 9)
        {
            $minutes = "0".$minutes;
        }
        if($hours < 9)
        {
            $hours = "0".$hours;
        }
        return "{$hours}:{$minutes}";
    }

    public function getWorkTimestampsByDate($date) {
        $returnArray = array();
        for($i = 6; $i >= 0; $i--) {
            $theDate = date("Y-m-d", strtotime("-".$i." day", strtotime($date)));
            array_push($returnArray, $theDate);
        }
        return $returnArray;
    }


    public function calcWeekRest($array) {
        $returnArray = array();
        if(count($array)) {
            $back = '';
            $overtime = strtotime(0);
            $minusValue = 0;
            $plusValue = 0;
            foreach($array as $x => $item) {
                if (strpos($item, '-') !== false) {
                    $str = substr($item, strrpos($item, '-')+1);
                    $str = strtotime($str);
                    //return $str;
                    $minusValue = $minusValue + $str;
                }
                else {
                    $str = strtotime($item);
                    $plusValue = $plusValue + $str;
                }
            }

            if($minusValue > $plusValue) {
                array_push($returnArray, '-');
                array_push($returnArray, $minusValue - $plusValue);
                return $returnArray;
            }
            else {
                array_push($returnArray, '+');
                array_push($returnArray, $plusValue - $minusValue);
                return $returnArray;
            }

        }
    }


    public function getWeekPlusMinus($IST, $SOLL) {
        // soll = 38:30
        // ist = 42:09
        $values = array();
        $result = 0;
        if($SOLL == 0) {
            return false;
        }
        if(strpos($IST, ':')) {
            $arr = explode(":", $IST, 2);
            $istHours = (int)$arr[0];
            $istMinutes = $arr[1];
        }
        else {
            $istHours = $IST;
            $istMinutes = 60;
        }
        if(strpos($SOLL, ':') !== false) {
            $arr = explode(":", $SOLL, 2);
            $sollHours = $arr[0];
            $sollMinutes = $arr[1];
        }
        else {
            $sollHours = $SOLL;
            $sollMinutes = 60;
        }
        if($istHours == 0 && $istMinutes == 0) {
            if($sollMinutes == 60) {
                array_push($values, true);
                array_push($values, "-$sollHours:00");
                return $values;
            }
            else {
                array_push($values, true);
                array_push($values, "-$sollHours:$sollMinutes");
                return $values;
            }
        }
        $negative = false;

        // soll = 38:30
        // ist = 42:09
        if($istMinutes >= $sollMinutes) {
            $resultMinutes = $istMinutes - $sollMinutes;
            if($istHours < $sollHours) {
                if($resultMinutes > 0) {
                    $resultMinutes = 60 - $resultMinutes;
                }
            }
        }
        else {
            if($sollMinutes == 60 || $istHours < $sollHours) {
                $resultMinutes = $sollMinutes - (int)$istMinutes;
            }
            else {
                $resultMinutes = $sollMinutes - (int)$istMinutes;
                $resultMinutes = 60 - $resultMinutes;
                //$istHours = $istHours - 1;
            }
        }
        // soll = 38:30
        // ist = 42:09
        if($istHours >= $sollHours) {
            if($sollMinutes == 60) {
                $resultMinutes = $istMinutes;
            }
            if($sollMinutes > $istMinutes) {
                $result = $istHours - ($sollHours + 1);
            }
            else {
                $result = $istHours - $sollHours;
            }
        }
        else {
            if($istMinutes != 60 && $sollMinutes > $istMinutes) {
                if($istMinutes == "00" && $sollMinutes == 60) {
                    $resultMinutes = "00";
                    $result = $sollHours - $istHours;
                }
                else {
                    if($sollHours > $istHours ) {
                        $result = $sollHours - $istHours;
                    }
                    else {
                        if(($sollHours - 1) === $istHours) {
                            $result = $sollHours - $istHours;
                        }
                        else {
                            $result = ($sollHours - 1) - $istHours;
                        }
                    }
                }
                /*if($result == 1) {
                    $result = 0;
                }*/
            }
            else {
                if($istMinutes > $sollMinutes) {
                    $result = ($sollHours - 1) - $istHours;
                }
                else {
                    $result = $sollHours - $istHours;
                }
            }
            $negative = true;
        }


        if($negative) {
            array_push($values, true);
            $resultMinutes = $this->lz($resultMinutes);
            array_push($values, "-$result:$resultMinutes");
            return $values;
        }
        $resultMinutes = $this->lz($resultMinutes);
        array_push($values, false);
        array_push($values, "$result:$resultMinutes");
        return $values;
    }

    public function calculateSumRest($array) {
        $sum = strtotime('00:00:00');
        $totalTimePlus = 0;
        $totalTimeMinus = 0;
        foreach($array as $x => $item) {
            if($item == 'verg.') {

            }
            else if ($item[0] == "-") {
                //return $item;
                $str = substr($item, strpos($item, '-')+1);
                $str = explode(' ', trim($str));
                //return $str[0];
                //$str = date('H:i:s', $str);
                $timeinsec = strtotime($str[0]) - $sum;
                $totalTimeMinus = $totalTimeMinus + $timeinsec;
            }
            else {
                $str = explode(' ', trim($item));
                $timeinsec = strtotime($str[0]) - $sum;
                $totalTimePlus = $totalTimePlus + $timeinsec;
            }
        }
        $minus = $totalTimeMinus;
        $h1 = intval($totalTimeMinus/3600);
        $totalTimeMinus = $totalTimeMinus - ($h1 * 3600);
        $m1 = intval($totalTimeMinus / 60);
        $s1 = $totalTimeMinus - ($m1 * 60);

        $plus = $totalTimePlus;
        $h = intval($totalTimePlus/3600);
        $totalTimePlus = $totalTimePlus - ($h * 3600);
        $m = intval($totalTimePlus / 60);
        $s = $totalTimePlus - ($m * 60);

        if($plus > $minus) {
            $result = $plus - $minus;
            $negative = false;
        }
        else {
            $result = $minus - $plus;
            $negative = true;
        }
        $hR = intval($result/3600);
        $result = $result - ($hR * 3600);
        $mR = intval($result / 60);
        $sR = $result - ($mR * 60);

        if($mR<10) {
            $mR = '0'.$mR;
        }

        if($hR == 0 && $mR == 0 && $sR == 0) {
            return "$hR:$mR";
        }
        if($negative) {
            return "-$hR:$mR:$sR";
        }
        return "$hR:$mR";

    }

    public function getRestForDay($entry, $soll = null) {
        $start = 0;
        $end = 0;
        $break_start = 0;
        $break_end = 0;
        $break_startSum = 0;
        $break_endSum = 0;
        $break = 0;
        $sum = strtotime('00:00:00');
        $IST = 0;


        foreach ($entry as $x => $item) {
            //$output .= strtotime($item['time']->format('Y-m-d H:i:s'));
            if($item['state'] == 'coming') {
                $start = strtotime($item['time']->format('H:i:s'));
            }
            if($item['state'] == 'leaving') {
                $end = strtotime($item['time']->format('H:i:s'));
            }
            if($item['state'] == 'break_start') {
                $break_start = strtotime($item['time']->format('H:i:s'));
                $break_startSum = $break_start + $break_startSum;
            }
            if($item['state'] == 'break_end') {
                $break_end = strtotime($item['time']->format('H:i:s'));
                $break_endSum = $break_end + $break_endSum;
            }
        }
        $break = $break_endSum - $break_startSum;
        $IST = $end - $start;
        $ISTplusBreak = $end - $start;
        $IST = $IST - $break;
        $hour = $soll;
        $hour = (int)$hour;
        $minutes = ($soll - $hour) * 60;
        $SOLL = strtotime(($hour.':'.$minutes.':00')) - $sum;

        $istMinutes = date('i', $IST);
        $sollMinutes = date('i', $SOLL);
        $sollHours = date('H', $SOLL);
        $istHours = date('H', $IST);

        if($IST > $SOLL) {
            $result = $IST - $SOLL;
            return date('H:i', $result);
        }
        else {
            $result = $SOLL - $IST;
            return '-'.date('H:i', $result);
        }
    }

    public function getBreaksForWorkDay($entry){
        $return = array();
        foreach($entry as $x => $item) {
            if($item && (gettype($item) !== "string") && (gettype($item) !== "boolean")) {
                if(array_key_exists('state', $item)) {
                    /*if($item['state'] == 'break_end' || $item['state'] == 'break_start') {*/
                        $temp = array();
                        $temp['time'] = $item['time'];
                        $temp['status'] = $item['state'];
                        array_push($return, $temp);
                        //array_push($return, $item['time']);
                    /*}*/
                }
                if(array_key_exists('status', $item)) {
                    /*if($item['status'] == 'break_end' || $item['status'] == 'break_start') {*/
                        $temp = array();
                        $temp['time'] = $item['timestamp'];
                        $temp['status'] = $item['status'];
                        array_push($return, $temp);
                        //array_push($return, $item['timestamp']);
                    /*}*/
                }
                if(gettype($item) == 'object') {
                    /*if($item->status == 'break_end' || $item->status == 'break_start') {*/
                        $temp = array();
                        $temp['time'] = $item['timestamp'];
                        $temp['status'] = $item['status'];
                        array_push($return, $temp);
                        //array_push($return, $item->timestamp);
                    /*}*/
                }
            }
        }
        return $return;
    }

    public function getWorkTimeNew($date, $id) {
        if($date[0]['time'] && (gettype($date[0]['time']) !== "string") && (gettype($date[0]['time']) !== "boolean")) {
            return DB::table('jannesnagelschmidt_mitarbeiter_calculated')
                ->where('lu_date', $date[0]['time']->format('Y-m-d'))
                ->where('user_id', $id)
                ->first();
            /*return Calculated::where([
                ['timestamp', 'LIKE', $date[0]['time']->format('Y-m-d H:i:s')],
                ['user_id', $id]
            ])->first();*/
        }
        else if(gettype($date) == 'object') {
            return DB::table('jannesnagelschmidt_mitarbeiter_calculated')
                ->where('lu_date', date('Y-m-d', strtotime($date[0]->timestamp)))
                ->where('user_id', $id)
                ->first();
            /*return Calculated::where([
                ['timestamp', 'LIKE', $date[0]->timestamp],
                ['user_id', $id]
            ])->first();*/
        }
    }



    public function getDepartments() {
        return Department::all();
    }



    public function getWorkWeekIntervals($userID, $date) {
        $employee = Mitarbeiter::where('id', $userID)->first();
        $strDate = strtotime($date);
        $workHours = $employee->work_repeater;
        if(isset($workHours)) {
            usort($workHours, function ($a, $b) {
                return strtotime($a['work_start']) - strtotime($b['work_start']);
            });
            $returner = $employee->hours_per_week;
            if($workHours) {
                foreach($workHours as $x => $item) {
                    if($x == 0) {
                        $strStart = strtotime($item['work_start']);
                        if($strStart <= $strDate) {
                            $returner = $item['work_time_real'];
                        }
                        $lastStart = $strStart;
                    }
                    else {
                        $strStart = strtotime($item['work_start']);
                        if($strStart <= $strDate && $lastStart < $strStart) {
                            $returner = $item['work_time_real'];
                        }
                        $lastStart = $strStart;
                    }
                }
            }
            else if(!isset($returner)){
                $returner = $employee->hours_per_week;
            }
        }
        else {
            $returner = $employee->hours_per_week;
        }
        return $returner;
    }

    public function convertStrToTime($strtotime) {
        $seconds = $strtotime;
        $hours = floor($seconds);
        // since we've "calculated" hours, let's remove them from the seconds variable
        $seconds -= $hours * 3600;
        // calculate minutes left
        $minutes = floor($seconds / 60);
        // remove those from seconds as well
        $seconds -= $minutes * 60;
        // return the time formatted HH:MM:SS
        return $this->lz($hours).":".$this->lz($minutes);
    }

    public function convertTime($dec) {
        // start by converting to seconds
        $seconds = ($dec * 3600);
        // we're given hours, so let's get those the easy way
        $hours = floor($dec);
        // since we've "calculated" hours, let's remove them from the seconds variable
        $seconds -= $hours * 3600;
        // calculate minutes left
        $minutes = floor($seconds / 60);
        // remove those from seconds as well
        $seconds -= $minutes * 60;
        // return the time formatted HH:MM:SS
        return $this->lz($hours).":".$this->lz($minutes);
    }

    public function lz($num){
        return (strlen($num) < 2) ? "0{$num}" : $num;
    }

    public function vacationHandling($id, $date): bool
    {
        $lookup = $date;
        $timestamp = Arbeitszeit::where([
            ['user_id', $id],
            ['lookup', $lookup]
        ])->first();
        if($timestamp) {
            if($timestamp['status'] == 'vacation') {
                return true;
            }
        }
        return false;
    }

    public function sickHandling($id, $date): bool
    {
        $lookup = $date;
        $timestamp = Arbeitszeit::where([
            ['user_id', $id],
            ['lookup', $lookup]
        ])->first();
        if($timestamp) {
            if($timestamp->status == 'sick') {
                return true;
            }
        }
        return false;
    }

    function byLastName($a, $b) {
        return strcmp($a['last_name'], $b['last_name']);
    }

    public function monthHandling($month) {
        if(array_key_exists('month', $month)) {
            $returner = $month['month'];
        }
        else {
            $returner = $this->getCurrentMonth();
        }
        return $returner;
    }

    // neuster scheiß
    public function getEmployeeForDepartmentNew($id, $month, $holidays, $year) {
        try {
        //$timestamps = $this->timestamps;
        $timestamps = Arbeitszeit::where('user_id', $id)->get();
        $user_id = $id;
        /*$vacations = Urlaub::where('mitarbeiter_id', $id)->get();
        $sick = Sick::where('mitarbeiter_id', $id)->get();*/
        $returnArray = array();
        $work = array();
        $tracking = array();


        $monthArray = array();
        $starttracking = microtime(TRUE);
        if(count($timestamps) && isset($timestamps)) {
            foreach($timestamps as $x => $item) {
                $timestampDate = $item->timestamp;
                $test = new \DateTime($timestampDate);
                $state = $item->status;
                $monthTest = $test->format("m");
                $yearTest = $test->format("Y");
                $dayTest = $test->format("D");
                $tempArray = array();
                if($month == $monthTest && $year == $yearTest) {
                    $tempArray['state'] = $item->status;
                    $tempArray['time'] = $test;
                    //$output .= $test->format('d') . '</br>';
                    $monthArray[(int)$test->format('d')][] = $tempArray;

                }
            }
        }
        $endtracking = microtime(TRUE);
        array_push($tracking, ('1. ' . ($endtracking-$starttracking)));
        $ddate = date($year."-".$this->lz($month)."-01");
        //$ddate = date("Y-".$month."-01");
        $date = new \DateTime($ddate);
        $returner = '';
        $end = $date->format("Y")."-".$date->format("m")."-" . date('t', strtotime($date->format("Y-m-d")));
        $empolyee = Mitarbeiter::where('id', $user_id)->first();
        if(strtolower($empolyee->status) == 'aktiv') {
            $overtime = $empolyee->overtime;
            $nextMonth = date("Y-".($month+1)."-01");
            $vacCounter = 0;
            $sickCounter = 0;


            $returnArray['employee'] = $empolyee;


            $today = date('N', strtotime($ddate));
            $datesBeforeFirst = array();
            $datesAfterLast = array();
            $endReal = date('N', strtotime($end));
            $endReal = 7-$endReal;
            $iterations = $today-1;
            $datesBeforedateArray = array();
            $starttracking = microtime(TRUE);
            // checks week before current month

            if($iterations > 1) {
                for ($i = $iterations; $i >= 1; $i--) {
                    $firstOfMonth = strtotime($ddate);
                    $blup = strtotime('-'.$i.' day', $firstOfMonth);
                    array_push($datesBeforedateArray, $blup);
                    $entries = $this->getTimestampForUser(date('Y-m-d', $blup), $user_id, $holidays);
                    if($entries) {
                        array_push($datesBeforeFirst, $entries);
                    }
                    else {
                        array_push($datesBeforeFirst, false);
                    }
                }
            }
            else if($iterations == 1){
                $firstOfMonth = strtotime($ddate);
                $blup = strtotime('-1 day', $firstOfMonth);
                array_push($datesBeforedateArray, $blup);
                $entries = $this->getTimestampForUser(date('Y-m-d', $blup), $user_id, $holidays);
                if($entries) {
                    array_push($datesBeforeFirst, $entries);
                }
            }

            if(count($datesBeforeFirst) > 1 && $iterations > 0 && isset($datesBeforeFirst)) {
                foreach($datesBeforeFirst as $x => $item) {
                    $tempArray = array();
                    if($item) {
                        //$returner .= '<div class="currentMonth--col-wrapper test2">';
                        if($this->vacationHandling($user_id, date('Y-m-d', $datesBeforedateArray[$x]))) {
                            $tempArray['work'] = 'URL.';
                            $tempArray['date'] = $datesBeforedateArray[$x];
                            //$returner .= '<p class="currentMonth--text currentMonth--text-ist">URL.</p>';
                        }
                        else if($this->sickHandling($user_id, date('Y-m-d', $datesBeforedateArray[$x]))) {
                            $tempArray['work'] = 'KRNK';
                            $tempArray['date'] = $datesBeforedateArray[$x];
                            //$returner .= '<p class="currentMonth--text currentMonth--text-ist">KRNK</p>';
                        }
                        else {
                            $workTimeArray = $this->getWorkTimeNew($item, $user_id);
                            //return var_dump ($workTimeArray['worked']);
                            if(is_object($workTimeArray)) {
                                $tempArray['work'] = $workTimeArray->worked;
                            }
                            else {
                                $tempArray['work'] = $workTimeArray['worked'];
                            }
                            //$tempArray['work'] = 'test';
                            $tempArray['date'] = $item;
                            //$returner .= '<p class="currentMonth--text currentMonth--text-ist">'.$workTimeArray['worked'].'</p>';
                        }
                        $breaks = $this->getBreaksForWorkDay($item);
                        $tempArray['break'] = $breaks;
                        //$returner .= $this->renderBreaks($breaks);
                        //$returner .= '<p class="currentMonth--text-soll currentMonth--text"></p>';
                        // $returner .= '<p class="currentMonth--text currentMonth--text--filler"></p>';
                        //$returner .= '<p class="currentMonth--text currentMonth--text--filler"></p>';
                        //$returner .= '</div>';
                    }
                    else {
                        //$returner .= '<div class="currentMonth--col-wrapper">';
                        if($this->vacationHandling($user_id, date('Y-m-d', $datesBeforedateArray[$x]))) {
                            $tempArray['work'] = 'URL.';
                            $tempArray['date'] = $datesBeforedateArray[$x];
                            //$returner .= '<p class="currentMonth--text currentMonth--text-ist">URL.</p>';
                        }
                        else if($this->sickHandling($user_id, date('Y-m-d', $datesBeforedateArray[$x]))) {
                            $tempArray['work'] = 'KRNK';
                            $tempArray['date'] = $datesBeforedateArray[$x];
                            //$returner .= '<p class="currentMonth--text currentMonth--text-ist">KRNK</p>';
                        }
                        else {
                            $tempArray['work'] = '00:00';
                            $tempArray['date'] = date('Y-m-d', $datesBeforedateArray[$x]);
                            //$tempArray['date'] = "pimmel";
                           // $returner .= '<p class="currentMonth--text currentMonth--text-ist">00:00</p>';
                        }
                        /*                    $returner .= '<p class="currentMonth--text currentMonth--text--filler"></p>';*/
                        /*$returner .= '<p class="currentMonth--text-break currentMonth--text"></p>';
                        $returner .= '<p class="currentMonth--text-soll currentMonth--text"></p>';
                        $returner .= '<p class="currentMonth--text currentMonth--text--filler"></p>';
                        $returner .= '</div>';*/
                    }
                    array_push($work, $tempArray);
                    $tempArray = array();
                    /*}*/
                }
            }
            else {
                $tempArray = array();
                if(count($datesBeforeFirst) >= 1) {
                    $returner .= '<div class="test currentMonth--col-wrapper">';
                    if($datesBeforeFirst) {
                        foreach($datesBeforeFirst as $x => $item) {
                            $breaks = $this->getBreaksForWorkDay($item);
                            $blaArray = array();
                            $workTimeArray = $this->getWorkTimeNew($item, $user_id);
                            // hier was geändert
                            $tempArray['work'] = $workTimeArray->worked;
                            //$tempArray['work'] = $workTimeArray['worked'];
                            //$tempArray['date'] = date('Y-m-d', strtotime($datesBeforeFirst[0]));
                            //$tempArray['date'] = $datesBeforeFirst[0][0]->timestamp;
                            $tempArray['date'] = $datesBeforeFirst[0][0]->timestamp;
                            $tempArray['break'] = $breaks;

                        }
                    }
                    else {
                        if(count($datesBeforedateArray)) {
                            if($this->vacationHandling($user_id, date('Y-m-d', $datesBeforedateArray[0]))) {
                                //$returner .= '<p class="currentMonth--text currentMonth--text-ist">URL.</p>';
                                $tempArray['work'] = 'URL.';
                                $tempArray['date'] = $datesBeforedateArray[0];
                                $vacCounter = $vacCounter + 1;
                            }
                            else if($this->sickHandling($user_id, date('Y-m-d', $datesBeforedateArray[0]))) {
                                //$returner .= '<p class="currentMonth--text currentMonth--text-ist">KRNK</p>';
                                $tempArray['work'] = 'KRNK';
                                $tempArray['date'] = $datesBeforedateArray[0];
                                $sickCounter = $sickCounter + 1;
                            }
                        }
                        else {
                            //$returner .= '<p class="currentMonth--text currentMonth--text-ist">00:00</p>';
                            $tempArray['work'] = '00:00';
                            $tempArray['date'] = date('Y-m-d', strtotime($datesBeforedateArray[0]));
                            //$tempArray['date'] = "blup";
                            //$returner .= '<p class="currentMonth--text currentMonth--text--filler"></p>';
                        }
                        //$returner .= '<p class="currentMonth--text currentMonth--text--filler"></p>';
                    }
                    //$returner .= '<p class="currentMonth--text currentMonth--text--filler"></p>';
                    //$returner .= '<p class="currentMonth--text currentMonth--text--filler"></p>';
                    //$returner .= '<p class="currentMonth--text currentMonth--text-date">'.$date_name.'</p>';
                    array_push($work, $tempArray);
                    $tempArray = array();
                    //$returner .= '</div>';
                }
                else {
                    if(count($datesBeforedateArray) > 0) {
                        //$returner .= '<div class="test currentMonth--col-wrapper" data-fick="'.count($datesBeforedateArray).'">';
                        //$returner .= '<p class="currentMonth--text currentMonth--text-ist">00:00</p>';
                        $tempArray['work'] = '00:00';
                        $tempArray['date'] = date('Y-m-d', $datesBeforedateArray[0]);
                        array_push($work, $tempArray);
                        $tempArray = array();
                        //$returner .= '<p class="currentMonth--text currentMonth--text-break"></p>';
                        /*$returner .= '<p class="currentMonth--text currentMonth--text-soll"></p>';
                        $returner .= '<p class="currentMonth--text currentMonth--text--filler"></p>';
                        $returner .= '</div>';*/
                    }
                }
            }
            $endtracking = microtime(TRUE);
            array_push($tracking, ('2 dates before first . ' . ($endtracking-$starttracking)));
            // checks current month
            // aktueller monat
            $starttracking = microtime(TRUE);
            while(strtotime($ddate) <= strtotime($end)) {
                $tempArrayIndex = count($tempArray);
                $day_name = date('l', strtotime($ddate));
                $dayOfWeek = date('N', strtotime($ddate));
                $date_name = date('d.m', strtotime($ddate));
                $dateYMD = date('Y-m-d', strtotime($ddate));
                $day_num = date('d', strtotime($ddate));
                //$starttracking = microtime(TRUE);
                $holiday = $this->checkPublicHolidays($dateYMD, $holidays);
                //$endtracking = microtime(TRUE);
                //array_push($tracking, ('checkpublicholidays . ' . ($endtracking-$starttracking)));
                $workHours = $this->getWorkWeekIntervals($user_id, $dateYMD);

                if(array_key_exists((int)$day_num, $monthArray)) {
                    //$returner .= '<div class="currentMonth--col-wrapper '.$holiday.'">';
                    if($this->vacationHandling($user_id, $dateYMD)) {
                        $vacCounter = $vacCounter + 1;
                        //$returner .= '<p class="currentMonth--text currentMonth--text-holiday currentMonth--text-ist">URL.</p>';
                        //$returner .= '<p class="currentMonth--text currentMonth--text-holiday currentMonth--text-ist"></p>';
                        $tempArray['work'] = 'URL.';
                        $tempArray['date'] = $dateYMD;
                    }
                    else if($this->sickHandling($user_id, $dateYMD)) {
                        $sickCounter = $sickCounter +1;
                        //$returner .= '<p class="currentMonth--text currentMonth--text-holiday currentMonth--text-ist">KRNK</p>';
                        $tempArray['work'] = 'KRNK';
                        $tempArray['date'] = $dateYMD;
                        //$returner .= '<p class="currentMonth--text currentMonth--text-holiday currentMonth--text-ist"></p>';
                    }
                    else {
                            // getWorkTimeNew
                            $starttracking = microtime(TRUE);
                            $workTimeArray = $this->getWorkTimeNew($monthArray[(int)$day_num], $user_id);
                            $endtracking = microtime(TRUE);
                            array_push($tracking, ('getWorkTimeNew . ' . ($endtracking-$starttracking)));
                            //$returner .= '<p class="currentMonth--text currentMonth--text-ist">'.$workTimeArray['worked'].'</p>';
                            // hier auch was geändert
                            if(is_object($workTimeArray )) {
                                $tempArray['work'] = $workTimeArray->worked;
                            }
                            else {
                                $tempArray['work'] = $workTimeArray['worked'];
                            }
                            //$tempArray['work'] = $workTimeArray['worked'];
                            $tempArray['date'] = $dateYMD;
                            //$returner .= '<p class="currentMonth--text currentMonth--text-ist">'.$this->getSumForWorkDay($monthArray[(int)$day_num]).'</p>';
                            $breaks = $this->getBreaksForWorkDay($monthArray[(int)$day_num]);
                            $tempArray['break'] = $breaks;
                            $tempArray['omega_test'] = $monthArray[(int)$day_num];
                            $tempArray['day_num'] = $day_num;
                            //$returner .= $this->renderBreaks($breaks);
                    }
                    //$returner .= '<p class="currentMonth--text-soll currentMonth--text">'.$this->getWorkWeekIntervals($user_id, $dateYMD).'</p>';
                    //$returner .= '<p class="currentMonth--text-break currentMonth--text"></p>';
                    /*$returner .= '<p class="currentMonth--text-soll currentMonth--text"></p>';
                    $returner .= '<p class="currentMonth--text currentMonth--text--filler"></p>';
                    $returner .= '</div>';*/
                    array_push($work, $tempArray);
                    $tempArray = array();
                    //array_push($overtimeArray, $overTimeDay);
                }
                else {
                    if($day_name == 'Sunday') {
                        /*$returner .= '<div class="currentMonth--col-wrapper currentMonth--col-sunday">';
                        $returner .= '<p class="currentMonth--text ">00:00</p>';*/
                        //getWorkWeekIntervals
                        //$returner .= '<p class="currentMonth--text-break currentMonth--text">00:00</p>';
                        //$returner .= '<p class="currentMonth--text-soll currentMonth--text">'.$this->getWorkWeekIntervals($user_id, $dateYMD).'</p>';
                        //$returner .= '<p class="currentMonth--text-soll currentMonth--text">'.$workHours.'</p>';
                        $starttracking = microtime(TRUE);

                        $test = $this->getWorkTimestampsByDate($ddate);
                        $endtracking = microtime(TRUE);
                        array_push($tracking, ('getWorkTimestampsByDate . ' . ($endtracking-$starttracking)));

                        $timestampsWeek = array();

                        $starttracking = microtime(TRUE);
                        foreach($test as $x => $item) {
                            $value = $this->getTimestampForUser($item, $user_id, $holidays);
                            array_push($timestampsWeek, $value);
                        }
                        $endtracking = microtime(TRUE);
                        array_push($tracking, ('getTimestampForUser . ' . ($endtracking-$starttracking)));

                        $starttracking = microtime(TRUE);
                        $workedVal = $this->getWorkedHoursWeek($timestampsWeek, $user_id, $holidays);
                        $endtracking = microtime(TRUE);
                        //array_push($tracking, ('getWorkedHoursWeek . ' . ($endtracking-$starttracking)));

                        $starttracking = microtime(TRUE);
                        $weekPlusMinus = $this->getWeekPlusMinus($workedVal, $workHours);
                        //$weekPlusMinus = $this->getWeekPlusMinus("0", $workHours);
                        $endtracking = microtime(TRUE);
                        //array_push($tracking, ('getWeekPlusMinus . ' . ($endtracking-$starttracking)));

                        if(is_array($weekPlusMinus)) {

                                if($weekPlusMinus[0]) {
                                    //$returner .= '<p class="currentMonth--text currentMonth--text-negative currentMonth--text-über aha">'.$weekPlusMinus[1].'</p>';
                                    $tempArray['redgreen'] = 'negative';
                                }
                                else {
                                    $tempArray['redgreen'] = 'positive';
                                }
                                $tempArray['work'] = '00:00';
                                $tempArray['test'] = $workedVal;
                                $tempArray['soll'] =  $workHours;
                                $tempArray['plusminus'] = $weekPlusMinus[1];
                                $tempArray['date'] = $dateYMD;

                        }
                        else {
                            $valueForWeekPlusMinus = "00:00";
                            $tempArray['work'] = '00:00';
                            $tempArray['redgreen'] = 'none';
                            $tempArray['soll'] =  $workHours;
                            $tempArray['plusminus'] = $weekPlusMinus[1];
                            $tempArray['date'] = $dateYMD;
                            //$returner .= '<p class="currentMonth--text currentMonth--text-über aha">'.$weekPlusMinus[1].'</p>';
                        }
                        if(($vacCounter >= 5) || $sickCounter >= 5) {
                            //$returner .= '<p class="currentMonth--text currentMonth--text-week  WEEKRESULT currentMonth--text-IST">'.$workHours.'</p>';
                            $tempArray['weekresult'] = $workHours;
                        }
                        else {
                            $tempArray['weekresult'] = $workedVal;
                            //$returner .= '<p class="currentMonth--text currentMonth--text-week  WEEKRESULT currentMonth--text-IST">'.$workedVal.'</p>';
                            //$timestampsWeek
                        }
                        //$returner .= '<p class="currentMonth--text currentMonth--text-week  currentMonth--text-IST">'.$timestampsWeek[2].'</p>';
                        //$returner .= '</div>';
                        $sickCounter = 0;
                        $vacCounter = 0;
                        array_push($work, $tempArray);
                        $tempArray = array();
                    }
                    else {
                        //$returner .= '<div class="currentMonth--col-wrapper '.$holiday.' '.$dayOfWeek.'">';
                        $tempArray['break'] = false;
                        if($this->vacationHandling($user_id, $dateYMD)) {
                            $vacCounter = $vacCounter + 1;
                            //$returner .= '<p class="currentMonth--text currentMonth--text-holiday currentMonth--text-ist">URL.</p>';
                            $tempArray['work'] = 'URL.';
                            $tempArray['date'] = $dateYMD;
                        }
                        else if($this->sickHandling($user_id, $dateYMD)) {
                            $sickCounter = $sickCounter +1;
                            //$returner .= '<p class="currentMonth--text currentMonth--text-holiday currentMonth--text-ist">KRNK</p>';
                            $tempArray['work'] = 'KRNK';
                            $tempArray['date'] = $dateYMD;
                        }
                        else {
                            //$returner .= '<p class="currentMonth--text currentMonth--text-ist">00:00</p>';
                            $tempArray['work'] = '00:00';
                            $tempArray['date'] = $dateYMD;
                        }
                        //$returner .= '<p class="currentMonth--text-soll currentMonth--text">'.$this->getWorkWeekIntervals($user_id, $dateYMD).'</p>';
                        if($holiday) {
                            /*$returner .= '<div class="currentMonth--text-break currentMonth--text text-break--hasBreak text--holiday"><i class="break--button-icon fas fa-calendar-day"></i></div>';
                            $returner .= '<div class="break--wrapper">';
                            $returner .= '<i class="fas fa-window-close break--close"></i>';
                            $returner .= '<p class="break--entry">'.$holiday.'</p>';*/
                            $tempArray['work'] = $holiday;
                            $tempArray['holiday'] = true;
                            $tempArray['date'] = $dateYMD;
                            //$returner .= '</div>';
                        }
                        else {
                            //$returner .= '<p class="currentMonth--text-break currentMonth--text"></p>';
                        }
                       /* $returner .= '<p class="currentMonth--text-soll currentMonth--text"></p>';
                        $returner .= '<p class="currentMonth--text currentMonth--text--filler"></p>';
                        $returner .= '</div>';*/
                        array_push($work, $tempArray);
                        $tempArray = array();
                    }
                }
                $ddate = date("Y-m-d", strtotime("+1 day", strtotime($ddate)));
            }

            /*for($i = $endReal; $i <= 7; $i++) {
                $firstOfNext = strtotime($nextMonth);
                $blup = strtotime('+'.$i.' day', $firstOfNext);
                $entries = $this->getTimestampForUser(date('Y-m-d', $blup), $user_id, $holidays);
                if($entries) {
                    array_push($datesAfterLast, $entries);
                }
            }*/

            //$returner .= '</div>';
        }
        $returnArray['work'] = $work;
        $returnArray['tracking'] = $tracking;
        return $returnArray;
        //return $returner;
        } catch(Exception $e) {
            return $e->getMessage();
        }
    }

    public function getSingleDepartment(Request $request) {
        //$times = '<p>1. ' . date('h:i:s').'</p>';
        $times ='';
        $input = $request->all();
        $department = $input['department'];
        $month = $this->monthHandling($input);
        if($department && $department !== 'none') {
            $theDep = Department::where('id', $department)->first();
        }
        else {
            $theDep = false;
        }
        $prev = $input['prev'];
        $next = $input['next'];
        if(array_key_exists('page', $input)) {
            $page = trim($input['page']);
        }
        else {
            $page = false;
        }
        setlocale(LC_TIME, 'de_DE');
        $time = mktime(0,0,0, $month);
        $monthString = date('F', $time);
        $returnArray = array();
        if(array_key_exists('letter', $input)) {
            $letter = trim($input['letter']);
            $employee = Mitarbeiter::where([
                ['last_name', 'LIKE', $letter.'%'],
                ['department_relation_id_id', $department]
            ])->get();
        }
        elseif ($input['fromNew'] && isset($input['user_id'])) {
            $employee = Mitarbeiter::where('id', $input['user_id'])->first();
        }
        else {
            $employee = Mitarbeiter::where('department_relation_id_id', $department)->get();
        }
        $employArray = $employee->toArray();
        if(count($employArray) == 0) {
            return "FALSE";
        }
        if(array_key_exists('year', $input)) {
            $year = trim($input['year']);
        }
        else {
            $year = date("Y");
        }

        //return $month;
        if($prev == 'true') {
            if($month == 0) {
                $yearsToSub = 1;
                $month = 12;
                $newYear = strtotime($year.'-01-01' . " -1 year");
                $year = date("Y", $newYear);

                //return $year.' '.$month.' '.$oldYear;
            }
            //return $month;
        }
        else if($next == 'true') {
            if($month > 12 ) {
                $newYear = strtotime($year.'-01-01' . " +1 year");
                //return $year . '  ' .$yearsToAdd;
                $month = 1;
                $year = date("Y", $newYear);
                //return $year;
            }
        }

        $holidays = $this->getHolidays($year);
        setlocale(LC_TIME, 'de_DE');
        if(!isset($input['fromNew'])) {
            usort($employArray, function($a, $b){return strcmp(strtolower($a['last_name']), strtolower($b['last_name']));});
        }
        else if(!$input['fromNew']){
            usort($employArray, function($a, $b){return strcmp(strtolower($a['last_name']), strtolower($b['last_name']));});
        }
        $startValue = 0;

        //$times .= '<p>2. ' . date('h:i:s').'</p>';
        if($page > 1) {
            $startValue = $page * 5 - 5;
        }
        $bla = array();
        if(isset($input['fromNew']) && $input['fromNew']) {
            $lol = $this->getEmployeeForDepartmentNew($employArray['id'], $month, $holidays, $year);
            array_push($bla, $lol);
            $returnArray['page'] = 1;
            $returnArray['month'] = intval($month);
            if($theDep) {
                $returnArray['department'] = $theDep->name;
            }
            $returnArray['year'] = intval($year);
            $returnArray['employees'] =  $bla;
            $returnArray['pages'] = 0;

            // array_push($returnArray, $times);
            return $returnArray;
        }
        else {
            if($page) {
                for($i = $startValue; $i < ($page*5); $i++) {
                    if($i < count($employArray) && isset($employArray)) {
                        $lol = $this->getEmployeeForDepartmentNew($employArray[$i]['id'], $month, $holidays, $year);
                        //$lol = $this->getEmployeeForDepartment($employArray[$i]['id'], $month, $holidays, $year);
                        array_push($bla, $lol);
                    }
                }
            }
            else {
                foreach($employArray as $x => $item) {
                    //$lol = $this->getEmployeeForDepartment($item['id'], $month, $holidays, $year);
                    $lol = $this->getEmployeeForDepartmentNew($item['id'], $month, $holidays, $year);
                    array_push($bla, $lol);
                }
            }
        }
        $returnArray['page'] = intval($page);
        $returnArray['month'] = intval($month);
        $returnArray['department'] = $theDep;
        $returnArray['year'] = intval($year);
        $returnArray['employees'] =  $bla;
        $returnArray['pages'] = intval(ceil(count($employArray) / 5));
        if(count($employArray) > ($page * 5)) {
            //$pages = '<button class="timedisplay--btn btn-page--plus" onClick="loadNext(&#39;'.$page.'&#39;)" data-week="next" data-week_value="'.($month).'"><i class="fas fa-arrow-down"></i></button>';
            // array_push($returnArray, $pages);
        }

       // array_push($returnArray, $times);
        return $returnArray;
    }

    public function calculateSumWork($array) {
        $sum = strtotime('00:00:00');
        $totalTime = 0;
        foreach($array as $x => $item) {
            $timeinsec = strtotime($item) - $sum;
            $totalTime = $totalTime + $timeinsec;
        }
        $h = intval($totalTime/3600);
        $totalTime = $totalTime - ($h * 3600);
        $m = intval($totalTime / 60);
        $s = $totalTime - ($m * 60);

        return "$h:$m:$s";
    }



    public function getCurrentMonth() {
        $ddate = date("Y-m-d");
        $date = new \DateTime($ddate);
        $month = $date->format("m");
        return $month;
    }


    function roundUpToAny($n,$x=5) {
        return ceil($n / $x) * $x;
    }

    public $mitarbeiter;
}