<?php

namespace JannesNagelschmidt\Mitarbeiter\Console;

use JannesNagelschmidt\Mitarbeiter\Models\Mitarbeiter;
use JannesNagelschmidt\Mitarbeiter\Plugin;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Exchange\AMQPExchangeType;
use PhpAmqpLib\Message\AMQPMessage;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class Available extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'emp:available';

    /**
     * @var string The console command description.
     */
    protected $description = 'Sendet availabillity an rabbitmq';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $available = (new \JannesNagelschmidt\Mitarbeiter\Models\Mitarbeiter)->availabillity();
        $test = [
            "type"=> "EmployeeStatus",
            "context"=> "EmployeeStatus",
            "payload"=>  $available
        ];
        $json = json_encode($test);
        //var_dump($json);
        //$bla = self::MqSendMessageToQueue("ha-nodered-temp", $json);
        $blup = self::MqSendMessageToQueue("ha-dashboard-it-1", $json);
        $blup2 = self::MqSendMessageToQueue("ha-dashboard-lagerbuero", $json);
        $blup3 = self::MqSendMessageToQueue("ha-dashboard-pausenraum", $json);
        $blup4 = self::MqSendMessageToQueue("ha-dashboard-grossraum", $json);
        //var_dump($json);
    }

    //ha-dashboard-it-1

    private static function MqConnection() {
        return new AMQPStreamConnection("192.168.14.151", 5672, "emt", "Z8KiXTHbg8jvBwM", "monitoring");
    }

    public static function MqSendMessageToQueue($queue, $messageString) {
        try {
            $connection = self::MqConnection();
            $channel = $connection->channel();
            $message = new AMQPMessage($messageString, array('content_type' => 'text/plain', 'delivery_mode' => AMQPMessage::DELIVERY_MODE_NON_PERSISTENT));
            $channel->basic_publish($message, '', $queue);
            \Log::info('Message sent to Queue: ' . $queue . ' // ' . $messageString);
            $channel->close();
            $connection->close();
            return true;
        } catch (\Exception $e) {
            return $e->getMessage();
            trace_log($e);
            return false;
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

}