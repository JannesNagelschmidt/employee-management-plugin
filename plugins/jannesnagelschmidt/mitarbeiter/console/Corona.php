<?php

namespace JannesNagelschmidt\Mitarbeiter\Console;

use JannesNagelschmidt\Mitarbeiter\Plugin;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class Corona extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'emp:corona';

    /**
     * @var string The console command description.
     */
    protected $description = 'Gibt Corona Zahlen';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        //(new \BHF\HolidayRenting\Plugin)->getJobs();
        Plugin::saveCoronaNumber();


        $this->info("juhu");
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

}