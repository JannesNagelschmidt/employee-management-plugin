<?php

namespace JannesNagelschmidt\Mitarbeiter\Console;

use JannesNagelschmidt\Mitarbeiter\Models\Mitarbeiter;
use JannesNagelschmidt\Mitarbeiter\Plugin;
use Illuminate\Console\Command;
use Models;
use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Redirect;
use Input;
use JannesNagelschmidt\Mitarbeiter\Models\Department;
use Mail;
use JannesNagelschmidt\Mitarbeiter\Models\Arbeitszeit;
use JannesNagelschmidt\Mitarbeiter\Models\ChangeRequests;
use October\Rain\Support\Facades\Flash;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class DepartmentCommand extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'emp:dep';

    /**
     * @var string The console command description.
     */
    protected $description = 'Macht Zauberei...';


    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $arbeiter = Mitarbeiter::all();
        foreach($arbeiter as $item) {
            $depOld = $item->department;
            if($depOld) {
                $depEntry = Department::where('name', $depOld)->first();
                if($depEntry) {
                    $item->department_relation_id_id = $depEntry->id;
                    $item->save();
                }
            }
        }
        echo "yooo";
        $this->info("juhu");

    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

}