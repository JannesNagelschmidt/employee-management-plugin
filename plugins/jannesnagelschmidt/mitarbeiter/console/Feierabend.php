<?php

namespace JannesNagelschmidt\Mitarbeiter\Console;

use Jannesnagelschmidt\Mitarbeiter\Components\Calculate;
use JannesNagelschmidt\Mitarbeiter\Models\Calculated;
use JannesNagelschmidt\Mitarbeiter\Plugin;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class Feierabend extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'emp:feierabend';

    /**
     * @var string The console command description.
     */
    protected $description = 'Feierabend wie das duftet...';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        //(new \BHF\HolidayRenting\Plugin)->getJobs();
        $blup = (new \Jannesnagelschmidt\Mitarbeiter\Components\Calculate)->onCalculate();
        Plugin::checkEveryoneLeftTheBuilding();
        echo "yooo";
        $this->info("juhu");

    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

}