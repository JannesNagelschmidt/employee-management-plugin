<?php namespace JannesNagelschmidt\Mitarbeiter\Controllers;

use Backend\Behaviors\ImportExportController;
use Backend\Classes\Controller;
use BackendMenu;
use Cassandra\Time;
use Event;
use Model;
use Jannesnagelschmidt\Mitarbeiter\FormWidgets\TimeDisplay;

class Employee extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\ReorderController',
        'Backend.Behaviors.ImportExportController',
        'Backend\Behaviors\RelationController' 
    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';
    public $relationConfig = 'config_relation.yaml';
    public $importExportConfig = 'config_import_export.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('JannesNagelschmidt.Mitarbeiter', 'Mitarbeiter');
    }
}
