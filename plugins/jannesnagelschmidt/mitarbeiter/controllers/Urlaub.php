<?php namespace JannesNagelschmidt\Mitarbeiter\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use JannesNagelschmidt\Mitarbeiter\Models\Arbeitszeit;
use Model;

class Urlaub extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController',        'Backend\Behaviors\ReorderController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('JannesNagelschmidt.Mitarbeiter', 'Mitarbeiter', 'side-menu-item5');
    }


}
