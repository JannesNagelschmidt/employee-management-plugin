<?php namespace Jannesnagelschmidt\Mitarbeiter\FormWidgets;

use Backend\Classes\FormWidgetBase;
use Illuminate\Http\Request;
use Backend\Classes\Controller;


/**
 * TimeDisplay Form Widget
 */
class TimeDisplay extends FormWidgetBase
{
    /**
     * Config attributes
     */
    protected $modelClass = null;
    protected $selectFrom = 'name';
    protected $pattern = 'text';
    protected $round = 5;
    public $sumForWeek = 0;

    /**
     * @inheritDoc
     */
    protected $defaultAlias = 'jannesnagelschmidt_mitarbeiter_time_display';

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->fillFromConfig([
            'modelClass',
            'selectFrom',
            'pattern'
        ]);
        $this->assertModelClass();

        parent::init();
    }

    protected function assertModelClass()
    {
        if( !isset($this->modelClass) || !class_exists($this->modelClass) )
        {
            throw new \InvalidArgumentException(sprintf("Model class {%s} not found.", $this->modelClass));
        }
    }
    

    /**
     * @inheritDoc
     */
    public function render()
    {
        $this->prepareVars();
        return $this->makePartial('timedisplay');
    }

    /**
     * Prepares the form widget view data
     */
    public function prepareVars()
    {
        $this->vars['name'] = $this->formField->getName();
        $this->vars['value'] = $this->getLoadValue();
        $this->vars['model'] = $this->model;
        $this->vars['currentweek'] = $this->GetCurrentWeek();
        $this->vars['output'] = $this->onGetTemplate();
        $this->vars['week'] = $this->GetCurrentWeek();
    }

    public function getModelsFromWidget() {
        return $this->model;
    }

    /**
     * @inheritDoc
     */
    public function loadAssets()
    {
        $this->addCss('css/timedisplay.css', 'Jannesnagelschmidt.Mitarbeiter');
        $this->addCss('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css', 'Jannesnagelschmidt.Mitarbeiter');
        $this->addJs('js/timedisplay.js', 'Jannesnagelschmidt.Mitarbeiter');
        $this->addJs('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/js/all.min.js', 'Jannesnagelschmidt.Mitarbeiter');
    }

    /**
     * @inheritDoc
     */
    public function getSaveValue($value)
    {
        return $value;
    }

    public function onTestFunction() {
        return "lol";
    }

    function dateDifference($start_date, $end_date)
    {
        // calulating the difference in timestamps
        $diff = strtotime($start_date) - strtotime($end_date);

        // 1 day = 24 hours
        // 24 * 60 * 60 = 86400 seconds
        return ceil(abs($diff / 86400));
    }

    public function getHolidays() {
        $year = date('Y');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://date.nager.at/api/v2/publicholidays/'.$year.'/DE');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $holidayList = curl_exec($ch);
        curl_close($ch);

        $jsonHoliday = json_decode($holidayList, true);

        return $jsonHoliday;

    }

    public function getSumForWeek($weekArray, $itemArray) {
        $endSumme = array();
        foreach($itemArray as $x => $item) {
            //echo $item;
            if(array_key_exists($item, $weekArray)) {
                $sumBreaks = new \DateTime('00:00');
                foreach($weekArray[$item] as $y => $day) {
                    if(in_array('coming', $day) || in_array('leaving', $day)) {
                        if($day['state'] == 'coming') {
                            $coming = $day['time'];
                        }
                        if($day['state'] == 'leaving') {
                            $leaving = $day['time'];
                        }
                        if($day['state'] == 'break_start') {
                            $break_start = $day['time'];
                        }
                        if($day['state'] == 'break_end') {
                            $break_end = $day['time'];
                            $breakInterval = $this->dateIntervalCalc($break_end, $break_start);
                            $sumBreaks->add($breakInterval);
                        }
                        if(($y+1) == (count($weekArray[$item]))) {
                            if(isset($coming) && isset($leaving)) {
                                $sumDay = $this->dateIntervalCalc($coming, $leaving);
                                if(isset($break_start) && isset($break_end)) {
                                    $value2 = new \DateTime($sumDay->format('%H:%i:%s'));
                                    $value3 = $sumBreaks;
                                    $sumWithoutBreak = $this->dateIntervalCalc($value3, $value2);
                                    array_push($endSumme, $sumWithoutBreak);
                                }
                                else {
                                    $sumWithoutBreak = $sumDay;
                                    $value2 = new \DateTime($sumDay->format('%H:%i:%s'));
                                    if($sumDay->format('%H') > 5) {
                                        $breakDiffer = new \DateTime('00:30');
                                        $sumWithoutBreak = $this->dateIntervalCalc($breakDiffer, $value2);
                                    }
                                    array_push($endSumme, $sumWithoutBreak);
                                }
                            }
                        }
                    }
                }
            }
        }
        return $endSumme;
    }

    public function dateIntervalCalc($firstDate, $secondDate) {
        $value1 = new \DateTime('00:00');


        $value1 = $secondDate->diff($firstDate);

        return $value1;
    }

    public function onGetTemplate($timestampPara = NULL, $vacationPara = NULL, $sickPara = NULL) {
        $output = '';
        $timestamps = $this->model->timestamp;
        $vacations = $this->model->vacation;
        $sick = $this->model->sick_employee;
        if($timestampPara === NULL) {
            $timestamps = $this->model->timestamp;
        }
        if(!$vacationPara) {
            $vacations = $this->model->vacation;
        }
        if(!$sickPara) {
            $sick = $this->model->sick;
        }


        $ddate = date("Y-m-d");
        $date = new \DateTime($ddate);
        $vacStartArray = array();
        $vacEndArray = array();
        $sickStartArray = array();
        $sickEndArray = array();
        $germanyHolidays = $this->getHolidays();



        
        if(post('week')) {
            $week = post('week');
        }
        else if(post('specificWeek')) {
            $week = post('specificWeek');
        }
        else {
            $week = $this->GetCurrentWeek();
        }

        $displayYear = $this->GetCurrentYear();
        $dateBla = new \DateTime();
        $displayWeekStart = $dateBla->setISODate($displayYear, $week);
        $displayWeekStartTimestamp = $displayWeekStart->getTimestamp();
        $currentDatesArray = array();
        $javascriptDateArray = array();
        for ($i=0; $i < 7; $i++) { 
            $blup = date('d-m-Y', strtotime('+'.$i.' day', $displayWeekStartTimestamp));
            $bla = date('Y-m-d', strtotime('+'.$i.' day', $displayWeekStartTimestamp));
            array_push($currentDatesArray, $blup);
            array_push($javascriptDateArray, $bla);
        }

        //$blup = date('d-M-Y', strtotime('+1 day', $displayWeekStartTimestamp));
        //var_dump($currentDatesArray);

        $weekArray = array();

        if($timestamps) {
            foreach($timestamps as $x => $item) {
                $timestampDate = $item->timestamp;
                $state = $item->status;
                $test = new \DateTime($timestampDate);
                $weekTest = $test->format("W");
                $dayTest = $test->format("D");
                $tempArray = array();
                if($week == $weekTest) {
                    $tempArray['state'] = $item->status;
                    $tempArray['time'] = $test;
                    $weekArray[$dayTest][] = $tempArray;

                }
            }
        }

        $item = array();
        array_push($item, 'Mon');
        array_push($item, 'Tue');
        array_push($item, 'Wed');
        array_push($item, 'Thu');
        array_push($item, 'Fri');
        array_push($item, 'Sat');
        array_push($item, 'Sun');

        $day = $date->format("D");
        $output .= '<div class="timedisplay--header-wrapper">';
        $output .= "<h4 id='currentWeek' value='".$week."' style='text-align: center;'>WOCHE: ".$week."</h4>";
        $blaupapapa = $this->getSumForWeek($weekArray, $item);
        $endHours = 0;
        $endMinutes = 0;
        $endSeconds = 0;
        $endResultWeek = 0;
        foreach ($blaupapapa as $x => $sum) {
            $hours = (int)$sum->format('%H');
            $minutes = (int)$sum->format('%i');
            $seconds = (int)$sum->format('%s');
            $endHours = $endHours + $hours;
            if(($minutes + $endMinutes) > 59) {
                $moreThanOneHour = $minutes + $endMinutes;
                $realMinutes = $moreThanOneHour - 60;
                $endHours = $endHours + 1;
                $endMinutes = $realMinutes;
            }
            else {
                $endMinutes = $endMinutes + $minutes;
            }
            if(($seconds + $endSeconds) > 59) {
                $moreThanOneMinute = $seconds + $endSeconds;
                $realSeconds = $moreThanOneMinute - 60;
                $endMinutes = $endMinutes +1;
                $endSeconds = $realSeconds;
            }
            else {
                $endSeconds = $endSeconds + $seconds;
            }
        }
        $endMinutes = $this->roundDownToAny($endMinutes, $this->round);
        if($endMinutes < 10) {
            $endMinutes = '0'.$endMinutes;
        }
        if($endSeconds < 10) {
            $endSeconds = '0' . $endSeconds;
        }
        $endResultWeek = $endHours . ':' . $endMinutes . ':' . $endSeconds;
        $output .= '<p style="text-align: center">SUMME GESAMT: '.$endResultWeek. '</p>';
        $output .= '<div class="specifictime--wrapper">';
        $output .= '<label class="timedisplay--specificweek-label">Gehe zu Woche:</label>';
        $output .= '<input type="number" name="specificWeek" data-request="onGetTemplate" data-request-success="updateCalendar(this, context, data, textStatus, jqXHR)" id="specificweek" value="" placeholder="'.$week.'">';
        $output .= '<button id="submitspecific" data-request="onGetTemplate" data-request-success="updateCalendar(this, context, data, textStatus, jqXHR)" data-request-data="">Bestätigen</button>';
        $output .= '<button id="jumpToCurrent" data-request="onGetTemplate" data-request-success="updateCalendar(this, context, data, textStatus, jqXHR)" data-request-data="week: '.$this->GetCurrentWeek().'">Aktuelle Woche</button>';
        $output .= '</div>';
        $output .= '</div>';

        foreach($vacations as $x => $vac) {
            $dateTimeStart = new \DateTime($vac->start_date);
            $dateTimeEnd = new \DateTime($vac->end_date);
            $holidayStartWeek = $dateTimeStart->format('W');
            $holidayEndWeek = $dateTimeEnd->format('W');
            $start = explode(' ', $vac->start_date);
            $end = explode(' ', $vac->end_date);
            $interval = $dateTimeEnd->diff($dateTimeStart);
            $vacSUM = 1+intval($this->dateDifference($dateTimeStart->format('Y-m-d'), $dateTimeEnd->format('Y-m-d')));
            $output .= '<input hidden="true" class="vacations--value vacations--sum" id="sumVac-'.$x.'" data-sumVac="'.$vacSUM.'" >';
            $output .= '<input hidden="true" class="vacations--value vacations--start" id="start_date-'.$x.'" data-start="'.$start[0].'" >';
            $output .= '<input hidden="true" class="vacations--value vacations--end" id="end_date-'.$x.'" data-end="'.$end[0].'" >';
            if($holidayEndWeek == $week || $holidayStartWeek == $week) {
                $output .= '<p class="vac--text">Urlaub vom '.$start[0].' bis '.$end[0].'</p>';
                if($vac->vacation_days) {
                    $output .= '<p class="vac--text">Urlaubstage: ' . $vac->vacation_days. '</p>';
                }
                else {
                    $output .= '<p class="vac--text">Urlaubstage: ' . $this->getActualHolidays($vac->start_date, $vac->end_date) . '</p>';
                }
            }
            array_push($vacStartArray, $start[0]);
            array_push($vacEndArray, $end[0]);
        }
        if($sick) {
            foreach($sick as $x => $sickItem) {
                $dateTimeStart = new \DateTime($sickItem->start_day);
                $dateTimeEnd = new \DateTime($sickItem->end_day);
                $sickStartWeek = $dateTimeStart->format('W');
                $sickEndWeek = $dateTimeEnd->format('W');
                $start = explode(' ', $sickItem->start_day);
                $end = explode(' ', $sickItem->end_day);
                $interval = $dateTimeEnd->diff($dateTimeStart);
                $sickSUM = 1+intval($this->dateDifference($dateTimeStart->format('Y-m-d'), $dateTimeEnd->format('Y-m-d')));
                $output .= '<input hidden="true" class="sick--value sick--sum" id="sumSick-'.$x.'" data-sumSick="'.$sickSUM.'" >';
                $output .= '<input hidden="true" class="sick--value sick--start" id="sick_start_date-'.$x.'" data-start="'.$start[0].'" >';
                $output .= '<input hidden="true" class="sick--value sick--end" id="sick_end_date-'.$x.'" data-end="'.$end[0].'" >';
                if($sickEndWeek == $week || $sickStartWeek == $week) {
                    $output .= '<p class="sick--text">Krank vom '.$start[0].' bis '.$end[0].'</p>';
                    if($sickItem->sick_days) {
                        $output .= '<p class="vac--text">Krankheitstage: ' . $sickItem->sick_days. '</p>';
                    }
                    else {
                        $output .= '<p class="vac--text">Krankheitstage: ' . $this->getActualHolidays($sickItem->start_day, $sickItem->end_day) . '</p>';
                    }
                }
                array_push($sickStartArray, $start[0]);
                array_push($sickEndArray, $end[0]);
            }
        }



        $output .= '<div class="timedisplay--outer-wrapper">';

        $output .= '<button class="timedisplay--btn btn-prev" data-week="prev" data-request="onGetTemplate" data-request-success="updateCalendar(this, context, data, textStatus, jqXHR)" data-request-data="week: '.($week-1).'"><i class="fas fa-arrow-left"></i></button>';
        $output .= '<div id="timedisplay" class="timedisplay--wrapper">';

        foreach($item as $x => $item) {
            switch($item) {
                case 'Mon':

                    $output .= '<div class="timedisplay--day-wrapper">';
                    $output.= '<h4 class="timedisplay--day-headline">Montag '.$currentDatesArray[$x].'</h4>';
                    $output .= '<p class="public--holiday">'.$this->checkPublicHolidays($javascriptDateArray[$x], $germanyHolidays).'</p>';
                    $output .= '<div data-vac="'.$javascriptDateArray[$x].'" data-day="'.$item.'" class="timedisplay--graph">';
                    $output .= $this->renderWorkHourGraphs($weekArray, $item);
                    $output .= '</div>';
                    $output .= $this->renderWorkTime($weekArray, $item);
                    $output .= '</div>';
                    break;
                case 'Tue':
                    $output .= '<div class="timedisplay--day-wrapper">';
                    $output.= '<h4 class="timedisplay--day-headline">Dienstag '.$currentDatesArray[$x].'</h4>';
                    $output .= '<p class="public--holiday">'.$this->checkPublicHolidays($javascriptDateArray[$x], $germanyHolidays).'</p>';
                    $output .= '<div data-vac="'.$javascriptDateArray[$x].'" data-day="'.$item.'" class="timedisplay--graph">';
                    $output .= $this->renderWorkHourGraphs($weekArray, $item);
                    $output .= '</div>';
                    $output .= $this->renderWorkTime($weekArray, $item);
                    $output .= '</div>';
                    break;
                case 'Wed':
                    $output .= '<div class="timedisplay--day-wrapper">';
                    $output.= '<h4 class="timedisplay--day-headline">Mittwoch '.$currentDatesArray[$x].'</h4>';
                    $output .= '<p class="public--holiday">'.$this->checkPublicHolidays($javascriptDateArray[$x], $germanyHolidays).'</p>';
                    $output .= '<div data-vac="'.$javascriptDateArray[$x].'" data-day="'.$item.'" class="timedisplay--graph">';
                    $output .= $this->renderWorkHourGraphs($weekArray, $item);
                    $output .= '</div>';
                    $output .= $this->renderWorkTime($weekArray, $item);
                    $output .= '</div>';
                    break;
                case 'Thu':
                    $output .= '<div class="timedisplay--day-wrapper">';
                    $output.= '<h4 class="timedisplay--day-headline">Donnerstag '.$currentDatesArray[$x].'</h4>';
                    $output .= '<p class="public--holiday">'.$this->checkPublicHolidays($javascriptDateArray[$x], $germanyHolidays).'</p>';
                    $output .= '<div data-vac="'.$javascriptDateArray[$x].'" data-day="'.$item.'" class="timedisplay--graph">';
                    $output .= $this->renderWorkHourGraphs($weekArray, $item);
                    $output .= '</div>';
                    $output .= $this->renderWorkTime($weekArray, $item);
                    $output .= '</div>';
                    break;
                case 'Fri':
                    $output .= '<div class="timedisplay--day-wrapper">';
                    $output.= '<h4 class="timedisplay--day-headline">Freitag '.$currentDatesArray[$x].'</h4>';
                    $output .= '<p class="public--holiday">'.$this->checkPublicHolidays($javascriptDateArray[$x], $germanyHolidays).'</p>';
                    $output .= '<div data-vac="'.$javascriptDateArray[$x].'" data-day="'.$item.'" class="timedisplay--graph">';
                    $output .= $this->renderWorkHourGraphs($weekArray, $item);
                    $output .= '</div>';
                    $output .= $this->renderWorkTime($weekArray, $item);
                    $output .= '</div>';
                    break;
                case 'Sat':
                    $output .= '<div class="timedisplay--day-wrapper">';
                    $output.= '<h4 class="">Samstag '.$currentDatesArray[$x].'</h4>';
                    $output .= '<p class="public--holiday">'.$this->checkPublicHolidays($javascriptDateArray[$x], $germanyHolidays).'</p>';
                    $output .= '<div data-vac="'.$javascriptDateArray[$x].'" data-day="'.$item.'" class="timedisplay--graph">';
                    $output .= $this->renderWorkHourGraphs($weekArray, $item);
                    $output .= '</div>';
                    $output .= $this->renderWorkTime($weekArray, $item);
                    $output .= '</div>';
                    break;
                case 'Sun':
                    $output .= '<div class="timedisplay--day-wrapper">';
                    $output.= '<h4 class="">Sonntag '.$currentDatesArray[$x].'</h4>';
                    $output .= '<p class="public--holiday">'.$this->checkPublicHolidays($javascriptDateArray[$x], $germanyHolidays).'</p>';
                    $output .= '<div data-vac="'.$javascriptDateArray[$x].'" data-day="'.$item.'" class="timedisplay--graph">';
                    $output .= $this->renderWorkHourGraphs($weekArray, $item);
                    $output .= '</div>';
                    $output .= $this->renderWorkTime($weekArray, $item);
                    $output .= '</div>';
                    break;
            }
        }

        $output .= '</div>';
        $output .= '<button class="timedisplay--btn btn-next" data-week="next" data-request="onGetTemplate" data-request-success="updateCalendar(this, context, data, textStatus, jqXHR)" data-request-data="week: '.($week+1).'"><i class="fas fa-arrow-right"></i></button>';
        $output .= '</div>';

        return $output;
    }

    public function checkPublicHolidays($date, $germanHolidays) {
        if($germanHolidays) {
            foreach($germanHolidays as $x => $item) {
                if ($date === $item['date']) {
                    return $item['localName'];
                }
            }
        }
    }

    public function renderWorkHourGraphs($weekArray = null, $item) {
        $output = '';
        $startTime = 0;
        $endTime = 0;
        $startMinute = 0;
        $endMinute = 0;
        for($i = 5; $i < 23; $i++) {
            if(isset($weekArray[$item])) {
                foreach($weekArray[$item] as $x => $times) {
                    $date = $times['time'];
                    $hour = $date->format('H');
                    $minutes = $date->format('i');
                    if($times['state'] === 'coming' && $hour == $i) {
                        $startTime = $hour;
                        $startMinute = $minutes;
                    }
                    if($times['state'] === 'leaving' && $hour == $i) {
                        $endTime = $hour;
                        $endMinute = $minutes;
                    }
                }
            }
        }

        for($i = 5; $i < 23; $i++) {
            if(isset($weekArray[$item])) {
                foreach($weekArray[$item] as $x => $times) {
                    if($times['state'] === 'coming' && $startTime == $i) {
                        $output .= '<div data-day="'.$item.'" class="'.$this->getClassForMinutes($startMinute).'  start--time timedisplay--hour" data-hour="'.$i.'">'.$i.'</div>';
                    }
                    if($times['state'] === 'leaving' && $endTime == $i && $startTime != $endTime) {
                        $output .= '<div data-day="'.$item.'" class="end--time timedisplay--hour" data-hour="'.$i.'">'.$i.'</div>';
                    }
                }
            }
            if($i == $endTime+1) {
                $output .= '<div data-day="'.$item.'" class="'.$this->getClassForMinutes($endMinute).' end--time-started timedisplay--hour" data-hour="'.$i.'">'.$i.'</div>';
            }
            else {
                if($i != $startTime && $i != $endTime) {
                    if($i > $startTime && $i < $endTime) {
                        $output .= '<div data-day="'.$item.'" class="timedisplay--hour timedisplay--hour-work" data-hour="'.$i.'">'.$i.'</div>';
                    }
                    else {
                        $output .= '<div data-day="'.$item.'" class="timedisplay--hour" data-hour="'.$i.'">'.$i.'</div>';
                    }
                }
            }
        }
        return $output;
    }

    public function getClassForMinutes($minutes) {
        $returner = '';
        $minutes = (int)$minutes;
        if($minutes > 55 || $minutes < 5) {
            $returner = 'full-Hour--nothing';
        }
        else if($minutes > 45) {
            $returner = 'full-Hour';
        }
        else if($minutes > 30) {
            $returner = 'three-quart-Hour';
        }
        else if($minutes > 15) {
            $returner = 'half-Hour';
        }
        else if($minutes > 0) {
            $returner = 'one-quart-Hour';
        }
        return $returner;
    }


    public function renderWorkTime($weekArrayParameter, $itemParameter = 0) {
        $weekArray = $weekArrayParameter;
        $item = $itemParameter;
        $firstBreak = true;
        $lastBreak = false;
        $forgotToLeave = false;
        $sumWeek = 0;
        $output = '';

        if(array_key_exists($item, $weekArray)) {
            $output = '<div class="timedisplay--stats-wrapper">';
            $sumBreakall = new \DateTime('00:00:00');
            $sumBreak = new \DateTime('00:00');
            $numberOfBreaks = 0;
            foreach($weekArray[$item] as $y => $day) {
                if($day['state'] == 'break_end') {
                    $numberOfBreaks++;
                }
            }

            $breakEndCounter = 0;
            foreach($weekArray[$item] as $y => $day) {
                if($day['state'] == 'forgot') {
                    $forgotToLeave = true;
                }
                if($day['state'] == 'coming') {
                    $coming = $day['time'];
                    $comingReal = new \DateTime('00:00');
                    $comingReal->setTime($coming->format('H'), $coming->format('i'), $coming->format('s'));
                    $startMinutes = $day['time']->format('i');
                    $startMinutes = $this->roundUpToAny($startMinutes, $this->round);
                    $sumWork = $coming;
                    $coming->setTime($coming->format('H'), $startMinutes, $coming->format('s'));
                    $output .= '<p data-day="'.$item.'" value="'.$coming->format('H:i:s').'" class="timedisplay--text timedisplay--start">Start: <span class="timedisplay--time">'.$coming->format('H:i:s').'</span></p>';
                }
                if($day['state'] == 'leaving') {
                    $leaving = $day['time'];
                    $leavingReal = new \DateTime('00:00');
                    $leavingReal->setTime($leaving->format('H'), $leaving->format('i'), $leaving->format('s'));
                    $leaveMinutes = $day['time']->format('i');
                    $leaveMinutes = $this->roundDownToAny($leaveMinutes, $this->round);
                    $leaving->setTime($leaving->format('H'), $leaveMinutes, $leaving->format('s'));
                    $output .= '<p class="timedisplay--end timedisplay--text" value="'.$leaving->format('H:i:s').'">Ende: <spanclass="timedisplay--time">'.$leaving->format('H:i:s').'</span></p>';
                }
                if($day['state'] == 'break_start') {
                    if($firstBreak) {
                        $output .= '<details class="break--wrapper">';
                        $output .= '<summary>Pausen</summary>';
                        $firstBreak = false;
                    }
                    $break_start = $day['time'];
                    $break_startReal = new \DateTime('00:00');
                    $break_startReal->setTime($break_start->format('H'), $break_start->format('i'), $break_start->format('s'));
                    $breakstartMinutes = $day['time']->format('i');
                    $breakstartMinutes = $this->roundDownToAny($breakstartMinutes, $this->round);
                    $break_start->setTime($break_start->format('H'), $breakstartMinutes, $break_start->format('s'));
                    $output .= '<p data-day="'.$item.'" class="timedisplay--break timedisplay--break_start timedisplay--text" value="'.$break_start->format('H:i:s').'">Pause Start: <span class="timedisplay--time">'.$break_start->format('H:i:s').'</span></p>';
                }
                if($day['state'] == 'break_end') {
                    $breakEndCounter++;
                    $break_end = $day['time'];
                    $break_endReal = new \DateTime('00:00');
                    $break_endReal->setTime($break_end->format('H'), $break_end->format('i'), $break_end->format('s'));
                    $breakendMinutes = $day['time']->format('i');
                    $breakendMinutes = $this->roundUpToAny($breakendMinutes, $this->round);
                    $break_end->setTime($break_end->format('H'), $breakendMinutes, $break_end->format('s'));
                    $output .= '<p value="'.$break_end->format('H:i:s').'" class="timedisplay--break timedisplay--break_end timedisplay--text">Pause Ende: <span class="timedisplay--time">'.$break_end->format('H:i:s').'</span></p>';
                    if($breakEndCounter == $numberOfBreaks) {
                        $output .= '</details>';
                    }
                    if(isset($break_startReal)) {
                        $sumBreak = $break_startReal->diff($break_endReal);
                        $sumBreakall->add($sumBreak);
                    }
                }
            }
            if(isset($leaving) && isset($coming) && !$forgotToLeave) {
                $interval1 = $leavingReal->diff($comingReal);
                $breakIntervall = new \DateTime('00:00');
                $minBreak = new \DateTime('00:30');
                //var_dump($sumBreakall);
                //print_r((int)$sumBreakall->format('i'));
                if((int)$sumBreakall->format('i') <= 30 && (int)$interval1->format('%H') >= 6 && (int)$sumBreakall->format('H') < 1) {
                    $diff2 = $breakIntervall->diff($minBreak);
                }
                else {
                    $diff2 = $breakIntervall->diff($sumBreakall);
                }
                $interval2 = new \DateTime('00:00');
                $diff = $interval2->add($interval1)->add($diff2);
                $sum = new \DateTime('00:00');
                $end = $sum->diff($diff);
                $compare1 = new \DateTime('00:00');
                $compare2 = new \DateTime('00:00');
                $compareBreak = new \DateTime('00:00');
                $sixHours = new \DateInterval('PT6H');
                $breakCompare = new \DateInterval('PT30M');
                $sixHours->invert = 1;
                $compare1->add($end);
                $compare2->add($sixHours);
                $compareBreak->add($breakCompare);
                $minutes = $end->format('%i');
                $breakMinutes = $sumBreakall->format('i');
                $minutesWB = $interval1->format('%i');
                $startHoursCompare = $coming->format('H');
                $startMinutesCompare = $coming->format('i');
                $endHoursCompare = $leaving->format('H');
                $endMinutesCompare = $leaving->format('i');
                $minutesCompared = '00';
                if(((int)$endMinutesCompare - (int)$startMinutesCompare) < 10) {
                    $minutesCompared = ((int)$endMinutesCompare - (int)$startMinutesCompare);
                }
                else {
                    $minutesCompared = (int)$endMinutesCompare - (int)$startMinutesCompare;
                }
                if($minutesCompared < 0) {
                    $minutesCompared = '00';
                }
                $sumWithOutBreak = ((int)$endHoursCompare - (int)$startHoursCompare).':'.$minutesCompared.':00';

                $minutesWB = $this->roundDownToAny($end->format('%i'), $this->round);
                $output .= '<p class="timedisplay--sum">Summe: '.$end->format('%H:'.$minutesWB.':%s').'</p>';
                $this->sumForWeek = $this->sumForWeek + ((int)$end->format('%H'));
                //$output .= 'test'.$leavingReal->format('H:i:s');
                $minutes = $this->roundDownToAny($interval1->format('%i'), $this->round);
                $output .= '<p class="timedisplay--sum">Summe ohne Pause: '.$interval1->format('%H:'.$minutes.':%s').'</p>';

                if((int)$end->format('%H') >= 6 && $breakMinutes < 30 && (int)$sumBreakall->format('H') < 1) {
                    $output .= '<p data-sum="'.$breakCompare->format('%H:%i:%s').'" class="timedisplay--break-sum timedisplay--sum">Pause: '.$breakCompare->format('%H:%i:%s').'</p>';
                }
                else {
                    $breakMinutes = $this->roundUpToAny($sumBreakall->format('i'), $this->round);
                    $output .= '<p data-sum="'.$sumBreakall->format('H:'.$breakMinutes.':s').'" class="timedisplay--break-sum timedisplay--sum">Pause: '.$sumBreakall->format('H:'.$breakMinutes.':s').'</p>';
                    //$output .= '<p data-sum="'.$sumBreakall->format('H:'.$breakMinutes.':s').'" class="timedisplay--break-sum timedisplay--sum">Pause: '.$sumBreakall->format('H:i:s').'</p>';
                }
            }
            else {
                if($forgotToLeave) {
                    $output .= '<p class="timedisplay--sum" data-sum="0">Vergessen sich aus zu stempeln!</p>';
                }
                else {
                    $output .= '<p class="timedisplay--sum" data-sum="0">Noch nicht gegangen</p>';
                }
            }

            $output .= '</div>';
            return $output;
        }
        else {
            $output .= '<div class="timedisplay--stats-wrapper">';
            $output .= '</div>';
            return $output;
        }
    }

    public function getActualHolidays($startDate, $endDate) {
        $start = strtotime($startDate);
        $end = strtotime($endDate);

        $iter = 24*60*60;
        $count = 0;

        for($i = $start; $i <= $end; $i = $i+$iter) {
            if(Date('D', $i) != 'Sat' && Date('D', $i) != 'Sun') {
                $count++;
            }
        }
        return $count;
    }

    public function GetCurrentWeek() {
        $ddate = date("Y-m-d");
        $date = new \DateTime($ddate);
        $week = $date->format("W");
        return $week;
    }

    public function GetCurrentYear() {
        $ddate = date("Y-m-d");
        $date = new \DateTime($ddate);
        $week = $date->format("Y");
        return $week;
    }

    public function onRenderWeek() {
        return "test yooo";
    }

    function roundUpToAny($n,$x=5) {
        return ceil($n / $x) * $x;
    }

    function roundDownToAny($n,$x=5) {
        return floor($n / $x) * $x;
    }
}
