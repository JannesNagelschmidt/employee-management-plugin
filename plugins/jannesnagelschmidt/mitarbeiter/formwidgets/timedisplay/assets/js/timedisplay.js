/*
 * This is a sample JavaScript file used by TimeDisplay
 *
 * You can delete this file if you want
 */
$(function() {

    $(window).on('load', function() {
        console.log("bla");
    });

    $(window).scroll(sticky);
    sticky();
    function sticky() {
        var window_top=$(window).scrollTop();
        var top_position=$('body').offset().top+510;
        var element_to_stick=$('.month--headline-wrapper');

        if (window_top > top_position) {
            element_to_stick.addClass('sticky');
        } else {
            element_to_stick.removeClass('sticky');
        }
    }



});

let holidayFunc = function () {
    var vacationDays = 0;
    var startVac;
    var endVac;
    var sumVac = 0;
    var startValues = [];
    var startValue = 0;
    var endValues = [];
    var endValue = 0;
    var vacThisWeek = false;
    var vacEndWeek = false;
    $('.vacations--value').each(function ($index) {
        if ($(this).hasClass("vacations--start")) {
            startVac = $(this).data('start');
            startValues.push(startVac);
            //console.log(startVac);
        } else if ($(this).hasClass('vacations--sum')) {
            sumVac = $(this).data('sumvac');
        } else {
            endVac = $(this).data('end');
            endValues.push(endVac);
        }
    });

    $('.timedisplay--graph').each(function ($index) {
        if (startValues.includes($(this).data('vac'))) {
            startValue = $index;
            vacThisWeek = true;
        }
        if (endValues.includes($(this).data('vac'))) {
            endValue = $index;
            vacEndWeek = true;
        }
    });
    if (vacThisWeek && vacEndWeek) {
        console.log('in start value loop');
        console.log(startValue);
        console.log(sumVac);
        for (var x = startValue; x <= endValue; x++) {
            $('.timedisplay--day-wrapper:eq(' + x + ')').addClass("has--holiday");
            $('.timedisplay--day-wrapper:eq(' + x + ')').find('.timedisplay--day-headline').append('</br> Urlaub');
            //timedisplay--day-headline
        }
    }
    else if(vacThisWeek) {
        console.log("in nur start");
        console.log(startValue);
        var days = $('.timedisplay--day-wrapper').length;
        for (var y = startValue; y < days; y++) {
            $('.timedisplay--day-wrapper:eq(' + y + ')').addClass("has--holiday");
            $('.timedisplay--day-wrapper:eq(' + y + ')').find('.timedisplay--day-headline').append('</br> Urlaub');

            console.log("huh")
        }
    }
    else if (vacEndWeek) {
        console.log('in end value loop');
        for (var i = endValue; i >= 0; i--) {
            $('.timedisplay--day-wrapper:eq(' + i + ')').addClass("has--holiday");
            $('.timedisplay--day-wrapper:eq(' + i + ')').find('.timedisplay--day-headline').append('</br> Urlaub');


        }
    }
};


let sickFunc = function() {
    var startSick;
    var endSick;
    var sumSick = 0;
    var startValue = 0;
    var startValues = [];
    var endValues = [];
    var endValue = 0;
    var sickThisWeek = false;
    var sickEndWeek = false;
    $('.sick--value').each(function ($index) {
        if ($(this).hasClass("sick--start")) {
            startSick = $(this).data('start');
            startValues.push(startSick);
        } else if ($(this).hasClass('sick--sum')) {
            sumSick = $(this).data('sumsick');
        } else {
            endSick = $(this).data('end');
            endValues.push(endSick);
        }
    });

    $('.timedisplay--graph').each(function ($index) {
        if (startValues.includes($(this).data('vac'))) {
            startValue = $index;
            sickThisWeek = true;
        }
        if (endValues.includes($(this).data('vac'))) {
            endValue = $index;
            sickEndWeek = true;
        }
    });

    if (sickThisWeek && sickEndWeek) {
        for (var i = startValue; i <= endValue; i++) {
            $('.timedisplay--day-wrapper:eq(' + i + ')').addClass("has--sick");
            $('.timedisplay--day-wrapper:eq(' + i + ')').find('.timedisplay--day-headline').append('</br> Krank');
        }
    } else if (sickThisWeek) {
        var days = $('.timedisplay--day-wrapper').length;
        for (var y = startValue; y < days; y++) {
            $('.timedisplay--day-wrapper:eq(' + i + ')').addClass("has--sick");
            $('.timedisplay--day-wrapper:eq(' + y + ')').find('.timedisplay--day-headline').append('</br> Krank');
        }
    }
    else if (sickEndWeek) {
        for (var i = endValue; i >= 0; i--) {
            $('.timedisplay--day-wrapper:eq(' + i + ')').addClass("has--sick");
            $('.timedisplay--day-wrapper:eq(' + i + ')').find('.timedisplay--day-headline').append('</br> Krank');
        }
    }
};