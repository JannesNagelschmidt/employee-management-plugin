<?php namespace JannesNagelschmidt\Mitarbeiter\Models;

use Model;

/**
 * Model
 */
class Arbeitszeit extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;



    /**
     * @var string The database table used by the model.
     */
    public $table = 'jannesnagelschmidt_mitarbeiter_timetracking';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'employee_relation' => [
            'Jannesnagelschmidt\Mitarbeiter\Models\Mitarbeiter',
            'key'   => 'user_id',
        ]
    ];

   

    public static function addTimetrack($id, $timestamp, $status, $source = null) {
        if($id && $timestamp && $status) {
            /* \DB::table('jannesnagelschmidt_mitarbeiter_timetracking')->insert(
                ['user_id' => $id, 'timestamp' => $timestamp, 'status' => $status]

            ); */
            $bla = new Arbeitszeit;
            $bla->user_id = $id;
            $bla->timestamp = $timestamp;
            $bla->status = $status;
            $bla->source = $source;
            $bla->lookup = date('Y-m-d', strtotime($timestamp));
            $bla->save();
        }
        else {
            return false;
        }
    }
}
