<?php namespace JannesNagelschmidt\Mitarbeiter\Models;

use Model;

/**
 * Model
 */
class Bereich extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    public $belongsTo = [
        'department_relation' => [
            'Jannesnagelschmidt\Mitarbeiter\Models\Department',
            'key' => 'department_id'
        ],
    ];

//department_relation
    /**
     * @var string The database table used by the model.
     */
    public $table = 'jannesnagelschmidt_mitarbeiter_subdepartment';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
