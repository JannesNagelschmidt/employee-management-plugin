<?php namespace JannesNagelschmidt\Mitarbeiter\Models;

use Model;

/**
 * Model
 */
class Calculated extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'jannesnagelschmidt_mitarbeiter_calculated';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

   /* public function beforeDelete() {

        return false;
    }*/

}
