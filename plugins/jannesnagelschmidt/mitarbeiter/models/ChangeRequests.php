<?php namespace JannesNagelschmidt\Mitarbeiter\Models;

use Model;

/**
 * Model
 */
class ChangeRequests extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;
    public $belongsTo = [
        'user_relation' => [
            'JannesNagelschmidt\Mitarbeiter\Models\Mitarbeiter',
            'table' => 'jannesnagelschmidt_mitarbeiter_talk_mitarbeiter',
            'key'   => 'user_id'
        ]
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jannesnagelschmidt_mitarbeiter_changerequest';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];


    public function scopeSortedNames($query) {
        return $query->orderBy('last_name', 'ASC');
    }
}
