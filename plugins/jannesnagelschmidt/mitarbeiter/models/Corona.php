<?php namespace JannesNagelschmidt\Mitarbeiter\Models;

use Model;

/**
 * Model
 */
class Corona extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jannesnagelschmidt_mitarbeiter_corona';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
