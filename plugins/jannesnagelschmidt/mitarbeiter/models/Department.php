<?php namespace JannesNagelschmidt\Mitarbeiter\Models;

use Model;

/**
 * Model
 */
class Department extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    public $belongsTo = [
        'leader_id' => [
            'JannesNagelschmidt\Mitarbeiter\Models\Mitarbeiter'
        ],

    ];
    /**
     * @var string The database table used by the model.
     */
    public $table = 'jannesnagelschmidt_mitarbeiter_department';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
