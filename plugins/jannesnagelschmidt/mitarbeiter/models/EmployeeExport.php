<?php namespace JannesNagelschmidt\Mitarbeiter\Models;

use BackendMenu;
use Backend\Classes\Controller;

use Backend\Models\ExportModel;
use JannesNagelschmidt\Mitarbeiter\Models\Mitarbeiter;
use RainLab\User\Models\User;

class EmployeeExport extends ExportModel
{
    public function exportData($columns, $sessionKey = null)
    {
        $employee = Mitarbeiter::all();
        $employee->each(function($employee) use ($columns) {
            $employee->addVisible($columns);
        });

        return $employee->toArray();
    }
}
