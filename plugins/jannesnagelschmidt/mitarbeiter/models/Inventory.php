<?php namespace JannesNagelschmidt\Mitarbeiter\Models;

use Model;

/**
 * Model
 */
class Inventory extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    protected $fillable = ['type'];



    /**
     * @var string The database table used by the model.
     */
    public $table = 'jannesnagelschmidt_mitarbeiter_inventory';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsToMany = [
        'inventories' => [
            'JannesNagelschmidt\Mitarbeiter\Models\Mitarbeiter',
            'table' => 'jannesnagelschmidt_mitarbeiter_inventory2employee',
            'key' => 'inventory_id',
            'pivot' => ['pivot_amount']
        ],

    ];

}
