<?php namespace JannesNagelschmidt\Mitarbeiter\Models;

use Carbon\Carbon;
use Cassandra\Time;
use Cms\Classes\ComponentBase;
use Jannesnagelschmidt\Mitarbeiter\Components\Timetrackfrontend;
use Model;
use DateTime;
use Illuminate\Http\Request;
use Jannesnagelschmidt\Mitarbeiter\Models\Token;
use Jannesnagelschmidt\Mitarbeiter\Models\Arbeitszeit;
use Symfony\Component\Ldap\Ldap;
use Mail;
use Input;
use Throwable;


/**
 * Model
 */
class Mitarbeiter extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\SoftDelete;

    public $settingsCode = 'JannesNagelschmidt_Mitarbeiter_Settings';
    public $settingsFields = 'fields.yaml';

    protected $jsonable = ['work_repeater', 'vacation_repeater', 'test_repeater', 'parent_time_repeater', 'emergency_repeater'];
    protected $dates = ['deleted_at'];
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = true;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'jannesnagelschmidt_mitarbeiter_';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
    public $attachOne = [
        'fileupload1' => 'System\Models\File',
        'imagePromo' => 'System\Models\File',
        'imageGroundplot' => 'System\Models\File',

    ];
    public $belongsTo = [
        'dptr' => [
            'Jannesnagelschmidt\Mitarbeiter\Models\Department',
            'key' => 'department_relation_id_id'
        ],
        'sdr' => [
            'Jannesnagelschmidt\Mitarbeiter\Models\Bereich',
            'key' => 'sub_department_relation_id'
        ]
    ];
    public $attachMany = [
        'documents' => 'System\Models\File',
        'protocols' => 'System\Models\File',
    ];
    public $hasMany = [
        'timestamp' => [
            'Jannesnagelschmidt\Mitarbeiter\Models\Arbeitszeit',
            'table' => 'jannesnagelschmidt_mitarbeiter_timetracking',
            'key' => 'user_id',
            'otherKey' => 'id',
        ],
        'relation2' => [
            'Jannesnagelschmidt\Mitarbeiter\Models\Token',
            'table' => 'jannesnagelschmidt_mitarbeiter_token',
            'key' => 'token',
            'otherKey' => 'token'
        ],
        'sick_employee' => [
            'Jannesnagelschmidt\Mitarbeiter\Models\Sick', 
            'table' => 'jannesnagelschmidt_mitarbeiter_sick',
            'key' => 'mitarbeiter_id'
        ],
        'vacation' => [
            'Jannesnagelschmidt\Mitarbeiter\Models\Urlaub', 
            'table' => 'jannesnagelschmidt_mitarbeiter_urlaub',
            'key' => 'mitarbeiter_id'
        ],
        'substitute' => [
            'Jannesnagelschmidt\Mitarbeiter\Models\Urlaub',
            'table' => 'jannesnagelschmidt_mitarbeiter_urlaub',
            'key' => 'mitarbeiter_id'
        ],
        'talks' => [
            'JannesNagelschmidt\Mitarbeiter\Models\Talk',
            'table' => 'jannesnagelschmidt_mitarbeiter_talk',
            'key' => 'mitarbeiter_id'
        ],
        'attendees' => [
            'JannesNagelschmidt\Mitarbeiter\Models\Talk',
            'table' => 'jannesnagelschmidt_mitarbeiter_talk',
            'key' => 'mitarbeiter_id'
        ],

    ];
    public $belongsToMany = [
      'talks' => [
          'JannesNagelschmidt\Mitarbeiter\Models\Talk'
      ],
        'inventory_relation' => [
            'JannesNagelschmidt\Mitarbeiter\Models\Inventory',
            'table' => 'jannesnagelschmidt_mitarbeiter_inventory2employee',
            'key' => 'user_id',
            /*'pivot' => ['pivot_amount']*/
        ],
    ];

    public function phpinfoFunc() {
        //return "test";
        phpinfo();
    }

    public function trackTime(Request $request) {
        $input = $request->all();

        $token = $input['token'];
        $state = $input['state'];
        if(array_key_exists('source', $input)) {
            $source = $input['source'];
        }
        else {
            $source = 'nix';
        }
        $mitarbeiter = Mitarbeiter::where('token', $token)->first();
        if($mitarbeiter) {
            $id = $mitarbeiter->id;
            $timestamp = time();
            $time = date('Y-m-d H:i:s', $timestamp);
            $converted = $this->convertTimeToUSERzone($time, 'Europe/Amsterdam');

            Arbeitszeit::addTimetrack($id, $converted, $state, $source);
            Token::updateTokenState($id, $token, $state);

            $returnArray = array();
            if($state == "break_end") {
                $theTimes = Arbeitszeit::where([['user_id', $id], ['lookup', date('Y-m-d', strtotime($converted))]])->get();
                $counts = count($theTimes);
                foreach($theTimes as $x => $item) {
                    if($item->status == 'break_start' && $item->lookup == date('Y-m-d', strtotime($time))) {
                        $breakStart = $item->timestamp;
                    }
                    if($x == $counts-1) {
                        $breakEnd = $item->timestamp;
                    }
                }
                $returnArray['start'] = $breakStart;
                $breakStartStr = strtotime($breakStart);
                $breakEndStr = strtotime($breakEnd);
                $breakTime = $breakEndStr - $breakStartStr;
                $hR = intval($breakTime/3600);
                $breakTime = $breakTime - ($hR * 3600);
                $mR = intval($breakTime / 60);
                //$returnArray['end'] = $breakEnd;
                $hr = (new \Jannesnagelschmidt\Mitarbeiter\Components\Timetrackfrontend)->lz($hR);
                $mr = (new \Jannesnagelschmidt\Mitarbeiter\Components\Timetrackfrontend)->roundUpToAny($mR, 1);
                $mr = (new \Jannesnagelschmidt\Mitarbeiter\Components\Timetrackfrontend)->lz($mr);
                return "$hr:$mr";
            }
            if($state == "leaving") {
                $theTimes = Arbeitszeit::where('user_id', $id)->get();
                $counts = count($theTimes);
                $startTime = 0;
                $endTime = 0;
                $breakTime = 0;
                $breakTooLow = true;
                $theBreaks = array();
                $lastBreakStart = 0;
                $lastBreakEnd = 0;
                $breakEnd = 0;
                $theBreakID = 0;
                $breakStart = 0;
                for($i = $counts-1; $i > 0; $i--) {
                    if($startTime == 0 && $theTimes[$i]->status == 'coming') {
                        $startTime = $theTimes[$i]->timestamp;
                    }
                    if($endTime == 0 && $theTimes[$i]->status == 'leaving') {
                        $endTime = $theTimes[$i]->timestamp;
                    }
                    if($theTimes[$i]->status == 'break_end' && $startTime == 0) {
                        $breakStart =   $theTimes[$i-1]->timestamp;
                        $breakEnd = $theTimes[$i]->timestamp;
                        $breakStartStr = strtotime($breakStart);
                        $breakEndStr = strtotime($breakEnd);
                        $currentBreak = $breakEndStr - $breakStartStr;
                        array_push($theBreaks, $currentBreak);
                        $currentBreakHour = date('H',  $breakEndStr - $breakStartStr);
                        if(date('i', $breakEndStr - $breakStartStr) >= 30 || $currentBreakHour > 0) {
                            $breakTooLow = false;
                        }
                        if($breakTime != 0) {
                            $breakTime = $breakTime + ($breakEndStr - $breakStartStr);
                        }
                        else {
                            $breakTime = $breakEndStr - $breakStartStr;
                        }
                    }
                }
                $startTimeStr = strtotime($startTime);
                $endTimeStr = strtotime($endTime);
                $workTime = $endTimeStr - $startTimeStr;
                $checkWorkH = date('H', $workTime);
                $checkWorkM = date('i', $workTime);
                if(((int)$checkWorkH == 6 && (int)$checkWorkM >= 30) || (int)$checkWorkH > 6) {
                    $tempBreak = 0;
                    $breakPos = 0;
                    if($breakTooLow) {
                        $sum = strtotime('00:00:00');
                        $mustBreak = strtotime( ' 00:30:00');
                        $mustBreak = $mustBreak - $sum;
                        $mustBreakReal = 0;
                        //$theBreaks[count($theBreaks)-1] = $mustBreak;
                        foreach($theBreaks as $x => $item) {
                            if($item > $tempBreak) {
                                $breakPos = $x;
                            }
                            $tempBreak = $item;
                        }
                        $theBreaks[$breakPos] = $mustBreak;
                        foreach($theBreaks as $item) {
                            $mustBreakReal = $mustBreakReal + $item;
                        }
                        $workTime = $workTime - $mustBreakReal;
                    }
                    else {
                        $workTime = $workTime - $breakTime;
                    }
                }
                else {
                    $workTime = $workTime - $breakTime;
                }

                $hR = intval($workTime/3600);
                $workTime = $workTime - ($hR * 3600);
                $mR = intval($workTime / 60);
                $hr = (new \Jannesnagelschmidt\Mitarbeiter\Components\Timetrackfrontend)->lz($hR);
                $mr = (new \Jannesnagelschmidt\Mitarbeiter\Components\Timetrackfrontend)->roundDownToAny($mR, 1);
                $mr = (new \Jannesnagelschmidt\Mitarbeiter\Components\Timetrackfrontend)->lz($mr);
                $blup = "";
                foreach($theBreaks as $item) {
                    $blup .= $item.' ';
                }
                return "$hr:$mr";
            }
            return $state . ' ' . $token . ' ' . $converted . ' ' . $id;
        }
        else {
            return false;
        }
    }

    public function testMail() {
        $blup = "melvinknop@gmx.de";
        $params = array(
            "names"  =>  "test",
        );
        Mail::sendTo($blup, 'jannesnagelschmidt.mitarbeiter::mail.forgotToLeave', $params, function($message) {
            $message->from('noreply@bla.tld', 'Bla');
            $message->subject('some subject');
            $message->replyTo('noreply@bla.tld');
        });
        return "heyaaaaaa";
    }

    public function addTokenToUser(Request $request) {
        $input = $request->all();
        $first_name = $input['first_name'];
        $last_name = $input['last_name'];
        $token = $input['token'];

        $checkUser = Mitarbeiter::where('token', '=', $token)->first();
        $checkToken = Token::where('token', '=', $token)->first();
        if((!isset($checkUser)) &&  (!isset($checkToken))) {
            $thePerson = Mitarbeiter::where([['first_name','=',$first_name],['last_name','=',$last_name]])->first();
            if($thePerson) {
                $thePerson->token = $token;
                $thePerson->save();
                $newToken = new Token;
                $newToken->token = $token;
                $newToken->user_id = $thePerson->id;
                $newToken->status = 'created';
                $newToken->save();
                $returner = array();
                array_push($returner, $thePerson);
                return $returner;
            }
            else {
            $user = new Mitarbeiter;
            $user->first_name = $first_name;
            $user->last_name = $last_name;
            $user->token = $token;
            $user->save();
            $personForToken = Mitarbeiter::where([['first_name','=',$first_name],['last_name','=',$last_name]])->first();
            $user_id = $personForToken->id;
            $newToken = new Token;
            $newToken->token = $token;
            $newToken->user_id = $user_id;
            $newToken->status = 'created';
            $newToken->save();
            return "yaaay";
            }
        }
        else {
            return "false";
        }
    }

    public function trackTimeNoToken(Request $request) {
        $input = $request->all();
        $first_name = $input['first_name'];
        $last_name = $input['last_name'];
        $state = $input['state'];
        $thePerson = Mitarbeiter::where([['first_name','=',$first_name],['last_name','=',$last_name]])->first();
        $thePerson->status = $state;
        $thePerson->save();
        $id = $thePerson->id;
        $timestamp = time();
        $time = date('Y-m-d H:i:s', $timestamp);
        $converted = $this->convertTimeToUSERzone($time, 'Europe/Amsterdam');

        Arbeitszeit::addTimetrack($id, $converted, $state);
        return $state . ' ' . ' ' . $converted . ' ' . $id;

    }

    public function checkldapstuff(Request $request) {
        $input = $request->all();

        $name = $input['name'];
        $password = $input['password'];
        $url = config('ldap.ldap_url');

        $body = array('name' => $name, 'password' => $password);
        $params = json_encode($body);

        //return $url;
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($params)
        ));

        $response = curl_exec($ch);
        //return $response;
        if($response != 'false') {
            list($firstName) = explode(' ', $response);
            $pieces = explode(' ', $response);
            $lastName = array_pop($pieces);
            //return "vorname: " . $firstName. ' nachname: '.$lastName;
            $thePerson = Mitarbeiter::where([['first_name','=',$firstName],['last_name','=',$lastName]])->first();
            //return $thePerson;
            $arbeitszeiten = Arbeitszeit::where('user_id', $thePerson->id)->get();
            if(count($arbeitszeiten)) {
                $arbeitszeit = $arbeitszeiten[count($arbeitszeiten)-1];
            }
            else {
                $arbeitszeit = false;
            }
            $returner = array();
            array_push($returner, $thePerson);
            array_push($returner, $arbeitszeit);

            if($thePerson) {
                //return 'Hallo '.$thePerson->first_name.' '. $thePerson->last_name;
                return $returner;
            }
            else {
                return 'Niemanden gefunden';
            }
        }
        return $response;
    }

    public function onWitnessCorona() {
        $witnesstoken = post('witnesstoken');
        $token = post('realtoken');
        $source = post('source');
        $today = date('Y-m-d');
        $witness = Mitarbeiter::where('token', $witnesstoken)->first();
        $user = Mitarbeiter::where('token', $token)->first();
        $user_id = $user->id;
        $name = $witness->first_name.' '.$witness->last_name;
        //return $user;
        $old = $user->test_repeater;
        $json = [[
           'test_name' => $name,
            'test_date' => $today

        ]];
        if($old) {
            $user->test_repeater = array_merge($old, $json);
        }
        else {
            $user->test_repeater = $json;
        }
        $user->save();
        $now = date('Y-m-d H:i:s');
        $converted = $this->convertTimeToUSERzone($now, 'Europe/Amsterdam');

        Arbeitszeit::addTimetrack($user_id, $converted, 'coming', $source);

        return $name . $today;
    }

    public function getMitarbeiterByToken(Request $request) {
        $input = $request->all();
        try {
            $thePerson = Mitarbeiter::where('token', $input['token'])->first();
            $returner = array();
            $arbeitszeiten = Arbeitszeit::where('user_id', $thePerson->id)->get();
            if(count($arbeitszeiten)) {
                for($i = 0; $i <= count($arbeitszeiten) - 1; $i++) {
                    if($arbeitszeiten[$i]->status != 'vacation' && $arbeitszeiten[$i]->status != 'sick') {
                        $arbeitszeit = $arbeitszeiten[$i];
                    }
                }
            }
            else {
                $arbeitszeit = false;
            }
            $testPflicht = Testpflicht::first();
            $isonoff = $testPflicht->witness;
            array_push($returner, $thePerson);
            array_push($returner, $arbeitszeit);
            array_push($returner, $isonoff);

            if($thePerson) {
                //return 'Hallo '.$thePerson->first_name.' '. $thePerson->last_name;
                return $returner;
            }
            else {
                return 'Niemanden gefunden';
            }
        }
        catch(Throwable $t) {
            return "false";
        }

    }

    public function scopeSortedNames($query) {
        return $query->orderBy('last_name', 'ASC');
    }

    public function timezonetest() {
        $offset = 1*60*60;

        return date('Y-m-d H:i:s',time() + $offset);
    }

    //this function converts string from UTC time zone to current user timezone
    function convertTimeToUSERzone($str, $userTimezone, $format = 'Y-m-d H:i:s'){
        if(empty($str)){
            return '';
        }
            
        $new_str = new DateTime($str, new \DateTimeZone('UTC') );
        $new_str->setTimeZone(new \DateTimeZone( $userTimezone ));
        return $new_str->format( $format);
    }

    function ldaptest(Request $request) {
        $input = $request->all();

        $name = $input['name'];
        $password = $input['password'];

        $url = config('ldap.ldap_serv');
        $admindn = 'CN=LDAP User,OU=Servicebenutzer,dc=hansnatur,dc=loc';

        $adminpassword = 'Pai6Peng!';
        $dnReturn = '';

        $ldap = Ldap::create('ext_ldap', [
            'host' => $url,
            'debug' => true
        ]);
        try {
            $ldap->bind($admindn, $adminpassword);
            $query = $ldap->query('DC=hansnatur,DC=loc', '(&(objectClass=*)(sAMAccountName='.$name.'))');
            $results = $query->execute();
            $collection = $results->toArray();

            foreach ($collection as $entry) {
                $bla = $entry->getAttributes();
                $dnReturn = $bla['distinguishedName'][0];
            }
            $ldap->bind($dnReturn, $password);
            $query = $ldap->query('DC=hansnatur,DC=loc', '(&(objectClass=*)(sAMAccountName='.$name.'))');
            $results = $query->execute();
            $collection = $results->toArray();

            foreach ($collection as $entry) {
                $blup = $entry->getAttributes();
            }

            /*list($firstName) = explode(' ',   $blup['cn'][0]);
            $pieces = explode(' ',   $blup['cn'][0]);
            $lastName = array_pop($pieces);*/
            $firstName = $blup['givenName'][0];
            $lastName = $blup['sn'][0];
            //return "vorname: " . $firstName. ' nachname: '.$lastName;
            $thePerson = Mitarbeiter::where([['first_name','=',$firstName],['last_name','=',$lastName]])->first();
            if(!(isset($thePerson))) {
                $returnNoDB = array();
                array_push($returnNoDB, $firstName);
                array_push($returnNoDB, $lastName);
                if(isset($thePerson->token)) {
                    array_push($returnNoDB, $thePerson->token);
                }
                else {
                    array_push($returnNoDB, 'emptyshit');
                }
                return $returnNoDB;
            }
            //return $thePerson;
            $returner = array();
            $arbeitszeiten = Arbeitszeit::where('user_id', $thePerson->id)->get();

            if(count($arbeitszeiten)) {
                for($i = 0; $i <= count($arbeitszeiten) - 1; $i++) {
                    if($arbeitszeiten[$i]->status != 'vacation' && $arbeitszeiten[$i]->status != 'sick') {
                        $arbeitszeit = $arbeitszeiten[$i];
                    }
                }
            }
            else {
                $arbeitszeit = false;
            }
            $testPflicht = Testpflicht::first();
            $isonoff = $testPflicht->witness;
            array_push($returner, $thePerson);
            array_push($returner, $arbeitszeit);
            array_push($returner, $isonoff);
        }
        catch(Throwable $t) {
            return "NEIN".$t;
        }


        if($thePerson) {
            //return 'Hallo '.$thePerson->first_name.' '. $thePerson->last_name;
            return $returner;
        }
        else {
            return false;
        }
    }

    function availabillity() {
        $departments = Department::where('id', '!=', 0)->orderBy('name', 'asc')->get();
        $returnArray = array();
        $helpArr = array();
        foreach($departments as $x => $dep) {
            $noPeople = true;
            $temp = Mitarbeiter::where([['status', 'aktiv'],['department_relation_id_id', $dep->id]])->orderBy('first_name', 'asc')->get();
            $tempEmpl = array();

            foreach($temp as $y => $item) {
                $today = date('Y-m-d');

                $now = date('Y-m-d H:i:s');
                $time = Arbeitszeit::where([
                    ['user_id', $item->id],
                    ['lookup', $today]
                ])->latest('id')->first();
                //var_dump($time);
                if($time) {
                    $status = $time->status;
                    if($time != Null && !str_contains($status,'Korrektur') && !str_contains($status, 'sick')) {
                        $blup['employ'] = $item->first_name .' '.$item->last_name;
                        $blup['first_name'] = $item->first_name;
                        $blup['last_name'] = $item->last_name;
                        $blup['phone'] = $item->telephone;
                        $blup['email'] = $item->email;
                        $blup['status'] = $time->status;
                        if($time->status !== 'leaving') {
                            $noPeople = false;
                            array_push($tempEmpl, $blup);
                        }
                    }
                }

            }
            if(!$noPeople) {
                $tempArr['employees'] = $tempEmpl;
                $name = $dep->name;
                $tempArr['department_name'] = $name;
                array_push($returnArray, $tempArr);
            }

        }
        //var_dump($returnArray);
        return $returnArray;
    }

    function birthdays() {
        $users = Mitarbeiter::where('status', '!=', 'passiv')->get();
        $returnArray = array();
        foreach($users as $x => $user) {
            if($user->birthday_is_public) {
                $tempArray = array();
                $tempArray['name'] = $user->first_name.' '.$user->last_name;
                $tempArray['birthday'] = $user->date_of_birth;
                array_push($returnArray, $tempArray);
            }
        }

        $jsonObj = json_encode($returnArray);

        header('Content-Type: application/json');
        print_r($jsonObj);
    }

    public function coronaNumbers() {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://public.opendatasoft.com/api/records/1.0/search/?dataset=covid-19-germany-landkreise&q=schleswig-flensburg&facet=last_update&facet=name&facet=rs&facet=bez&facet=bl");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $out = curl_exec($ch);
        curl_close($ch);



        $blup = json_decode($out, true);
        $number = $blup['records'][0]['fields']['cases7_per_100k'];
        $temparray = array();
        $temparray['type'] = 'corona';
        $temparray['number'] = $number;

        $jsonObj = json_encode($temparray);



        print_r($jsonObj);
    }

}

