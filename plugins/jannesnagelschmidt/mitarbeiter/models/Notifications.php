<?php namespace JannesNagelschmidt\Mitarbeiter\Models;

use Model;

/**
 * Model
 */
class Notifications extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    public $belongsTo = [
        'employ_id_id' => [
            'JannesNagelschmidt\Mitarbeiter\Models\Mitarbeiter',
        ],
        'request_id' => [
            'JannesNagelschmidt\Mitarbeiter\Models\ChangeRequests',
        ],
    ];
    /**
     * @var string The database table used by the model.
     */
    public $table = 'jannesnagelschmidt_mitarbeiter_notifications';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
