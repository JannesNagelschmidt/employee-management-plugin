<?php namespace JannesNagelschmidt\Mitarbeiter\Models;

use JannesNagelschmidt\Mitarbeiter\Plugin;
use Model;
use DateTime;
use Mail;

/**
 * Model
 */
class Sick extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];
    protected $fillable = ['sickconfigs','name', 'start_day', 'end_day'];
    public $settingsCode = 'JannesNagelschmidt_Mitarbeiter_Settings';
    public $settingsFields = 'fields.yaml';



    /**
     * @var string The database table used by the model.
     */
    public $table = 'jannesnagelschmidt_mitarbeiter_sick';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'sick_employee' => [
            'Jannesnagelschmidt\Mitarbeiter\Models\Mitarbeiter',
            'key'   => 'mitarbeiter_id',
        ]
    ];
    public $attachMany = [
        'documents' => 'System\Models\File',
        ];

    public function beforeSave() {
        $start =  new DateTime($this->start_day);
        $end = new DateTime($this->end_day);
        $child_sick = $this->child_sick;
        $count_sick = 0;
        if(strtotime($start->format('Y-m-d')) == strtotime($end->format('Y-m-d'))) {
            $count_sick = 1;
            if($child_sick) {
                $this->sick_days = $count_sick;
            }
            else {
                $this->count_sick = $count_sick;
            }
        }
        else {
            $end = $end->modify('+1 day');
            $interval = \DateInterval::createFromDateString('1 day');
            $period = new \DatePeriod($start, $interval, $end);
            foreach($period as $dt) {
                if($dt->format('w') != 0 && $dt->format('w') != 6) {
                    $count_sick++;
                }
            }
            if($child_sick) {
                $this->sick_days = $count_sick;
            }
            else {
                $this->count_sick = $count_sick;
            }
        }
    }

    public function afterCreate() {
        $start =  new DateTime($this->start_day);
        $end = new DateTime($this->end_day);
        $entry_id = $this->id;
        $user_id = $this->mitarbeiter_id;
        $count_sick = 0;
        if(strtotime($start->format('Y-m-d')) == strtotime($end->format('Y-m-d'))) {
            Arbeitszeit::addTimetrack($user_id, $start->format('Y-m-d H:i:s'), 'sick');
        }
        else {
            $end = $end->modify('+1 day');
            $interval = \DateInterval::createFromDateString('1 day');
            $period = new \DatePeriod($start, $interval, $end);
            $count_sick = 5;
            $entry = Sick::where('id', $entry_id)->first();
            $entry->count_sick = $count_sick;
            $entry->save();
            foreach($period as $dt) {
                if($dt->format('w') != 0 && $dt->format('w') != 6) {
                    Arbeitszeit::addTimetrack($user_id, $dt->format('Y-m-d H:i:s'), 'sick');
                }
            }
        }
        $id = $user_id;
        $employee = Mitarbeiter::where('id', $id)->first();
        $department = Department::where('id', $employee->department_relation_id_id)->first();
        $vorname = $employee->first_name;
        $nachname = $employee->last_name;
        $abteilung = $department->name;
        $start = date('d.m.Y', strtotime($this->start_day));
        $ende = date('d.m.Y', strtotime($this->end_day));
        $is_update = false;
        $now = date('d.m.Y. H:i');
        $now = date('d.m.Y H:i', strtotime('+ 1 hour', strtotime($now)));
        $subject = $vorname.' '.$nachname. ' krank!';
        $text = "$vorname $nachname aus Abteilung $abteilung
        ist vom $start bis vorraussichtlich $ende krank!
         Stand: $now";


        $params = array(
            "subject" => $subject,
            "text" => $text
        );
        $receiver = ['mhans@hans-natur.de', 'cranft@hans-natur.de', 'rranft@hans-natur.de', 'pmoldenhauer@hans-natur.de'];
        //$receiver = ['mknop@hans-natur.de'];
        foreach($receiver as $item) {
            Mail::sendTo($item, 'jannesnagelschmidt.mitarbeiter::mail.krankmeldung', $params);
        }
        //Mail::sendTo("mknop@hans-natur.de", 'jannesnagelschmidt.mitarbeiter::mail.krankmeldung', $params);
        //Plugin::createLog("in sick afterCreate, start = ".$start." ende = ".$end->format('Y-m-d') . "test = ".date('Y-m-d', strtotime($this->end_day)));
    }

    public function afterUpdate() {
        $start =  new DateTime($this->start_day);
        $end = new DateTime($this->end_day);
        $user_id = $this->mitarbeiter_id;
        if(strtotime($start->format('Y-m-d')) == strtotime($end->format('Y-m-d'))) {
            Arbeitszeit::addTimetrack($user_id, $start->format('Y-m-d H:i:s'), 'sick');
        }
        else {
            $end = $end->modify('+1 day');
            $interval = \DateInterval::createFromDateString('1 day');
            $period = new \DatePeriod($start, $interval, $end);
            foreach($period as $dt) {
                $entry = Arbeitszeit::where([
                    ['user_id', $user_id],
                    ['lookup', $dt->format('Y-m-d')],
                    ['status', 'sick']
                ])->get();
                if($entry) {
                    foreach($entry as $item) {
                        $item->delete();
                    }
                }
                $calc = Calculated::where([
                    ['user_id', $user_id],
                    ['manually_created', 0],
                    ['lu_date', $dt->format('Y-m-d')]
                ])->get();
                if($calc) {
                    foreach($calc as $item) {
                        $item->delete();
                    }
                }
            }
            foreach($period as $dt) {
                if($dt->format('w') != 0 && $dt->format('w') != 6) {
                    Arbeitszeit::addTimetrack($user_id, $dt->format('Y-m-d H:i:s'), 'sick');
                }
            }
        }
        $id = $user_id;
        $employee = Mitarbeiter::where('id', $id)->first();
        $department = Department::where('id', $employee->department_relation_id_id)->first();
        $vorname = $employee->first_name;
        $nachname = $employee->last_name;
        $abteilung = $department->name;
        $start = date('d.m.Y', strtotime($this->start_day));
        $ende = date('d.m.Y', strtotime($this->end_day));
        $is_update = true;
        $now = date('d.m.Y H:i');
        if(strtotime($ende) >= strtotime($now)) {
            $subject = $vorname.' '.$nachname. ' krank! (Update)';
            $now = date('d.m.Y H:i', strtotime('+ 1 hour', strtotime($now)));
            $text = "$vorname $nachname aus Abteilung $abteilung
            ist vom $start bis vorraussichtlich $ende krank!
            Die Krankmeldung wurde aktualisiert am $now";

            $params = array(
                "subject" => $subject,
                "text" => $text,
            );

            $receiver = ['mhans@hans-natur.de', 'cranft@hans-natur.de', 'rranft@hans-natur.de', 'pmoldenhauer@hans-natur.de'];
            //$receiver = ['mknop@hans-natur.de'];

            foreach($receiver as $item) {
                Mail::sendTo($item, 'jannesnagelschmidt.mitarbeiter::mail.krankmeldung', $params);
            }
        }
        //Plugin::createLog("in sick afterUpdate, start = ".$start." ende = ".$end . "test = ".$this->end_day. "test2 = ".$this->start_day);
    }

    public function beforeDelete() {
        $start =  new DateTime($this->start_day);
        $end = new DateTime($this->end_day);
        $user_id = $this->mitarbeiter_id;
        $end = $end->modify('+1 day');
        $interval = \DateInterval::createFromDateString('1 day');
        $period = new \DatePeriod($start, $interval, $end);
        foreach($period as $dt) {
            $entry = Arbeitszeit::where([
                ['user_id', $user_id],
                ['lookup', $dt->format('Y-m-d')],
                ['status', 'sick']
            ])->first();
            if($entry) {
                $entry->delete();
            }
            $calc = Calculated::where([
                ['user_id', $user_id],
                ['lu_date', $dt->format('Y-m-d')],
                ['manually_created', 0]
            ])->first();
            if($calc) {
                $calc->delete();
            }
        }
    }

    //sick_employee
}
