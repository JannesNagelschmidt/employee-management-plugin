<?php namespace JannesNagelschmidt\Mitarbeiter\Models;

use Model;

/**
 * Model
 */
class Talk extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    protected $fillable = ['talk','date', 'cause', 'matter'];



    /**
     * @var string The database table used by the model.
     */
    public $table = 'jannesnagelschmidt_mitarbeiter_talk';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
    public $attachMany = [
        'documents' => 'System\Models\File',
        ];

    public $belongsTo = [
        'talks' => [
            'JannesNagelschmidt\Mitarbeiter\Models\Mitarbeiter',
            'key'   => 'mitarbeiter_id',
        ],

    ];

    public $belongsToMany = [
        'attendees' => [
            'JannesNagelschmidt\Mitarbeiter\Models\Mitarbeiter',
            'table' => 'jannesnagelschmidt_mitarbeiter_talk_mitarbeiter',
            'key' => 'mitarbeiter_id',
            'otherKey' => 'talk_id'

        ]
    ];

    public function scopeSortedNames($query) {
        return $query->orderBy('last_name', 'ASC');
    }

}
