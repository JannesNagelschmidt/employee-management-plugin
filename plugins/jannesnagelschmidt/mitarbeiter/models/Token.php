<?php namespace JannesNagelschmidt\Mitarbeiter\Models;

use Model;

/**
 * Model
 */
class Token extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'jannesnagelschmidt_mitarbeiter_token';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
    public $hasOne = [
        'relation1' => [
            'Jannesnagelschmidt\Mitarbeiter\Models\Mitarbeiter',
            'table' => 'jannesnagelschmidt_mitarbeiter_',
            'key'   => 'token',
            'otherKey' => 'token'
        ]
    ];

    public static function updateTokenState($id, $token, $status) {
        Token::where(['user_id' => $id, 'token' => $token])->update(['status' => $status]);
    }
}
