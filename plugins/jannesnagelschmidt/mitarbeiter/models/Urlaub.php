<?php namespace JannesNagelschmidt\Mitarbeiter\Models;

use Cms\Classes\ComponentBase;
use Jannesnagelschmidt\Mitarbeiter\Components\Timetrackfrontend;
use Model;
use DateInterval;
use DatePeriod;
use DateTime;
/**
 * Model
 */
class Urlaub extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    protected $fillable = ['vacation','name', 'start_day', 'end_day', 'substitute'];



    /**
     * @var string The database table used by the model.
     */
    public $table = 'jannesnagelschmidt_mitarbeiter_urlaub';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'vacation' => [
            'Jannesnagelschmidt\Mitarbeiter\Models\Mitarbeiter',
            'key'   => 'mitarbeiter_id',
        ],
        'leader_id' => [
            'Jannesnagelschmidt\Mitarbeiter\Models\Mitarbeiter',
            'otherKey' => 'id'
        ],
        'substitute' => [
            'Jannesnagelschmidt\Mitarbeiter\Models\Mitarbeiter',
        ]
    ];

    public function afterCreate() {
        $start =  new DateTime($this->start_date);
        $end = new DateTime($this->end_date);
        $startYear = date('Y', strtotime($this->start_date));
        $holidays = (new Timetrackfrontend)->getHolidays($startYear);
        $weihnachten['date'] = $startYear.'-12-24';
        $weihnachten['global'] = true;
        $weihnachten['localName'] = 'Heiligabend';
        array_push($holidays, $weihnachten);
        $silvester['date'] = $startYear.'-12-31';
        $silvester['global'] = true;
        $silvester['localName'] = 'Silvester';
        array_push($holidays, $silvester);
        $user_id = $this->mitarbeiter_id;
        if(strtotime( $start->format('Y-m-d H:i:s')) == strtotime($end->format('Y-m-d H:i:s'))) {
            $entry = Arbeitszeit::where([
                ['user_id', $user_id],
                ['lookup', $start->format('Y-m-d')],
                ['status', 'vacation']
            ])->get();
            foreach($entry as $x => $item) {
                if($item) {
                    $item->delete();
                }
            }
            $calc = Calculated::where([
                ['user_id', $user_id],
                ['manually_created', 0],
                ['lu_date', $start->format('Y-m-d')]
            ])->get();
            foreach($calc as $x => $item) {
                if($item) {
                    $item->delete();
                }
            }
            if($this->status == 'Genehmigt' && !((new \Jannesnagelschmidt\Mitarbeiter\Components\Timetrackfrontend)->checkPublicHolidays($this->start_date, $holidays))) {
                Arbeitszeit::addTimetrack($user_id, $start->format('Y-m-d H:i:s'), 'vacation');
            }
        }
        else {
            $end = $end->modify('+1 day');
            $interval = \DateInterval::createFromDateString('1 day');
            $period = new \DatePeriod($start, $interval, $end);
            foreach($period as $dt) {
                $entry = Arbeitszeit::where([
                    ['user_id', $user_id],
                    ['lookup', $dt->format('Y-m-d')],
                    ['status', 'vacation']
                ])->get();
                if($entry) {
                    foreach($entry as $item) {
                        $item->delete();
                    }
                }
                $calc = Calculated::where([
                    ['user_id', $user_id],
                    ['manually_created', 0],
                    ['lu_date', $dt->format('Y-m-d')]
                ])->get();
                if($calc) {
                    foreach($calc as $item) {
                        $item->delete();
                    }
                }
            }
            if($this->status == 'Genehmigt') {
                foreach($period as $dt) {
                    $startYear = date('Y', strtotime($dt->format('Y-m-d')));
                    $holidays = (new Timetrackfrontend)->getHolidays($startYear);
                    $weihnachten['date'] = $startYear.'-12-24';
                    $weihnachten['global'] = true;
                    $weihnachten['localName'] = 'Heiligabend';
                    array_push($holidays, $weihnachten);
                    $silvester['date'] = $startYear.'-12-31';
                    $silvester['global'] = true;
                    $silvester['localName'] = 'Silvester';
                    array_push($holidays, $silvester);
                    if($dt->format('w') != 0 && $dt->format('w') != 6 && !((new \Jannesnagelschmidt\Mitarbeiter\Components\Timetrackfrontend)->checkPublicHolidays($dt->format('Y-m-d'), $holidays))) {
                        $time_entry = Arbeitszeit::where([
                            ['user_id', $user_id],
                            ['status', 'vacation'],
                            ['lookup', $dt->format('Y-m-d')]
                        ])->exists();
                        if(!$time_entry) {
                            Arbeitszeit::addTimetrack($user_id, $dt->format('Y-m-d H:i:s'), 'vacation');
                        }
                    }
                }
            }
        }
    }

    public function afterUpdate() {
        $start =  new DateTime($this->start_date);
        $end = new DateTime($this->end_date);
        $user_id = $this->mitarbeiter_id;
        if(strtotime( $start->format('Y-m-d H:i:s')) == strtotime($end->format('Y-m-d H:i:s'))) {
            $entry = Arbeitszeit::where([
                ['user_id', $user_id],
                ['lookup', $start->format('Y-m-d')],
                ['status', 'vacation']
            ])->get();
            foreach($entry as $x => $item) {
                if($item) {
                    $item->delete();
                }
            }
            $calc = Calculated::where([
                ['user_id', $user_id],
                ['manually_created', 0],
                ['lu_date', $start->format('Y-m-d')]
            ])->get();
            foreach($calc as $x => $item) {
                if($item) {
                    $item->delete();
                }
            }
            if($this->status == 'Genehmigt') {
                Arbeitszeit::addTimetrack($user_id, $start->format('Y-m-d H:i:s'), 'vacation');
            }
        }
        else {
            $end = $end->modify('+1 day');
            $interval = \DateInterval::createFromDateString('1 day');
            $period = new \DatePeriod($start, $interval, $end);
            foreach($period as $dt) {
                $entry = Arbeitszeit::where([
                    ['user_id', $user_id],
                    ['lookup', $dt->format('Y-m-d')],
                    ['status', 'vacation']
                ])->get();
                if($entry) {
                    foreach($entry as $item) {
                        $item->delete();
                    }
                }
                $calc = Calculated::where([
                    ['user_id', $user_id],
                    ['manually_created', 0],
                    ['lu_date', $dt->format('Y-m-d')]
                ])->get();
                if($calc) {
                    foreach($calc as $item) {
                        $item->delete();
                    }
                }
            }
            if($this->status == 'Genehmigt') {
                foreach($period as $dt) {
                    $startYear = date('Y', strtotime($dt->format('Y-m-d')));
                    $holidays = (new Timetrackfrontend)->getHolidays($startYear);
                    $weihnachten['date'] = $startYear.'-12-24';
                    $weihnachten['global'] = true;
                    $weihnachten['localName'] = 'Heiligabend';
                    array_push($holidays, $weihnachten);
                    $silvester['date'] = $startYear.'-12-31';
                    $silvester['global'] = true;
                    $silvester['localName'] = 'Silvester';
                    array_push($holidays, $silvester);
                    if($dt->format('w') != 0 && $dt->format('w') != 6 && !((new \Jannesnagelschmidt\Mitarbeiter\Components\Timetrackfrontend)->checkPublicHolidays($dt->format('Y-m-d'), $holidays))) {
                        $time_entry = Arbeitszeit::where([
                            ['user_id', $user_id],
                            ['status', 'vacation'],
                            ['lookup', $dt->format('Y-m-d')]
                        ])->exists();
                        if(!$time_entry) {
                            Arbeitszeit::addTimetrack($user_id, $dt->format('Y-m-d H:i:s'), 'vacation');
                        }
                    }
                }
            }
        }
    }

    /*public function beforeUpdate() {
        $id = $this->id;
        $entry = Urlaub::where('id', $id)->first();
        $start =  new DateTime($entry->start_date);
        $end = new DateTime($entry->end_date);
        $user_id = $this->mitarbeiter_id;
        $end = $end->modify('+1 day');
        $interval = \DateInterval::createFromDateString('1 day');
        $period = new \DatePeriod($start, $interval, $end);
        if($this->status == 'Genehmigt' || $this->status == 'Abgelehnt') {
            foreach($period as $dt) {
                $entry = Arbeitszeit::where([
                    ['user_id', $user_id],
                    ['lookup', $dt->format('Y-m-d')],
                    ['status', 'vacation']
                ])->first();
                if($entry) {
                    $entry->delete();
                }
                $calc = Calculated::where([
                    ['user_id', $user_id],
                    ['lu_date', $dt->format('Y-m-d')]
                ])->first();
                if($calc) {
                    $calc->delete();
                }
            }
        }
    }*/

    public function beforeDelete() {
        $start =  new DateTime($this->start_date);
        $end = new DateTime($this->end_date);
        $user_id = $this->mitarbeiter_id;
        $end = $end->modify('+1 day');
        $interval = \DateInterval::createFromDateString('1 day');
        $period = new \DatePeriod($start, $interval, $end);
        if($this->status == 'Genehmigt' || $this->status == 'Abgelehnt') {
            foreach($period as $dt) {
                $entry = Arbeitszeit::where([
                    ['user_id', $user_id],
                    ['lookup', $dt->format('Y-m-d')],
                    ['status', 'vacation']
                ])->first();
                if($entry) {
                    $entry->delete();
                }
                $calc = Calculated::where([
                    ['user_id', $user_id],
                    ['lu_date', $dt->format('Y-m-d')],
                    ['manually_created', 0]
                ])->first();
                if($calc) {
                    $calc->delete();
                }
            }
        }
    }

}
