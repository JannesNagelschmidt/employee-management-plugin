<?php 


Route::get('getMitarbeiter', 'Jannesnagelschmidt\Mitarbeiter\Models\Mitarbeiter@getMitarbeiterByToken');

Route::post('setWitnessCorona', 'Jannesnagelschmidt\Mitarbeiter\Models\Mitarbeiter@onWitnessCorona');

Route::post('timetracking/setWitnessCorona', 'Jannesnagelschmidt\Mitarbeiter\Models\Mitarbeiter@onWitnessCorona');

Route::post('trackTime', 'Jannesnagelschmidt\Mitarbeiter\Models\Mitarbeiter@trackTime');

Route::get('timezonetest', 'Jannesnagelschmidt\Mitarbeiter\Models\Mitarbeiter@timezonetest');

Route::post('trackTimeNoToken', 'Jannesnagelschmidt\Mitarbeiter\Models\Mitarbeiter@trackTimeNoToken');

Route::get('phpinfo', 'Jannesnagelschmidt\Mitarbeiter\Models\Mitarbeiter@phpinfoFunc');

Route::get('testEmail', 'Jannesnagelschmidt\Mitarbeiter\Models\Mitarbeiter@testMail');

Route::get('coronaNumbers', 'Jannesnagelschmidt\Mitarbeiter\Models\Mitarbeiter@coronaNumbers');

Route::post('addTokenToUser', 'Jannesnagelschmidt\Mitarbeiter\Models\Mitarbeiter@addTokenToUser');

/*Route::post('getEmployById', 'Jannesnagelschmidt\Mitarbeiter\Components\Timetrackfrontend@renderEmployee');*/

Route::post('getWorkForFullCalendar', 'Jannesnagelschmidt\Mitarbeiter\Components\Emt@getWorkForFullCalendar');

Route::post('getPlusMinusInCalc', 'Jannesnagelschmidt\Mitarbeiter\Components\Emt@getPlusMinusAjax');

Route::get('testmashit', 'Jannesnagelschmidt\Mitarbeiter\Components\Emt@onGetSundayShit');

Route::post('getSingleDepartment', 'Jannesnagelschmidt\Mitarbeiter\Components\Timetrackfrontend@getSingleDepartment');

Route::get('getSingleEmployeeByWindows', 'Jannesnagelschmidt\Mitarbeiter\Components\Singleemployee@getEmployee');

Route::get('checkUserForCorrection', 'Jannesnagelschmidt\Mitarbeiter\Components\Correction@checkAuthorizedUser');

Route::get('checkUserLoggedInCorrection', 'Jannesnagelschmidt\Mitarbeiter\Components\Correction@checkUserLoggedIn');

Route::post('updateTimeForUser', 'Jannesnagelschmidt\Mitarbeiter\Components\Correction@updateTime');

Route::get('getUserByDateAndName', 'Jannesnagelschmidt\Mitarbeiter\Components\Correction@getUserByNameAndDate');

Route::post('deleteTimeForUser', 'Jannesnagelschmidt\Mitarbeiter\Components\Correction@deleteTime');

Route::get('checkUser', 'Jannesnagelschmidt\Mitarbeiter\Components\Singleemployee@checkUserLoggedIn');

Route::get('endSessionEmployee', 'Jannesnagelschmidt\Mitarbeiter\Components\Singleemployee@endSessionEmployee');

Route::get('getDepartments', 'Jannesnagelschmidt\Mitarbeiter\Components\Timetrackfrontend@getDepartments');

Route::post('getFullOvertime', 'Jannesnagelschmidt\Mitarbeiter\Components\Timetrackfrontend@getFullOvertime');

Route::get('getMitarbeiterLdap', 'Jannesnagelschmidt\Mitarbeiter\Models\Mitarbeiter@checkldapstuff');

Route::get('ldaptest', 'Jannesnagelschmidt\Mitarbeiter\Models\Mitarbeiter@ldapTest');

Route::get('getEmployeeBirthdays', 'Jannesnagelschmidt\Mitarbeiter\Models\Mitarbeiter@birthdays');

Route::get('getEmployeesForDashboard', 'Jannesnagelschmidt\Mitarbeiter\Models\Mitarbeiter@availabillity');

Route::get('getEventsFromCampaignr', 'Hendrikerz\Campaignr\Models\Event@getNextSevenDays');

Route::post('foo/bar', function () {
    return 'Hello World';
});

Route::put('foo/bar', function () {
    //
});

Route::delete('foo/bar', function () {
    //
});