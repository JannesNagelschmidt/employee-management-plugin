<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJannesnagelschmidtMitarbeiter extends Migration
{
    public function up()
    {
        Schema::create('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('mitarbeiter');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jannesnagelschmidt_mitarbeiter_');
    }
}