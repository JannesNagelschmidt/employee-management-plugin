<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJannesnagelschmidtMitarbeiterCalculated extends Migration
{
    public function up()
    {
        Schema::create('jannesnagelschmidt_mitarbeiter_calculated', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('user_id')->nullable();
            $table->dateTime('timestamp')->nullable();
            $table->string('worked')->nullable();
            $table->string('break')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jannesnagelschmidt_mitarbeiter_calculated');
    }
}
