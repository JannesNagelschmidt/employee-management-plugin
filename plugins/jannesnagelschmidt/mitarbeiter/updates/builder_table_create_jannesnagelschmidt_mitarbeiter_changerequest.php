<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJannesnagelschmidtMitarbeiterChangerequest extends Migration
{
    public function up()
    {
        Schema::create('jannesnagelschmidt_mitarbeiter_changerequest', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('type');
            $table->string('status');
            $table->string('memo');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jannesnagelschmidt_mitarbeiter_changerequest');
    }
}
