<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJannesnagelschmidtMitarbeiterInventory2 extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('jannesnagelschmidt_mitarbeiter_inventory')) {
            Schema::create('jannesnagelschmidt_mitarbeiter_inventory', function($table)
            {
                $table->engine = 'InnoDB';
                $table->increments('id')->unsigned();
                $table->string('type');
            });    
        }
        
    }
    
    public function down()
    {
        Schema::dropIfExists('jannesnagelschmidt_mitarbeiter_inventory');
    }
}