<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJannesnagelschmidtMitarbeiterInventory2employee extends Migration
{
    public function up()
    {
        Schema::create('jannesnagelschmidt_mitarbeiter_inventory2employee', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('inventory_id')->unsigned();
            $table->integer('amount')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jannesnagelschmidt_mitarbeiter_inventory2employee');
    }
}
