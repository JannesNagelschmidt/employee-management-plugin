<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJannesnagelschmidtMitarbeiterNotifications extends Migration
{
    public function up()
    {
        Schema::create('jannesnagelschmidt_mitarbeiter_notifications', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('text', 128);
            $table->string('type', 128);
            $table->string('link', 128);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jannesnagelschmidt_mitarbeiter_notifications');
    }
}
