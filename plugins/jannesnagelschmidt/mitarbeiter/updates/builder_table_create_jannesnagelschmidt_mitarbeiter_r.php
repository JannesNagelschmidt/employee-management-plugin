<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJannesnagelschmidtMitarbeiterR extends Migration
{
    public function up()
    {
        Schema::create('jannesnagelschmidt_mitarbeiter_r', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('m_id');
            $table->integer('a_id');
            $table->primary(['m_id','a_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jannesnagelschmidt_mitarbeiter_r');
    }
}
