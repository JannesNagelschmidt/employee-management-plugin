<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJannesnagelschmidtMitarbeiterSubdepartment extends Migration
{
    public function up()
    {
        Schema::create('jannesnagelschmidt_mitarbeiter_subdepartment', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('color')->nullable();
            $table->string('name')->nullable();
            $table->integer('employ_relation');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jannesnagelschmidt_mitarbeiter_subdepartment');
    }
}
