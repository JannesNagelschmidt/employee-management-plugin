<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJannesnagelschmidtMitarbeiterTestpflicht extends Migration
{
    public function up()
    {
        Schema::create('jannesnagelschmidt_mitarbeiter_testpflicht', function($table)
        {
            $table->engine = 'InnoDB';
            $table->boolean('witness')->default(0);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jannesnagelschmidt_mitarbeiter_testpflicht');
    }
}
