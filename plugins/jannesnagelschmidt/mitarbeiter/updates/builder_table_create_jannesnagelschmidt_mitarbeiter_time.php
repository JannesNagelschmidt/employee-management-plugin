<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJannesnagelschmidtMitarbeiterTime extends Migration
{
    public function up()
    {
        Schema::create('jannesnagelschmidt_mitarbeiter_time', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('worker_id');
            $table->integer('time_id');
            $table->primary(['worker_id','time_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jannesnagelschmidt_mitarbeiter_time');
    }
}
