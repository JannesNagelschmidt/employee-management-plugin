<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJannesnagelschmidtMitarbeiterTimetracking extends Migration
{
    public function up()
    {
        Schema::create('jannesnagelschmidt_mitarbeiter_timetracking', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->dateTime('starttime');
            $table->dateTime('endtime');
            $table->dateTime('startbreak')->nullable();
            $table->dateTime('endbreak')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jannesnagelschmidt_mitarbeiter_timetracking');
    }
}
