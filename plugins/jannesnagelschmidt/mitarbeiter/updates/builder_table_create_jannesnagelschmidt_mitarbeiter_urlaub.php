<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJannesnagelschmidtMitarbeiterUrlaub extends Migration
{
    public function up()
    {
        Schema::create('jannesnagelschmidt_mitarbeiter_urlaub', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('status', 32)->default('beantragt');
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->integer('mitarbeiter_id');
            $table->string('name', 32);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jannesnagelschmidt_mitarbeiter_urlaub');
    }
}