<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteJannesnagelschmidtMitarbeiterAttendees extends Migration
{
    public function up()
    {
        Schema::dropIfExists('jannesnagelschmidt_mitarbeiter_attendees');
    }
    
    public function down()
    {
        Schema::create('jannesnagelschmidt_mitarbeiter_attendees', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
        });
    }
}
