<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteJannesnagelschmidtMitarbeiterRelate extends Migration
{
    public function up()
    {
        Schema::dropIfExists('jannesnagelschmidt_mitarbeiter_relate');
    }
    
    public function down()
    {
        Schema::create('jannesnagelschmidt_mitarbeiter_relate', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('mitarbeiter_id');
            $table->integer('arbeitszeit_id');
        });
    }
}
