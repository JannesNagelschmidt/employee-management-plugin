<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteJannesnagelschmidtMitarbeiterTimeemployee extends Migration
{
    public function up()
    {
        Schema::dropIfExists('jannesnagelschmidt_mitarbeiter_timeemployee');
    }
    
    public function down()
    {
        Schema::create('jannesnagelschmidt_mitarbeiter_timeemployee', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('worker_id');
            $table->integer('time_id');
        });
    }
}
