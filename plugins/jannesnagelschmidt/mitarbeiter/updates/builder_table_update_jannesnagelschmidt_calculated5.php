<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtCalculated5 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_calculated', function($table)
        {  
             if (!Schema::hasColumn('jannesnagelschmidt_mitarbeiter_calculated', 'type')) {
                 
                $table->string('type', 191)->nullable();
             }
             if (!Schema::hasColumn('jannesnagelschmidt_mitarbeiter_calculated', 'note')) {
                 $table->string('note', 191)->nullable();
                 }
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_calculated', function($table)
        {
            $table->dropColumn('type');
            $table->dropColumn('note');
        });
    }
}