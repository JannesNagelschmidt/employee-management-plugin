<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiter79 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            if (!Schema::hasColumn('jannesnagelschmidt_mitarbeiter_', 'test_date')) {
                
             
            $table->date('test_date')->nullable();
            }
            if (!Schema::hasColumn('jannesnagelschmidt_mitarbeiter_', 'test_name')) {
                            
            $table->text('test_name')->nullable();
           
            }

            $table->boolean('vaccinated')->default(null)->change();
            $table->date('date_vaccine')->default(null)->change();
            $table->text('test_repeater')->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            $table->dropColumn('test_date');
            $table->dropColumn('test_name');
            $table->boolean('vaccinated')->default(NULL)->change();
            $table->date('date_vaccine')->default('NULL')->change();
            $table->text('test_repeater')->default('NULL')->change();
        });
    }
}