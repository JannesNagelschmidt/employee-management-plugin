<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiter20 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            $table->boolean('minijob');
            $table->string('mobile_phone', 20);
            $table->string('private_phone', 20);
            $table->dropColumn('tax_id');
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            $table->dropColumn('minijob');
            $table->dropColumn('mobile_phone');
            $table->dropColumn('private_phone');
            $table->integer('tax_id');
        });
    }
}
