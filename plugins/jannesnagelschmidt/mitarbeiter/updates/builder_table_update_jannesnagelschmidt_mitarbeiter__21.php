<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiter21 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            $table->string('tax_id', 11);
            $table->string('iban', 30)->change();
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            $table->dropColumn('tax_id');
            $table->string('iban', 25)->change();
        });
    }
}
