<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiter22 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            $table->date('temporary_contract_until');
            $table->date('trial_period_until');
            $table->date('contract_start');
            $table->dropColumn('kids');
            $table->dropColumn('trial_period');
            $table->dropColumn('temporary_contract');
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            $table->dropColumn('temporary_contract_until');
            $table->dropColumn('trial_period_until');
            $table->dropColumn('contract_start');
            $table->integer('kids');
            $table->string('trial_period', 64);
            $table->string('temporary_contract', 5);
        });
    }
}
