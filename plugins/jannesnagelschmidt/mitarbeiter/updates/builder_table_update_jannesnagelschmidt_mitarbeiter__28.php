<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiter28 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            $table->date('temporary_contract_until')->nullable()->change();
            $table->date('trial_period_until')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            $table->date('temporary_contract_until')->nullable(false)->change();
            $table->date('trial_period_until')->nullable(false)->change();
        });
    }
}
