<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiter29 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            $table->string('second_job_address', 64);
            $table->date('temporary_contract_until')->default(null)->change();
            $table->date('trial_period_until')->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            $table->dropColumn('second_job_address');
            $table->date('temporary_contract_until')->default('NULL')->change();
            $table->date('trial_period_until')->default('NULL')->change();
        });
    }
}
