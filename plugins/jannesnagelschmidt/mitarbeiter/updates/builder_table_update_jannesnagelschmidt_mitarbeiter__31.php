<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiter31 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            $table->string('telephone', 10)->nullable()->default('-')->change();
            $table->string('second_job_company', 64)->nullable()->change();
            $table->string('adress_second_job', 64)->nullable()->change();
            $table->string('minijob_work', 64)->nullable()->change();
            $table->string('bank', 64)->nullable()->change();
            $table->string('mobile_phone', 20)->nullable()->change();
            $table->string('private_phone', 20)->nullable()->change();
            $table->date('temporary_contract_until')->default(null)->change();
            $table->date('trial_period_until')->default(null)->change();
            $table->string('second_job_address', 64)->nullable()->change();
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->dropColumn('deleted_at');
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            $table->string('telephone', 10)->nullable(false)->default(null)->change();
            $table->string('second_job_company', 64)->nullable(false)->change();
            $table->string('adress_second_job', 64)->nullable(false)->change();
            $table->string('minijob_work', 64)->nullable(false)->change();
            $table->string('bank', 64)->nullable(false)->change();
            $table->string('mobile_phone', 20)->nullable(false)->change();
            $table->string('private_phone', 20)->nullable(false)->change();
            $table->date('temporary_contract_until')->default('NULL')->change();
            $table->date('trial_period_until')->default('NULL')->change();
            $table->string('second_job_address', 64)->nullable(false)->change();
            $table->timestamp('created_at')->nullable()->default('NULL');
            $table->timestamp('updated_at')->nullable()->default('NULL');
            $table->timestamp('deleted_at')->nullable()->default('NULL');
        });
    }
}
