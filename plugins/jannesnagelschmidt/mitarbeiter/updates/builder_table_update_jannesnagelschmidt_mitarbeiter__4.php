<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiter4 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            $table->string('birth_name', 64);
            $table->string('place_of_birth', 64);
            $table->string('street', 64);
            $table->string('house_number', 6);
            $table->integer('plz');
            $table->string('city', 64);
            $table->string('email', 64);
            $table->integer('mobile_phone');
            $table->integer('private_phone');
            $table->integer('kids');
            $table->integer('kinderfreibetrag');
            $table->string('denomination', 64);
            $table->string('nationality', 64);
            $table->integer('tax_class');
            $table->integer('tax_id');
            $table->integer('social_insurance_number');
            $table->boolean('second_job');
            $table->string('medical_insurance', 64);
            $table->string('second_job_company', 64);
            $table->string('adress_second_job', 64);
            $table->boolean('minijob');
            $table->string('minijob_work', 64);
            $table->date('entry_date');
            $table->integer('hours_per_week');
            $table->string('bank', 64);
            $table->string('account_owner', 64);
            $table->string('iban', 25);
            $table->string('bic', 64);
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->dropColumn('deleted_at');
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            $table->dropColumn('birth_name');
            $table->dropColumn('place_of_birth');
            $table->dropColumn('street');
            $table->dropColumn('house_number');
            $table->dropColumn('plz');
            $table->dropColumn('city');
            $table->dropColumn('email');
            $table->dropColumn('mobile_phone');
            $table->dropColumn('private_phone');
            $table->dropColumn('kids');
            $table->dropColumn('kinderfreibetrag');
            $table->dropColumn('denomination');
            $table->dropColumn('nationality');
            $table->dropColumn('tax_class');
            $table->dropColumn('tax_id');
            $table->dropColumn('social_insurance_number');
            $table->dropColumn('second_job');
            $table->dropColumn('medical_insurance');
            $table->dropColumn('second_job_company');
            $table->dropColumn('adress_second_job');
            $table->dropColumn('minijob');
            $table->dropColumn('minijob_work');
            $table->dropColumn('entry_date');
            $table->dropColumn('hours_per_week');
            $table->dropColumn('bank');
            $table->dropColumn('account_owner');
            $table->dropColumn('iban');
            $table->dropColumn('bic');
            $table->timestamp('created_at')->nullable()->default('NULL');
            $table->timestamp('updated_at')->nullable()->default('NULL');
            $table->timestamp('deleted_at')->nullable()->default('NULL');
        });
    }
}
