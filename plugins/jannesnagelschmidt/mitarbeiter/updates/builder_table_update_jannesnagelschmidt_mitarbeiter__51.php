<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiter51 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            $table->string('conversation_partner', 64);
            $table->date('conversation');
            $table->string('telephone', 10)->default(null)->change();
            $table->string('second_job_company', 64)->default(null)->change();
            $table->string('adress_second_job', 64)->default(null)->change();
            $table->string('minijob_work', 64)->default(null)->change();
            $table->string('bank', 64)->default(null)->change();
            $table->string('mobile_phone', 20)->default(null)->change();
            $table->string('private_phone', 20)->default(null)->change();
            $table->date('temporary_contract_until')->default(null)->change();
            $table->date('trial_period_until')->default(null)->change();
            $table->string('second_job_address', 64)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            $table->dropColumn('conversation_partner');
            $table->dropColumn('conversation');
            $table->string('telephone', 10)->default('NULL')->change();
            $table->string('second_job_company', 64)->default('NULL')->change();
            $table->string('adress_second_job', 64)->default('NULL')->change();
            $table->string('minijob_work', 64)->default('NULL')->change();
            $table->string('bank', 64)->default('NULL')->change();
            $table->string('mobile_phone', 20)->default('NULL')->change();
            $table->string('private_phone', 20)->default('NULL')->change();
            $table->date('temporary_contract_until')->default('NULL')->change();
            $table->date('trial_period_until')->default('NULL')->change();
            $table->string('second_job_address', 64)->default('NULL')->change();
        });
    }
}
