<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiter53 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            $table->string('department', 64);
            $table->integer('kundennummer');
            $table->integer('personalnummer');
            $table->date('contractsign');
            $table->date('parental_begin');
            $table->date('parental_end');
            $table->date('childbirth');
            $table->date('contract_end_sign');
            $table->date('contract_end');
            $table->string('contract_end_why', 100);
            $table->string('contract_end_by', 64);
            $table->string('contract_end_category');
            $table->string('telephone', 10)->default(null)->change();
            $table->string('second_job_company', 64)->default(null)->change();
            $table->string('adress_second_job', 64)->default(null)->change();
            $table->string('minijob_work', 64)->default(null)->change();
            $table->string('bank', 64)->default(null)->change();
            $table->string('mobile_phone', 20)->default(null)->change();
            $table->string('private_phone', 20)->default(null)->change();
            $table->date('temporary_contract_until')->default(null)->change();
            $table->date('trial_period_until')->default(null)->change();
            $table->string('second_job_address', 64)->default(null)->change();
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            $table->dropColumn('department');
            $table->dropColumn('kundennummer');
            $table->dropColumn('personalnummer');
            $table->dropColumn('contractsign');
            $table->dropColumn('parental_begin');
            $table->dropColumn('parental_end');
            $table->dropColumn('childbirth');
            $table->dropColumn('contract_end_sign');
            $table->dropColumn('contract_end');
            $table->dropColumn('contract_end_why');
            $table->dropColumn('contract_end_by');
            $table->dropColumn('contract_end_category');
            $table->string('telephone', 10)->default('NULL')->change();
            $table->string('second_job_company', 64)->default('NULL')->change();
            $table->string('adress_second_job', 64)->default('NULL')->change();
            $table->string('minijob_work', 64)->default('NULL')->change();
            $table->string('bank', 64)->default('NULL')->change();
            $table->string('mobile_phone', 20)->default('NULL')->change();
            $table->string('private_phone', 20)->default('NULL')->change();
            $table->date('temporary_contract_until')->default('NULL')->change();
            $table->date('trial_period_until')->default('NULL')->change();
            $table->string('second_job_address', 64)->default('NULL')->change();
            $table->timestamp('created_at')->nullable()->default('NULL');
            $table->timestamp('updated_at')->nullable()->default('NULL');
        });
    }
}
