<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiter55 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            $table->string('telephone', 10)->default(null)->change();
            $table->string('second_job_company', 64)->default(null)->change();
            $table->string('adress_second_job', 64)->default(null)->change();
            $table->string('minijob_work', 64)->default(null)->change();
            $table->string('bank', 64)->default(null)->change();
            $table->string('mobile_phone', 20)->default(null)->change();
            $table->string('private_phone', 20)->default(null)->change();
            $table->date('temporary_contract_until')->default(null)->change();
            $table->date('trial_period_until')->default(null)->change();
            $table->string('second_job_address', 64)->default(null)->change();
            $table->string('inventory', 100)->nullable()->change();
            $table->string('conversation_partner', 64)->nullable()->change();
            $table->date('conversation')->nullable()->change();
            $table->integer('kundennummer')->nullable()->change();
            $table->date('parental_begin')->nullable()->change();
            $table->date('parental_end')->nullable()->change();
            $table->date('childbirth')->nullable()->change();
            $table->date('contract_end_sign')->nullable()->change();
            $table->date('contract_end')->nullable()->change();
            $table->string('contract_end_why', 100)->nullable()->change();
            $table->string('contract_end_by', 64)->nullable()->change();
            $table->string('contract_end_category', 191)->nullable()->change();
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            $table->string('telephone', 10)->default('NULL')->change();
            $table->string('second_job_company', 64)->default('NULL')->change();
            $table->string('adress_second_job', 64)->default('NULL')->change();
            $table->string('minijob_work', 64)->default('NULL')->change();
            $table->string('bank', 64)->default('NULL')->change();
            $table->string('mobile_phone', 20)->default('NULL')->change();
            $table->string('private_phone', 20)->default('NULL')->change();
            $table->date('temporary_contract_until')->default('NULL')->change();
            $table->date('trial_period_until')->default('NULL')->change();
            $table->string('second_job_address', 64)->default('NULL')->change();
            $table->string('inventory', 100)->nullable(false)->change();
            $table->string('conversation_partner', 64)->nullable(false)->change();
            $table->date('conversation')->nullable(false)->change();
            $table->integer('kundennummer')->nullable(false)->change();
            $table->date('parental_begin')->nullable(false)->change();
            $table->date('parental_end')->nullable(false)->change();
            $table->date('childbirth')->nullable(false)->change();
            $table->date('contract_end_sign')->nullable(false)->change();
            $table->date('contract_end')->nullable(false)->change();
            $table->string('contract_end_why', 100)->nullable(false)->change();
            $table->string('contract_end_by', 64)->nullable(false)->change();
            $table->string('contract_end_category', 191)->nullable(false)->change();
            $table->timestamp('created_at')->nullable()->default('NULL');
            $table->timestamp('updated_at')->nullable()->default('NULL');
        });
    }
}
