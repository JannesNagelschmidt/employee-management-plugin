<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiter61 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('telephone', 10)->default(null)->change();
            $table->string('second_job_company', 64)->default(null)->change();
            $table->string('adress_second_job', 64)->default(null)->change();
            $table->string('minijob_work', 64)->default(null)->change();
            $table->string('bank', 64)->default(null)->change();
            $table->string('mobile_phone', 20)->default(null)->change();
            $table->string('private_phone', 20)->default(null)->change();
            $table->date('temporary_contract_until')->default(null)->change();
            $table->date('trial_period_until')->default(null)->change();
            $table->string('second_job_address', 64)->default(null)->change();
            $table->string('inventory', 100)->default(null)->change();
            $table->string('conversation_partner', 64)->default(null)->change();
            $table->date('conversation')->default(null)->change();
            $table->integer('kundennummer')->default(null)->change();
            $table->date('parental_begin')->default(null)->change();
            $table->date('parental_end')->default(null)->change();
            $table->date('childbirth')->default(null)->change();
            $table->date('contract_end_sign')->default(null)->change();
            $table->date('contract_end')->default(null)->change();
            $table->string('contract_end_why', 100)->default(null)->change();
            $table->string('contract_end_by', 64)->default(null)->change();
            $table->string('contract_end_category', 191)->default(null)->change();
            $table->boolean('end_contract')->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->string('telephone', 10)->default('NULL')->change();
            $table->string('second_job_company', 64)->default('NULL')->change();
            $table->string('adress_second_job', 64)->default('NULL')->change();
            $table->string('minijob_work', 64)->default('NULL')->change();
            $table->string('bank', 64)->default('NULL')->change();
            $table->string('mobile_phone', 20)->default('NULL')->change();
            $table->string('private_phone', 20)->default('NULL')->change();
            $table->date('temporary_contract_until')->default('NULL')->change();
            $table->date('trial_period_until')->default('NULL')->change();
            $table->string('second_job_address', 64)->default('\'null\'')->change();
            $table->string('inventory', 100)->default('NULL')->change();
            $table->string('conversation_partner', 64)->default('NULL')->change();
            $table->date('conversation')->default('NULL')->change();
            $table->integer('kundennummer')->default(NULL)->change();
            $table->date('parental_begin')->default('NULL')->change();
            $table->date('parental_end')->default('NULL')->change();
            $table->date('childbirth')->default('NULL')->change();
            $table->date('contract_end_sign')->default('NULL')->change();
            $table->date('contract_end')->default('NULL')->change();
            $table->string('contract_end_why', 100)->default('NULL')->change();
            $table->string('contract_end_by', 64)->default('NULL')->change();
            $table->string('contract_end_category', 191)->default('NULL')->change();
            $table->boolean('end_contract')->default(NULL)->change();
        });
    }
}
