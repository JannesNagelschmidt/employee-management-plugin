<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiter71 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            $table->text('work_repeater')->nullable();
            $table->integer('sub_department_relation')->nullable();
            $table->text('timedisplay')->default(null)->change();
            $table->dropColumn('workrepeater');
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            $table->dropColumn('work_repeater');
            $table->dropColumn('sub_department_relation');
            
        });
    }
}
