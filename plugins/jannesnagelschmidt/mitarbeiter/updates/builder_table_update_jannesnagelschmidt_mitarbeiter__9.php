<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiter9 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            $table->integer('hours_per_week');
            $table->string('trial_period', 64);
            $table->double('hourly_wage', 10, 2);
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            $table->dropColumn('hours_per_week');
            $table->dropColumn('trial_period');
            $table->dropColumn('hourly_wage');
        });
    }
}
