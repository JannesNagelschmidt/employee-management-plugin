<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterCalculated extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_calculated', function($table)
        {
            $table->integer('user_id')->default(null)->change();
            $table->dateTime('timestamp')->default(null)->change();
            $table->string('worked', 191)->default(null)->change();
            $table->string('break', 191)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_calculated', function($table)
        {
            $table->integer('user_id')->default(NULL)->change();
            $table->dateTime('timestamp')->default('NULL')->change();
            $table->string('worked', 191)->default('NULL')->change();
            $table->string('break', 191)->default('NULL')->change();
        });
    }
}
