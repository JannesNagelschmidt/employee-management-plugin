<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterCalculated3 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_calculated', function($table)
        {
            $table->boolean('needs_correction')->default(false);
            $table->integer('user_id')->default(null)->change();
            $table->dateTime('timestamp')->default(null)->change();
            $table->string('worked', 191)->default(null)->change();
            $table->string('break', 191)->default(null)->change();
            $table->date('lu_date')->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_calculated', function($table)
        {
            $table->dropColumn('needs_correction');
            $table->integer('user_id')->default(NULL)->change();
            $table->dateTime('timestamp')->default('NULL')->change();
            $table->string('worked', 191)->default('NULL')->change();
            $table->string('break', 191)->default('NULL')->change();
            $table->date('lu_date')->default('NULL')->change();
        });
    }
}
