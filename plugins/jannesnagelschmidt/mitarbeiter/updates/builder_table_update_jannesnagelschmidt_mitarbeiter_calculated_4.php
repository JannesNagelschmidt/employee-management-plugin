<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterCalculated4 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_calculated', function($table)
        {
            $table->boolean('manually_created')->nullable()->default(0);
            
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_calculated', function($table)
        {
            $table->dropColumn('manually_created');
            
        });
    }
}
