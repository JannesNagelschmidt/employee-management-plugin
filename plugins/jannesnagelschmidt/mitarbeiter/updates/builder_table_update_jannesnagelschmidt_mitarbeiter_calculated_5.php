<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterCalculated5 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_calculated', function($table)
        {
            $table->boolean('negative')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_calculated', function($table)
        {
            $table->dropColumn('negative');
        });
    }
}
