<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterChangerequest extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_changerequest', function($table)
        {
            $table->integer('user_id');
            $table->integer('leader_id');
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_changerequest', function($table)
        {
            $table->dropColumn('user_id');
            $table->dropColumn('leader_id');
        });
    }
}
