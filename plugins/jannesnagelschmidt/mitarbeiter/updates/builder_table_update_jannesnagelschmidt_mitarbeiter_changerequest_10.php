<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterChangerequest10 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_changerequest', function($table)
        {
            $table->string('type', 191)->nullable()->change();
            $table->string('status', 191)->nullable()->change();
            $table->string('memo', 191)->nullable()->change();
            $table->integer('user_id')->default(null)->change();
            $table->integer('leader_id')->default(null)->change();
            $table->dateTime('new_time')->default(null)->change();
            $table->dateTime('og_time')->default(null)->change();
            $table->string('timestamp_status', 191)->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_changerequest', function($table)
        {
            $table->string('type', 191)->nullable(false)->change();
            $table->string('status', 191)->nullable(false)->change();
            $table->string('memo', 191)->nullable(false)->change();
            $table->integer('user_id')->default(NULL)->change();
            $table->integer('leader_id')->default(NULL)->change();
            $table->dateTime('new_time')->default('NULL')->change();
            $table->dateTime('og_time')->default('NULL')->change();
            $table->string('timestamp_status', 191)->nullable(false)->change();
        });
    }
}
