<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterChangerequest11 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_changerequest', function($table)
        {
            $table->boolean('visible')->default(true);
            $table->string('type', 191)->default(null)->change();
            $table->string('status', 191)->default(null)->change();
            $table->string('memo', 191)->default(null)->change();
            $table->integer('user_id')->default(null)->change();
            $table->integer('leader_id')->default(null)->change();
            $table->dateTime('new_time')->default(null)->change();
            $table->dateTime('og_time')->default(null)->change();
            $table->string('timestamp_status', 191)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_changerequest', function($table)
        {
            $table->dropColumn('visible');
            $table->string('type', 191)->default('NULL')->change();
            $table->string('status', 191)->default('NULL')->change();
            $table->string('memo', 191)->default('NULL')->change();
            $table->integer('user_id')->default(NULL)->change();
            $table->integer('leader_id')->default(NULL)->change();
            $table->dateTime('new_time')->default('NULL')->change();
            $table->dateTime('og_time')->default('NULL')->change();
            $table->string('timestamp_status', 191)->default('NULL')->change();
        });
    }
}
