<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterChangerequest12 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_changerequest', function($table)
        {
            $table->integer('timestamp_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_changerequest', function($table)
        {
            $table->dropColumn('timestamp_id');
            $table->string('type', 191)->default('NULL')->change();
            $table->string('status', 191)->default('NULL')->change();
            $table->string('memo', 191)->default('NULL')->change();
            $table->integer('user_id')->default(NULL)->change();
            $table->integer('leader_id')->default(NULL)->change();
            $table->dateTime('new_time')->default('NULL')->change();
            $table->dateTime('og_time')->default('NULL')->change();
            $table->string('timestamp_status', 191)->default('NULL')->change();
        });
    }
}
