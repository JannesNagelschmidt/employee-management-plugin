<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterChangerequest7 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_changerequest', function($table)
        {
            $table->integer('user_id')->default(null)->change();
            $table->integer('leader_id')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_changerequest', function($table)
        {
            $table->integer('user_id')->default(NULL)->change();
            $table->integer('leader_id')->nullable(false)->change();
        });
    }
}
