<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterChangerequest8 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_changerequest', function($table)
        {
            $table->integer('user_id')->default(null)->change();
            $table->integer('leader_id')->default(null)->change();
            $table->dateTime('og_time')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_changerequest', function($table)
        {
            $table->integer('user_id')->default(NULL)->change();
            $table->integer('leader_id')->default(NULL)->change();
            $table->dateTime('og_time')->nullable(false)->change();
        });
    }
}
