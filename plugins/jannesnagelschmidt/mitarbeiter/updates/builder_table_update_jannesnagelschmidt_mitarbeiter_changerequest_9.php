<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterChangerequest9 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_changerequest', function($table)
        {
            $table->integer('user_id')->default(null)->change();
            $table->integer('leader_id')->default(null)->change();
            $table->dateTime('new_time')->nullable()->change();
            $table->dateTime('og_time')->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_changerequest', function($table)
        {
            $table->integer('user_id')->default(NULL)->change();
            $table->integer('leader_id')->default(NULL)->change();
            $table->dateTime('new_time')->nullable(false)->change();
            $table->dateTime('og_time')->default('NULL')->change();
        });
    }
}
