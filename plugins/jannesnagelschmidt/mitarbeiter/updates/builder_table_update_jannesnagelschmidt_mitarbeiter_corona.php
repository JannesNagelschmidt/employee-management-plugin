<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterCorona extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_corona', function($table)
        {
            $table->boolean('witness')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_corona', function($table)
        {
            $table->dropColumn('witness');
           
        });
    }
}
