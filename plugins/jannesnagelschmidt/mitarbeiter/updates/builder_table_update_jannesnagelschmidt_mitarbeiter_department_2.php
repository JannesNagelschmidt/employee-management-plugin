<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterDepartment2 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_department', function($table)
        {
            $table->integer('mitarbeiter_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_department', function($table)
        {
            $table->dropColumn('mitarbeiter_id');
        });
    }
}
