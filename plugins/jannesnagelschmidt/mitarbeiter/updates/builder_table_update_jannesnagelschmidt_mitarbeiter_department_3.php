<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterDepartment3 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_department', function($table)
        {
            $table->integer('leader_id')->nullable();
            $table->integer('mitarbeiter_id')->default(null)->change();
            $table->dropColumn('leader');
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_department', function($table)
        {
            $table->dropColumn('leader_id');
            $table->integer('mitarbeiter_id')->default(NULL)->change();
            $table->string('leader', 191);
        });
    }
}
