<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterDepartment5 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_department', function($table)
        {
            $table->integer('punctuallity')->default(10)->change();
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_department', function($table)
        {
            $table->integer('punctuallity')->default(NULL)->change();
        });
    }
}
