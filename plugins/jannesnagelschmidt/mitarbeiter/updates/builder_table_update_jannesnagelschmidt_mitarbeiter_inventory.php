<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterInventory extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_inventory', function($table)
        {
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_inventory', function($table)
        {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->dropColumn('deleted_at');
        });
    }
}