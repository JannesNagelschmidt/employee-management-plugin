<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterInventory2employee extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_inventory2employee', function($table)
        {
            $table->renameColumn('amount', 'pivot_amount');
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_inventory2employee', function($table)
        {
            $table->renameColumn('pivot_amount', 'amount');
        });
    }
}
