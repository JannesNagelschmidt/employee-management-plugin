<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterInventory2 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_inventory', function($table)
        {
            $table->integer('number')->nullable();
            $table->integer('mitarbeiter_id')->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_inventory', function($table)
        {
            $table->dropColumn('number');
            $table->integer('mitarbeiter_id')->default(NULL)->change();
        });
    }
}
