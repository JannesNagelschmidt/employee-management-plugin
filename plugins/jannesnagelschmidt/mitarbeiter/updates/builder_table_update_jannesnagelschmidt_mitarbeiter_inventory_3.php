<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterInventory3 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_inventory', function($table)
        {
            $table->integer('mitarbeiter_id')->default(null)->change();
            $table->dropColumn('number');
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_inventory', function($table)
        {
            $table->integer('mitarbeiter_id')->default(NULL)->change();
            $table->integer('number')->nullable()->default(NULL);
        });
    }
}
