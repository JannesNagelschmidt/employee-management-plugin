<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterInventory4 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_inventory', function($table)
        {
            $table->integer('amount')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_inventory', function($table)
        {
            $table->dropColumn('amount');
            });
    }
}
