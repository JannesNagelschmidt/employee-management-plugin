<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterNotifications extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_notifications', function($table)
        {
            $table->integer('leader_id');
            $table->integer('employ_id');
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_notifications', function($table)
        {
            $table->dropColumn('leader_id');
            $table->dropColumn('employ_id');
        });
    }
}
