<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterNotifications2 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_notifications', function($table)
        {
            $table->string('text', 128)->nullable()->change();
            $table->string('type', 128)->nullable()->change();
            $table->string('link', 128)->nullable()->change();
            $table->integer('leader_id')->nullable()->change();
            $table->integer('employ_id')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_notifications', function($table)
        {
            $table->string('text', 128)->nullable(false)->change();
            $table->string('type', 128)->nullable(false)->change();
            $table->string('link', 128)->nullable(false)->change();
            $table->integer('leader_id')->nullable(false)->change();
            $table->integer('employ_id')->nullable(false)->change();
        });
    }
}
