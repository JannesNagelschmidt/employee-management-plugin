<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterNotifications3 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_notifications', function($table)
        {
            $table->boolean('seen')->default(false);
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_notifications', function($table)
        {
            $table->dropColumn('seen');
     
        });
    }
}
