<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterNotifications4 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_notifications', function($table)
        {
            $table->integer('employ_id_id')->nullable();
            $table->dropColumn('employ_id');
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_notifications', function($table)
        {
            $table->dropColumn('employ_id_id');
            $table->integer('employ_id')->nullable()->default(NULL);
        });
    }
}
