<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterNotifications6 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_notifications', function($table)
        {
            $table->integer('request_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_notifications', function($table)
        {
            $table->dropColumn('request_id');
        });
    }
}
