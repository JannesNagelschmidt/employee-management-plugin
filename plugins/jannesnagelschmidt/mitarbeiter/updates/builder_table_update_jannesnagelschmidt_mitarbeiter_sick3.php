<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterSick3 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_sick', function($table)
        {
            $table->increments('id')->unsigned();
        });
    }

    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_sick', function($table)
        {
            $table->dropColumn('id');
        });
    }
}