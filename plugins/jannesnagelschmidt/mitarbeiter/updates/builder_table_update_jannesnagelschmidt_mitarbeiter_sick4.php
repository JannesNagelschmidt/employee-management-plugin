<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterSick4 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_sick', function($table)
        {
            $table->string('name', 32);
            $table->dateTime('end_day');
            $table->dateTime('start_day');
        });
    }

    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_sick', function($table)
        {
            $table->dropColumn('name');
            $table->dropColumn('end_day');
            $table->dropColumn('start_day');
        });
    }
}