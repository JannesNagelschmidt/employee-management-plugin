<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterSick3 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_sick', function($table)
        {
            $table->boolean('child_sick');
            $table->string('child_name')->nullable();
            $table->integer('sick_days')->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_sick', function($table)
        {
            $table->dropColumn('child_sick');
            $table->dropColumn('child_name');
            $table->integer('sick_days')->default(NULL)->change();
        });
    }
}
