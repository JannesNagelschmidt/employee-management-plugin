<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterSickInit extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('jannesnagelschmidt_mitarbeiter_sick')) {
            Schema::create('jannesnagelschmidt_mitarbeiter_sick', function($table){
                $table->integer('mitarbeiter_id')->unsigned();
                $table->timestamp('created_at');
                $table->timestamp('updated_at');
                $table->timestamp('deleted_at');
            });
       }
    }
    
    public function down()
    {
        
    }
}