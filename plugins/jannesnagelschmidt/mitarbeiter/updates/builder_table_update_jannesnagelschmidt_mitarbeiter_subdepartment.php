<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterSubdepartment extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_subdepartment', function($table)
        {
            $table->integer('employ_relation')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_subdepartment', function($table)
        {
            $table->integer('employ_relation')->nullable(false)->change();
        });
    }
}
