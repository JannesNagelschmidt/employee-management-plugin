<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterTalk extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_talk', function($table)
        {
            $table->date('date');
            $table->string('cause');
            $table->string('attendees');
            $table->string('matter');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_talk', function($table)
        {
            $table->dropColumn('date');
            $table->dropColumn('cause');
            $table->dropColumn('attendees');
            $table->dropColumn('matter');
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->dropColumn('deleted_at');
        });
    }
}