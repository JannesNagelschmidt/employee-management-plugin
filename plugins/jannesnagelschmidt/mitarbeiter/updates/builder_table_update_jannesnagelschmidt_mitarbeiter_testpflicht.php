<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterTestpflicht extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_testpflicht', function($table)
        {
            $table->increments('id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_testpflicht', function($table)
        {
            $table->dropColumn('id');
        });
    }
}
