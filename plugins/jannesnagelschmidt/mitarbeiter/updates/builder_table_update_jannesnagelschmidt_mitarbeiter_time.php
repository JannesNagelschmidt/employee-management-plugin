<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterTime extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_time', function($table)
        {
            $table->renameColumn('time_id', 'timetracking_id');
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_time', function($table)
        {
            $table->renameColumn('timetracking_id', 'time_id');
        });
    }
}