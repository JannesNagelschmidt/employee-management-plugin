<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterTime2 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_time', function($table)
        {
            $table->integer('m_id');
            $table->integer('t_id');
            $table->dropColumn('mitarbeiter_id');
            $table->dropColumn('timetracking_id');
            $table->primary(['m_id','t_id']);
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_time', function($table)
        {
            $table->dropPrimary(['m_id','t_id']);
            $table->dropColumn('m_id');
            $table->dropColumn('t_id');
            $table->integer('mitarbeiter_id');
            $table->integer('timetracking_id');
        });
    }
}
