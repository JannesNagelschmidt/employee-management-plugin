<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterTimetracking extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_timetracking', function($table)
        {
            $table->time('starttime')->nullable(false)->unsigned(false)->default(null)->change();
            $table->time('endtime')->nullable(false)->unsigned(false)->default(null)->change();
            $table->time('startbreak')->nullable(false)->unsigned(false)->default(null)->change();
            $table->time('endbreak')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_timetracking', function($table)
        {
            $table->dateTime('starttime')->nullable(false)->unsigned(false)->default(null)->change();
            $table->dateTime('endtime')->nullable(false)->unsigned(false)->default(null)->change();
            $table->dateTime('startbreak')->nullable()->unsigned(false)->default('NULL')->change();
            $table->dateTime('endbreak')->nullable()->unsigned(false)->default('NULL')->change();
        });
    }
}
