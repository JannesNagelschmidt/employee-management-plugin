<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterTimetracking12 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_timetracking', function($table)
        {
            $table->string('note', 109);
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_timetracking', function($table)
        {
            $table->dropColumn('note');
        });
    }
}
