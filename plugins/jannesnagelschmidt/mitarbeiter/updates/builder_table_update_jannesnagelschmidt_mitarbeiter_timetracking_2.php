<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterTimetracking2 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_timetracking', function($table)
        {
            $table->renameColumn('id', 'arbeitszeit_id');
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_timetracking', function($table)
        {
            $table->renameColumn('arbeitszeit_id', 'id');
        });
    }
}
