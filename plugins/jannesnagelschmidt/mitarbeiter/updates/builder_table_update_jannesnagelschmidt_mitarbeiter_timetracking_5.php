<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterTimetracking5 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_timetracking', function($table)
        {
            $table->time('timestamp');
            $table->dropColumn('starttime');
            $table->dropColumn('endtime');
            $table->dropColumn('startbreak');
            $table->dropColumn('endbreak');
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_timetracking', function($table)
        {
            $table->dropColumn('timestamp');
            $table->time('starttime');
            $table->time('endtime');
            $table->time('startbreak');
            $table->time('endbreak');
        });
    }
}
