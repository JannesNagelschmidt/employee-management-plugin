<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterTimetracking7 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_timetracking', function($table)
        {
            $table->text('tokenrepeater');
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_timetracking', function($table)
        {
            $table->dropColumn('tokenrepeater');
        });
    }
}
