<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterTokenTime extends Migration
{
    public function up()
{
    Schema::table('jannesnagelschmidt_mitarbeiter_token_time', function($table)
    {
        $table->primary(['t1_id','t2_id']);
    });
}

public function down()
{
    Schema::table('jannesnagelschmidt_mitarbeiter_token_time', function($table)
    {
        $table->dropPrimary(['t1_id','t2_id']);
    });
}
}