<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterTokenTime2 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_token_time', function($table)
        {
            $table->text('t1_id');
            $table->text('t2_id');
            $table->dropColumn('time_id');
            $table->dropColumn('token_id');
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_token_time', function($table)
        {
            $table->dropColumn('t1_id');
            $table->dropColumn('t2_id');
            $table->text('time_id');
            $table->text('token_id');
        });
    }
}