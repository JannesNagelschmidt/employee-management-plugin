<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterTokenTime3 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_token_time', function($table)
        {
            $table->string('t1_id', 32)->nullable(false)->unsigned(false)->default(null)->change();
            $table->string('t2_id', 32)->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_token_time', function($table)
        {
            $table->text('t1_id')->nullable(false)->unsigned(false)->default(null)->change();
            $table->text('t2_id')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
}
