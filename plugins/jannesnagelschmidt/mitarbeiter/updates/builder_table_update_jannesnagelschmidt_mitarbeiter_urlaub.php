<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterUrlaub extends Migration
{

    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_urlaub', function($table)
        {
            $table->integer('vacation_days')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_urlaub', function($table)
        {
            $table->dropColumn('vacation_days');
        });
    }
}
