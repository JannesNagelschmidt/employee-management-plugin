<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterUrlaub3 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_urlaub', function($table)
        {
            $table->integer('leader_id_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_urlaub', function($table)
        {
            $table->dropColumn('leader_id_id');
        });
    }
}
