<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterUrlaub5 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_urlaub', function($table)
        {
            $table->string('name', 191)->change();
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_urlaub', function($table)
        {
            $table->string('name', 32)->change();
        });
    }
}
