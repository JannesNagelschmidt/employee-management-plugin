<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterUrlaub6 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_urlaub', function($table)
        {
            $table->text('substitute')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_urlaub', function($table)
        {
            $table->dropColumn('substitute');
        });
    }
}
