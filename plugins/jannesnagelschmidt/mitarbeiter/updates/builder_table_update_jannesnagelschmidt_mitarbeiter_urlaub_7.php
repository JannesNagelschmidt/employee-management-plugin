<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJannesnagelschmidtMitarbeiterUrlaub7 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_urlaub', function($table)
        {
            $table->integer('substitute_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_urlaub', function($table)
        {
            $table->dropColumn('substitute_id');
        
        });
    }
}
