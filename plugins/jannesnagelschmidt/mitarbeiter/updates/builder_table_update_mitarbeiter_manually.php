<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMitarbeiterManually extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            $table->date('date_of_birth_child')->nullable();
            $table->string('telephone', 10)->default(null)->change();
            $table->string('birth_name', 64)->default(null)->change();
            $table->string('place_of_birth', 64)->default(null)->change();
            $table->string('street', 64)->default(null)->change();
            $table->string('house_number', 6)->default(null)->change();
            $table->string('plz', 64)->nullable()->unsigned(false)->default(null)->change();
            $table->string('city', 64)->default(null)->change();
            $table->string('email', 64)->default(null)->change();
            $table->integer('kinderfreibetrag')->default(null)->change();
            $table->string('denomination', 64)->default(null)->change();
            $table->integer('tax_class')->default(null)->change();
            $table->string('medical_insurance', 64)->default(null)->change();
            $table->string('second_job_company', 64)->default(null)->change();
            $table->string('adress_second_job', 64)->default(null)->change();
            $table->string('minijob_work', 64)->default(null)->change();
            $table->integer('hours_per_week_minijob')->default(null)->change();
            $table->string('bank', 64)->default(null)->change();
            $table->string('account_owner', 64)->default(null)->change();
            $table->string('iban', 30)->default(null)->change();
            $table->string('bic', 64)->default(null)->change();
            $table->integer('hours_per_week')->default(null)->change();
            $table->boolean('minijob')->default(null)->change();
            $table->string('mobile_phone', 20)->default(null)->change();
            $table->string('private_phone', 20)->default(null)->change();
            $table->string('tax_id', 11)->default(null)->change();
            $table->date('temporary_contract_until')->default(null)->change();
            $table->date('trial_period_until')->default(null)->change();
            $table->date('contract_start')->default(null)->change();
            $table->smallInteger('kids_number')->default(null)->change();
            $table->boolean('second_job')->default(null)->change();
            $table->string('country', 64)->default(null)->change();
            $table->string('second_job_address', 64)->default(null)->change();
            $table->boolean('temporary_contract')->default(null)->change();
            $table->boolean('trial_period')->default(null)->change();
            $table->boolean('key')->default(null)->change();
            $table->string('inventory', 100)->default(null)->change();
            $table->string('conversation_partner', 64)->default(null)->change();
            $table->date('conversation')->default(null)->change();
            $table->string('department', 64)->default(null)->change();
            $table->integer('kundennummer')->default(null)->change();
            $table->integer('personalnummer')->default(null)->change();
            $table->date('contractsign')->default(null)->change();
            $table->date('parental_begin')->default(null)->change();
            $table->date('parental_end')->default(null)->change();
            $table->date('childbirth')->default(null)->change();
            $table->date('contract_end_sign')->default(null)->change();
            $table->date('contract_end')->default(null)->change();
            $table->string('contract_end_why', 100)->default(null)->change();
            $table->string('contract_end_by', 64)->default(null)->change();
            $table->string('contract_end_category', 20)->default(null)->change();
            $table->boolean('end_contract')->default(null)->change();
            $table->string('employment_status', 64)->default(null)->change();
            $table->integer('sick_days')->default(null)->change();
            $table->text('timedisplay')->default(null)->change();
            $table->string('token', 64)->default(null)->change();
            $table->string('overtime', 120)->default(null)->change();
            $table->text('workrepeater')->default(null)->change();
            $table->text('status')->default(null)->change();
        });
    }

    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            $table->dropColumn('date_of_birth_child');
            $table->date('date_of_birth')->default(null)->change();
            $table->string('telephone', 10)->default(null)->change();
            $table->string('birth_name', 64)->default(null)->change();
            $table->string('place_of_birth', 64)->default(null)->change();
            $table->string('street', 64)->default(null)->change();
            $table->string('house_number', 6)->default(null)->change();
            $table->integer('plz')->nullable()->unsigned(false)->default(NULL)->change();
            $table->string('city', 64)->default(null)->change();
            $table->string('email', 64)->default(null)->change();
            $table->integer('kinderfreibetrag')->default(NULL)->change();
            $table->string('denomination', 64)->default(null)->change();
            $table->integer('tax_class')->default(NULL)->change();
            $table->string('medical_insurance', 64)->default(null)->change();
            $table->string('second_job_company', 64)->default(null)->change();
            $table->string('adress_second_job', 64)->default(null)->change();
            $table->string('minijob_work', 64)->default(null)->change();
            $table->integer('hours_per_week_minijob')->default(NULL)->change();
            $table->string('bank', 64)->default(null)->change();
            $table->string('account_owner', 64)->default(null)->change();
            $table->string('iban', 30)->default(null)->change();
            $table->string('bic', 64)->default(null)->change();
            $table->integer('hours_per_week')->default(NULL)->change();
            $table->double('hourly_wage', 10, 2)->default(NULL)->change();
            $table->boolean('minijob')->default(NULL)->change();
            $table->string('mobile_phone', 20)->default(null)->change();
            $table->string('private_phone', 20)->default(null)->change();
            $table->string('tax_id', 11)->default(null)->change();
            $table->date('temporary_contract_until')->default(null)->change();
            $table->date('trial_period_until')->default(null)->change();
            $table->date('contract_start')->default(null)->change();
            $table->smallInteger('kids_number')->default(NULL)->change();
            $table->boolean('second_job')->default(NULL)->change();
            $table->string('country', 64)->default(null)->change();
            $table->string('second_job_address', 64)->default(null)->change();
            $table->boolean('temporary_contract')->default(NULL)->change();
            $table->boolean('trial_period')->default(NULL)->change();
            $table->boolean('key')->default(NULL)->change();
            $table->string('inventory', 100)->default(null)->change();
            $table->string('conversation_partner', 64)->default(null)->change();
            $table->date('conversation')->default(null)->change();
            $table->string('department', 64)->default(null)->change();
            $table->integer('kundennummer')->default(NULL)->change();
            $table->integer('personalnummer')->default(NULL)->change();
            $table->date('contractsign')->default(null)->change();
            $table->date('parental_begin')->default(null)->change();
            $table->date('parental_end')->default(null)->change();
            $table->date('childbirth')->default(null)->change();
            $table->date('contract_end_sign')->default(null)->change();
            $table->date('contract_end')->default(null)->change();
            $table->string('contract_end_why', 100)->default(null)->change();
            $table->string('contract_end_by', 64)->default(null)->change();
            $table->string('contract_end_category', 20)->default(null)->change();
            $table->boolean('end_contract')->default(NULL)->change();
            $table->string('employment_status', 64)->default(null)->change();
            $table->integer('sick_days')->default(NULL)->change();
            $table->text('TimeDisplay')->default(null)->change();
            $table->string('token', 64)->default(null)->change();
            $table->string('overtime', 120)->default(null)->change();
            $table->text('workRepeater')->default(null)->change();
            $table->text('status')->default(null)->change();
        });
    }
}