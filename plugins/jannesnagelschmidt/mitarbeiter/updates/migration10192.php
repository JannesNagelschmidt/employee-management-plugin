<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration10192 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            if (!Schema::hasColumn('jannesnagelschmidt_mitarbeiter_', 'email_private')) {
                 $table->text('email_private')->nullable();            
            }
        });

        // Schema::create('jannesnagelschmidt_mitarbeiter_table', function($table)
        // {
        // });
    }

    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            if (Schema::hasColumn('jannesnagelschmidt_mitarbeiter_', 'email_private')) {
                $table->dropColumn('email_private');
            }
        });
        // Schema::drop('jannesnagelschmidt_mitarbeiter_table');
    }
}