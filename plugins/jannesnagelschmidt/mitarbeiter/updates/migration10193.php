<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration10193 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            if (!Schema::hasColumn('jannesnagelschmidt_mitarbeiter_', 'rest_urlaub_21')) {
                 $table->integer('rest_urlaub_21')->nullable();            
            }
        });
    }

    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            if (Schema::hasColumn('jannesnagelschmidt_mitarbeiter_', 'rest_urlaub_21')) {
                $table->dropColumn('rest_urlaub_21');
            }
        });
    }
}