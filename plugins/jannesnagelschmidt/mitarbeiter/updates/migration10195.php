<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration10195 extends Migration
{
    public function up()
    {
        // Schema::create('jannesnagelschmidt_mitarbeiter_table', function($table)
        // {
        // });
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            if (!Schema::hasColumn('jannesnagelschmidt_mitarbeiter_', 'boosted')) {
                $table->boolean('boosted')->nullable;        
            }
        });
        
    }

    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            if (Schema::hasColumn('jannesnagelschmidt_mitarbeiter_', 'boosted')) {
                $table->dropColumn('boosted');
            }
        });
        // Schema::drop('jannesnagelschmidt_mitarbeiter_table');
    }
}