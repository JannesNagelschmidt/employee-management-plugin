<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration10196 extends Migration
{
    public function up()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            if (!Schema::hasColumn('jannesnagelschmidt_mitarbeiter_', 'boosted')) {
                $table->boolean('boosted')->default(0);
            }
        });
    }

    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            if (Schema::hasColumn('jannesnagelschmidt_mitarbeiter_', 'boosted')) {
                $table->dropColumn('boosted');
            }
        });
    }
}