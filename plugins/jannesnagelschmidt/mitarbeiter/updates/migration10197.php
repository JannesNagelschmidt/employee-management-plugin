<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration10197 extends Migration
{
    public function up()
    {
       Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            if (!Schema::hasColumn('jannesnagelschmidt_mitarbeiter_', 'parent_time_repeater')) {
                $table->json('parent_time_repeater');
            }
        });
    }

    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            $table->json('parent_time_repeater')->delete();
        });
        // Schema::drop('jannesnagelschmidt_mitarbeiter_table');
    }
}