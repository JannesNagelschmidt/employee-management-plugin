<?php namespace JannesNagelschmidt\Mitarbeiter\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration10199 extends Migration
{
     public function up()
    {
        // Schema::create('jannesnagelschmidt_mitarbeiter_table', function($table)
        // {
        // });
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            if (!Schema::hasColumn('jannesnagelschmidt_mitarbeiter_', 'parent_time_repeater')) {
                $table->json('parent_time_repeater')->nullable;        
            }
        });
         Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            if (!Schema::hasColumn('jannesnagelschmidt_mitarbeiter_', 'emergency_repeater')) {
                $table->json('emergency_repeater')->nullable;        
            }
        });
        
    }

    public function down()
    {
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            if (Schema::hasColumn('jannesnagelschmidt_mitarbeiter_', 'parent_time_repeater')) {
                $table->dropColumn('parent_time_repeater');
            }
        });
        Schema::table('jannesnagelschmidt_mitarbeiter_', function($table)
        {
            if (Schema::hasColumn('jannesnagelschmidt_mitarbeiter_', 'emergency_repeater')) {
                $table->dropColumn('emergency_repeater');
            }
        });
        // Schema::drop('jannesnagelschmidt_mitarbeiter_table');
    }
}