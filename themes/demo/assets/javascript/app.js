import MaterialDateTimePicker from './material-datetime-picker.js';
import {MDCTextField} from '@material/textfield';
import {MDCDrawer} from "@material/drawer";
import {MDCList} from '@material/list';
import {MDCSelect} from '@material/select';
import {MDCDialog} from '@material/dialog';
import {MDCSnackbar} from '@material/snackbar';
import {MDCTabBar} from '@material/tab-bar';
import {MDCSwitch} from '@material/switch';


import duDatePicker from './duDatepicker.js';
import rome from './rome';
import table from './tableDisplay';
import vacation from './urlaubsplanung';
import calculation from './calculation';
import {MDCTopAppBar} from "@material/top-app-bar";


$(window).load(function() {
    var drawer = false
    var state = '';
    var notiOpen = false;
    var correctDate;
    const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    const optionsTimestamp = {}
    var list = $('.mdc-list');
    var menuDesktop = $('.material-icons.mdc-top-app-bar__navigation-icon.mdc-icon-button');
    if(document.querySelector('#vacationButtonToOpenIt')) {
        document.querySelector('#vacationButtonToOpenIt').addEventListener('datechanged', function(e) {
            console.log('New date', e.data, this.value)
        })
    }
    var today = moment();

    var user_id = $('input[name="user_id"]').val();
    var timePick = today.toLocaleString();
    correctDate = today;
    var sum = "";
    if($('#is_on_time').length) {
        $.request('onGetCalculatedData', {
            data:{time: timePick, user_id: user_id},
            async: false,
            success: function(data) {
                /*out += '<li role="separator" class="mdc-list-divider emt--list-divider"></li>';*/
                sum += '<li class="emt--list-summary mdc-list-item emt--list-item">';
                sum += '<span class="mdc-list-item__ripple"></span>';
                sum += '<span class="mdc-list-item__text emt--list-header_WORK">';
                sum += '<span class="mdc-list-item__primary-text emt--worktime-work">Arbeitszeit: '+data['worked']+'</span>';
                sum += '<span class="mdc-list-item__secondary-text emt--worktime-break">Pause: '+data['break']+'</span>';
                sum += '</span>';
                sum += '</li>';
            }
        });
        $.request('onGetWorkForDate',
            {
                data:{time: timePick, user_id: user_id},
                async: false,
                success: function(data) {
                    renderEntries(data, today, sum);
                }
            });
    }


    $(document).click(function(e) {
        var target = e.target;

        if (!$(target).is('.notification--wrapper') && !$(target).parents().is('.notification--wrapper')) {
            $('.notification--content').hide();
            notiOpen = false;
        }
    });
    $('.notification--wrapper').on('click', function(e) {
        var target = e.target;
        if($('.notification--content:hidden').length == 0 && !$(target).is('.material-icons.mdc-button__icon.notification--icon')) {
            $('.notification--content').hide();
            notiOpen = true;
        }
        else {
            if(notiOpen) {
                $('.notification--content').hide();
                notiOpen = false;
            }
            else if(!notiOpen){
                $('.notification--content').show();
                notiOpen = true;
            }
        }
    });

    if(document.querySelectorAll('.mdc-switch')) {
        for (const el of document.querySelectorAll('.mdc-switch')) {
            const switchControl = new MDCSwitch(el);
        }
    }
    if(document.querySelector('.mdc-drawer') != null) {
        drawer = MDCDrawer.attachTo(document.querySelector('.mdc-drawer'));
    }
    if(document.querySelector('.mdc-snackbar') != null) {
        var snackbar = new MDCSnackbar(document.querySelector('.mdc-snackbar'));
    }
    if(document.querySelector('.mdc-dialog_delete')) {
        var dialog = new MDCDialog(document.querySelector('.mdc-dialog_delete'));
    }
    if(document.querySelector('.mdc-dialog_vacation')) {
        var dialog4 = new MDCDialog(document.querySelector('.mdc-dialog_vacation'));
    }
    if(document.querySelector('.mdc-tab-bar')) {
        const tabBar = new MDCTabBar(document.querySelector('.mdc-tab-bar'));
        var contentEls = document.querySelectorAll('.tab-content');

        tabBar.listen('MDCTabBar:activated', function(event) {
            // Hide currently-active content
            document.querySelector('.tab-content--active').classList.remove('tab-content--active');
            // Show content for newly-activated tab
            contentEls[event.detail.index].classList.add('tab-content--active');
        });
    }
    if(document.querySelector('.mdc-dialog_add')) {
        var dialog2 = new MDCDialog(document.querySelector('.mdc-dialog_add'));
    }
    if(document.querySelector('.mdc-dialog_correct')) {
        var dialog3 = new MDCDialog(document.querySelector('.mdc-dialog_correct'));
    }


    if($('.punctually--item').length) {
        $('.punctually--item').on('click', function() {
            $(this).find('.punctually--timeitem-wrapper').toggle();
            $(this).find('.punctually--icon-close').toggle();
            $(this).find('.punctually--icon-open').toggle();
        });
    }
    $('.emt--login-form').ajaxComplete(function(event, xhr, settings) {
        console.log(event);
        console.log(xhr);
        console.log(settings);
    });



    $('#email_notification').on('click', function(e) {
        var notifcations = $(this).context.ariaChecked;
        var id = $(this).data('user_id');
        console.log($(this).context.ariaChecked);
        e.stopPropagation();
        $.request('onChangeMailNotification', {
            data: {mail: notifcations, id: id},
            success: function(data) {
                snackbar.labelText = 'Erfolgreich geändert';
                snackbar.open();
            }
        });
    });
    $('#birthday_dashboard').on('click', function(e) {
        var notifcations = $(this).context.ariaChecked;
        var id = $(this).data('user_id');
        console.log($(this).context.ariaChecked);
        e.stopPropagation();
        $.request('onChangeBirthdayVisible', {
            data: {birthday: notifcations, id: id},
            success: function(data) {
                snackbar.labelText = 'Erfolgreich geändert';
                snackbar.open();
            }
        });
    });

    $('.notification--link ').on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        var id = $(this).data('id');
        var link = $(this).attr('href');
        $.request('onSetNotifactionSeen',
            {
                data:{id: id},
                success: function(data) {
                    setTimeout(function(){
                        location.href = link;
                    }, 50);
                }
            });
    });

    $('.mdc-dialog_add').ajaxComplete(function(event, xhr, settings) {
        console.log(settings.status);
        if(settings.status == 500) {
            snackbar.labelText = 'Bitte erforderliche Felder ausfüllen!';
        }
        else {
            snackbar.labelText = 'Antrag Erfolgreich erstellt';
        }
        snackbar.open();
        setTimeout(
            function(){
                location.reload();
            }, 500
        );
    });

    $('.emt--internalcorrection-form').ajaxComplete(function(event, xhr, settings) {
        console.log(settings.status);
        if(settings.status == 500) {
            snackbar.labelText = 'Bitte erforderliche Felder ausfüllen!';
        }
        else {
            snackbar.labelText = 'Korrektur Erfolgreich erstellt';
        }
        snackbar.open();
    });



    $('.mdc-dialog_vacation ').ajaxComplete(function(event, xhr, settings){
        if(settings.status == 500) {
            snackbar.labelText = 'Bitte erforderliche Felder ausfüllen!';
        }
        else {
            snackbar.labelText = 'Antrag Erfolgreich erstellt';
        }
        snackbar.open();
    });

    $('.mdc-dialog_correct').ajaxComplete(function(event, xhr, settings) {
        if(settings.status == 500) {
            snackbar.labelText = 'Bitte erforderliche Felder ausfüllen!';
        }
        else {
            snackbar.labelText = 'Antrag Erfolgreich erstellt';
        }
        snackbar.open();
    });
    $('.mdc-dialog_delete').ajaxComplete(function(event, xhr, settings) {
        snackbar.labelText = 'Antrag Erfolgreich erstellt';
        snackbar.open();
    });

    $('.emt--lead_button-deny').on('click', function() {
        var dialogid = $(this).data('denydialog');
        var denyMemo = new MDCDialog(document.querySelector('.mdc-dialog_decline-'+dialogid));
        var textFieldDeny = new MDCTextField(document.querySelector('.mdc-text-field_deny-'+dialogid));
        denyMemo.open();
    })
    $('.emt--lead_button-accept').on('click', function() {
        var id = $(this).data('id');
        var status = $(this).data('status');
        var user_id = $(this).data('user_id');
        var isVacation = 0;

        if(confirm('Wirklich bestätigen?')) {
            $.request('onChangeStatus',
                {
                    data:{status: status, id: id, user_id: user_id, isVacation: isVacation},
                    success: function(data) {
                        setTimeout(function(){// wait for 5 secs(2)
                            location.reload(); // then reload the page.(3)
                        }, 500);
                    }
                });
        }
    })

    $('.emt--lead_button-accept_vacation').on('click', function() {
        var id = $(this).data('id');
        var status = $(this).data('status');
        var user_id = $(this).data('user_id');
        var isVacation = 1;
        if(confirm('Wirklich bestätigen?')) {
            $.request('onChangeStatus',
                {
                    data:{status: status, id: id, user_id: user_id, isVacation: isVacation},
                    success: function(data) {
                        setTimeout(function(){// wait for 5 secs(2)
                            location.reload(); // then reload the page.(3)
                        }, 500);
                    }
                });
        }
    })

    $('#emt--time_listOut').on('click', '.emt--button-delete', function() {
        var user_id = $(this).data('user_id');
        var name = $(this).data('name');
        var id = $(this).parent().parent().data('id');
        $('#emt_time_id_delete').val(id);
        dialog.open();
    });
    $('#emt--time_listOut').on('click', '.emt--button-add', function() {
        var id = $(this).data('id');
        var user_id = $(this).data('user_id');
        var name = $(this).data('name');
        $('#timeShow').text("Uhrzeit wählen");
        dialog2.open();
        var select = new MDCSelect(document.querySelector('.mdc-select2'));

        select.listen('MDCSelect:change', () => {
            state = select.value;
            $('#timestamp_status').val(state);
            console.log(state);
        });
    });
    $('#emt--time_listOut').on('click', '.emt--lead_button-memo', function() {
        var memo_num = $(this).data('memo');
        var dialogMemo = new MDCDialog(document.querySelector('.mdc-dialog_memo-'+memo_num));
        var textFieldMemo = new MDCTextField(document.querySelector('.mdc-text-field_'+memo_num));
        dialogMemo.open();
       console.log(memo_num);
    });
    $('#emt--time_listOut2').on('click', '.emt--button_request-edit_vacation', function() {
        var memo_num = $(this).data('memo');
        var dialogMemo = new MDCDialog(document.querySelector('.mdc-dialog_memo_vacation-'+memo_num));
        var textFieldMemo = new MDCTextField(document.querySelector('.mdc-text-field_vacation_'+memo_num));
        dialogMemo.open();
        console.log(memo_num);
    });
    //emt--button_request-edit_vacation
   /* $('.emt--time-output').on('click', '.emt--button_request-edit', function() {

        console.log($(this));
    })*/

    /*$('.emt--time-output').on('click', '.mdc-list-item.emt--list-item.emt--list-item_leader', function() {
        console.log($(this).data('vacationplanid'));
        var vacPlanId = $(this).data('vacationplanid');
        var dialogtoopen = new MDCDialog(document.querySelector('.mdc-dialog_vacationplan-'+vacPlanId));
        var toAdd = $(dialogtoopen + ' .mdc-dialog__content');
        dialogtoopen.open();
    });*/

    $('#vacationRequest').on('click', function() {
       dialog4.open();
    });

    if(document.querySelector('.mdc-select_internalcorrection')) {
        var selectCorrection = new MDCSelect(document.querySelector('.mdc-select_internalcorrection'));
        selectCorrection.listen('MDCSelect:change', () => {
            state = selectCorrection.value;
            $('#employ_id').val(state);
        });
    }
    //mdc-select5
    if(document.querySelector('.mdc-select5')) {
        var selectCorrection3 = new MDCSelect(document.querySelector('.mdc-select5'));
        selectCorrection3.listen('MDCSelect:change', () => {
            state = selectCorrection3.value;
            $('#sub').val(state);
        });
    }
    if(document.querySelector('.mdc-select_internalcorrection2')) {
        var selectCorrection2 = new MDCSelect(document.querySelector('.mdc-select_internalcorrection2'));
        selectCorrection2.listen('MDCSelect:change', () => {
            state = selectCorrection2.value;
            $('#type').val(state);
        });
    }

    $('#emt--time_listOut').on('click', '.emt--button-correct', function() {
        var id = $(this).parent().parent().data('id');
        var user_id = $(this).parent().parent().data('user_id');
        var status = $(this).data('status');
        var oldtime = new Date($(this).data('pretime'));
        var oldtimeReal = oldtime.toLocaleTimeString([], {hour12: false, hour: '2-digit', minute: '2-digit'});
        console.log(id);
        console.log(user_id);
        $('#emt_time_id').val(id);
        var select = new MDCSelect(document.querySelector('.mdc-select1'));
        $('#emt_time_correct').val(oldtimeReal);
        $('#timeShow2').text('Uhrzeit: '+oldtimeReal);
        select.value = status;
        $('#timestamp_status_correct').val(status)
        select.listen('MDCSelect:change', () => {
            state = select.value;
            $('#timestamp_status_correct').val(state);
            console.log(state);
        });
        dialog3.open();

    })

    $('.main-content').on('click', function() {
        if(drawer) {
            if(drawer.open) {
                drawer.open = !drawer.open;
            }
        }
    });
    if(menuDesktop) {
        menuDesktop.on('click', function() {
            drawer.open = !drawer.open;
        })
    }

    /*
     * Application
     */

    if(document.querySelector('.mdc-text-field1') != null) {
        const textField1 = new MDCTextField(document.querySelector('.mdc-text-field1'));
    }
    if(document.querySelector('.mdc-text-field2') != null) {
        const textField2 = new MDCTextField(document.querySelector('.mdc-text-field2'));
    }
    if(document.querySelector('.mdc-text-field3') != null) {
        const textFiel3 = new MDCTextField(document.querySelector('.mdc-text-field3'));
    }
    if(document.querySelector('.emp--label-wrapper1') != null) {
        const textField4 = new MDCTextField(document.querySelector('.emp--label-wrapper1'));
    }
    if(document.querySelector('.emp--label-wrapper2') != null) {
        const textField5 = new MDCTextField(document.querySelector('.emp--label-wrapper2'));
    }
    if(document.querySelector('.mdc-text-field_vacation') != null) {
        const textField6 = new MDCTextField(document.querySelector('.mdc-text-field_vacation'));
    }
    //mdc-text-field_vacation
    /*if(document.querySelector('.mdc-text-field4') != null) {
        const textField4 = new MDCTextField(document.querySelector('.mdc-text-field4'));
    }*/
    var output = $('.my_out');
    var states = {
        coming: 'Kommt',
        leaving: 'Geht',
        break_start: 'Pause Start',
        break_end: 'Pause Ende',
        URLAUB: 'URLAUB',
        KRANK: 'KRANK',
        forgot: 'Vergessen'
    }


$('.emt--time-output').on('click', '.emt--button-prev', function() {
    var user_id = $('input[name="user_id"]').val();
    var newDate = correctDate.subtract(1, 'day');
    var timePick = newDate.toLocaleString();
    $('#emt_time_add').val(timePick);
    var sum = "";
    $.request('onGetCalculatedData', {
        data:{time: timePick, user_id: user_id},
        async: false,
        success: function(data) {
            /*out += '<li role="separator" class="mdc-list-divider emt--list-divider"></li>';*/
            sum += '<li class="emt--list-summary mdc-list-item emt--list-item">';
            sum += '<span class="mdc-list-item__ripple"></span>';
            sum += '<span class="mdc-list-item__text emt--list-header_WORK">';
            sum += '<span class="mdc-list-item__primary-text emt--worktime-work">Arbeitszeit: '+data['worked']+'</span>';
            sum += '<span class="mdc-list-item__secondary-text emt--worktime-break">Pause: '+data['break']+'</span>';
            sum += '</span>';
            sum += '</li>';
        }
    });
    $.request('onGetWorkForDate',
        {
            data:{time: timePick, user_id: user_id},
            async: false,
            success: function(data) {
                renderEntries(data, newDate, sum);
            }
        });
});

$('.emt--time-output').on('click', '.emt--button-next', function() {
    var user_id = $('input[name="user_id"]').val();
    var newDate = correctDate.add(1, 'day');
    var timePick = newDate.toLocaleString();
    $('#emt_time_add').val(timePick);
    var sum = "";
    $.request('onGetCalculatedData', {
        data:{time: timePick, user_id: user_id},
        async: false,
        success: function(data) {
            /*out += '<li role="separator" class="mdc-list-divider emt--list-divider"></li>';*/
            sum += '<li class="emt--list-summary mdc-list-item emt--list-item">';
            sum += '<span class="mdc-list-item__ripple"></span>';
            sum += '<span class="mdc-list-item__text emt--list-header_WORK">';
            sum += '<span class="mdc-list-item__primary-text emt--worktime-work">Arbeitszeit: '+data['worked']+'</span>';
            sum += '<span class="mdc-list-item__secondary-text emt--worktime-break">Pause: '+data['break']+'</span>';
            sum += '</span>';
            sum += '</li>';
        }
    });
    $.request('onGetWorkForDate',
        {
            data:{time: timePick, user_id: user_id},
            async: false,
            success: function(data) {
                renderEntries(data, newDate, sum);
            }
        });
});

function renderEntries(data, d, sum) {
    var states = {
        coming: 'Kommt',
        leaving: 'Geht',
        break_start: 'Pause Start',
        break_end: 'Pause Ende',
        URLAUB: 'URLAUB',
        KRANK: 'KRANK',
        vacation: 'URLAUB',
        sick: 'KRANK',
        korrektur: 'Korrektur',
        forgot: 'Vergessen'
    }
    var out = '';
    if(data.length) {
        for(var i = 0; i < data.length; i++) {
            if(i == 0) {
                out += '<div class="emt--list-wrapper_head">';
                out += '<div class="emt--list-button_container">';
                out += '<button class="emt--button-prev emt--list-button mdc-button mdc-button--outlined mdc-button--touch"><i class="material-icons mdc-list-item__graphic" aria-hidden="true">chevron_left</i></button>';
                out += '<p class="emt--list-header mdc-list-item__primary-text">'+d._d.toLocaleDateString('de-DE', options)+'</p>';
                out += '<button class="emt--button-next emt--list-button mdc-button mdc-button--outlined mdc-button--touch"><i class="material-icons mdc-list-item__graphic" aria-hidden="true">chevron_right</i></button>';
                out += '</div>';
                out += '<button class="emt--button-add emt--list-button mdc-button mdc-button--outlined mdc-button--touch" tabindex="-1">';
                out += '<i class="material-icons mdc-list-item__graphic" aria-hidden="true">add</i>Hinzufügen';
                out += '</button>';
                out += '</div>';
                /*out += '<li role="separator" class="mdc-list-divider"></li>';*/
                out += sum;
                out += '<li role="separator" class="mdc-list-divider"></li>';
            }
            out += '<li class="mdc-list-item emt--list-item" data-id="'+data[i]['id']+'" data-user_id="'+data[i]['user_id']+'">';
            out += '<span class="mdc-list-item__ripple"></span>';
            out += '<span class="mdc-list-item__text">';
            const theDate = new Date(data[i]['timestamp']);
            out += '<span class="mdc-list-item__primary-text">'+theDate.toLocaleTimeString('de-DE')+'</span>';
            /*out += '<span class="mdc-list-item__primary-text">'+data[i]['timestamp']+'</span>';*/
            out += '<span class="mdc-list-item__secondary-text">'+states[data[i]['status']]+'</span>';
            out += '</span>';
            out += '<span class="mdc-list-item__graphic emt--list-item__graphic emt--list-item__graphic-desktop">';
            out += '<button data-status="'+data[i]['status']+'" data-pretime="'+data[i]['timestamp']+'" class="emt--button-correct emt--list-button mdc-button mdc-button--outlined mdc-button--touch" tabindex="-1">';
            out += '<i class="material-icons mdc-list-item__graphic" aria-hidden="true">edit</i>Bearbeiten';
            out += '</button>';
            out += '<button class="emt--button-delete emt--list-button mdc-button mdc-button--outlined mdc-button--touch" tabindex="-1">';
            out += '<i class="material-icons mdc-list-item__graphic" aria-hidden="true">delete</i>Löschen';
            out += '</button>';
            out += '</span>';
            out += '<span class="mdc-list-item__graphic emt--list-item__graphic-mobile">';
            out += '<button data-status="'+data[i]['status']+'" data-pretime="'+data[i]['timestamp']+'" class="emt--button-correct emt--list-button mdc-button mdc-button--outlined mdc-button--touch" tabindex="-1">';
            out += '<i class="material-icons mdc-list-item__graphic" aria-hidden="true">edit</i>';
            out += '</button>';
            /*out += '<button class="emt--button-add emt--list-button mdc-button mdc-button--outlined mdc-button--touch" tabindex="-1">';
            out += '<i class="material-icons mdc-list-item__graphic" aria-hidden="true">add</i>';
            out += '</button>';*/
            out += '<button class="emt--button-delete emt--list-button mdc-button mdc-button--outlined mdc-button--touch" tabindex="-1">';
            out += '<i class="material-icons mdc-list-item__graphic" aria-hidden="true">delete</i>';
            out += '</button>';
            out += '</span>';
            out += '</li>';
            out += '<li role="separator" class="mdc-list-divider emt--list-divider"></li>';
        }
    }
    else {
        out += '<div class="emt--list-wrapper_head">';
        out += '<div class="emt--list-button_container">';
        out += '<button class="emt--button-prev emt--list-button mdc-button mdc-button--outlined mdc-button--touch"><i class="material-icons mdc-list-item__graphic" aria-hidden="true">chevron_left</i></button>';
        out += '<p class="emt--list-header mdc-list-item__primary-text">Keine Zeiteinträge für '+d._d.toLocaleDateString('de-DE', options)+' gefunden.</p>';
        out += '<button class="emt--button-next emt--list-button mdc-button mdc-button--outlined mdc-button--touch"><i class="material-icons mdc-list-item__graphic" aria-hidden="true">chevron_right</i></button>';
        out += '</div>';
        out += '<button class="emt--button-add emt--list-button mdc-button mdc-button--outlined mdc-button--touch" tabindex="-1">';
        out += '<i class="material-icons mdc-list-item__graphic" aria-hidden="true">add</i>Hinzufügen';
        out += '</button>';
        /*out += '<li role="separator" class="mdc-list-divider"></li>';*/
        out += '</div>';
    }
    $('#emt--time_listOut').empty();
    $('#emt--time_listOut').append(out);
}

var picker = new MaterialDateTimePicker({})
    .on('submit', function(d) {
        var timePick = d.toLocaleString();
        var user_id = $('input[name="user_id"]').val();
        correctDate = d;
        var formatedDate = correctDate.format('YYYY-MM-DD');
        $('#date_add').val(formatedDate);
        $('#date_correct').val(formatedDate);
        var out = "";
        var sum = "";
        $.request('onGetCalculatedData', {
            data:{time: timePick, user_id: user_id},
            async: false,
            success: function(data) {
                /*out += '<li role="separator" class="mdc-list-divider emt--list-divider"></li>';*/
                sum += '<li class="emt--list-summary mdc-list-item emt--list-item">';
                sum += '<span class="mdc-list-item__ripple"></span>';
                sum += '<span class="mdc-list-item__text emt--list-header_WORK">';
                sum += '<span class="mdc-list-item__primary-text emt--worktime-work">Arbeitszeit: '+data['worked']+'</span>';
                sum += '<span class="mdc-list-item__secondary-text emt--worktime-break">Pause: '+data['break']+'</span>';
                sum += '</span>';
                sum += '</li>';
            }
        });
        $.request('onGetWorkForDate',
            {
                data:{time: timePick, user_id: user_id},
                async: false,
                success: function(data) {
                    renderEntries(data, d, sum);
                }
            });
        //console.log(d._d);
    });

    var el = $('#id-of-button-to-open-it');
    if(document.querySelector('#add_time_button')) {
        var time = $('#add_time_button');
    }

    var correction_date = $('#correction_date_picker');
    var time2 = $('#correct_time_button');
    var vacation = $('#vacationButtonToOpenIt');
    var calculation_date_picker = $('#calculationButtonToOpenIt')

    if(calculation_date_picker.length == 0) {
    }
    else {
        if(document.querySelector('#calculationButtonToOpenIt')) {
            document.querySelector('#calculationButtonToOpenIt').addEventListener('datechanged', function(e) {
                //console.log('New date', e.data, this.value)
            })
            duDatePicker('#calculationButtonToOpenIt', {
                format: 'dd.mm.yyyy', range: true, clearBtn: true,
                events: {
                    dateChanged: function (data) {
                        $('#dateFrom').val(data.dateFrom);
                        $('#dateFromShow').text('Von: ' + data.dateFrom);
                        $('#dateTo').val(data.dateTo);
                        $('#dateToShow').text('Bis: ' + data.dateTo)
                        console.log( data.dateFrom + '\nTo: ' + data.dateTo);
                    },

                }
            })
        }
    }

    if(vacation.length == 0) {
    }
    else {
        if(document.querySelector('#vacationButtonToOpenIt')) {
            document.querySelector('#vacationButtonToOpenIt').addEventListener('datechanged', function(e) {
                //console.log('New date', e.data, this.value)
            })
            duDatePicker('#vacationButtonToOpenIt', {
                format: 'dd.mm.yyyy', range: true, clearBtn: true,
                disabledDays: ['Sam', 'Son'],
                events: {
                    dateChanged: function (data) {
                        $('#dateFrom').val(data.dateFrom);
                        $('#dateFromShow').text('Von: ' + data.dateFrom);
                        $('#dateTo').val(data.dateTo);
                        $('#dateToShow').text('Bis: ' + data.dateTo)
                        console.log( data.dateFrom + '\nTo: ' + data.dateTo);
                    },

                }
            })
        }
    }
    if(typeof el != 'undefined') {
        el.on('click', function() {
            picker.open();
        });
    }
    if(typeof time != 'undefined') {
        time.on('click', function() {
            var timePicker = new MaterialDateTimePicker({
                container: document.querySelector('.mdc-dialog_add'),
                default: correctDate,
                ampm: false
            }).on('submit', function(d) {
                console.log(d);
                var timeAdd = d.toLocaleString();
                var user_id = $('input[name="user_id"]').val();
                $('#emt_time_add').val(timeAdd);
                $('#timeShow').text('Uhrzeit: '+d.format('HH:mm'));

            });
            timePicker.open();

        });
    }
    if(typeof correction_date != 'undefined') {
        correction_date.on('click', function () {
            console.log("clicked");
            var correctionPicker = new MaterialDateTimePicker({})
                .on('submit', function(d) {
                    var timePick = d.toLocaleString();
                    //var user_id = $('input[name="user_id"]').val();
                    correctDate = d;
                    var formatedDate = correctDate.format('YYYY-MM-DD');
                    console.log(formatedDate);
                    $('#correction_date_show').text('Datum: '+correctDate.format('DD.MM.YYYY'))
                    $('#input_date').val(formatedDate);
                });
            correctionPicker.open();
        });
    }
    if(time2.length == 0) {
    }
    else {
        time2.on('click', function() {
            console.log(correctDate);
            var timePicker2 = new MaterialDateTimePicker({
                container: document.querySelector('.mdc-dialog_correct'),
                default: correctDate,
                ampm: false,
            }).on('submit', function(d) {
                console.log(d);
                var timeAdd = d.toLocaleString();
                var user_id = $('input[name="user_id"]').val();
                $('#emt_time_correct').val(timeAdd);
                $('#timeShow2').text('Uhrzeit: '+d.format('HH:mm'));
            });
            timePicker2.open();
        });
    }

});



/*const listEl = document.querySelector('.mdc-drawer .mdc-list');


listEl.addEventListener('click', (event) => {
    mainContentEl.querySelector('input, button').focus();
});

listEl.addEventListener('click', (event) => {
    drawer.open = false;
});*/
/*if($('#app-bar').length) {
    var topbar = $('#app-bar');
    //topbar.setScrollTarget(document.getElementById('main-content'));
    topbar.listen('MDCTopAppBar:nav', () => {
        drawer.open = !drawer.open;
    });
}*/







/*
$(document).tooltip({
    selector: "[data-toggle=tooltip]"
})*/

/*
 * Auto hide navbar
 */
jQuery(document).ready(function($){
    var $header = $('.navbar-autohide'),
        scrolling = false,
        previousTop = 0,
        currentTop = 0,
        scrollDelta = 10,
        scrollOffset = 150

    $(window).on('scroll', function(){
        if (!scrolling) {
            scrolling = true

            if (!window.requestAnimationFrame) {
                setTimeout(autoHideHeader, 250)
            }
            else {
                requestAnimationFrame(autoHideHeader)
            }
        }
    })

    function autoHideHeader() {
        var currentTop = $(window).scrollTop()

        // Scrolling up
        if (previousTop - currentTop > scrollDelta) {
            $header.removeClass('is-hidden')
        }
        else if (currentTop - previousTop > scrollDelta && currentTop > scrollOffset) {
            // Scrolling down
            $header.addClass('is-hidden')
        }

        previousTop = currentTop
        scrolling = false
    }
});