import {MDCSnackbar} from "@material/snackbar";

$(function(e) {
    if(document.querySelector('.mdc-snackbar') != null) {
        var snackbar = new MDCSnackbar(document.querySelector('.mdc-snackbar'));
    }
    const button = $('#calculation_button');

    button.on("click", function() {
        let id = $('#employ_id').val();
        let start = $('#dateFrom').val();
        let end = $('#dateTo').val();
        if(id.length && start.length && end.length) {
            ajaxCall2().then( response =>
                newCalendar(response),
            );
            getPlusMinus(id);
        }
        else {
            snackbar.labelText = 'Bitte erforderliche Felder ausfüllen!';
            snackbar.open();
        }
    });
});

function getPlusMinus(id) {
    $.ajax({
        url: "/getPlusMinusInCalc",
        type: "POST",
        data: {id: id},
        success: function(data){
            if(data) {
                $('.emt--calculation-wrapper').append("<p>Stundenkonto: "+data.plusminus.time+"</p>");
            }
            return data;
        }
    });
}

function newCalendar(data) {
    console.log(data);
    //console.log( document.getElementById('calendar'));
    //$('.loading--background').show();
    console.log("adding...");
    var calendarEl = document.getElementById('calculation_out');
    var calendar = new FullCalendar.Calendar(calendarEl, {
        initialView: 'dayGridMonth',
        locale: 'de',
        weekNumbers: true,
        firstDay: 1,
        weekText: 'KW',
        buttonText: {
            today: 'Heute'
        },
        rerenderDelay: 100,
        eventClick: function (info) {
            $('.emt--timestamp-tooltip').remove();
            //console.log(info.event.extendedProps);
            //console.log(info.el);
            var itemPos = $(info.el).position();
            //console.log(itemPos);
            var timestamps = info.event.extendedProps['timestamps'];
            var soll = info.event.extendedProps['soll'];
            if(soll) {
                var out = '<div class="emt--timestamp-tooltip emt--timestamp-tooltip_soll">';
            }
            else {
                var out = '<div class="emt--timestamp-tooltip">';
            }
            out += '<i class="material-icons mdc-button__icon close--icon">close</i>';
            if(timestamps) {
                for (let p = 0; p < timestamps.length; p++) {
                    out += '<p class="emt--timestamp-entry">'+timestamps[p]['timestamp']+' - '+timestamps[p]['status']+'</p>';
                }
            }
            if(soll) {
                out += '<p class="emt--timestamp-entry">SOLL: '+soll+'</p>';
            }
            out += '</div>';
            $(info.el).parent().append(out);
        }
    });
    calendar.render();
    //addCalendarEvents(data, calendar);
    //getWorkFullCalendar(user_id, calendar);
    console.log("adding events: "+ data['entries'].length);
    var punctuallity = data['toolate'];
    var holidays = data['holidays'];

    for(let i = 0; i < data['entries'].length; i++) {
        var item = data['entries'][i];
        if(item['calculated'] != null && (item['timestamps'][0]['status'] !== 'vacation' && item['timestamps'][0]['status'] !== 'sick')) {
            for(let u = 0; u < item['calculated'].length; u++) {
                var calcNew = item['calculated'][u];
                if(!calcNew['manually_created']) {
                    //console.log(i);
                    calendar.addEvent(
                        {
                            title: calcNew['worked'] + " Gearbeitet",
                            start: calcNew['lu_date']+' 00:00',
                            allDay: false,
                            classNames: 'emt--work-entry emt--work-entry_calculated',
                            extendedProps: {
                                timestamps: item['timestamps'],
                            }
                        }
                    );
                    calendar.addEvent(
                        {
                            title: calcNew['break'] + " Pause",
                            start: calcNew['lu_date']+' 00:00',
                            allDay: false,
                            classNames: 'emt--work-entry emt--work-entry_calculated',
                            extendedProps: {
                                timestamps: item['timestamps'],
                            }
                        }
                    );
                }
                else {
                    if(calcNew['negative']) {
                        var negative = '-';
                    }
                    else {
                        var negative = '';
                    }
                    calendar.addEvent(
                        {
                            title: negative+calcNew['worked'] + " Interne Korrektur",
                            start: calcNew['lu_date']+' 00:00',
                            allDay: false,
                            classNames: 'emt--work-entry emt--work-entry_correction',
                            extendedProps: {
                                timestamps: item['timestamps'],
                            }
                        }
                    );
                }

            }

            for(var j = 0; j < item['timestamps'].length; j++) {
                var entry = item['timestamps'][j];
                if(entry['status'] !== 'break_start' && entry['status'] !== 'break_end') {
                    if(entry['status'] == 'coming') {
                        var toolateforcool = false;
                        for(var z = 0; z < punctuallity.length; z++) {
                            if(punctuallity[z]['timestamp'] === entry['timestamp']) {
                                //console.log('funzt ' + punctuallity[z]['timestamp']);
                                toolateforcool = true;
                            }
                        }
                        if(toolateforcool) {
                            calendar.addEvent(
                                {
                                    title: states[entry['status']],
                                    start:  entry['timestamp'],
                                    allDay: false,
                                    classNames: 'emt--work-entry emt--work-timestamp emt--work-timestamp_coming emt--work-timestamp_toolate',
                                    extendedProps: {
                                        timestamps: item['timestamps'],
                                    }
                                }
                            );
                        }
                        else {
                            calendar.addEvent(
                                {
                                    title: states[entry['status']],
                                    start:  entry['timestamp'],
                                    allDay: false,
                                    classNames: 'emt--work-entry emt--work-timestamp emt--work-timestamp_coming',
                                    extendedProps: {
                                        timestamps: item['timestamps'],
                                    }
                                }
                            );
                        }
                    }
                    else if(entry['status'] == 'leaving') {
                        calendar.addEvent(
                            {
                                title: states[entry['status']],
                                start:  entry['timestamp'],
                                allDay: false,
                                classNames: 'emt--work-entry emt--work-timestamp emt--work-timestamp_leaving',
                                extendedProps: {
                                    timestamps: item['timestamps'],
                                }
                            }
                        );
                    }
                }
            }
        }
        else if(item['timestamps'][0]['status'] === 'vacation') {
            calendar.addEvent(
                {
                    title: 'Urlaub',
                    start: item['timestamps'][0]['lookup']+' 00:00',
                    allDay: false,
                    classNames: 'emt--work-entry emt--work-entry_vacation',
                    extendedProps: {
                        timestamps: item['timestamps'],
                    }
                }
            );
        }
        else if(item['timestamps'][0]['status'] === 'sick') {
            calendar.addEvent(
                {
                    title: "Krank",
                    start: item['timestamps'][0]['lookup']+' 00:00',
                    allDay: false,
                    classNames: 'emt--work-entry emt--work-entry_sick',
                    extendedProps: {
                        timestamps: item['timestamps'],
                    }
                }
            );
        }
        else if(item['timestamps'][0]) {
            calendar.addEvent(
                {
                    title: "Noch nicht berechnet",
                    start: item['timestamps'][0]['lookup']+' 00:00',
                    allDay: false,
                    classNames: 'emt--work-entry emt--work-entry_calculated',
                    extendedProps: {
                        timestamps: item['timestamps'],
                    }
                }
            );
            for(var j = 0; j < item['timestamps'].length; j++) {
                var entry = item['timestamps'][j];
                if(entry['status'] !== 'break_start' && entry['status'] !== 'break_end') {
                    if(entry['status'] == 'coming') {
                        var toolateforcool = false;
                        for(var z = 0; z < punctuallity.length; z++) {
                            if(punctuallity[z]['timestamp'] === entry['timestamp']) {
                                //console.log('funzt ' + punctuallity[z]['timestamp']);
                                toolateforcool = true;
                            }
                        }
                        if(toolateforcool) {
                            calendar.addEvent(
                                {
                                    title: states[entry['status']],
                                    start:  entry['timestamp'],
                                    allDay: false,
                                    classNames: 'emt--work-entry emt--work-timestamp emt--work-timestamp_coming emt--work-timestamp_toolate',
                                    extendedProps: {
                                        timestamps: item['timestamps'],
                                    }
                                }
                            );
                        }
                        else {
                            calendar.addEvent(
                                {
                                    title: states[entry['status']],
                                    start:  entry['timestamp'],
                                    allDay: false,
                                    classNames: 'emt--work-entry emt--work-timestamp emt--work-timestamp_coming',
                                    extendedProps: {
                                        timestamps: item['timestamps'],
                                    }
                                }
                            );
                        }
                    }
                    else if(entry['status'] == 'leaving') {
                        calendar.addEvent(
                            {
                                title: states[entry['status']],
                                start:  entry['timestamp'],
                                allDay: false,
                                classNames: 'emt--work-entry emt--work-timestamp emt--work-timestamp_leaving',
                                extendedProps: {
                                    timestamps: item['timestamps'],
                                }
                            }
                        );
                    }
                }
            }
        }
    }
    for(let i = 0; i < data['sundays'].length; i++) {
        var item = data['sundays'][i];
        calendar.addEvent(
            {
                title: "Gesamt: " + item['workedVal'],
                start: item['date']+' 00:00',
                allDay: false,
                classNames: 'emt--work-entry emt--work-entry_calculated emt--work-entry_calculated_sunday',
                extendedProps: {
                    soll: item['soll'],
                }
            }
        );
        if(item['plusminus'][0]) {
            calendar.addEvent(
                {
                    title: item['plusminus'][1],
                    start: item['date']+' 00:00',
                    allDay: false,
                    color: '#CD5C5CFF',
                    classNames: 'emt--work-entry emt--work-entry_calculated emt--work-entry_calculated_sunday',
                    extendedProps: {
                        soll: item['soll'],
                    }
                }
            )
        }
        else if(item['plusminus'].length > 0){
            calendar.addEvent(
                {
                    title: '+'+item['plusminus'][1],
                    start: item['date']+' 00:00',
                    allDay: false,
                    color: '#98FB98FF',
                    classNames: 'emt--work-entry emt--work-entry_calculated emt--work-entry_calculated_sunday',
                    extendedProps: {
                        soll: item['soll'],
                    }
                }
            )
        }

    }

    for(let i = 0; i < holidays.length; i++) {
        var holidayYear = holidays[i];
        for(let x = 0; x < holidayYear.length; x++) {
            var item = holidayYear[x];
            if(item['global']) {
                calendar.addEvent(
                    {
                        title: item['localName'],
                        start: item['date'],
                        allDay: true,
                        classNames: 'emt--work-entry '
                    }
                );
            }
            else if(item['counties'].includes('DE-SH')) {
                calendar.addEvent(
                    {
                        title: item['localName'],
                        start: item['date'],
                        allDay: true,
                        classNames: 'emt--work-entry '
                    }
                );
            }
        }
    }

}


function ajaxCall2() {
    let id = $('#employ_id').val();
    let start = $('#dateFrom').val();
    let end = $('#dateTo').val();
    if(id.length && start.length && end.length) {
        let bigShit;
        return $.request('onCalcTime', {
            data:{user_id: id, start: start, end: end},
            success: function(data) {
                bigShit = data;
            }
        });
    }
    else {
        return false;
    }
}