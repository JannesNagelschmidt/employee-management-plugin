var first_name, last_name, user_id, date, status, isAuthorized = false;
$(window).on("load", function () {
    checkIfUserLoggedIn();

    $('.search--result-output').on('click', '.time--entry-icon', function (e) {
        var timeId = $(this).parent().data('timeid');
        if(confirm("Zeiteintrag wirklich löschen?")) {
            $.ajax({
                type: 'POST',
                url: '/deleteTimeForUser',
                data: {timeId: timeId},
                success:function(data) {
                    if(data == "NEIN") {

                    }
                    else {
                        alert("Eintrag gelöscht");
                    }
                }
            });
        }
    });

    $('.corrections--output').on('click', '#getDataOfDay', function (e) {
        var searchName = $('#corrLastName').val();
        var firstName = $('#corrFirstName').val();
        date = $('#corrDate').val();
        var output = '';
        if(searchName.length && date.length) {
            $.ajax({
                type: 'GET',
                url: '/getUserByDateAndName',
                data: {date: date, searchName: searchName, firstName: firstName},
                success:function(data) {
                    if(data == "NEIN") {
                        alert("Niemanden gefunden!");
                    }
                    else if(data == "ZEIT NÖ") {
                        alert("Keine Zeit Einträge gefunden!");
                    }
                    else {
                        user_id = data[1].id;
                        $('.search--result-output').empty();
                        for(var i = data[0].length-1; i >= 0; i--) {
                            console.log(data[i]);
                            output += '<div class="time--entry-row" data-timeId="'+data[0][i].id+'">';
                            output += '<p class="time--entry">'+data[0][i].status+'</p>';
                            output += '<p class="time--entry">'+data[0][i].timestamp+'</p>';
                            output += '<span class="time--entry-icon">';
                            output += '<i class="fas fa-trash-alt"></i>';
                            output += '</span>';
                            output += '</div>';
                        }
                        $('.search--result-output').append(output);
                        $('.search--result-output').addClass("bordered");
                    }
                }
            });
        }
        else {
            console.log("nich ausgefüllt")
        }
    });

    $('#statusSelect').on('change', function() {
        status = $(this).val();
    });

    $('#correction').on('click', function() {
        var hour = $('#correctionHour').val();
        var minutes = $('#correctionMinute').val();
        console.log(hour.length);
        console.log(minutes.length);
        console.log(status.length);
        console.log(user_id);
        console.log(date);
        if(hour.length && minutes.length && date !== undefined && user_id !== undefined && status.length) {
            if(hour > 23 || minutes > 59) {
                alert("Bitte gültige Zeit eintragen!");
            }
            else {
                var time = date + ' ' + hour+':'+minutes+':00';
                if(confirm("Es wird folgender Zeiteintrag erstellt " + time +" Status: " + status)) {
                    $.ajax({
                        type:'POST',
                        url:'/updateTimeForUser',
                        data:{time:time, status: status, id: user_id, date: date},
                        success:function(data){
                            if(data === 'NEIN') {
                                console.log(data);
                            }
                            else {

                            }
                        }
                    });
                }
                else {
                    location.reload();
                }
            }
        }
        else {
            if(user_id == undefined) {
                alert("Bitte gültigen Nutzer wählen!")
            }
            if(date == undefined) {
                alert("Bitte gültiges Datum wählen!")
            }
            if(status.length == 0) {
                alert("Bitte gültigen Status wählen!")
            }
            if(hour.length == 0 || minutes.length == 0) {
                alert("Bitte gültige Zeit eintragen!");
            }
        }
    });

    $('.singleemp--textinput').keypress(function(e) {
        if(e.which == 13){//Enter key pressed
            $('#correctionLoginButton').click();
        }
    });

    $('#correctionLoginButton').on('click', function(e) {
        var name, password;
        if($('#keyNameInput').val() && $('#keyPasswordInput').val()) {
            name = $('#keyNameInput').val();
            password = $('#keyPasswordInput').val();
        }
        else {
            alert("Bitte beide Felder ausfüllen!");
        }
        e.preventDefault();
        $.ajax({
            type:'GET',
            url:'/checkUserForCorrection',
            data:{name:name, password: password},
            success:function(data){
                if(data === 'NEIN') {
                    console.log(data);
                    alert("Login verkehrt!");
                }
                else if(data === 'UNAUTHORISIERT') {
                    alert('Nutzer nicht authorisiert!');
                }
                else {
                    isAuthorized = true;
                    location.reload();
                }
            }
        });
    });

    $('#logoutButton').on('click', function() {
        $.ajax({
            type:'GET',
            url:'/endSessionEmployee',
            success:function(data) {
                console.log(data);
                isAuthorized = false;
                location.reload();
            }
        });
    });

});

function checkIfUserLoggedIn() {
    $.ajax({
        type:'GET',
        url:'/checkUserLoggedInCorrection',
        success:function(data){
            if(data === 'NEIN') {
                isAuthorized = false;
                console.log(data);
            }
            else if(data === 'UNAUTHORISIERT') {
                alert('Nutzer nicht authorisiert!');
            }
            else {
                $('.hidden--stuff').show();
                $('.singleemp--wrapper').hide();
                $('.singleemp--headline-1').text('Hallo ' + data[0].first_name + ' ' + data[0].last_name)
                $('.corrections--output').append(data[2]);
                isAuthorized = true;
            }
        }
    });
}

