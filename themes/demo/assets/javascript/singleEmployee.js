var monate = ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'];
var wochentag = ["Sonntag","Montag","Dienstag","Mittwoch","Donnerstag","Freitag","Samstag"];
$(window).load(function() {
    var time = 500;
    checkIfUserLoggedIn();


    $('.singleemp--content').on('click', '.singleemp--item-wrapper', function() {
        $('.singleemp--item-innerWrapper').hide();
        $(this).find('.singleemp--item-innerWrapper').show();
    });

    $('#logoutButton').on('click', function() {
        $.ajax({
            type:'GET',
            url:'/endSessionEmployee',
            success:function(data) {
                console.log(data);
                location.reload();
            }
        });
    });


    $('.singleemp--content').on('click', '.action--btn', function(e) {
        var token = $('#timetrackingOutput').attr('data-token');
        var state = $(this).data('timetrack');
        $('#'+state).attr('disabled', 'disabled');
        $('.singleemp--content').attr('disabled', true);
        var text = "AKTION ERFOLGREICH";
        console.log(token);
        console.log(state);
        $.ajax({
            type:'POST',
            url:'/trackTime',
            data:{token:token, state:state},
            success:function(data){
                switch (state) {
                    case "coming":
                        text = "Willkommen"
                        break;
                    case "leaving":
                        text = "Tschüss: "+data;
                        break;
                    case "break_end":
                        text = "Pause: "+data;
                        break;
                    default:
                        break;
                }
                $('body').append('<div class="action--popup"><h2>'+text+'</<h2></div>');
                setTimeout(function() {
                    $('.action--popup').remove();
                    $('#'+state).hide();
                    if(state == 'leaving') {
                        $('#coming').show();
                        $('.break--btn').hide();
                    }
                    if(state == 'coming') {
                        $('#break_end').hide();
                        $('#break_start').show();
                        $('#leaving').show();
                    }
                    if(state == 'break_start') {
                        $('#coming').hide();
                        $('#leaving').hide();
                        $('#break_end').show();
                    }
                    if(state == 'break_end') {
                        $('#coming').hide();
                        $('#break_end').hide();
                        $('#leaving').show();
                        $('#break_start').show();
                    }
                    $('.singleemp--content').attr("disabled", false);
                    $('#'+state).removeAttr('disabled');
                }, time)
            },
            complete: function() {
                console.log('in complete')
            }
        });
    });




    $('.singleemp--textinput').keypress(function(e) {
        if(e.which == 13){//Enter key pressed
            $('#getSingleMitarbeiter').click();//Trigger search button click event
        }
    });
    $('#getSingleMitarbeiter').on('click', function(e) {
        var name, password;
        if($('#keyNameInput').val() && $('#keyPasswordInput').val()) {
            name = $('#keyNameInput').val();
            password = $('#keyPasswordInput').val();
        }
        else {
            alert("Bitte beide Felder ausfüllen!");
        }
        e.preventDefault();
        $.ajax({
            type:'GET',
            url:'/getSingleEmployeeByWindows',
            data:{name:name, password: password},
            success:function(data){
                ajaxRenderFunc(data);
            }
        });
    });
});

function checkIfUserLoggedIn() {
    $.ajax({
        type:'GET',
        url:'/checkUser',
        success:function(data){
           // console.log("kommt aber von hier... " + data);
            if(data === 'NEIN') {
                console.log(data);
            }
            else {
                ajaxRenderFunc(data);
            }
        }
    });
}

function ajaxRenderFunc(data) {
    if(data !== 'NEIN') {
        $('.singleemp--searchwrapper').show();
        $('.singleemp--headline-1').text('Hallo ' + data[0].first_name + ' ' + data[0].last_name);
        $('#singleempOutput').empty();
        $('#timetrackingOutput').attr('data-token', data[0].token);
        var out = '';
        var currentDate = 0;
        const options = {day: '2-digit', month: '2-digit', year: 'numeric'};
        var currentMonth = 0;
        var lastMonth = 0;
        var lastDate = 0;

        for(var i = data[1].length-1; i >= 0; i--) {
            currentDate = new Date(data[1][i]['timestamp']);
            currentDate.setHours(0,0,0,0,);
            currentMonth = currentDate.getMonth()+1;
            var time = new Date (data[1][i]['timestamp']);
            var hours = time.getHours();
            var minutes = time.getMinutes();
            var seconds = time.getSeconds();
            if(hours < 10) {
                hours = "0"+hours;
            }
            if(minutes < 10) {
                minutes = "0"+minutes;
            }
            if(seconds < 10) {
                seconds = "0"+seconds;
            }
            if(i == data[1].length-1) {
                out += '<span class="singleemp--monthName">'+monate[currentDate.getMonth()]+'</span>';
                out += '<div class="month--seperator">';
                out += '<div data-date="'+currentDate.toLocaleDateString('de-DE')+'" class="singleemp--item-wrapper">';
                out += '<span class="singleemp--dayName">'+wochentag[currentDate.getDay()]+'</span>';
                out += '<span class="singleemp-toggler">'+currentDate.toLocaleDateString('de-DE', options)+'</span>';
                out += '<div class="singleemp--item-innerWrapper">';
                out += '<i class="fas fa-window-close singleemp--close"></i>';
                out += "<p class='singleemp--item'>"+hours+":"+minutes+":"+seconds+" - "+data[1][i]['status']+"</p>";
            }
            else if(lastDate.toLocaleDateString() === currentDate.toLocaleDateString()) {
                out += "<p class='singleemp--item'>"+hours+":"+minutes+":"+seconds+" - "+data[1][i]['status']+"</p>";
            }
            else {
                out += '</div>';
                out += '</div>';
                if(currentMonth !== lastMonth) {
                    out += '</div>';
                    out += '<span class="singleemp--monthName">'+monate[currentDate.getMonth()]+'</span>';
                    out += '<div class="month--seperator">';
                }
                out += '<div data-date="'+currentDate.toLocaleDateString('de-DE')+'" class="singleemp--item-wrapper">';
                out += '<span class="singleemp--dayName">'+wochentag[currentDate.getDay()]+'</span>';
                out += '<span class="singleemp-toggler">'+currentDate.toLocaleDateString('de-DE',options)+'</span>';
                out += '<div class="singleemp--item-innerWrapper">';
                out += '<i class="fas fa-window-close singleemp--close"></i>';
                out += "<p class='singleemp--item'>"+hours+":"+minutes+":"+seconds+" - "+data[1][i]['status']+"</p>";
            }
            lastDate = new Date(data[1][i]['timestamp']);
            lastDate.setHours(0,0,0,0);
            lastMonth = currentDate.getMonth()+1;
        }
        $('#'+data[1][[data[1].length-1]].status).hide();
        $('.action--wrapper').removeClass("action--wrapper-hidden");

        if(data[1][data[1].length-1].status == 'leaving') {
            $('.break--btn').hide();
        }
        if(data[1][data[1].length-1].status == 'coming') {
            $('#break_end').hide();
        }
        if(data[1][data[1].length-1].status == 'break_start') {
            $('#coming').hide();
            $('#leaving').hide();
        }
        if(data[1][data[1].length-1].status == 'break_end') {
            $('#coming').hide();
        }
        if(data[1][data[1].length-1].status == 'forgot') {
            $('#leaving').hide();
            $('#break_end').hide();
            $('#break_start').hide();
        }
    }
    else {
        alert("keinen Nutzer gefunden");
    }
    $('#singleempOutput').append(out);
}

function searchFunc() {
    // Declare variables
    var input, filter, ul, li, a, i, txtValue;
    input = $('#searchInput');
    filter = input.val();
    li = $('.singleemp--item-wrapper');

    // Loop through all list items, and hide those who don't match the search query
     li.each(function(index) {
         a = $(this);
         txtValue = a.data('date');
         if (txtValue.indexOf(filter) > -1) {
             $(this)[0].style.display = "flex";
         } else {
             $(this)[0].style.display = "none";
         }
     });
}
