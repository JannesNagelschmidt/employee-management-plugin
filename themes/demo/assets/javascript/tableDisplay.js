import {MDCSelect} from '@material/select';
import {MDCSnackbar} from "@material/snackbar";
$(function() {
    if($('#outputDiv').length) {
        var targetButton = false;
        if(document.querySelector('.mdc-snackbar') != null) {
            var snackbar = new MDCSnackbar(document.querySelector('.mdc-snackbar'));
        }
        $(window).load(function() {
            if($('#idForDepartment').length) {
                var time = 500;
                var department = $('#idForDepartment').val();
                console.log(department);

                //getSingleDep(department);
                $('.emt--body-wrapper').on('click', '#next', function() {
                    var department = $(this).data('department');
                    var pages = $(this).data('pages');
                    nextMonth(department, pages);
                });
                $('.emt--body-wrapper').on('click', '#prev', function() {
                    var department = $(this).data('department');
                    var pages = $(this).data('pages');
                    prevMonth(department, pages);
                });
                $('.emt--body-wrapper').on('click', '#loadPrev', function() {
                    var page = $(this).data('page');
                    var pages = $(this).data('pages');
                    loadPrev(page, pages);
                });
                $('.emt--body-wrapper').on('click', '#loadNext', function() {
                    var page = $(this).data('page');
                    var pages = $(this).data('pages');
                    loadNext(page, pages);
                });
                $('#lastNameFirstLetter').on('change', function() {
                    var letter = this.value;
                    console.log(letter);
                    getSingleDep(department);
                })
                var select = new MDCSelect(document.querySelector('.mdc-select3'));

                select.listen('MDCSelect:change', () => {
                    var letter = select.value;
                    console.log(letter);
                    $('#lastNameFirstLetter').val(letter);
                    //getSingleDep(department);
                });
            }


        });

        $('.singleemp--textinput').keypress(function(e) {
            if(e.which == 13){//Enter key pressed
                $('#getSingleMitarbeiter_timedisplay').click();//Trigger search button click event
            }
        });

        $('#getSingleMitarbeiter_timedisplay').on('click', function(e) {
            var name, password;
            if($('#keyNameInput').val() && $('#keyPasswordInput').val()) {
                name = $('#keyNameInput').val();
                password = $('#keyPasswordInput').val();
            }
            else {
                alert("Bitte beide Felder ausfüllen!");
            }
            e.preventDefault();
            $.ajax({
                type:'GET',
                url:'/getSingleEmployeeByWindows',
                data:{name:name, password: password},
                success:function(data){
                    location.reload();
                }
            });
        });

        $(".dropdown--headline").on('click', function() {
            $('.dropdown--wrapper').toggle();
            $('.department--wrapper').hide();
        });

        $('#resetStuff').on('click', function() {
            $('#lastNameFirstLetter').val('0').change();
        });

        $(".user--item").on('click', function() {
            $('.dropdown--wrapper').toggle();
        });

        $('.user--item').on("click" , function () {
            $('.loading--background').show();
            var userID = $(this).data('userid');
            $('#outputDiv').removeClass("departmentView");
            $.ajax({
                type:'POST',
                url:'/getEmployById',
                data:{id:userID},
                success:function(data){
                    $('#outputDiv').empty().append(data);
                    $('.loading--background').hide();
                }
            });
        });

        $('.timetracker--body').on('click', '.text-break--hasBreak', function() {
            //console.log("hasBreak clicked");
            var window = $(this).next();
            $('.break--wrapper').hide();
            $('.break--wrapper').removeClass('is--visible');
            if(window.hasClass('is--visible')) {
                //console.log("in has class is visible");
                window.removeClass('is--visible');
                window.hide();
            }
            else {
                //console.log("in else is visible");
                window.show();
                window.addClass("is--visible");
            }
        });

        $('.timetracker--body').on('click', '.break--button-icon', function() {
            //console.log("hasBreak clicked");
            var window = $(this).closest().find('.break--wrapper');
            //console.log(window);
            $('.break--wrapper').hide();
            $('.break--wrapper').removeClass('is--visible');
            if(window.hasClass('is--visible')) {
                //console.log("in has class is visible");
                window.removeClass('is--visible');
                window.hide();
            }
            else {
                //console.log("in else is visible");
                targetButton = true;
                window.show();
                window.addClass("is--visible");
            }
        });

        $(document).on('click', function(e) {
            var item = $(e.target);
            if(!item.is($('.text-break--hasBreak')) && (!item.parent().hasClass('fa-coffee') && !item.hasClass('fa-coffee')) && !targetButton) {
                //console.log("document clicked");
                //console.log(item.hasClass('fa-coffee'));
                //console.log("in document in if");
                $('.break--wrapper').hide();
                $('.break--wrapper').removeClass('is--visible');
            }
            else {
                targetButton = false;
            }
        })

        $('#outputDiv').on('click', '.break--close', function() {
            console.log("break close clicked");
            var window = $(this).parent();
            window.hide();
            window.removeClass('is--visible');
        });

        function getSingleDep(department) {
            const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
            var page = 1;
            $('.loading--background').show();
            var year = $('.currentMonthtoShow').data('currentyear');
            $('body').css('overflow', 'hidden');
            var letter = $('#lastNameFirstLetter').val();
            $('.department--wrapper').toggle();
            $('.dropdown--wrapper').hide();
            var month = $('.currentMonthtoShow').data('current');
            if(month) {
                console.log(month);
            }
            else {
                var d = new Date();
                var n = d.getMonth();
                month = n+1;
                console.log("eyooo month is " + month );
            }

            $.ajax({
                type: 'POST',
                url: '/getSingleDepartment',
                data: {fromNew: 0, department: department, month: month, letter: letter, year: year, prev: 0, next: 0},
                success: function (data) {
                    var html = '';
                    console.log(data);
                    $('.loading--background').hide();
                    $('body').css('overflow', 'auto');
                    var department = data.department;
                    var month = data.month;
                    var letter = data.letter;
                    var employees = data.employees;
                    var page = data.page;
                    var year = data.year;
                    var prev = data.prev;
                    var next = data.next;
                    var pages = data.pages;
                    if(data !== 'FALSE') {
                        renderFrontend(department, employees, month, letter, page, year, prev, next, pages);
                    }
                    else {
                        snackbar.labelText = 'Niemanden gefunden';
                        snackbar.open();
                    }


                }
            });
        }


        function renderMonthHeadline(month, year) {
            var out = "";
            var week = ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'];
            var daysOfMonth = function(month,year) {
                return new Date(year, month, 0).getDate();
            }

            //$ddate = date($year."-".$month."-01");
            var ddate = new Date(year, month-1, '01');
            var today = new Date();

            var iterations = ddate.getDay() - 1;
            if(iterations < 0) {
                iterations = 6;
            }
            var endDay = daysOfMonth(month, year);

            out += '<div class="month--headline-wrapper" data-year="0">';

            if(iterations > 1) {
                var blup = ddate;
                for (var i = iterations; i >= 1; i--) {
                    if(i < iterations) {
                        blup.setDate(blup.getDate() + 1);
                    }
                    else {
                        blup.setDate(blup.getDate() - i);
                    }
                    var dateYo = ('0' + blup.getDate()).slice(-2);
                    var monthYo = ('0' + (1 + blup.getMonth())).slice(-2);
                    var dateName = dateYo + '.' + monthYo;
                    var weekDayName = week[blup.getDay()];
                    out += '<p class="currentMonth--text currentMonth--text-date">'+weekDayName+'</br>'+dateName+'</p>';
                }
            }
            else if(iterations == 1){
                var blup = ddate;
                blup.setDate(blup.getDate() - 1);
                var dateYo = ('0' + blup.getDate()).slice(-2);
                var monthYo = ('0' + (1 + blup.getMonth())).slice(-2);
                var dateName = dateYo + '.' + monthYo;
                var weekDayName = week[blup.getDay()];
                out += '<p class="currentMonth--text currentMonth--text-date">'+weekDayName+'</br>'+dateName+'</p>';
            }
            if(ddate.getDay() > 1) {
                ddate.setDate(ddate.getDate()+1);
            }
            for(var p = 1; p <= endDay; p++) {
                var blup = ddate;
                var dateYo = ('0' + p).slice(-2);
                var monthYo = ('0' + (1 + ddate.getMonth())).slice(-2);
                var dateName = dateYo + '.' + monthYo;
                var weekDayName = week[blup.getDay()];
                out += '<p class="currentMonth--text currentMonth--text-date">'+weekDayName+'</br>'+dateName+'</p>';
                ddate.setDate(ddate.getDate() + 1);
            }

            out += '</div>';


            return out;
        }

        function renderFrontend(department = null, employs = null, month = null, letter = null, page = null, year = null, prev = null, next = null, pages = null) {
            const monthNames = ["Januar", "Februar", "März", "April", "Mai", "Juni",
                "Juli", "August", "September", "Oktober", "November", "Dezember"
            ];
            //console.log("pages: " + pages);
            var outputEl = $('#outputDiv');

            var header = '<div class="currentMonth--wrapper-top">';
            header += '<p data-current="'+month+'" data-currentyear="'+year+'"  class="currentMonthtoShow">'+monthNames[(month-1)]+' - '+year+'</p>';
            header += '<p class="current--department" data-department="'+department['id']+'">Abteilung: '+department['name']+'</p>';
            header += '</div>';
            var buttons = '<div class="timedisplay--btn-wrapper">';
            if(typeof pages != 'undefined') {
                if(page <= pages && page > 1) {
                    buttons += '<button class="timedisplay--btn btn-page--minus" id="loadPrev" data-page="'+page+'" data-pages="'+pages+'" data-week="next" data-week_value="'+(month)+'"><i class="fas fa-arrow-up"></i></button>';
                }
            }
            buttons += '<input id="currentPage" type="hidden" data-currentpage="'+page+'">';
            buttons += '<button class="timedisplay--btn btn-prev" id="prev" data-week="prev"  data-department="'+department['id']+'" data-pages="'+pages+'" data-week_value="'+(parseInt(month) - 1)+'"><i class="fas fa-arrow-left"></i></button>';
            buttons += '<button class="timedisplay--btn btn-next" id="next" data-week="next"  data-department="'+department['id']+'" data-pages="'+pages+'" data-week_value="'+(parseInt(month) + 1)+'"><i class="fas fa-arrow-right"></i></button>';
            buttons += '</div>';
            var monthHeadline = renderMonthHeadline(month, year);
            //monthHeadline += '</div>';

            outputEl.addClass("departmentView");
            outputEl.empty();
            outputEl.append(header);
            outputEl.append(buttons);
            //outputEl.append(monthHeadline);
            var user;
            if(employs) {
                for(var i = 0; i < employs.length; i++) {
                    var employee = employs[i]['employee'];
                    if(employee) {


                        user = '<div class="currentMonth--wrapper">';
                        user += monthHeadline;
                        user += '<div class="currentMonth--col-wrapper headcol--wrapper">';
                        user += '<span class="headcol--text headcol--text-name">'+employee['first_name']+'</span>';
                        user += '<span class="headcol--text headcol--text-name">'+employee['last_name']+'</span>';
                        user += '<span class="headcol--text headcol--text-ist">IST</span>';
                        user += '<span class="headcol--text headcol--text-break">PAUSE</span><span class="headcol--text headcol--text-soll">SOLL</span><span class="headcol--text">ÜBER</span><span class="headcol--text">Ges. Woche</span>';
                        user += '</div>';

                        var work = employs[i]['work'];
                        //console.log(work.length);
                        var workString = '';
                        for(var x = 0; x < work.length; x++) {
                            //console.log(work[x]);
                            if('soll' in work[x]) {
                                user += '<div class="currentMonth--col-wrapper currentMonth--col-sunday">';
                            }
                            else {
                                user += '<div class="currentMonth--col-wrapper">';
                            }
                            if('holiday' in work[x]) {
                                var holiday = true;
                                user += '<p class="currentMonth--text currentMonth--text-ist">00:00</p>';
                                user += '<div class="currentMonth--text-break currentMonth--text text-break--hasBreak text--holiday"><i class="break--button-icon fas fa-calendar-day"></i></div>';
                                user += '<div class="break--wrapper">';
                                user += '<i class="fas fa-window-close break--close"></i>';
                                user += '<p class="break--entry">'+work[x]['work']+'</p>';
                                user += '</div>';
                            }
                            else {
                                var holiday = false;
                                user += '<p class="currentMonth--text currentMonth--text-ist">';
                                user += work[x]['work'];
                                user += '</p>';
                            }
                            if(work[x]['break']) {
                                if(work[x]['work'] == 'URL.') {
                                    user += '<div class="currentMonth--text-break currentMonth--text text-break--hasVacation">';
                                    user += '<i class="fas fa-umbrella-beach vacation--buton-icon"></i>';
                                    user += '</div>';
                                }
                                else if(work[x]['work'] == 'KRNK') {
                                    user += '<div class="currentMonth--text-break currentMonth--text text-break--hasSick">';
                                    user += '<i class="fas fa-medkit vacation--buton-icon"></i>';
                                    user += '</div>';
                                }
                                else {
                                    user += '<div class="currentMonth--text-break currentMonth--text text-break--hasBreak">';
                                    user += '<i class="break--button-icon fas fa-coffee"></i>';
                                    user += '</div>';
                                    user += '<div class="break--wrapper">';
                                    user += '<i class="fas fa-window-close break--close"></i>';
                                    for(var y = 0; y < work[x]['break'].length; y++) {
                                        user += '<p class="break--entry ">';
                                        var datetime = work[x]['break'][y]['time'];
                                        if(typeof datetime === 'object' && typeof work[x]['break'][y]['time'].date !== null) {
                                            var d = new Date(Date.parse(datetime.date));
                                            var newDate = pad(d.getHours(),2) + ':'+pad(d.getMinutes(),2);
                                            user += newDate+ ' - ';
                                        }
                                        else {
                                            user += datetime + ' - ';
                                        }
                                        user += work[x]['break'][y]['status'];
                                        user += '</p>';
                                    }
                                    user += '</div>';
                                }
                            }
                            else {
                                if(work[x]['work'] == 'URL.') {
                                    user += '<div class="currentMonth--text-break currentMonth--text text-break--hasVacation">';
                                    user += '<i class="fas fa-umbrella-beach vacation--buton-icon"></i>';
                                    user += '</div>';
                                }
                                else if(work[x]['work'] == 'KRNK') {
                                    user += '<div class="currentMonth--text-break currentMonth--text text-break--hasSick">';
                                    user += '<i class="fas fa-medkit vacation--buton-icon"></i>';
                                    user += '</div>';
                                }
                                else {
                                    user += '<div class="currentMonth--text-break currentMonth--text">';
                                    user += '</div>';
                                }
                            }
                            if('soll' in work[x]) {
                                user += '<p class="currentMonth--text-soll currentMonth--text">'+work[x]['soll']+'</p>';
                                user += '<p class="currentMonth--text currentMonth--text-'+work[x]['redgreen']+' currentMonth--text-über aha">'+work[x]['plusminus']+'</p>'
                                user += '<p class="currentMonth--text currentMonth--text-week  WEEKRESULT currentMonth--text-IST">'+work[x]['weekresult']+'</p>';
                            }
                            else {
                                user += '<p class="currentMonth--text-soll currentMonth--text"></p>';
                                if(!holiday) {
                                    user += '<p class="currentMonth--text currentMonth--text--filler"></p>';
                                }
                            }


                            user += '</div>';
                        }
                        user += '</div>';
                        outputEl.append(user);
                    }
                }
            }
            var pageString = '';
            if(typeof pages != 'undefined') {
                if(page < pages && page != 0) {
                    pageString = '<button class="timedisplay--btn btn-page--plus" id="loadNext" data-page="'+page+'" data-pages="'+pages+'" data-week="next" data-week_value="'+(month)+'"><i class="fas fa-arrow-down"></i></button>';
                }
            }
            outputEl.append(pageString);
        }

        function pad(num, size) {
            num = num.toString();
            while (num.length < size) num = "0" + num;
            return num;
        }
        function getDepartments() {
            console.log("get departments called in js");
            $('.department--wrapper').toggle();
            $('.dropdown--wrapper').hide();
            $.ajax({
                type: 'GET',
                url: '/getDepartments',
                success:function (data) {
                    $('#departmentWrapper').empty();
                    for(var i = 0; i < data.length; i++) {
                        var appender = '<div class="user--item-wrapper"><p style="cursor: pointer" class="user--item department--item" onclick="getSingleDep(&#39;'+data[i]+'&#39;)" data-department="'+data[i]+'" >'+data[i]+'</p></div>';
                        $('#departmentWrapper').append(appender);
                    }
                    //$('.fullOverTime--output').text(data);
                }
            });
        }

        function handleFullOvertime() {
            var userID = $('#user_id_selected').data('user_id');
            console.log(userID);

            $.ajax({
                type: 'POST',
                url: '/getFullOvertime',
                data: {id:userID},
                success:function (data) {
                    console.log(data);
                    $('.fullOverTime--output').text(data);
                }
            });
        }

        function jumpToCurrent() {
            var userID = $('#theUserID').data('userid');
            $.ajax({
                type:'POST',
                url:'/getEmployById',
                data:{id:userID},
                success:function(data){
                    $('#outputDiv').empty().append(data);
                }
            });
        }

        function nextMonth(department, pages) {
            var month = $('.btn-next').data('week_value');
            var page = $('#currentPage').data('currentpage');
            var year = $('.currentMonthtoShow').data('currentyear');
            $('body').css('overflow', 'hidden');
            $('.loading--background').show();
            var letter = $('#lastNameFirstLetter').val();
            $.ajax({
                type: 'POST',
                url: '/getSingleDepartment',
                data: {fromNew: 0, department: department, month: month, letter: letter, page: page, year: year, prev: false, next: true, pages: pages},
                success: function (data) {
                    console.log(data);
                    $('.loading--background').hide();
                    $('body').css('overflow', 'auto');
                    var department = data.department;
                    var month = data.month;
                    var letter = data.letter;
                    var employees = data.employees;
                    var page = data.page;
                    var year = data.year;
                    var prev = data.prev;
                    var next = data.next;
                    var pages = data.pages;
                    renderFrontend(department, employees, month, letter, page, year, prev, next, pages);
                    /*if(data !== 'FALSE') {
                        $('#outputDiv').addClass("departmentView");
                        $('#outputDiv').empty();
                        for(var i = 0; i < data.length; i++) {
                            $('#outputDiv').append(data[i]);
                        }
                        $('body').css('overflow', 'auto');
                        $('.loading--background').hide();
                    }
                    else {
                        alert("niemanden gefunden");
                        $('.loading--background').hide();
                        $('body').css('overflow', 'auto');
                    }*/
                }
            });
        }

        function loadPrev(page, pages) {
            var month = $('.currentMonthtoShow').data('current');
            $('body').css('overflow', 'hidden');
            var department = $('.current--department').data('department');
            //console.log(department);
            if(page <= 1) {
                page = 1;
            }
            else {
                page--;
            }
            $('.loading--background').show();
            var letter = $('#lastNameFirstLetter').val();
            $.ajax({
                type: 'POST',
                url: '/getSingleDepartment',
                data: {fromNew: 0, department: department, month: month, letter: letter, page: page, prev: false, next: false, pages: pages},
                success: function (data) {
                    console.log(data);
                    $('.loading--background').hide();
                    $('body').css('overflow', 'auto');
                    var department = data.department;
                    var month = data.month;
                    var letter = data.letter;
                    var employees = data.employees;
                    var page = data.page;
                    var year = data.year;
                    var prev = data.prev;
                    var next = data.next;
                    var pages = data.pages;
                    renderFrontend(department, employees, month, letter, page, year, prev, next, pages);

                }
            });
        }

        function loadNext(page, pages) {
            var month = $('.currentMonthtoShow').data('current');
            $('body').css('overflow', 'hidden');
            var department = $('.current--department').data('department');
            //console.log(department);
            page++;
            $('.loading--background').show();
            var letter = $('#lastNameFirstLetter').val();
            $.ajax({
                type: 'POST',
                url: '/getSingleDepartment',
                data: {fromNew: 0, department: department, month: month, letter: letter, page: page, prev: false, next: false, pages: pages},
                success: function (data) {
                    console.log(data);
                    $('.loading--background').hide();
                    $('body').css('overflow', 'auto');
                    var department = data.department;
                    var month = data.month;
                    var letter = data.letter;
                    var employees = data.employees;
                    var page = data.page;
                    var year = data.year;
                    var prev = data.prev;
                    var next = data.next;
                    var pages = data.pages;
                    renderFrontend(department, employees, month, letter, page, year, prev, next, pages);

                }
            });
        }

        function prevMonth(department, pages) {
            $('.loading--background').show();
            var page = $('#currentPage').data('currentpage');
            var year = $('.currentMonthtoShow').data('currentyear')
            $('body').css('overflow', 'hidden');
            var month = $('.btn-prev').data('week_value');
            var letter = $('#lastNameFirstLetter').val();
            $.ajax({
                type: 'POST',
                url: '/getSingleDepartment',
                data: {fromNew: 0, department: department, month: month, letter: letter, page: page, year: year, prev: true, next: false, pages: pages},
                success: function (data) {
                    console.log(data);
                    $('.loading--background').hide();
                    $('body').css('overflow', 'auto');
                    var department = data.department;
                    var month = data.month;
                    var letter = data.letter;
                    var employees = data.employees;
                    var page = data.page;
                    var year = data.year;
                    var prev = data.prev;
                    var next = data.next;
                    var pages = data.pages;
                    renderFrontend(department, employees, month, letter, page, year, prev, next, pages);
                    /*if(data !== 'FALSE') {
                        $('#outputDiv').addClass("departmentView");
                        $('#outputDiv').empty();
                        for(var i = 0; i < data.length; i++) {
                            $('#outputDiv').append(data[i]);
                        }
                        $('.loading--background').hide();
                        $('body').css('overflow', 'auto');
                    }
                    else {
                        alert("niemanden gefunden");
                        $('.loading--background').hide();
                        $('body').css('overflow', 'auto');
                    }*/
                }
            });
        }

        function specificWeek() {
            var userID = $('#theUserID').data('userid');
            var month = $('#specificweek').val();
            console.log(month);
            $.ajax({
                type:'POST',
                url:'/getEmployById',
                data:{id:userID, month: month},
                success:function(data){
                    $('#outputDiv').empty().append(data);
                }
            });
        }
    }

});