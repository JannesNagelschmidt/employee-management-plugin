$(function() {
    $('#tokenInput').focus();
    var secondStep = false;
    var socket = io("ws://localhost:3000",{reconnectionDelay: 10000, secure: true});
    var tokenSend = false;
    var globalToken;
    var witnessSend = false;
    var coronabla = false;
    var time = 500;
    startTime();

    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
        return false;
    };



    $('#tokenInput').on('change', function() {
        if($('#tokenInput').val()) {
            $('#getMitarbeiter').trigger('click');
        }
    });

    $('#tokenInput').on('focusout', function () {
        //console.log("should keep focus");
        if(!secondStep && !coronabla) {
            $('#tokenInput').focus();
        }
        else if(coronabla) {
            $('#witnessInput').focus();
        }
    });


    $('#witnessInput').on('change', function() {
        if($('#witnessInput').val()) {
            $('#setWitness').trigger('click');
        }
    });

    $('#witnessInput').on('focusout', function () {
        console.log("should keep focus");
        if(coronabla) {
            $('#witnessInput').focus();
        }
    });

    //witnessInput

    $('#addTokenInput').on('focusout', function () {
        //console.log("should keep focus");
        if(secondStep) {
            $('#addTokenInput').focus();
        }
    });


    $('#addTokenInput').on('change', function() {
        if($('#addTokenInput').val()) {
            var firstname = $('#firstnameLdap').val();
            var lastname = $('#lastnameLdap').val();
            var token = $('#addTokenInput').val();
            console.log(token);
            console.log(firstname);
            console.log(lastname);
            $.ajax({
                type:'POST',
                url:'/addTokenToUser',
                data:{first_name:firstname, last_name:lastname, token:token},
                success:function(data){
                    console.log(data);
                    var text = "Token Erfolgreich hinterlegt";
                    $('body').append('<div class="action--popup"><h2>'+text+'</<h2></div>');
                    setTimeout(function() {
                        location.reload();
                    }, time)
                }
            })
        }
    });

    function handleButtons(data) {
        //var ip = socket.request.connection.remoteAddress;
        //var ip = socket.remoteAddress;
        $('.keybutton--wrapper').hide();
        $('.action--wrapper').removeClass("action--wrapper-hidden");
        $('.greetings--headline').text('Hallo ' + data[0].first_name + ' ' +data[0].last_name);
        if(data[1]) {
            $('#'+data[1].status).hide();
            if(data[1].status == 'leaving' || data[1].status == 'vacation' || data[1].status == 'sick') {
                $('.break--btn').hide();
            }
            if(data[1].status == 'coming') {
                $('#break_end').hide();
            }
            if(data[1].status == 'break_start') {
                $('#coming').hide();
                $('#leaving').hide();
            }
            if(data[1].status == 'break_end') {
                $('#coming').hide();
            }
            if(data[1].status == 'forgot') {
                $('#leaving').hide();
                $('#break_end').hide();
                $('#break_start').hide();
                $('.greetings--headline').text('Hallo ' + data[0].first_name + ' ' +data[0].last_name + ' du hast vergessen dich aus zu stempeln!');
            }
        }
        else {
            $('.action--btn').hide();
            $('#coming').show();
        }
        setTimeout(function() {
            location.reload();
        }, 20000);
    }

    function handleCorona(person) {
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        var todayReal = yyyy+'-'+mm+'-'+dd;
        var entries = person['test_repeater'];
        if(person['vaccinated']) {
            if(person['vaccinated'] != null) {
                return true
            }
        }
        else if(entries.length) {
            if (entries.length > 1) {
                if(entries[entries.length-1]['test_date'] == todayReal) {
                    return true;
                }
            }
        }
        return false;
    }

    $('#reload').on('click', function(e) {
        location.reload();
    });

    $('#setWitness').on('click', function(e){
        if(!witnessSend) {
            var witnessToken = $('#witnessInput').val();
            witnessSend = true;
            var source = getUrlParameter('name');
            e.preventDefault();
            if(witnessToken != globalToken) {
                $.ajax({
                    type: 'POST',
                    url: 'setWitnessCorona',
                    data:{witnesstoken: witnessToken, realtoken: globalToken, source: source},
                    async: false,
                    success: function(data) {
                        var text = "Zeugin/Zeuge für heute hinterlegt!"
                        $('body').append('<div class="action--popup"><h2>'+text+'</<h2></div>');
                        setTimeout(function() {
                            location.reload();
                        }, 2000);
                    }
                });
            }
            else {
                $('.greetings--headline').text('Bitte andere Person scannen!');
                setTimeout(function() {
                    location.reload();
                }, 2000);
            }
        }
    });

    $('#getMitarbeiter').on('click', function(e) {
        if(!tokenSend) {
            console.log($('#tokenInput').val());
            var token = $('#tokenInput').val();
            globalToken = token;
            tokenSend = true;
            e.preventDefault();
            $.ajax({
                type:'GET',
                url:'/getMitarbeiter',
                data:{token:token},
                success:function(data){
                    console.log(data);
                    if(data !== 'false') {
                        if(data[2]) {
                            if (handleCorona(data[0])) {
                                handleButtons(data);
                            } else {
                                $('.greetings--headline').text("Bitte Negativ Test durch mitarbeitende Person bezeugen lassen!");
                                $('.keybutton--wrapper').hide();
                                coronabla = true;
                                var hasTest = false;
                                $('#witnessInput').focus();
                                if(data[0]['test_repeater'] != null) {
                                    for (let i = 0; i < data[0]['test_repeater'].length; i++) {
                                        const item = data[0]['test_repeater'][i];
                                        var date = new Date(item['test_date']);
                                        var today = new Date();
                                        var dateWithoutTime = date.getDate() + '' + (date.getMonth() + 1) + '' + date.getFullYear();
                                        var todayWithoutTime = today.getDate() + '' + (today.getMonth() + 1) + '' + today.getFullYear();
                                        console.log(dateWithoutTime);
                                        console.log(todayWithoutTime);
                                        if (dateWithoutTime == todayWithoutTime) {
                                            hasTest = true;
                                        }
                                    }
                                }
                                if (hasTest) {
                                    handleButtons(data);
                                } else {
                                    $('.help--wrapper-timetracking').hide();
                                    $('.corona--wrapper').removeClass('corona--wrapper-hidden');
                                    $('.corona--wrapper').show();
                                    $('#witnessInput').focus();
                                }
                                setTimeout(function() {
                                    location.reload();
                                }, 20000)
                            }
                        }
                        else {
                            handleButtons(data);
                        }
                    }
                    else {
                        alert("Keinen Nutzer gefunden!");
                        location.reload();
                    }
                }
            });
        }
    });

    $('#ldapSend').on('click', function(e) {
        var name, password;
        if($('#keyNameInput').val() && $('#keyPasswordInput').val()) {
            name = $('#keyNameInput').val();
            password = $('#keyPasswordInput').val();
        }
        else {
            alert("Bitte beide Felder ausfüllen!");
        }
        e.preventDefault();
        $.ajax({
            type:'GET',
            url:'/ldaptest',
            data:{name:name, password: password},
            success:function(data){
                console.log(data);
                if(data !== 'false' && data !== "NEIN" && data !== 'no db user' && data.length < 4) {
                    //$('.action--wrapper').removeClass("action--wrapper-hidden");
                    $('.forgotkey--wrapper').hide();
                    $('.scankey--text').text('Aktion wählen!');
                    $('.btn--forgotkey').hide();
                    $('.timetracker--container').show();
                    console.log(data[0].first_name);
                    $('#firstnameLdap').val(data[0].first_name);
                    $('#lastnameLdap').val(data[0].last_name);


                    if(data[0].token) {
                        $('#tokenInput').val(data[0].token);
                        globalToken = data[0].token;
                    }
                    else {
                        $('.addToken--wrapper').show();
                    }
                    if(data[2]) {
                        if (handleCorona(data[0])) {
                            handleButtons(data);
                        } else {
                            $('.greetings--headline').text("Bitte Negativ Test durch mitarbeitende Person bezeugen lassen!");
                            $('.keybutton--wrapper').hide();
                            var hasTest = false;
                            for (let i = 0; i < data[0]['test_repeater'].length; i++) {
                                const item = data[0]['test_repeater'][i];
                                var date = new Date(item['test_date']);
                                var today = new Date();
                                var dateWithoutTime = date.getDate() + '' + (date.getMonth() + 1) + '' + date.getFullYear();
                                var todayWithoutTime = today.getDate() + '' + (today.getMonth() + 1) + '' + today.getFullYear();
                                console.log(dateWithoutTime);
                                console.log(todayWithoutTime);
                                if (dateWithoutTime == todayWithoutTime) {
                                    hasTest = true;
                                }
                            }
                            if (hasTest) {
                                handleButtons(data);
                            } else {
                                $('.help--wrapper-timetracking').hide();
                                $('.corona--wrapper').removeClass('corona--wrapper-hidden');
                                $('.corona--wrapper').show();
                                $('#witnessInput').focus();
                            }
                        }
                    }
                    else {
                        handleButtons(data);
                    }
                    setTimeout(function() {
                        location.reload();
                    }, 20000);
                }
                else {
                    if(data.length === 3) {
                        $('.addToken--wrapper').show();
                        $('#firstnameLdap').val(data[0]);
                        $('.forgotkey--wrapper').hide();
                        $('#lastnameLdap').val(data[1]);
                        $('.greetings--headline').text('Hallo ' + data[0] + ' ' +data[1]);
                    }
                    else {
                        console.log(data);
                        alert("Keinen Nutzer gefunden!");
                        location.reload();
                    }
                }
            }
        });
    });

    $('.add--token-hidden').on('click', function(e) {
        $('.timetracker--container').hide();
        $('.keybutton--wrapper').hide();
        $('.addToken--scan-text').show();
        $('*').bind('touchmove', false);
        $('*').off('click');
        secondStep = true;
        $('#addTokenInput').focus();
        $('.add--token-hidden').hide();
    });

    $('.btn--forgotkey').on('click', function(e) {
        $('.forgotkey--wrapper').show();
        $('.timetracker--container').hide();
        $('.btn--back').show();
        $('.btn--forgotkey').hide();
    });

    $('#mail').on('click', function(e) {
         $.ajax({
             type: 'GET',
             url: '/testEmail',
             success: function(data) {
                 console.log(data);
             }
         });
    });

    $('.btn--back').on('click', function(e) {
        $('.btn--back').hide();
        $('.forgotkey--wrapper').hide();
        $('.timetracker--container').show();
    });

    $('.action--btn').one('click', function(e) {
        e.preventDefault();
        var state = $(this).data('timetrack');
        var text = "AKTION ERFOLGREICH";
        var token = $('#tokenInput').val();
        //var ip = socket.remoteAddress;
        var source = getUrlParameter('name');
        if(token) {
            $.ajax({
                type:'POST',
                url:'/trackTime',
                data:{token:token, state:state, source: source},
                success:function(data){
                    console.log(data);
                    switch (state) {
                        case "coming":
                            text = "Willkommen"
                            break;
                        case "leaving":
                            text = "Tschüss: "+data;
                            break;
                        case "break_end":
                            text = "Pause: "+data;
                            break;
                        default:
                            break;
                    }
                    socket.emit('new message', "LOL");
                    $('body').append('<div class="action--popup"><h2>'+text+'</<h2></div>');
                    setTimeout(function() {
                        location.reload();
                    }, time)
                }
            });
        }
        else {
            var firstname = $('#firstnameLdap').val();
            var lastname = $('#lastnameLdap').val();
            $.ajax({
                type:'POST',
                url:'/trackTimeNoToken',
                data:{first_name:firstname, last_name:lastname, state: state},
                success:function(data){
                    switch (state) {
                        case "coming":
                            text = "Willkommen"
                            break;
                        case "leaving":
                            text = "Tschüss"
                        default:
                            break;
                    }
                    $('body').append('<div class="action--popup"><h2>'+text+'</<h2></div>');
                    socket.emit('new message', "LOL");
                    setTimeout(function() {
                        location.reload();
                    }, time)
                }
            })
        }

    })
});

function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    $('#clock').html(
        h + ":" + m + ":" + s);
    var t = setTimeout(startTime, 500);
}
function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}

