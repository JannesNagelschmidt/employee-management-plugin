$(function(e) {
    if($('#vacationforever').length) {
            var targetButton = false;
            $(document).on('click', function(e) {
                var item = $(e.target);
                if(!item.is($('.emt--plan-tooltip')) && (!item.parent().hasClass('fc-event-title-container')) && (!item.hasClass('fc-event-title-container')) && !targetButton) {
                    $('.emt--plan-tooltip').remove();
                }
                else {
                    targetButton = false;
                }
            });
            $.when(ajaxCall1()).done(function(ajax1Results) {
                if($('#vacationforeveremployee').length) {
                    createAnonymusCalendar(ajax1Results);
                }
                else {
                    createCalendar(ajax1Results);
                }
            });
    }

});

function createAnonymusCalendar(data) {
    var sickColor = '#cddc39';
    const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    var calendarEl = document.getElementById('vacationforever');
    var calendar = new FullCalendar.Calendar(calendarEl, {
        initialView: 'dayGridMonth',
        locale: 'de',
        weekNumbers: true,
        firstDay: 1,
        weekText: 'KW',
        buttonText: {
            today: 'Heute'
        },
        rerenderDelay: 100,
        eventClick: function (info) {
            $('.emt--plan-tooltip').remove();
            var information = info.event.extendedProps['information'];
            var start = info.event.extendedProps['start'];
            var end = info.event.extendedProps['end'];
            var name = info.event.extendedProps['name'];
            var html = '<div class="emt--plan-tooltip">';
            html += '<i class="material-icons mdc-button__icon close--icon">close</i>';
            html += '<p class="emt--timestamp-entry">'+information+'</p>';
            if(start && end) {
                html += '<p class="emt--timestamp-entry">Von: '+start+'</p>';
                html += '<p class="emt--timestamp-entry">Bis: '+end+'</p>';
            }
            if(name) {
                html += '<p class="emt--timestamp-entry">Notiz: '+name+'</p>';
            }
            html += '</div>';
            if(information) {
                $(info.el).parent().append(html);
            }
        },
        eventMouseEnter(info) {
            $('.emt--plan-tooltip-hover').remove();
            var tooltip = info.event.extendedProps['tooltip'];

            var html = '<div class="emt--plan-tooltip-hover">';

            html += '<p class="emt--timestamp-entry">'+tooltip+'</p>';
            html += '</div>';
            if(tooltip) {
                $(info.el).parent().append(html);
            }
            console.log()
        },
        eventMouseLeave() {
            $('.emt--plan-tooltip-hover').remove();
        }
    });
    calendar.render();

    for(var i = 0; i < data['entries'].length; i++) {
        var employ = data['entries'][i]['employee'];
        var isSelf = data['entries'][i]['self'];
        if (data['entries'][i]['subdep']) {
            var color = data['entries'][i]['subdep']['color'];
        }
        else {
            var color = '#2491b5';
        }
        var borderColor = color;
        var vacation = data['entries'][i]['vacation'];
        var sick = data['entries'][i]['sick'];
        for(var j = 0; j < vacation.length; j++) {
            var colorReal = color;
            var vac_item = vacation[j];
            if(vac_item['status'] === 'Beantragt') {
                colorReal = '#ddd';
            }
            if(vac_item['start_date'] !== vac_item['end_date']) {
                if(isSelf) {
                    borderColor = '#000'
                }
                const endReal = new Date(new Date(vac_item['end_date']).valueOf() + 1000*3600*24);
                const endGerShow = new Date(new Date(vac_item['end_date']).valueOf() + 1000*3600*24);
                endGerShow.setDate(endGerShow.getDate()-1);
                var endGer = endGerShow.toLocaleDateString('de-DE');
                const startGer = new Date(vac_item['start_date']).toLocaleDateString('de-DE');
                calendar.addEvent(
                    {
                        title: 'Urlaub',
                        start: vac_item['start_date'],
                        end: endReal,
                        allDay: true,
                        color: colorReal,
                        borderColor: borderColor,
                        classNames: 'emt--work-entry emt--plan-entry emt--plan-entry_vacation emt--plan-entry_vacation-'+j,
                        extendedProps: {
                            information: vac_item['status'],
                            start: startGer,
                            end: endGer,
                            color: color,
                            tooltip: 'Urlaub ' + ' '+startGer+'-'+endGer,
                            identify: j,
                            name: vac_item['name']
                        }
                    }
                );
            }
            else {
                if(isSelf) {
                    borderColor = '#000'
                }
                calendar.addEvent(
                    {
                        title: 'Urlaub',
                        start: vac_item['start_date'],
                        allDay: true,
                        color: color,
                        borderColor: borderColor,
                        classNames: 'emt--work-entry emt--plan-entry emt--plan-entry_vacation',
                        extendedProps: {
                            color: colorReal,
                            information: vac_item['status'],
                            tooltip: 'Urlaub ',
                            name: vac_item['name']
                        }
                    }
                );
            }
        }
    }
    for(let i = 0; i < data['holidays'].length; i++) {
        var holidayYear = data['holidays'][i];
        for(let x = 0; x < holidayYear.length; x++) {
            var item = holidayYear[x];
            if(item['global']) {
                calendar.addEvent(
                    {
                        title: item['localName'],
                        start: item['date'],
                        allDay: true,
                        classNames: 'emt--work-entry '
                    }
                );
            }
            else if(item['counties'].includes('DE-SH')) {
                calendar.addEvent(
                    {
                        title: item['localName'],
                        start: item['date'],
                        allDay: true,
                        classNames: 'emt--work-entry '
                    }
                );
            }
        }
    }
}

function createCalendar(data) {
    var sickColor = '#cddc39';
    const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    var calendarEl = document.getElementById('vacationforever');
    var calendar = new FullCalendar.Calendar(calendarEl, {
        initialView: 'dayGridMonth',
        locale: 'de',
        weekNumbers: true,
        firstDay: 1,
        weekText: 'KW',
        buttonText: {
            today: 'Heute'
        },
        rerenderDelay: 100,
        eventClick: function (info) {
            $('.emt--plan-tooltip').remove();
            var information = info.event.extendedProps['information'];
            var start = info.event.extendedProps['start'];
            var end = info.event.extendedProps['end'];
            var name = info.event.extendedProps['name'];
            var html = '<div class="emt--plan-tooltip">';
            html += '<i class="material-icons mdc-button__icon close--icon">close</i>';
            html += '<p class="emt--timestamp-entry">'+information+'</p>';
            if(start && end) {
                html += '<p class="emt--timestamp-entry">Von: '+start+'</p>';
                html += '<p class="emt--timestamp-entry">Bis: '+end+'</p>';
            }
            if(name) {
                html += '<p class="emt--timestamp-entry">Notiz: '+name+'</p>';
            }
            html += '</div>';
            if(information) {
                $(info.el).parent().append(html);
            }
        },
        eventMouseEnter(info) {
            $('.emt--plan-tooltip-hover').remove();
            var tooltip = info.event.extendedProps['tooltip'];

            var html = '<div class="emt--plan-tooltip-hover">';

            html += '<p class="emt--timestamp-entry">'+tooltip+'</p>';
            html += '</div>';
            if(tooltip) {
                $(info.el).parent().append(html);
            }
            console.log()
        },
        eventMouseLeave() {
            $('.emt--plan-tooltip-hover').remove();
        }
    });
    calendar.render();
    $('.emt--body-wrapper').on("click", '.mdc-list-item.emt--list-item.emt--list-item_leader', function() {
        var jumper = $(this).data('jumper');
        if(jumper != null) {
            calendar.gotoDate(jumper);
        }
    });
    for(var i = 0; i < data['entries'].length; i++) {
        var employ = data['entries'][i]['employee'];
        if (data['entries'][i]['subdep']) {
            var color = data['entries'][i]['subdep']['color'];
        }
        else {
            var color = '#2491b5';
        }
        var vacation = data['entries'][i]['vacation'];
        var sick = data['entries'][i]['sick'];
        for(var j = 0; j < vacation.length; j++) {
            var colorReal = color;
            var vac_item = vacation[j];
            if(vac_item['status'] === 'Beantragt') {
                colorReal = '#ddd';
            }
            if(vac_item['start_date'] !== vac_item['end_date']) {
                const endReal = new Date(new Date(vac_item['end_date']).valueOf() + 1000*3600*24);
                const endGerShow = new Date(new Date(vac_item['end_date']).valueOf() + 1000*3600*24);
                endGerShow.setDate(endGerShow.getDate()-1);
                var endGer = endGerShow.toLocaleDateString('de-DE');
                const startGer = new Date(vac_item['start_date']).toLocaleDateString('de-DE');
                calendar.addEvent(
                    {
                        title: employ['first_name'] +' '+ employ['last_name'],
                        start: vac_item['start_date'],
                        end: endReal,
                        allDay: true,
                        color: colorReal,
                        classNames: 'emt--work-entry emt--plan-entry emt--plan-entry_vacation emt--plan-entry_vacation-'+j,
                        extendedProps: {
                            information: vac_item['status']+': '+vac_item['approved'],
                            start: startGer,
                            end: endGer,
                            color: color,
                            tooltip: 'Urlaub ' +employ['first_name'] +' '+ employ['last_name'] + ' '+startGer+'-'+endGer,
                            identify: j,
                            name: vac_item['name']
                        }
                    }
                );
            }
            else {
                calendar.addEvent(
                    {
                        title: employ['first_name'] +' '+ employ['last_name'],
                        start: vac_item['start_date'],
                        allDay: true,
                        color: color,
                        classNames: 'emt--work-entry emt--plan-entry emt--plan-entry_vacation',
                        extendedProps: {
                            color: colorReal,
                            information: vac_item['status']+': '+vac_item['approved'],
                            tooltip: 'Urlaub ' +employ['first_name'] +' '+ employ['last_name'],
                            name: vac_item['name']
                        }
                    }
                );
            }
        }
        for(var k = 0; k < sick.length; k++) {
            var sick_item = sick[k];
            if(sick_item['start_day'] !== sick_item['end_day']) {
                var endReal = new Date(new Date(sick_item['end_day']).valueOf() + 1000*3600*24);
                calendar.addEvent(
                    {
                        title: employ['first_name'] +' '+ employ['last_name'],
                        start: sick_item['start_day'],
                        end: endReal,
                        allDay: true,
                        color: sickColor,
                        classNames: 'emt--work-entry emt--plan-entry emt--plan-entry_sick bla',
                        extendedProps: {
                            color: sickColor,
                            tooltip: 'Krank '+employ['first_name'] +' '+ employ['last_name'],
                            information: 'Krank',
                            name: sick_item['name']
                        }
                    }
                );
            }
            else {
                calendar.addEvent(
                    {
                        title: employ['first_name'] +' '+ employ['last_name'],
                        start: sick_item['start_day'],
                        allDay: true,
                        color: sickColor,
                        classNames: 'emt--work-entry emt--plan-entry emt--plan-entry_sick',
                        extendedProps: {
                            color: sickColor,
                            tooltip: 'Krank '+employ['first_name'] +' '+ employ['last_name'],
                            information: 'Krank',
                            name: sick_item['name']
                        }
                    }
                );
            }
        }
    }
    for(let i = 0; i < data['holidays'].length; i++) {
        var holidayYear = data['holidays'][i];
        for(let x = 0; x < holidayYear.length; x++) {
            var item = holidayYear[x];
            if(item['global']) {
                calendar.addEvent(
                    {
                        title: item['localName'],
                        start: item['date'],
                        allDay: true,
                        classNames: 'emt--work-entry '
                    }
                );
            }
            else if(item['counties'].includes('DE-SH')) {
                calendar.addEvent(
                    {
                        title: item['localName'],
                        start: item['date'],
                        allDay: true,
                        classNames: 'emt--work-entry '
                    }
                );
            }
        }
    }
}

function ajaxCall1() {
    if($('#vacationforever').length) {
        var department_id = $('#idForDepartment').val();
        var user_id = $('#idForLeader').val();
        console.log(user_id);
        let bigChunk;
        console.log(department_id);
        return $.request('onGetCalenderData', {
            data: {id: department_id, user_id: user_id},
            success: function(data) {
                console.log(data);
                bigChunk = data;
            }
        })
    }
}

