let mix = require('laravel-mix');

mix.setPublicPath('themes/demo/assets/');

mix.js('themes/demo/assets/javascript/app.js', 'dist/js')
    .sass('themes/demo/assets/css/app.scss', 'dist/css');
